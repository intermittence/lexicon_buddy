## todo Generate Build System

### Handle Dependency

First install all the dependencies manually. Install with brew on macOS is recommended. After that follow the instruction and examples bellow to find these dependencies and generate build system.

#### OpenSSL

**macOS**. Install latest `OpenSSL` library with brew and then specify cmake cache var `OPENSSL_ROOT_DIR` (through command line or cmake gui, [example](example_macos_generate_ninja.sh)) . macOS comes with its own `OpenSSL` but it has a different include directory structure than the `OpenSSL` brew installation and this project likes the latter.

**Windows**. Install latest `OpenSSL` and Specify cmake cache var `OPENSSL_ROOT_DIR`.

#### Boost

Either specify cmake cache var `BOOST_ROOT` or `BOOST_INCLUDEDIR` and `BOOST_LIBRARYDIR` if it's not installed on default location.

#### ICU

Specify cmake cache var  `ICU_ROOT`.

#### other dependencies

Other dependencies should be automatically found by cmake. If not, add their install location prefixs to cache CMAKE_PREFIX_PATH (check [CMAKE_PREFIX_PATH](https://cmake.org/cmake/help/latest/variable/CMAKE_PREFIX_PATH.html) for more details)

### Example of Generate Build System

- Generate Ninja (you should install Ninja first) Build System on macOS: [example](example_macos_generate_ninja.sh) 
- todo Generate Visual Studio 2019 Project (multiple configuration) on Windows 10:

## Build and Install

Build and Install (as cmake Config-file Packages) with cmake command-line:

```sh
cd <your_build_dir>
cmake --build . --target all && cmake -P cmake_install.cmake
```
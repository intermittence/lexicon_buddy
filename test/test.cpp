//
//  test.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/7/11.
//  Copyright © 2018 Lohengrin. All rights reserved.
//
#include "test.h"
#include <functional>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <memory>
#include <future>
#include <sstream>
#include <lexicon_buddy/lexicon/stardictcontainer.h>
#include <lexicon_buddy/utilities/unzip_tool.h>
#include <lexicon_buddy/lexicon/http_client3.h>
#include <lexicon_buddy/lexicon/online_dict.h>
#include <lexicon_buddy/lexicon/buddy.h>
#include <lexicon_buddy/lexicon/offline_query.h>
namespace lexicon
{
namespace test
{
using std::cout;
using std::endl;
using std::cerr;

void dz_index_logging(const char* dz_path, const char* log_path) {
	unzip::unzip_para<unzip::comp_type::dictzip> para;
	std::ifstream dz{dz_path};
	std::ofstream log{log_path};
	para.init(dz);
	uint32_t count{};
	for (auto x : para.index) {
		log << std::get<0>(x) << " " << std::get<1>(x) << '\n';
		++count;
	}
	log << "=====================\n"
		<< "record count: " << count;
}

void stardict_query_dz_random_test(std::ostream& ostrm, const char* ifo_path, const char* idx_path,
								   const char* dz_path, unsigned n, unsigned num) {
	lexicon::stardict::ifo_str ifo{ifo_path};
	util::mmap_w idx_mmap{idx_path};
	std::ifstream dict{dz_path};
	unzip::unzip_agent<unzip::comp_type::dictzip> _unzip;
	auto idx_iter_entry_type = lexicon::stardict::get_idx_iter_entry_type(ifo.version, ifo.idxoffsetbits);
	if (idx_iter_entry_type) {
		// 64bit offset
		lexicon::stardict::idx_agent<1> agent{&idx_mmap};
		auto itr = agent.begin();
		while (n--) {
			++itr;
		}
		for (unsigned i = 0; i < num; ++i) {
			auto v = *itr;
			std::unique_ptr<char[]> buf{new char[v.word_data_size_]};
			_unzip.unzip_random(dict, v.word_data_size_, v.word_data_offset_, buf.get());
			lexicon::stardict::dict_record record{ifo.sametypesequence, buf.get(), v.word_data_size_};
			ostrm << "================" << v.word_ << "(length: " << v.word_data_size_
				  << ")================\n";
			ostrm << record.str << '\n';
			ostrm << "============================\n\n";
			++itr;
		}
	} else {
		// 32bit offset
		lexicon::stardict::idx_agent<0> agent{&idx_mmap};
		auto itr = agent.begin();
		while (n--) {
			++itr;
		}
		for (unsigned i = 0; i < num; ++i) {
			auto v = *itr;
			std::unique_ptr<char[]> buf{new char[v.word_data_size_]};
			_unzip.unzip_random(dict, v.word_data_size_, v.word_data_offset_, buf.get());
			lexicon::stardict::dict_record record{ifo.sametypesequence, buf.get(), v.word_data_size_};
			ostrm << "================" << v.word_ << "(length: " << v.word_data_size_
				  << ")================\n";
			ostrm << record.str << '\n';
			ostrm << "============================\n\n";
			++itr;
		}
	}
}
void stardict_query_mmp_dz_random_test(std::ostream& ostrm, const char* ifo_path, const char* idx_path,
									   const char* dz_path, unsigned n, unsigned num) {
	lexicon::stardict::ifo_str ifo{ifo_path};
	util::mmap_w idx_mmap{idx_path};
	util::mmap_w dict{dz_path};
	unzip::unzip_agent<unzip::comp_type::dictzip> _unzip;
	auto idx_iter_entry_type = lexicon::stardict::get_idx_iter_entry_type(ifo.version, ifo.idxoffsetbits);
	if (idx_iter_entry_type) {
		// 64bit offset
		lexicon::stardict::idx_agent<1> agent{&idx_mmap};
		auto itr = agent.begin();
		while (n--) {
			++itr;
		}
		for (unsigned i = 0; i < num; ++i) {
			auto v = *itr;
			std::unique_ptr<char[]> buf{new char[v.word_data_size_]};
			_unzip.unzip_random(dict, dict.get_filesize(), v.word_data_size_, v.word_data_offset_, buf.get());
			lexicon::stardict::dict_record record{ifo.sametypesequence, buf.get(), v.word_data_size_};
			ostrm << "================" << v.word_ << "(length: " << v.word_data_size_
				  << ")================\n";
			ostrm << record.str << '\n';
			ostrm << "============================\n\n";
			++itr;
		}
	} else {
		// 32bit offset
		lexicon::stardict::idx_agent<0> agent{&idx_mmap};
		auto itr = agent.begin();
		while (n--) {
			++itr;
		}
		for (unsigned i = 0; i < num; ++i) {
			auto v = *itr;
			std::unique_ptr<char[]> buf{new char[v.word_data_size_]};
			_unzip.unzip_random(dict, dict.file_length, v.word_data_size_, v.word_data_offset_, buf.get());
			lexicon::stardict::dict_record record{ifo.sametypesequence, buf.get(), v.word_data_size_};
			ostrm << "================" << v.word_ << "(length: " << v.word_data_size_
				  << ")================\n";
			ostrm << record.str << '\n';
			ostrm << "============================\n\n";
			++itr;
		}
	}
}

class test_coro_handler
{
public:
	void operator()(boost::beast::http::response<boost::beast::http::string_body> res) {
		std::cout << "=======http response=======\n" << res << std::endl;
	}
};

void test_index_stardict(const char* dict_file_path, const char* target_trie_without_suffix,
						 const char* target_coordinate_without_suffix) {
	using lexicon::index::index_agent;
	// build index test
	auto ret = index_agent::build<res_type::startdict_idx>(dict_file_path, target_trie_without_suffix,
														   target_coordinate_without_suffix);
}
void test_query_stardict(const char* trie, const char* coordinate, const char* word, const char* ifo_path,
						 const char* idx_path, const char* dict_path) {
	using lexicon::offline_query::search_mmap;
	using lexicon::stardict::dict_container;
	std::map<string, uint64_t> result{};
	auto is64 = !(string{coordinate}.find(".co64") == string::npos);
	// prefix search test
	if (is64) {
		search_mmap<dict_container<1>> p(trie, coordinate);
		search_mmap<dict_container<1>>::result_type r{};
		p.predict(word, r, 16);
		cout << "predict search" << '\n';
		for (auto i : r) {
			cout << "key: " << i.first << "||id: " << i.second << '\n';
			result.try_emplace(i.first, i.second);
		}
	} else {
		search_mmap<dict_container<0>> p(trie, coordinate);
		search_mmap<dict_container<0>>::result_type r{};
		p.predict(word, r, 16);
		cout << "predict search" << '\n';
		for (auto i : r) {
			cout << "key: " << i.first << "||id: " << i.second << '\n';
			result.try_emplace(i.first, i.second);
		}
	}
	if (ifo_path) {
		if (is64) {
			search_mmap<dict_container<1>> p(trie, coordinate);
			search_mmap<dict_container<1>>::result_type r{};
			dict_container<1> dict_c(ifo_path, idx_path, dict_path);
			for (auto& i : result) {
				cout << i.first << ": \n";
				auto query_result = offline_query::id_to_data_flex(p, dict_c, i.second);
				for (auto& j : query_result) {
					cout << j.str << '\n';
				}
			}
		} else {
			search_mmap<dict_container<0>> p(trie, coordinate);
			search_mmap<dict_container<0>>::result_type r{};
			dict_container<0> dict_c(ifo_path, idx_path, dict_path);
			for (auto& i : result) {
				cout << i.first << ": \n";
				auto query_result = offline_query::id_to_data_flex(p, dict_c, i.second);
				for (auto& j : query_result) {
					cout << j.str << '\n';
				}
			}
		}
	}
}

void test_index_mdx(const char* dict_file_path, const char* target_trie_without_suffix,
					const char* target_coordinate_without_suffix) {
	using lexicon::index::index_agent;
	// build index test
	auto ret = index_agent::build<res_type::mdx>(dict_file_path, target_trie_without_suffix,
												 target_coordinate_without_suffix);
}
void test_query_mdx(const char* trie, const char* coordinate, const char* word, const char* dict_path) {
	using lexicon::offline_query::search_mmap;
	using lexicon::mdx::mdx_data_mmap;
	// prefix search test
	search_mmap<mdx_data_mmap> p(trie, coordinate);
	search_mmap<mdx_data_mmap>::result_type r{};
	auto trie_ptr = p.trie_ptr();
	marisa::Agent agent;
	agent.set_query(word);
	mdx_data_mmap dict{dict_path};
	cout << endl << "lookup" << endl;
	if (trie_ptr->lookup(agent)) {
		cout << "key: " << agent.key().ptr() << "||id: " << agent.key().id() << '\n';
		if (dict_path) {
			auto results = offline_query::id_to_data_flex(p, dict, agent.key().id());
			for (auto& r_i : results) {
				cout << r_i;
			}
		}
	}
	cout << "\n\n";
	p.predict(word, r);
	cout << "prefix search" << '\n';
	if (!dict_path) {
		for (auto& i : r) {
			cout << "key: " << i.first << "||id: " << i.second << '\n';
		}
	} else {
		for (auto& i : r) {
			cout << "\n================\n";
			cout << "key: " << i.first << "||id: " << i.second << '\n';
			auto results = offline_query::id_to_data_flex(p, dict, i.second);
			for (auto& r_i : results) {
				cout << "\n--------------\n";
				cout << r_i;
			}
		}
	}
	cout << '\n';
}

void test_cfg(const char* dict_dir_, const char* index_dir_, const char* cfg_path_, const char* import) {
	using lexicon::index::index_agent;
	index_agent agent(dict_dir_, index_dir_, cfg_path_);
	agent.cfg_item_discovery();
#ifndef NDEBUG
	agent.debug();
#endif
	agent.import(import);
#ifndef NDEBUG
	agent.debug();
#endif
}

class buddy_debug_handler_offline
{

public:
	buddy_debug_handler_offline() {}

	template <class T>
	void operator()(const T& arg) {
		cout << arg << endl;
	}
};

class buddy_debug_handler_online
{
	// bool using_promise_;
	std::promise<void>* promise_;

public:
	buddy_debug_handler_online(std::promise<void>* promise) : promise_{promise} {}
	// buddy_debug_handler_online(const buddy_debug_handler_online&) = default;

	void operator()(boost::system::error_code ec) {
		cout << ec.message() << endl;
		promise_->set_value();
	}

	template <class T>
	void operator()(const T& arg) {
		cout << arg << endl;
		promise_->set_value();
	}

	auto get_future() { return promise_->get_future(); }
};

/// @warning  thread unsafe
struct buddy_debug_strm_handler
{
	std::ofstream* strm_;
	buddy_debug_strm_handler(std::ofstream* strm) : strm_{strm} {}
	buddy_debug_strm_handler(const buddy_debug_strm_handler& arg) : strm_{arg.strm_} {}
	buddy_debug_strm_handler& operator=(const buddy_debug_strm_handler& arg) {
		strm_ = arg.strm_;
		return *this;
	}
	buddy_debug_strm_handler(buddy_debug_strm_handler&& arg) : strm_{arg.strm_} { arg.strm_ = nullptr; }
	buddy_debug_strm_handler& operator=(buddy_debug_strm_handler&& arg) {
		strm_ = arg.strm_;
		arg.strm_ = nullptr;
		return *this;
	}

	void operator()(boost::system::error_code ec) {}

	template <class body_type_>
	void operator()(const boost::beast::http::response<body_type_>& arg) {
		cout << arg.base() << endl;
		*strm_ << arg.body();
	}
};

void test_buddy(int argc, char* argv[]) {
	namespace buddy = lexicon::buddy;
	using buddy::manager;
	using std::cin;
	using namespace std::chrono_literals;
	auto tips = []() { cout << "type help to get more information\n" << std::flush; };
	auto help = []() {
		cout << "command list: \n"
			 << "lang = set the languages\n"
			 << "offq = offline query\n"
			 << "offp = offline predict\n"
			 << "offl = offline lookup\n" //<< "onq_oxford = oxford online query" << '\n'
			 << "onq_webster = MERRIAM-WEBSTER online query\n"
			 << "l = list all dictionaries\n"
			 << "import = import dictionary\n"
			 << "delete = offline delete dict\n"
			 << "set_api = set API key\n"
			 << "q = quit\n"
			 << std::flush;
	};

	buddy::search_ctx ctx{};
	ctx.lang_pair.first = buddy::lang::en_gb;
	ctx.lang_pair.second = buddy::lang::en_gb;
	buddy_debug_handler_offline h{};
	constexpr unsigned buf_s = 300;
	char buf[buf_s]{};

	auto interact = [&buf](const char* msg) {
		cout << msg << std::flush;
		cin.getline(buf, buf_s);
	};

	// prepare paths for dict manager initialization
	string dict_dir_;
	string index_dir_;
	string cfg_path_;

	if (argc == 4) {
		// read paths from command line arguments
		dict_dir_ = string{argv[1]};
		index_dir_ = string{argv[2]};
		cfg_path_ = string{argv[3]};
	} else {
		interact("type dict folder path: ");
		dict_dir_ = string{buf};
		memset(buf, 0, sizeof(buf));
		interact("type index folder path: ");
		index_dir_ = string{buf};
		memset(buf, 0, sizeof(buf));
		interact("type configuration file path (create new if file doesn't exist): ");
		cfg_path_ = string{buf};
		memset(buf, 0, sizeof(buf));
	}

	manager<buddy_debug_handler_offline, buddy_debug_handler_online> m{dict_dir_.c_str(), index_dir_.c_str(),
																	   cfg_path_.c_str(), true, true};
	// set oxford api key
	// m.load_oxford_setting();
	// lambda to set lang_pair
	m.save_webster_setting();
	m.load_webster_setting();
	auto set_lang_pair = [&]() {
		interact("set search context language (e.g. en:de)\n"
				 "(full list of lang: all;en_gb;en_us;es;hi;nso;tn;zu;de;pt;zh_CN;zh_TW;empty)\n");
		string cfg_path_{buf};
		auto lan = buddy::str_to_lang_pair_type(cfg_path_);
		ctx.lang_pair.first = lan.first;
		ctx.lang_pair.second = lan.second;
		memset(buf, 0, sizeof(buf));
	};
	// lambda to update API key
	auto update_api_key = [&]() {
		memset(buf, 0, sizeof(buf));
		interact("which API do you want to update(\"webster\"/\"oxford(deprecated)\"): ");
		string s = buf;
		memset(buf, 0, sizeof(buf));
		//		if (s == "oxford") {
		//			buddy::oxford_service::api_key_t k;
		//			interact("oxford API ID: ");
		//			k.ID = buf;
		//			memset(buf, 0, sizeof(buf));
		//			interact("oxford API Key: ");
		//			k.Key = buf;
		//			memset(buf, 0, sizeof(buf));
		//			m.oxford_.set_access_key(k);
		//			m.save_oxford_setting();
		//		} else
		if (s == "oxford") {
			cout << "oxford api is deprecated\n";
		} else if (s == "webster") {
			buddy::webster_service::api_key_t k;
			interact("The API Key for Merriam-Webster's Collegiate® Dictionary with Audio: ");
			k.Collegiate_key = buf;
			memset(buf, 0, sizeof(buf));
			interact("The API Key for Merriam-Webster's Collegiate® Thesaurus: ");
			k.Thesaurus_key = buf;
			memset(buf, 0, sizeof(buf));
			m.webster_.set_access_key(k);
			m.save_webster_setting();
		}
	};
	// lambda to import dict
	auto import_dict = [&]() {
		interact("type dict file path for import: ");
		string import{buf};
		m.import_dict(buddy::fs::path{import});
		memset(buf, 0, sizeof(buf));
	};
	// lambda to delete dict
	auto delete_dict = [&]() {
		interact("type dict filename for deletion: ");
		string d{buf};
		m.remove_dict(d);
		memset(buf, 0, sizeof(buf));
	};
	// lambda to do offline query
	auto offline_query = [&]() {
		interact("word for query: ");
		string word{buf};
		m.offline_query_str(word, h, ctx);
		memset(buf, 0, sizeof(buf));
	};
	// lambda to do offline preview
	auto offline_predict = [&]() {
		interact("word for predict: ");
		string word_preview{buf};
		buddy::query_cluster::predict_type p{};
		m.predict<6>(p, string{word_preview}, ctx);
		cout << p << std::flush;
		memset(buf, 0, sizeof(buf));
	};
	// lambda to do offline word lookup
	auto offline_lookup = [&]() {
		interact("word for lookup (marisa trie id): ");
		string word{buf};
		m.offline_query_str(word, h, ctx);
		memset(buf, 0, sizeof(buf));
	};

	// lambda to do oxford online word query
	//	auto oxford_online_query = [&]() {
	//		interact("word (english) for oxford online query: ");
	//		string word_lookup{buf};
	//		std::promise<void> promise_;
	//		buddy_debug_handler_online h_p{&promise_};
	//		auto f = h_p.get_future();
	//		m.query_oxford_entries(word_lookup, h_p, ctx);
	//		m.notify_http_thread();
	//		f.wait();
	//		cout << std::flush;
	//		memset(buf, 0, sizeof(buf));
	//	};

	// lambda to do MERRIAM-WEBSTER online word query
	auto MERRIAM_WEBSTER_online_query = [&]() {
		interact("word (english) for MERRIAM-WEBSTER'S COLLEGIATE® DICTIONARY and COLLEGIATE® THESAURUS "
				 "online query: ");
		string word_lookup{buf};
		std::promise<void> promise_0;
		std::promise<void> promise_1;
		buddy_debug_handler_online h_p0{&promise_0};
		buddy_debug_handler_online h_p1{&promise_1};
		auto f0 = h_p0.get_future();
		auto f1 = h_p1.get_future();
		m.query_webster_Collegiate_Dictionary(word_lookup, h_p0);
		m.query_webster_Collegiate_Thesaurus(word_lookup, h_p1);
		m.notify_http_thread();
		f0.wait();
		f1.wait();
		cout << std::flush;
		memset(buf, 0, sizeof(buf));
	};

	// lambda to list all dict
	auto list_dict = [&]() {
		auto dict_p = m.get_interface();
		auto& dict = *dict_p;
		cout << "offline dictionaries:\n";
		for (auto& i : dict) {
			cout << i.first << '\n';
		}
		cout << "online dictionaries:\n";
		// cout << "Oxford Dictionaries\n";
		cout << "MERRIAM-WEBSTER'S COLLEGIATE® DICTIONARY WITH AUDIO\n";
		cout << "MERRIAM-WEBSTER'S COLLEGIATE® THESAURUS\n";
	};

	bool quit_flag{};
	auto reception = [&]() {
		interact("type the task type (type help to show the list): ");
		string cmd{buf};
		if (cmd == "help") {
			help();
		} else if (cmd == "offq") {
			offline_query();
		} else if (cmd == "offp") {
			offline_predict();
		} else if (cmd == "lang") {
			set_lang_pair();
		} else if (cmd == "offl") {
			offline_lookup();
		}
		//		else if (cmd == "onq_oxford") {
		//			oxford_online_query();
		//		}
		else if (cmd == "onq_webster") {
			MERRIAM_WEBSTER_online_query();
		} else if (cmd == "l") {
			list_dict();
		} else if (cmd == "import") {
			import_dict();
		} else if (cmd == "delete") {
			delete_dict();
		} else if (cmd == "set_api") {
			update_api_key();
		} else if (cmd == "q") {
			quit_flag = true;
		} else {
			cout << "unkown command, quit\n";
			quit_flag = true;
		}
	};

	tips();
	while (!quit_flag) {
		reception();
	}
}

void test_control_flex(bool use_proxy, const char* https_url, const char* https_file, const char* http_url,
					   const char* http_file) {
	using namespace std::chrono_literals;
	online::socks_url proxy{};
	if (use_proxy) {
		proxy.host = "127.0.0.1";
		proxy.port = "1080";
		proxy.scheme = online::SCHEME_SOCKS5;
	}
	online::control<buddy_debug_strm_handler> ctrl{proxy};
	ctrl.init_worker_thread();
	// https request
	auto u_https = online::urlstr_2_URL(https_url);
	u_https.port = "443";
	std::ofstream https_fstrm{https_file};
	buddy_debug_strm_handler https_handler{&https_fstrm};
	// submit task
	ctrl.task_enqueue(u_https, std::move(https_handler));
	//  http request
	auto u_http = online::urlstr_2_URL(http_url);
	u_http.port = "80";
	std::ofstream http_fstrm{http_file};
	buddy_debug_strm_handler http_handler{&http_fstrm};
	// submit task
	ctrl.task_enqueue(u_http, std::move(http_handler));
	ctrl.notify_thread();
	std::this_thread::sleep_for(10s);
}

void dict_content_iteration(int argc, char* argv[]) {
	if (argc != 3) {
		cout << "run test with <dict_file> <dst> arguments" << endl;
		return;
	}
	return dict_content_iteration(argv[1], argv[2]);
}

void dict_content_iteration(const fs::path& dict, const fs::path& dst) {
	namespace mdx = lexicon::mdx;
	namespace stardict = lexicon::stardict;
	if (dict.extension() == ".mdx") {
		std::ofstream output{dst.string()};
		mdx::mdx_guest<mdx::memory> dict_container{dict.string()};
		mdx::WCR_agent<mdx::memory> agent(&dict_container);
		std::size_t count = 0;
		for (; !agent.reach_end(); ++agent) {
			auto& [word_and_coor, record] = *agent;
			output << word_and_coor.word << '\n';
			output << record << '\n';
			++count;
		}
		output << "count: " << count;
	} else if (dict.filename().string().find(".dict.dz") != string::npos) {
		std::ofstream output{dst.string()};
		fs::path ifo = dict;
		fs::path idx = dict;
		ifo.replace_extension("").replace_extension(".ifo");
		idx.replace_extension("").replace_extension(".idx");
		stardict::ifo_str ifo_s{ifo.c_str()};
		auto entry_type = stardict::get_idx_iter_entry_type(ifo_s);
		std::size_t count = 0;
		if (entry_type == stardict::bit64) {
			stardict::dict_container<stardict::bit64, false> dict_container{ifo.c_str(), idx.c_str(),
																			dict.c_str()};
			stardict::WCR_agent<stardict::bit64, false> agent{&dict_container};
			for (; !agent.reach_end(); ++agent) {
				auto& [word_and_coor, record] = *agent;
				output << word_and_coor.word_ << '\n';
				output << record << '\n';
				++count;
			}
		} else {
			stardict::dict_container<stardict::bit32, false> dict_container{ifo.c_str(), idx.c_str(),
																			dict.c_str()};
			stardict::WCR_agent<stardict::bit32, false> agent{&dict_container};
			for (; !agent.reach_end(); ++agent) {
				auto& [word_and_coor, record] = *agent;
				output << word_and_coor.word_ << '\n';
				output << record << '\n';
				++count;
			}
		}
		output << "count: " << count;
	}
}

} // namespace test
} // namespace lexicon
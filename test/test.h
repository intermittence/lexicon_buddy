//
//  test.h
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/7/11.
//  Copyright © 2018 Lohengrin. All rights reserved.
//
#ifndef TEST_H
#define TEST_H

#include <iostream>
#include <map>
#include <lexicon_buddy/utilities/lang_platform.hpp>
#if Unix_Style
#include <boost/filesystem.hpp>
#else
#include <filesystem>
#endif

#if Unix_Style
namespace fs = boost::filesystem;
using boost::system::error_code;
#else
namespace fs = std::filesystem;
using std::error_code;
#endif

// dz file have only one header
namespace lexicon
{
namespace test
{
void dz_index_logging(const char* dz_path, const char* log_path);

void stardict_query_dz_random_test(std::ostream& ostrm, const char* ifo_path, const char* idx_path,
								   const char* dz_path, unsigned n, unsigned num);

void stardict_query_mmp_dz_random_test(std::ostream& ostrm, const char* ifo_path, const char* idx_path,
									   const char* dz_path, unsigned n, unsigned num);

void test_index_stardict(const char* dict_file_path, const char* target_trie_without_suffix,
						 const char* target_coordinate_without_suffix);

void test_query_stardict(const char* trie, const char* coordinate, const char* word,
						 const char* ifo_path = nullptr, const char* idx_path = nullptr,
						 const char* dict_path = nullptr);

void test_index_mdx(const char* dict_file_path, const char* target_trie_without_suffix,
					const char* target_coordinate_without_suffix);

void test_query_mdx(const char* trie, const char* coordinate, const char* word,
					const char* dict_path = nullptr);

// TODO: test cfg
void test_cfg(const char* dict_dir_, const char* index_dir_, const char* cfg_path_, const char* import);

void test_buddy(int argc, char* argv[]);

void test_control_flex(bool use_proxy, const char* https_url, const char* https_file, const char* http_url,
					   const char* http_file);

void dict_content_iteration(const fs::path& dict, const fs::path& dst);
void dict_content_iteration(int argc, char* argv[]);
} // namespace test
} // namespace lexicon
#endif // TEST_H

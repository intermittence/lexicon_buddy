//
//  main.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/03/12.
//  Copyright © 2018 Lohengrin. All rights reserved.
//
#include "test.h"
#include <lexicon_buddy/lexicon/http_common.h>
#include <lexicon_buddy/utilities/utilities.hpp>
#include <chrono>
#include <iostream>
#include <sstream>
#include <marisa.h>
#include <lexicon_buddy/utilities/unzip_tool.h>
namespace test = lexicon::test;
int main(int argc, char* argv[]) {
	// test::test_buddy(argc, argv);
	test::dict_content_iteration(argc, argv);
	// todo unzip to stream test
	//	ifstream in{"/Users/zhanggelin/Desktop/files/dictStudy/dict file/mwc.dict.dz"};
	//	ofstream out{"/Users/zhanggelin/Desktop/files/dictStudy/dict file/mwc.dict"};
	//	unzip::unzip_agent<unzip::comp_type::dictzip>::unzip_all(in, out);
}
﻿//
// Copyright (c) 2018 jackarain (jack dot wgm at gmail dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/boostorg/beast
//

#pragma once

#include <boost/system/system_error.hpp>
#include <boost/system/error_code.hpp>

namespace socks
{

class error_category_impl;

template <class error_category>
const boost::system::error_category& error_category_single() {
	static error_category error_category_instance;
	return reinterpret_cast<const boost::system::error_category&>(error_category_instance);
}

inline const boost::system::error_category& error_category() {
	return error_category_single<socks::error_category_impl>();
}

namespace errc
{
enum errc_t
{
	/// SOCKS unsupported version.
	socks_unsupported_version = 1000,

	/// SOCKS username required.
	socks_username_required,

	/// SOCKS unsupported authentication version.
	socks_unsupported_authentication_version,

	/// SOCKS authentication error.
	socks_authentication_error,

	/// SOCKS general failure.
	socks_general_failure,

	/// SOCKS command not supported.
	socks_command_not_supported,

	/// SOCKS no identd running.
	socks_no_identd,

	/// SOCKS no identd running.
	socks_identd_error,

	/// request rejected or failed.
	socks_request_rejected_or_failed,

	/// request rejected becasue SOCKS server cannot connect to identd on the client.
	socks_request_rejected_cannot_connect,

	/// request rejected because the client program and identd report different user - ids
	socks_request_rejected_incorrect_userid,
};

inline boost::system::error_code make_error_code(errc_t e) {
	return boost::system::error_code(static_cast<int>(e), socks::error_category());
}
} // namespace errc

class error_category_impl : public boost::system::error_category
{
	virtual const char* name() const BOOST_SYSTEM_NOEXCEPT { return "SOCKS"; }

	virtual std::string message(int e) const {
		switch (e) {
			case errc::socks_unsupported_version:
				return "SOCKS unsupported version";
			case errc::socks_username_required:
				return "SOCKS username required";
			case errc::socks_unsupported_authentication_version:
				return "SOCKS unsupported authentication version";
			case errc::socks_authentication_error:
				return "SOCKS authentication error";
			case errc::socks_general_failure:
				return "SOCKS general failure";
			case errc::socks_command_not_supported:
				return "SOCKS command not supported";
			case errc::socks_no_identd:
				return "SOCKS no identd running";
			case errc::socks_identd_error:
				return "SOCKS no identd running";
			case errc::socks_request_rejected_or_failed:
				return "request rejected or failed";
			case errc::socks_request_rejected_cannot_connect:
				return "request rejected becasue SOCKS server cannot connect to identd on the client";
			case errc::socks_request_rejected_incorrect_userid:
				return "request rejected because the client program and identd report different user";
			default:
				return "Unknown PROXY error";
		}
	}
};
} // namespace socks

namespace boost
{
namespace system
{

template <>
struct is_error_code_enum<socks::errc::errc_t>
{ static const bool value = true; };

} // namespace system
} // namespace boost

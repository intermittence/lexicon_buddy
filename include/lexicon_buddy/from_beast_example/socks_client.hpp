﻿//
// Copyright (c) 2018 jackarain (jack dot wgm at gmail dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/boostorg/beast
//

#pragma once

#include "error.hpp"

#include <boost/asio/spawn.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>

#include <iostream>
#include <string>
#include <memory>

using boost::asio::ip::tcp;

namespace socks
{

template <typename Handler>
class socks_client : public std::enable_shared_from_this<socks_client<Handler>>
{
public:
	enum
	{
		SOCKS_VERSION_4 = 4,
		SOCKS_VERSION_5 = 5
	};
	enum
	{
		SOCKS5_AUTH_NONE = 0x00,
		SOCKS5_AUTH = 0x02,
		SOCKS5_AUTH_UNACCEPTABLE = 0xFF
	};
	enum
	{
		SOCKS_CMD_CONNECT = 0x01,
		SOCKS_CMD_BIND = 0x02,
		SOCKS5_CMD_UDP = 0x03
	};
	enum
	{
		SOCKS5_ATYP_IPV4 = 0x01,
		SOCKS5_ATYP_DOMAINNAME = 0x03,
		SOCKS5_ATYP_IPV6 = 0x04
	};
	enum
	{
		SOCKS5_SUCCEEDED = 0x00,
		SOCKS5_GENERAL_SOCKS_SERVER_FAILURE,
		SOCKS5_CONNECTION_NOT_ALLOWED_BY_RULESET,
		SOCKS5_NETWORK_UNREACHABLE,
		SOCKS5_CONNECTION_REFUSED,
		SOCKS5_TTL_EXPIRED,
		SOCKS5_COMMAND_NOT_SUPPORTED,
		SOCKS5_ADDRESS_TYPE_NOT_SUPPORTED,
		SOCKS5_UNASSIGNED
	};
	enum
	{
		SOCKS4_REQUEST_GRANTED = 90,
		SOCKS4_REQUEST_REJECTED_OR_FAILED,
		SOCKS4_CANNOT_CONNECT_TARGET_SERVER,
		SOCKS4_REQUEST_REJECTED_USER_NO_ALLOW,
	};

public:
	explicit socks_client(tcp::socket& socket, const std::string& hostname, unsigned short port, int version,
						  const std::string& username, const std::string& password, bool use_hostname,
						  Handler handler)
		: socket_(socket), hostname_(hostname), port_(port), version_(version), username_(username),
		  password_(password), use_hostname_(use_hostname), handler_(handler) {}

	void handshake() {
		auto self = std::enable_shared_from_this<socks_client<Handler>>::shared_from_this();
		switch (version_) {
			case SOCKS_VERSION_5:
				boost::asio::spawn(socket_.get_io_context(), [this, self](boost::asio::yield_context yield) {
					do_socks5_handshake(yield);
				});
				break;
			case SOCKS_VERSION_4:
				boost::asio::spawn(socket_.get_io_context(), [this, self](boost::asio::yield_context yield) {
					do_socks4_handshake(yield);
				});
				break;
			default:
				BOOST_ASSERT("socks version error!" && false);
		}
	}

private:
	template <typename type, typename source>
	type read(source& p) {
		type ret = 0;
		for (std::size_t i = 0; i < sizeof(type); i++)
			ret = (ret << 8) | (static_cast<unsigned char>(*p++));
		return ret;
	}

	template <typename type, typename target>
	void write(type v, target& p) {
		for (auto i = (int)sizeof(type) - 1; i >= 0; i--, p++)
			*p = static_cast<unsigned char>((v >> (i * 8)) & 0xff);
	}

	void do_socks5_handshake(boost::asio::yield_context yield) {
		boost::asio::streambuf request;
		boost::system::error_code ec;

		{
			std::size_t bytes_to_write = username_.empty() ? 3 : 4;
			auto req1 = boost::asio::buffer_cast<char*>(request.prepare(bytes_to_write));

			write<uint8_t>(5, req1); // SOCKS VERSION 5.
			if (username_.empty()) {
				write<uint8_t>(1, req1); // 1 authentication method (no auth)
				write<uint8_t>(0, req1); // no authentication
			} else {
				write<uint8_t>(2, req1); // 2 authentication methods
				write<uint8_t>(0, req1); // no authentication
				write<uint8_t>(2, req1); // username/password
			}

			request.commit(bytes_to_write);
			boost::asio::async_write(socket_, request, boost::asio::transfer_exactly(bytes_to_write),
									 yield[ec]);
			if (ec) {
				handler_(ec);
				return;
			}
		}

		boost::asio::streambuf response;
		boost::asio::async_read(socket_, response, boost::asio::transfer_exactly(2), yield[ec]);
		if (ec) {
			handler_(ec);
			return;
		}

		int method;
		bool authed = false;

		auto rp1 = boost::asio::buffer_cast<const char*>(response.data());
		auto version = read<uint8_t>(rp1);
		method = read<uint8_t>(rp1);
		if (version != 5) {
			ec = socks::errc::socks_unsupported_version;
			handler_(ec);
			return;
		}

		if (method == 2) {
			if (username_.empty()) {
				ec = socks::errc::socks_username_required;
				handler_(ec);
				return;
			}

			// start sub-negotiation.
			request.consume(request.size());

			std::size_t bytes_to_write = username_.size() + password_.size() + 3;
			auto up = boost::asio::buffer_cast<char*>(request.prepare(bytes_to_write));

			write<uint8_t>(1, up);
			write<uint8_t>(static_cast<uint8_t>(username_.size()), up);
			std::copy(username_.begin(), username_.end(), up); // username.
			up += username_.size();
			write<uint8_t>(static_cast<int8_t>(password_.size()), up);
			std::copy(password_.begin(), password_.end(), up); // password.
			up += password_.size();
			request.commit(bytes_to_write);

			// write username & password.
			boost::asio::async_write(socket_, request, boost::asio::transfer_exactly(bytes_to_write),
									 yield[ec]);
			if (ec) {
				handler_(ec);
				return;
			}

			response.consume(response.size());
			boost::asio::async_read(socket_, response, boost::asio::transfer_exactly(2), yield[ec]);
			if (ec) {
				handler_(ec);
				return;
			}

			auto cp = boost::asio::buffer_cast<const char*>(response.data());

			auto ver = read<uint8_t>(cp);
			auto status = read<uint8_t>(cp);

			if (ver != 1) {
				ec = errc::socks_unsupported_authentication_version;
				handler_(ec);
				return;
			}

			if (status != 0) {
				ec = errc::socks_authentication_error;
				handler_(ec);
				return;
			}

			authed = true;
		}

		if (method == 0 || authed) {
			request.consume(request.size());
			std::size_t bytes_to_write = 7 + hostname_.size();
			boost::asio::mutable_buffer mb = request.prepare(std::max<std::size_t>(bytes_to_write, 22));
			auto wp = boost::asio::buffer_cast<char*>(mb);

			write<uint8_t>(5, wp); // SOCKS VERSION 5.
			write<uint8_t>(1, wp); // CONNECT command.
			write<uint8_t>(0, wp); // reserved.

			if (use_hostname_) {
				write<uint8_t>(3, wp); // atyp, domain name.
				BOOST_ASSERT(hostname_.size() <= 255);
				write<uint8_t>(static_cast<int8_t>(hostname_.size()), wp); // domainname size.
				std::copy(hostname_.begin(), hostname_.end(), wp);		   // domainname.
				wp += hostname_.size();
				write<uint16_t>(port_, wp); // port.
			} else {
				auto endp = boost::asio::ip::address::from_string(hostname_, ec);
				if (ec) {
					handler_(ec);
					return;
				}

				if (endp.is_v4()) {
					write<uint8_t>(1, wp); // ipv4.
					write<uint32_t>(endp.to_v4().to_ulong(), wp);
					write<uint16_t>(port_, wp);
					bytes_to_write = 10;
				} else {
					write<uint8_t>(4, wp); // ipv6.
					auto bytes = endp.to_v6().to_bytes();
					std::copy(bytes.begin(), bytes.end(), wp);
					wp += 16;
					write<uint16_t>(port_, wp);
					bytes_to_write = 22;
				}
			}

			request.commit(bytes_to_write);
			boost::asio::async_write(socket_, request, boost::asio::transfer_exactly(bytes_to_write),
									 yield[ec]);
			if (ec) {
				handler_(ec);
				return;
			}

			std::size_t bytes_to_read = 10;
			response.consume(response.size());
			boost::asio::async_read(socket_, response, boost::asio::transfer_exactly(bytes_to_read),
									yield[ec]);
			if (ec) {
				handler_(ec);
				return;
			}
			boost::asio::const_buffer cb = response.data();
			auto rp = boost::asio::buffer_cast<const char*>(cb);
			auto ver = read<uint8_t>(rp);
			auto resp = read<uint8_t>(rp);
			read<uint8_t>(rp); // skip RSV.
			int atyp = read<uint8_t>(rp);

			if (atyp == 1) // ADDR.PORT
			{
				tcp::endpoint remote_endp(boost::asio::ip::address_v4(read<uint32_t>(rp)),
										  read<uint16_t>(rp));

				std::cout << "* SOCKS remote host: " << remote_endp.address().to_string() << ":"
						  << remote_endp.port() << std::endl;
			} else if (atyp == 3) // DOMAIN
			{
				auto domain_length = read<uint8_t>(rp);

				boost::asio::async_read(socket_, response, boost::asio::transfer_exactly(domain_length - 3),
										yield[ec]);
				if (ec) {
					handler_(ec);
					return;
				}

				rp = boost::asio::buffer_cast<const char*>(response.data()) + 5;

				std::string domain;
				for (int i = 0; i < domain_length; i++)
					domain.push_back(read<uint8_t>(rp));
				auto port = read<uint16_t>(rp);

				std::cout << "* SOCKS remote host: " << domain << ":" << port << std::endl;
			} else {
				ec = errc::socks_general_failure;
				handler_(ec);
				return;
			}

			if (ver != 5) {
				ec = errc::socks_unsupported_version;
				handler_(ec);
				return;
			}

			if (resp != 0) {
				ec = errc::socks_general_failure;
				switch (resp) {
					case 2:
						ec = boost::asio::error::no_permission;
						break;
					case 3:
						ec = boost::asio::error::network_unreachable;
						break;
					case 4:
						ec = boost::asio::error::host_unreachable;
						break;
					case 5:
						ec = boost::asio::error::connection_refused;
						break;
					case 6:
						ec = boost::asio::error::timed_out;
						break;
					case 7:
						ec = errc::socks_command_not_supported;
						break;
					case 8:
						ec = boost::asio::error::address_family_not_supported;
						break;
				}

				handler_(ec);
				return;
			}

			// successed.
			ec = boost::system::error_code();
			handler_(ec);
			return;
		}

		ec = boost::asio::error::address_family_not_supported;
		handler_(ec);
		return;
	}

	void do_socks4_handshake(boost::asio::yield_context yield) {
		boost::system::error_code ec;

		boost::asio::streambuf request;
		std::size_t bytes_to_write = 9 + username_.size();
		boost::asio::mutable_buffer b = request.prepare(bytes_to_write);
		auto wp = boost::asio::buffer_cast<char*>(b);

		write<uint8_t>(4, wp); // SOCKS VERSION 5.
		write<uint8_t>(1, wp); // CONNECT.

		write<uint16_t>(port_, wp); // DST PORT.

		auto address = boost::asio::ip::address_v4::from_string(hostname_, ec);
		if (ec) {
			handler_(ec);
			return;
		}
		write<uint32_t>(address.to_uint(), wp); // DST IP.

		if (!username_.empty()) {
			std::copy(username_.begin(), username_.end(), wp); // USERID
			wp += username_.size();
		}
		write<uint8_t>(0, wp); // NULL.

		request.commit(bytes_to_write);
		boost::asio::async_write(socket_, request, boost::asio::transfer_exactly(bytes_to_write), yield[ec]);
		if (ec) {
			handler_(ec);
			return;
		}

		boost::asio::streambuf response;
		boost::asio::async_read(socket_, response, boost::asio::transfer_exactly(8), yield[ec]);
		if (ec) {
			handler_(ec);
			return;
		}

		boost::asio::const_buffer cb = response.data();
		auto rp = boost::asio::buffer_cast<const char*>(cb);

		read<uint8_t>(rp); // VN is the version of the reply code and should be 0.
		auto cd = read<uint8_t>(rp);

		if (cd != SOCKS4_REQUEST_GRANTED) {
			switch (cd) {
				case SOCKS4_REQUEST_REJECTED_OR_FAILED:
					ec = errc::socks_request_rejected_or_failed;
					break;
				case SOCKS4_CANNOT_CONNECT_TARGET_SERVER:
					ec = errc::socks_request_rejected_cannot_connect;
					break;
				case SOCKS4_REQUEST_REJECTED_USER_NO_ALLOW:
					ec = errc::socks_request_rejected_incorrect_userid;
					break;
				default:
					ec = errc::socks_general_failure;
					break;
			}
		}

		handler_(ec);
	}

private:
	tcp::socket& socket_;
	std::string hostname_;
	unsigned short port_;
	int version_;
	std::string username_;
	std::string password_;
	bool use_hostname_;
	Handler handler_;
};

template <typename CompletionToken>
BOOST_ASIO_INITFN_RESULT_TYPE(CompletionToken, void(boost::system::error_code))
async_handshake(tcp::socket& socket, const std::string& hostname, unsigned short port, int version,
				const std::string username, std::string password, bool use_hostname,
				CompletionToken&& token) {
	boost::asio::async_completion<CompletionToken, void(boost::system::error_code)> init{token};
	using socks_client_op =
		socks_client<BOOST_ASIO_HANDLER_TYPE(CompletionToken, void(boost::system::error_code))>;
	auto op = std::make_shared<socks_client_op>(socket, hostname, port, version, username, password,
												use_hostname, init.completion_handler);
	op->handshake();
	return init.result.get();
}

} // namespace socks

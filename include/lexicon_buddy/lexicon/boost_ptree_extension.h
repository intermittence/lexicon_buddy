//
//  boost_ptree_extension.h
//  lexicon_buddy
//
//  Created by: Lohengrin on 2018/04/6
//  Copyright © 2018 Lohengrin. All rights reserved.
//

#ifndef boost_ptree_extension_h
#define boost_ptree_extension_h
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <string>
#include <lexicon_buddy/lexicon/lexicon_types.h>
// Custom translator for bool (only supports std::string)
struct BoolTranslator
{
	typedef std::string internal_type;
	typedef bool external_type;

	// Converts a string to bool
	boost::optional<external_type> get_value(const internal_type& str) {
		if (!str.empty()) {
			using boost::algorithm::iequals;
			if (iequals(str, "false") || iequals(str, "no") || str == "0")
				return boost::optional<external_type>(false);
			else
				return boost::optional<external_type>(true);
		} else
			return boost::optional<external_type>(boost::none);
	}

	// Converts a bool to string
	boost::optional<internal_type> put_value(const external_type& b) {
		return boost::optional<internal_type>(b ? "true" : "false");
	}
};

struct MediaTypeTranslator
{
	typedef std::string internal_type;
	typedef lexicon::res_media_type external_type;

	// Converts a string to bool
	boost::optional<external_type> get_value(const internal_type& str) {
		if (!str.empty()) {
			using boost::algorithm::iequals;
			if (iequals(str, "unknown")) {
				return boost::optional<external_type>(external_type::unknown);
			} else if (iequals(str, "text")) {
				return boost::optional<external_type>(external_type::text);
			} else if (iequals(str, "pic")) {
				return boost::optional<external_type>(external_type::pic);
			} else if (iequals(str, "sound")) {
				return boost::optional<external_type>(external_type::sound);
			} else {
				return boost::optional<external_type>(external_type::invalid);
			}

		} else
			return boost::optional<external_type>(boost::none);
	}

	// Converts a bool to string
	boost::optional<internal_type> put_value(const external_type& t) {
		switch (t) {
			case external_type::invalid:
				return boost::optional<internal_type>("invalid");
			case external_type::unknown:
				return boost::optional<internal_type>("unknown");
			case external_type::text:
				return boost::optional<internal_type>("text");
			case external_type::pic:
				return boost::optional<internal_type>("pic");
			case external_type::sound:
				return boost::optional<internal_type>("sound");
		}
	}
};

struct ResTypeTranslator
{
	typedef std::string internal_type;
	typedef lexicon::res_type external_type;

	// Converts a string to bool
	boost::optional<external_type> get_value(const internal_type& str) {
		if (!str.empty()) {
			using boost::algorithm::iequals;
			if (iequals(str, ".mdx")) {
				return boost::optional<external_type>(external_type::mdx);
			} else if (iequals(str, ".mdd")) {
				return boost::optional<external_type>(external_type::mdd);
			} else if (iequals(str, ".dict.dz")) {
				return boost::optional<external_type>(external_type::startdict_dict_dz);
			} else if (iequals(str, ".idx")) {
				return boost::optional<external_type>(external_type::startdict_idx);
			} else if (iequals(str, ".ifo")) {
				return boost::optional<external_type>(external_type::startdict_ifo);
			} else {
				return boost::optional<external_type>(external_type::invalid);
			}

		} else
			return boost::optional<external_type>(boost::none);
	}

	// Converts a bool to string
	boost::optional<internal_type> put_value(const external_type& t) {
		switch (t) {
			case lexicon::res_type::invalid:
				return boost::optional<internal_type>("invalid");
			case lexicon::res_type::mdx:
				return boost::optional<internal_type>(".mdx");
			case lexicon::res_type::mdd:
				return boost::optional<internal_type>(".mdd");
			case lexicon::res_type::startdict_dict_dz:
				return boost::optional<internal_type>(".dict.dz");
			case lexicon::res_type::startdict_idx:
				return boost::optional<internal_type>(".idx");
			case lexicon::res_type::startdict_ifo:
				return boost::optional<internal_type>(".ifo");
		}
	}
};

/*  Specialize translator_between so that it uses our custom translator for
 bool value types. Specialization must be in boost::property_tree
 namespace. */
namespace boost
{
namespace property_tree
{

template <typename Ch, typename Traits, typename Alloc>
struct translator_between<std::basic_string<Ch, Traits, Alloc>, bool>
{ typedef BoolTranslator type; };

template <typename Ch, typename Traits, typename Alloc>
struct translator_between<std::basic_string<Ch, Traits, Alloc>, lexicon::res_media_type>
{ typedef MediaTypeTranslator type; };

template <typename Ch, typename Traits, typename Alloc>
struct translator_between<std::basic_string<Ch, Traits, Alloc>, lexicon::res_type>
{ typedef ResTypeTranslator type; };
} // namespace property_tree
} // namespace boost
#endif /* boost_ptree_extension_h */

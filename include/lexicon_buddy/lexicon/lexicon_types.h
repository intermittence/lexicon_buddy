//
//  lexicon_types.h
//  lexicon_buddy
//
//  Created by Lohengrin on 2019/1/27.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef LEXICON_TYPES_H
#define LEXICON_TYPES_H

#include <cstring>
#include <string>

namespace lexicon
{
// for offline file
using std::string;
using std::string_view;

enum class res_type
{
	invalid,
	mdx,
	mdd,
	startdict_dict_dz,
	startdict_idx,
	startdict_ifo
};

enum class res_bundle_type
{
	invalid,
	mdx,
	mdd,
	startdict_dict_dz_bundle,
};
// for offline and online
enum class res_media_type
{
	invalid,
	unknown, /*unknown or mix media type*/
	text,
	pic,
	sound
};

// std::pair<string, string> dict_filename_separation(const string& arg);

inline bool supported_res_main_file_ext(const std::string& ext) {
	if (ext == ".dict.dz" /*or ext == ".ifo" or ext == ".idx"*/ or ext == ".mdx") return true;
	return false;
}

res_bundle_type res_type_to_bundle_type(res_type arg);
const char* res_bundle_type_to_str(res_bundle_type arg);
res_type str_to_res_type(const char* str);
res_type str_to_res_type(const std::string& str);
const char* res_type_to_str(res_type t);
res_media_type str_to_media_type(const char* str);
res_media_type str_to_media_type(const string& str);
const char* media_type_to_str(res_media_type t);

namespace buddy
{

/// @brief some abbreviations are borrowed from @link
/// https://developer.oxforddictionaries.com/documentation/languages
enum class lang
{
	all, /*all language*/
	en,
	en_gb,
	en_us,
	es,
	hi,
	nso,
	tn,
	zu,
	de,
	pt,
	ms,
	id,
	ur,
	el,
	qu,
	te,
	tk,
	tpi,
	tt,
	xh,
	lv,
	sw,
	ta,
	tg,
	gu,
	ro,
	zh_CN,
	zh_TW,
	empty, /*null*/
	first = all,
	last = empty
};
using lang_type = std::pair<lang, lang>;
const char* lang_type_to_str(lang l);
lang str_to_lang_type(const string& str);
lang str_to_lang_type(const char* first, const char* last);
string lang_pair_type_to_str(lang_type l);
lang_type str_to_lang_pair_type(const string& str);
} // namespace buddy

namespace index
{
// exception types
class xml_error : public std::runtime_error
{
	// use runtime_error constructor
	using std::runtime_error::runtime_error;
};
class dict_error : public std::runtime_error
{
	// use runtime_error constructor
	using std::runtime_error::runtime_error;
};
class dict_index_err : public dict_error
{
	// use runtime_error constructor
	using dict_error::dict_error;
};
class dict_duplication : public dict_error
{
	// use runtime_error constructor
	using dict_error::dict_error;
};
class dict_corruption : public dict_error
{
	// use runtime_error constructor
	using dict_error::dict_error;
};
class dict_file_err : public dict_error
{
	// use runtime_error constructor
	using dict_error::dict_error;
};
class dict_unknown_err : public dict_error
{
	// use runtime_error constructor
	using dict_error::dict_error;
};
} // namespace index

namespace online
{

using lang = lexicon::buddy::lang;

enum class dict_api
{
	Oxford = 0,
	// TODO: impl merriam-webster
	Merriam_Webster = 1,
	// TODO:

};

enum class oxford_query_type // only the implemented operation
{
	// Lemmatron
	Dictionary_entries = 0,
	Thesaurus = 1,
	Search = 2,
	Translation = 3,
	Wordlist = 4,
	Sentences = 5,
	LexiStats = 6,
	first = Dictionary_entries,
	last = LexiStats
};

enum class merriam_webster_dict
{
	// Merriam-Webster's Collegiate® Dictionary with Audio
	Collegiate_Dictionary,
	// Merriam-Webster's Collegiate® Thesaurus
	Collegiate_Thesaurus
};

} // namespace online
} // namespace lexicon

namespace unzip
{
enum class comp_type
{
	no_comp = 0,
	LZO = 1,
	zlib = 2,
	gzip = 3,
	dictzip = 4,
	raw = 5,
	FIRST = no_comp,
	LAST = raw
};
}
#endif // LEXICON_TYPES_H
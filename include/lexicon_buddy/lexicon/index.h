//
//  index.h
//  lexicon_buddy
//
//  Created by Lohengrin on 12/28/2018 03:24:45.
//  Copyright © 2018 Lohengrin. All rights reserved.
//
#ifndef INDEX_H
#define INDEX_H

#include <lexicon_buddy/lexicon_buddy_export.h>
#include <lexicon_buddy/utilities/lang_platform.hpp>
#include <lexicon_buddy/utilities/utilities.hpp>
#include <cstdint>
#include <algorithm>
#include <lexicon_buddy/lexicon/boost_ptree_extension.h>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <exception>
#include <iostream>
#include <set>
#if Unix_Style
#include <boost/filesystem.hpp>
#else
#include <filesystem>
#endif
#include <marisa.h>
#include <lexicon_buddy/lexicon/lexicon_types.h>
#include <lexicon_buddy/lexicon/stardictcontainer.h>
#include <lexicon_buddy/lexicon/mdxContainer.hpp>

namespace lexicon
{
namespace index
{

constexpr char mdx_coor32_suffix[] = ".mco32";
constexpr char mdx_coor64_suffix[] = ".mco64";
constexpr char mdx_trie32_suffix[] = ".mtrie32";
constexpr char mdx_trie64_suffix[] = ".mtrie64";
constexpr char stardict_coor32_suffix[] = ".sco32";
constexpr char stardict_coor64_suffix[] = ".sco64";
constexpr char stardict_trie32_suffix[] = ".strie32";
constexpr char stardict_trie64_suffix[] = ".strie64";

// filesystem namespace aliasing
// std::filesystem on mac is not officially supported by clang rightnow
#if Unix_Style
namespace fs = boost::filesystem;
using boost::system::error_code;
#else
namespace fs = std::filesystem;
using std::error_code;
#endif
namespace pt = boost::property_tree;
using std::chrono::system_clock;
using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::set;
using std::map;

bool res_file_ext_filter(const string& extension);
bool res_file_ext_filter(const char* extension);
bool res_file_ext_filter(res_type t);

class index_agent
{
public:
	struct build_result_type
	{
		bool success{};
		string trie_path{};
		string coor_path{};
		build_result_type() = default;
		build_result_type(const build_result_type&) = default;
		build_result_type(build_result_type&&) = default;
		build_result_type& operator=(const build_result_type&) = default;
		build_result_type& operator=(build_result_type&&) = default;
	};
	using value_type = pt::ptree::value_type;
	using ptree_type = pt::ptree;
	static constexpr char magic_word[] = "this is a lexicon configuration";
	fs::path get_dict_dir() { return fs::path{cfg_.get<string>("instance.<xmlattr>.res_dir")}; }
	fs::path get_index_dir() { return fs::path{cfg_.get<string>("instance.<xmlattr>.index_dir")}; }
	pt::ptree* get_cfg_instance_node() { return &(cfg_.get_child("instance")); }
	pt::ptree* get_cfg() { return &cfg_; }
	/// @brief build return a success flag and names of trie and coordinate file;
	/// @param dict_file_path mdict's .mdx or stardict's .ifo
	build_result_type build_generic(const char* dict_file_path);
	/// @brief build return a success flag and names of trie and coordinate file;
	/// @param dict_file_path mdict's .mdx or stardict's .ifo
	build_result_type build_generic(const fs::path& dict_file_path);
	//	static build_result_type build_generic_to_dst(const fs::path& dict_file_path,
	//												  const fs::path& dst_index_dir);
	/// @brief build return a success flag and names of trie and coordinate file;
	/// @param dict_file_path mdict's .mdx or stardict's .idx
	static build_result_type build_generic(const char* dict_file_path, const char* target_trie_without_suffix,
										   const char* target_coordinate_without_suffix);
	// build a trie and write trie and coordinate to disk
	// word encoding of trie is utf8 despite origin encoding in dict file
	template <res_type type>
	static build_result_type build(const char* dict_file_path, const char* target_trie_without_suffix,
								   const char* target_coordinate_without_suffix) noexcept;
	index_agent(const char* dict_dir, const char* index_dir, const char* cfg_path);
	// find out new dictionary
	bool cfg_item_discovery();
	~index_agent();
	/// @note this method will generate index file and copy files to destination and @class index_agent
	/// internal state is untouched. you can invoke the method asynchronously;
	//	static build_result_type import_file_gen(const fs::path& dst_index_dir, const fs::path& dst_dict_dir,
	//											 const fs::path& src_dict_file);
	/// @note accept the result of import_file_gen()
	/// @warning must runs synchronously
	//	index_agent::ptree_type* import_accept(const build_result_type& result);
	// ptree_type* import(const char* dict_file_path);
	ptree_type* import(const fs::path& src_dict_file); /*{ return import(GET_U8_C_STR(dict_file_path)); }*/
	// remove item from map, node and delete its files
	void remove_item_node(const string& filename_with_ext);
	void dirty() { dirty_ = true; }
	void cfg_save();
	void cfg_save_if_dirty() {
		if (dirty_) {
			cfg_save();
			dirty_ = false;
		}
	}
#ifndef NDEBUG
	void debug() {
		pt::write_xml(cout, cfg_, pt::xml_writer_make_settings<string>('\t', 1));
		cout << endl;
	}
#endif

protected:
	static string get_node_res_filename(const ptree_type& item_node);

private:
	enum class node_genre
	{
		instance,
		item,
		index,
		magic,
		unknown,
	};
	enum class node_state
	{
		ok,
		item_empty,
		no_index,
		index_empty,
		index_invalid,
		node_file_error,
		res_outdate,
		index_outdate,
		error
	};

	fs::path cfgpath_;
	pt::ptree cfg_{};
	bool dirty_;
	// key: file path, mapped type: alias
	map<fs::path, string> res_map{};
	void res_map_rebuild();
	void res_map_add_item(const ptree_type& item_container, const fs::path& dict_path);
	bool res_already_exist(const fs::path& file_path);
	// std::vector<item> dict_res{}; /*mdx file and dict.dz file (with ifo file)*/
	// std::time_t cfg_instance_updatetime{};
	node_genre get_node_genre(const string& name);
	node_state get_node_state(const value_type& node);
	void node_update(value_type& node, node_genre genre, node_state state);
	void delete_error_node();
	// static res_type category_filename(const char* dict_filename);
	static res_type category_filename(const fs::path& dict_filename);
	// not completely implemented. validation of files which are not implemented will simply return true;
	static bool validate(const char* dict_filepath, res_type dict_type);
	static bool validate(const fs::path& dict_filepath, res_type dict_type) {
		return validate(GET_U8_C_STR(dict_filepath), dict_type);
	}
	static bool validate(const char* dict_filepath) {
		return validate(dict_filepath, category_filename(dict_filepath));
	}
	static bool validate(const fs::path& dict_filepath) { return validate(GET_U8_C_STR(dict_filepath)); }
	template <unsigned idx_iter_entry_type>
	static build_result_type build_stardict_helper(const char* dict_file_path, fs::path trie_path,
												   fs::path coor_path) noexcept;
	template <bool version_2_0>
	static build_result_type build_mdx_helper(mdx::mdx_data_mmap& dict, fs::path trie_path,
											  fs::path coor_path) noexcept;
	// TODO: checked against lexicon cfg magic word <magic>this is a lexicon configuration</magic>
	bool cfg_instance_validation_fix(bool through_check, bool delete_error);
	void cfg_reset();
	void cfg_reset(const char* dict_dir, const char* index_dir, const char* cfg_path);
	void cfg_load();

	ptree_type cfg_build_result_to_index_node(const build_result_type& build_result);
	ptree_type cfg_build_and_add_index(res_type t, const fs::path& file_path);
	ptree_type cfg_gen_item_node(res_type t, const fs::path& file_path,
								 const build_result_type& build_result);
	// add media type
	void cfg_item_set_media_type(pt::ptree& item);
	// overload for stardict
	void cfg_item_stardict_set_media_type(pt::ptree& item);
	static void cfg_item_stardict_set_media_type(pt::ptree& item, string sametypesequence);
	// return files related to the node
	std::vector<fs::path> item_node_files(const ptree_type& pt);
};

template <>
index_agent::build_result_type
index_agent::build<res_type::mdx>(const char* dict_file_path, const char* target_trie_without_suffix,
								  const char* target_coordinate_without_suffix) noexcept;
template <>
index_agent::build_result_type
index_agent::build<res_type::startdict_idx>(const char* dict_file_path,
											const char* target_trie_without_suffix,
											const char* target_coordinate_without_suffix) noexcept;

template <unsigned idx_iter_entry_type>
index_agent::build_result_type index_agent::build_stardict_helper(const char* dict_file_path,
																  fs::path trie_path,
																  fs::path coor_path) noexcept {
	bool dirty{};
	index_agent::build_result_type result{};
	try {
		// count
		util::mmap_w idx_map{dict_file_path}; /*load idx*/
		stardict::idx_agent<idx_iter_entry_type> idx_agent_{&idx_map};
		if (!trie_path.has_filename() || !coor_path.has_filename())
			throw dict_file_err{"trie_path or coor_path do not has a filename\n"};
		uint32_t count = idx_agent_.count();
		// a buffer to save coordinate(location of each word)
		using offset_type = typename stardict::idx_iter<idx_iter_entry_type>::offset_type;
		using size_type = typename stardict::idx_iter<idx_iter_entry_type>::size_type;
		uint32_t coor_buffer_size =
			(sizeof(offset_type) + 2 + sizeof(offset_type) + sizeof(size_type)) * count;
		std::unique_ptr<char[]> coor_buffer{new char[coor_buffer_size]};
		//		stardict::idx_iter<idx_iter_entry_type> iter(idx_map.data(), idx_map.data() +
		// idx_map.get_filesize(), 													 idx_map.data());
		// build trie and save coordinate
		marisa::Trie trie_;
		uint64_t out_size{};
		// if (!stardict::build_trie_and_coordinate_flex(iter, trie_, coor_buffer.get(), &out_size))
		if (!stardict::build_trie_and_coordinate_flex(idx_agent_.begin(), idx_agent_.end(), trie_,
													  coor_buffer.get(), &out_size))
			throw std::runtime_error{"error when build_trie_and_coordinate_flex()"};
		// if success, save trie and coordinate(.co32 for 32bit idxoffsetbits, .co64 for 64bit
		trie_.save(GET_U8_C_STR(trie_path));
		dirty = true;
		std::ofstream coor_ofstrm{coor_path.c_str()};
		coor_ofstrm.write(coor_buffer.get(), out_size);
	} catch (const std::exception& e) {
		cerr << e.what() << '\n';
		error_code ec;
		if (dirty) {
			cerr << "remove file: " << trie_path << endl;
			fs::remove(trie_path, ec);
			if (ec) cerr << ec.message() << '\n';
			cerr << "remove file: " << coor_path << endl;
			fs::remove(coor_path, ec);
			if (ec) cerr << ec.message() << '\n';
		}
		return result;
	}
	result.success = true;
	result.trie_path = GET_U8STRING(trie_path);
	result.coor_path = GET_U8STRING(coor_path);
	return result;
}

template <bool version_2_0>
index_agent::build_result_type index_agent::build_mdx_helper(mdx::mdx_data_mmap& dict, fs::path trie_path,
															 fs::path coor_path) noexcept {
	index_agent::build_result_type result{};
	mdx::key_index::key_value_map<version_2_0> m1{};
	try {
		if (!trie_path.has_filename() || !coor_path.has_filename())
			throw dict_file_err{"trie_path or coor_path do not has a filename\n"};
		marisa::Trie trie_;
		marisa::Keyset key_set;
		// key encoding converted to utf8 and buffer to save coordinate
		uint64_t word_count{};
		for (auto& i : dict._key_index_v) {
			word_count += i.num_entries;
		}
		std::unique_ptr<char[]> coor_buff(new char[word_count * 18]);
		auto key_index_v_count = dict.key_block_count();
		for (decltype(key_index_v_count) i = 0; i < key_index_v_count; ++i) {
			auto key_block_c = dict.get_key_block(i);
			auto key_block_iter = key_block_c.begin();
			auto key_block_iter_end = key_block_c.end();
			// save key and coordinate
			if (!mdx::key_index::store_key_and_index_flex<version_2_0>(key_block_iter, key_block_iter_end,
																	   dict.get_encoding_canonical(), m1))
				return result;
		}
		// build and save trie
		uint64_t out_size{};
		mdx::key_index::build_trie_and_coordinate_flex<version_2_0>(m1, trie_, coor_buff.get(), &out_size);
		trie_.save(GET_U8_C_STR(trie_path));
		// save coordinate
		std::ofstream ofstrm{coor_path.c_str()};
		ofstrm.write(coor_buff.get(), out_size);
	} catch (const std::exception& e) {
		cerr << e.what() << '\n';
		error_code ec;
		cerr << "remove file: " << trie_path << endl;
		// TODO: test
		fs::remove(trie_path, ec);
		if (ec) cerr << ec.message() << '\n';
		cerr << "remove file: " << coor_path << endl;
		// TODO: test
		fs::remove(coor_path, ec);
		if (ec) cerr << ec.message() << '\n';
		return result;
	}
	result.success = true;
	result.trie_path = GET_U8STRING(trie_path);
	result.coor_path = GET_U8STRING(coor_path);
	return result;
}

/*
template <bool version_2_0>
index_agent::build_result_type index_agent::build_mdx_helper(mdx::mdx_data_mmap& dict, fs::path trie_path,
															 fs::path coor_path) noexcept {
	index_agent::build_result_type result{};
	using offset_type = std::conditional_t<version_2_0, uint64_t, uint32_t>;
	mdx::key_index::key_value_map<version_2_0> m1{};
	try {
		if (!trie_path.has_filename() || !coor_path.has_filename())
			throw dict_file_err{"trie_path or coor_path do not has a filename\n"};
		marisa::Trie trie_;
		marisa::Keyset key_set;
		// key encoding converted to utf8 and buffer to save coordinate
		uint64_t word_count{};
		for (auto& i : dict._key_index_v) {
			word_count += i.num_entries;
		}
		std::unique_ptr<char[]> coor_buff(new char[word_count * 18]);
		auto coor_buff_ptr = coor_buff.get();
		auto& key_index_v__ = dict._key_index_v;
		std::unique_ptr<char[]> decom_buffer{};
		uint32_t decom_buff_size{};
		auto key_block_raw_begin = dict.t.data() + dict._k_sec.key_blocks_offset;
		for (auto& i : key_index_v__) {
			const char* key_block_ptr{};
			// for each block, check and decompress block
			auto key_block_raw_i = key_block_raw_begin + i.origin_offset_block_begin + 8;
			if (i.comp_type) {
				// decompression
				auto decom_buff_size_tmp = i.decomp_size;
				if (decom_buff_size < decom_buff_size_tmp) {
					decom_buffer.reset(new char[decom_buff_size_tmp]);
					decom_buff_size = decom_buff_size_tmp;
				}
				key_block_ptr = decom_buffer.get();
				// comp_size[i] ( decomp_size[0] does not) include the mdx extra header part (comp_type +
				// checksum) before compressed data
				unzip::unzip_strm_generic(static_cast<unzip::comp_type>(i.comp_type), key_block_raw_i,
										  i.comp_size - 8, decom_buffer.get(), decom_buff_size_tmp);
			} else {
				key_block_ptr = key_block_raw_i;
			}
			// an iterator to extract offset (coordinate) and key string
			mdx::key_index::iter iter_(key_block_ptr, key_block_ptr + i.decomp_size, true,
									   dict.encoding_canonical.c_str(), dict.is_version_2_0());
			// save key and coordinate
			if (!mdx::key_index::store_key_and_index_flex<version_2_0>(
					iter_, dict.get_encoding_canonical().c_str(), m1)) {
				return result;
			} else {
				// coordinate occupies 8 bytes
				coor_buff_ptr += i.num_entries * sizeof(offset_type);
			}
		}
		// build and save trie
		uint64_t out_size{};
		mdx::key_index::build_trie_and_coordinate_flex<version_2_0>(m1, trie_, coor_buff.get(), &out_size);
		trie_.save(GET_U8_C_STR(trie_path));
		// save coordinate
		std::ofstream ofstrm{coor_path.c_str()};
		ofstrm.write(coor_buff.get(), out_size);
	} catch (const std::exception& e) {
		cerr << e.what() << '\n';
		error_code ec;
		cerr << "remove file: " << trie_path << endl;
		// TODO: test
		fs::remove(trie_path, ec);
		if (ec) cerr << ec.message() << '\n';
		cerr << "remove file: " << coor_path << endl;
		// TODO: test
		fs::remove(coor_path, ec);
		if (ec) cerr << ec.message() << '\n';
		return result;
	}
	result.success = true;
	result.trie_path = GET_U8STRING(trie_path);
	result.coor_path = GET_U8STRING(coor_path);
	return result;
}
*/
} // namespace index
} // namespace lexicon
#endif // INDEX_H
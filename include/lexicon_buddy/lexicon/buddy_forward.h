//
//  buddy_forward.h
//  lexicon_buddy
//
//  Created by Lohengrin on 2019/5/17.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef BUDDY_FORWARD_H
#define BUDDY_FORWARD_H
#include <lexicon_buddy/lexicon_buddy_export.h>
#include <lexicon_buddy/lexicon/lexicon_types.h>
#include <lexicon_buddy/lexicon/index.h>
#include <lexicon_buddy/lexicon/offline_query.h>
#include <lexicon_buddy/lexicon/online_dict.h>
#include <lexicon_buddy/lexicon/http_client3.h>
namespace lexicon
{
namespace buddy
{

namespace fs = index::fs;
namespace pt = index::pt;
using offline_query::search_mmap;
using lang_type = std::pair<lang, lang>;
using index::index_agent;
using std::move;
using std::forward;
using cfg_node_type = index_agent::value_type;
using cfg_ptree_type = index_agent::ptree_type;

class dict_meta
{
public:
	using media_type = res_media_type;
	// content type: translation, sentences, thesaurus(synonyms, antonyms), etc.
	struct meta
	{
		bool enabled{true};
		string alias_;
		// media: text, sound, pic
		media_type m_type_{media_type::unknown};
		// language
		// destination language can be 'emtpy'
		lang_type lang_pair0_{lang::all, lang::all};
		bool secondary_lan_{};
		lang_type lang_pair1_;
		meta(string alias, media_type m_type, lang_type lang_pair0, bool secondary_lan, lang_type lang_pair1)
			: alias_{alias}, m_type_{m_type}, lang_pair0_{lang_pair0}, secondary_lan_{secondary_lan},
			  lang_pair1_{lang_pair1} {}
		meta() = default;
		meta(const meta&) = default;
		meta& operator=(const meta& arg) = default;
		void operator<<(const cfg_ptree_type& item);
		void operator>>(cfg_ptree_type& item);
	};

private:
	meta m_;
	cfg_ptree_type* cfg_p_{};

public:
	dict_meta() {}
	dict_meta(const dict_meta&) = default;
	dict_meta& operator=(const dict_meta& arg) = default;
	dict_meta(cfg_ptree_type* cfg_p) : cfg_p_{cfg_p} { load(); }
	// get data view
	const meta& get_meta() const { return m_; }
	// used by user
	void set_enabled(bool arg) {
		m_.enabled = arg;
		save();
	}
	// used by user
	bool enabled() { return m_.enabled; }
	// used by user
	string alias() { return m_.alias_; }
	// used by user
	void set_alias(string&& arg) {
		m_.alias_ = arg;
		save();
	}
	// used by user
	void set_alias(const string& arg) {
		m_.alias_ = arg;
		save();
	}
	// used by program
	void set_media_type(media_type t) {
		m_.m_type_ = t;
		save();
	}
	// used by user
	void set_lang_type(lang_type t) {
		m_.lang_pair0_ = t;
		save();
	}
	// used by user
	void set_second_lang_type(lang_type t) {
		m_.secondary_lan_ = true;
		m_.lang_pair1_ = t;
		save();
	}
	// used by user
	lang_type lang() { return m_.lang_pair0_; }
	// used by user
	lang_type second_lang() { return m_.lang_pair1_; }
	// used by user
	bool second_lang_enabled() { return m_.secondary_lan_; }
	// used by user
	void delete_second_lang() { m_.secondary_lan_ = false; }
	void load() {
		if (cfg_p_) m_ << *cfg_p_;
	}
	void save() {
		if (cfg_p_) m_ >> *cfg_p_;
	}
};

template <class dict_type>
class LEXICON_BUDDY_EXPORT dict_bundle;

struct LEXICON_BUDDY_EXPORT mdx_record_bundle
{
	using record_type = mdx::dict_record_v;
	dict_meta d_mt;
	record_type record;
	mdx_record_bundle() = default;
	mdx_record_bundle(mdx_record_bundle&&) = default;
	mdx_record_bundle& operator=(const mdx_record_bundle&) = delete;
	mdx_record_bundle& operator=(mdx_record_bundle&&) = default;
};

struct LEXICON_BUDDY_EXPORT stardict_record_bundle
{
	using record_type = stardict::dict_record_v;
	dict_meta d_mt;
	record_type record;
	stardict_record_bundle() = default;
	stardict_record_bundle(stardict_record_bundle&&) = default;
	stardict_record_bundle& operator=(const stardict_record_bundle&) = delete;
	stardict_record_bundle& operator=(stardict_record_bundle&&) = default;
};

LEXICON_BUDDY_EXPORT std::ostream& operator<<(std::ostream& out, const mdx_record_bundle& r);

LEXICON_BUDDY_EXPORT std::ostream& operator<<(std::ostream& out, const stardict_record_bundle& r);

template <class T>
class LEXICON_BUDDY_EXPORT dict_bundle<mdx::mdx_data<T>>
{

public:
	using dict_type = mdx::mdx_data<T>;
	// prefix search type
	// using search_type = typename dict_type::search_type;
	using query_type = size_t;
	// using record_type = mdx::dict_record_v;
	using record_bundle = mdx_record_bundle;
	using search_type = search_mmap<dict_type>;
	using search_result_type = typename search_type::result_type;
	// using prefix_preview_type = typename prefix_search_type::preview_type;

private:
	using file_set = struct file_set_
	{
		string mdx_path;
		string trie_path;
		string coor_path;
		file_set_(const fs::path& dict_dir, const fs::path& index_dir, pt::ptree& item);
	};
	file_set files;
	dict_type dict;
	/// filename (including extension)
	string dict_name;

public:
	search_type search_agent;
	dict_meta d_mt{};
	dict_bundle(const fs::path& dict_dir, const fs::path& index_dir, pt::ptree& item);

	void query(query_type q, record_bundle& container) {
		container.d_mt = d_mt;
		container.record = id_to_data_flex(search_agent, dict, q);
	}
	template <class query_handler>
	void async_query(query_type q, record_bundle& container, query_handler&& handler);
};

template <unsigned idx_iter_entry_type>
class LEXICON_BUDDY_EXPORT dict_bundle<stardict::dict_container<idx_iter_entry_type>>
{
public:
	using dict_type = stardict::dict_container<idx_iter_entry_type>;
	// prefix search type
	// using search_type = typename dict_type::search_type;
	using query_type = size_t;
	// using record_type = stardict::dict_record_v;
	using record_bundle = stardict_record_bundle;
	using search_type = search_mmap<dict_type>;
	using search_result_type = typename search_type::result_type;
	// using prefix_preview_type = typename prefix_search_type::preview_type;

private:
	using file_set = struct file_set_
	{
		string ifo_path;
		string idx_path;
		string dictdz_path;
		string trie_path;
		string coor_path;
		file_set_(const fs::path& dict_dir, const fs::path& index_dir, const pt::ptree& item);
	};
	file_set files;
	dict_type dict;
	/// filename (including extension)
	string dict_name;

public:
	search_type search_agent;
	dict_meta d_mt{};
	dict_bundle(const fs::path& dict_dir, const fs::path& index_dir, pt::ptree& item);

	void query(query_type q, record_bundle& container) {
		container.d_mt = d_mt;
		container.record = id_to_data_flex(search_agent, dict, q);
	}
	template <class query_handler>
	void async_query(query_type search, record_bundle& container, query_handler&& handler);
};

/// stardict offset 32bit
using stardict_v32 = stardict::dict_container<0>;
using b_stardict_v32 = dict_bundle<stardict_v32>;
/// stardict offset 64bit
using stardict_v64 = stardict::dict_container<1>;
using b_stardict_v64 = dict_bundle<stardict_v64>;
/// mdx file stream
using mdx::mdx_data_std_stream;
using b_mdx_data_std_stream = dict_bundle<mdx_data_std_stream>;
/// mdx memory mapping
using mdx::mdx_data_mmap;
using b_mdx_data_mmap = dict_bundle<mdx_data_mmap>;

template <class dict_type>
struct LEXICON_BUDDY_EXPORT dict_map
{
	using key_t = string;
	using mapped_t = dict_type;
	using map_t = std::map<key_t, mapped_t>;
};

// TODO: query and assemble result
struct LEXICON_BUDDY_EXPORT query_cluster
{
	// TODO: the element type of preview should be word_str with n set of {dict_bundle ptr, id}
	using key_t = string;
	struct mapped_t
	{
		std::vector<std::pair<b_stardict_v32*, size_t>> stardict_v32_result;
		std::vector<std::pair<b_stardict_v64*, size_t>> stardict_v64_result;
		std::vector<std::pair<b_mdx_data_std_stream*, size_t>> mdx_data_std_stream_result;
		std::vector<std::pair<b_mdx_data_mmap*, size_t>> mdx_data_mmap_result;
	};
	using predict_type = std::map<key_t, mapped_t>;
};

std::ostream& operator<<(std::ostream& out, const query_cluster::predict_type& p);

// TODO: search context
struct LEXICON_BUDDY_EXPORT search_ctx
{
	lang_type lang_pair;
	uint8_t preview_row_n;
};

LEXICON_BUDDY_EXPORT bool cool_with(const search_ctx& ctx, dict_meta meta);
namespace http = boost::beast::http;
using online::provider_oxford;
using online::provider_webster;

/// @deprecated
class oxford_service
{

public:
	using api_key_t = provider_oxford::key_type;
	using query_type = provider_oxford::query_type;

private:
	template <query_type t>
	bool cool_with_src_lang(lang l);
	template <>
	bool cool_with_src_lang<query_type::Translation>(lang l);
	template <>
	bool cool_with_src_lang<query_type::Thesaurus>(lang l) {
		return l == lang::en;
	}
	template <>
	bool cool_with_src_lang<query_type::Sentences>(lang l) {
		return l == lang::en || l == lang::es;
	}

	template <query_type t>
	bool cool_with(const search_ctx& ctx);

public:
	provider_oxford req_builder{};
	struct oxford_setting
	{
		using state_t = std::bitset<4>;
		static constexpr char key_default[] = "";
		static constexpr char state_default[] = "1111";
		api_key_t key;
		state_t state{0x0f};
		static api_key_t str_2_key(const string& arg);
		static string key_2_str(const api_key_t& arg);
		static string state_2_str(state_t arg);
		static oxford_setting create(const pt::ptree& service);
		void save(pt::ptree& service);
	};

private:
	oxford_setting setting_;

public:
	static constexpr uint8_t B_TRANS = 0;
	static constexpr uint8_t B_ENTRY = 1;
	static constexpr uint8_t B_SENT = 2;
	static constexpr uint8_t B_THESA = 3;
	void func_activation(uint8_t type, bool on_off) { setting_.state[type] = on_off; }
	oxford_service(const api_key_t& key) : req_builder(key) { setting_.key = key; }
	api_key_t get_access_key() { return req_builder.get_key(); }
	const oxford_setting* get_setting() { return &setting_; }
	void set_state(std::bitset<4> arg) { setting_.state = arg; }
	void set_access_key(const api_key_t& arg) {
		req_builder.set_key(arg);
		setting_.key = arg;
	}
	void load_setting(const pt::ptree& service) {
		setting_ = oxford_setting::create(service);
		req_builder.set_key(setting_.key);
	}
	void save_setting(pt::ptree& service) { setting_.save(service); }
	pt::ptree setting_2_ptree() {
		pt::ptree p{};
		p.put("<xmlattr>.state", oxford_setting::state_2_str(setting_.state));
		p.put("key", oxford_setting::key_2_str(setting_.key));
		return p;
	}

	/// @brief query with source target language and word id
	template <class handler>
	void query_translation(const search_ctx& ctx, const string& s, online::control<handler>& https_service,
						   handler h) {
		if (ctx.lang_pair.first != ctx.lang_pair.second && this->cool_with<query_type::Translation>(ctx)) {
			auto&& req = req_builder._build_request<query_type::Translation>(s, ctx.lang_pair);
			https_service.task_enqueue(online::OXFORD_HOST, online::PORT_HTTPS, true, move(req), h);
		}
	}

	/// @brief query with source language and word id
	template <class handler>
	void query_entries(const search_ctx& ctx, const string& s, online::control<handler>& https_service,
					   handler h) {
		if (this->cool_with_src_lang<query_type::Dictionary_entries>(ctx.lang_pair.first)) {
			auto&& req = req_builder._build_request<query_type::Dictionary_entries>(s, ctx.lang_pair.first);
			https_service.task_enqueue(online::OXFORD_HOST, online::PORT_HTTPS, true, move(req), h);
		}
	}

	/// @brief query with source language and word id
	template <class handler>
	void query_sentence(const search_ctx& ctx, const string& s, online::control<handler>& https_service,
						handler h) {
		if (this->cool_with_src_lang<query_type::Sentences>(ctx.lang_pair.first)) {
			auto&& req = req_builder._build_request<query_type::Sentences>(s, ctx.lang_pair.first);
			https_service.task_enqueue(online::OXFORD_HOST, online::PORT_HTTPS, true, move(req), h);
		}
	}

	/// @brief query with source language and word id
	template <class handler>
	void query_thesaurus(const search_ctx& ctx, const string& s, online::control<handler>& https_service,
						 handler h) {
		if (this->cool_with_src_lang<query_type::Thesaurus>(ctx.lang_pair.first)) {
			auto&& req = req_builder._build_request<query_type::Thesaurus>(s, ctx.lang_pair.first);
			https_service.task_enqueue(online::OXFORD_HOST, online::PORT_HTTPS, true, move(req), h);
		}
	}
};

class webster_service
{
public:
	using api_key_t = provider_webster::key_type;
	using query_type = provider_webster::query_type;

	provider_webster req_builder{};
	struct webster_setting
	{
		// bit 0 for Collegiate_Dictionary
		// bit 1 for Collegiate_Thesaurus
		using state_t = std::bitset<2>;
		static constexpr char key_default[] = "";
		static constexpr char state_default[] = "11";
		api_key_t key;
		state_t state{0x03};
		static api_key_t str_2_key(const string& arg);
		static string key_2_str(const api_key_t& arg);
		static string state_2_str(state_t arg);

		static webster_setting create(const pt::ptree& service);
		void save(pt::ptree& service);
	};

private:
	webster_setting setting_;

public:
	static constexpr uint8_t Collegiate_Dictionary = 0;
	static constexpr uint8_t Collegiate_Thesaurus = 1;
	void dict_activation(uint8_t dictionary, bool enable) { setting_.state[dictionary] = enable; }

	bool dict_activated(uint8_t dictionary) { return setting_.state[dictionary]; }

	webster_service(const api_key_t& key) : req_builder(key) { setting_.key = key; }

	api_key_t get_access_key() { return req_builder.get_key(); }

	const webster_setting* get_setting() { return &setting_; }

	void set_state(std::bitset<2> arg) { setting_.state = arg; }

	void set_access_key(const api_key_t& arg) {
		req_builder.set_key(arg);
		setting_.key = arg;
	}

	void load_setting(const pt::ptree& service) {
		setting_ = webster_setting::create(service);
		req_builder.set_key(setting_.key);
	}

	void save_setting(pt::ptree& service) { setting_.save(service); }

	pt::ptree setting_2_ptree() {
		pt::ptree p{};
		p.put("<xmlattr>.state", webster_setting::state_2_str(setting_.state));
		p.put("key", webster_setting::key_2_str(setting_.key));
		return p;
	}

	template <class handler>
	void query_Collegiate_Dictionary(const string& s, online::control<handler>& https_service, handler h) {
		auto&& req = req_builder._build_request<online::merriam_webster_dict::Collegiate_Dictionary>(s);
		https_service.task_enqueue(online::MERRIAM_WEBSTER_HOST, online::PORT_HTTPS, true, move(req), h);
	}

	template <class handler>
	void query_Collegiate_Thesaurus(const string& s, online::control<handler>& https_service, handler h) {
		auto&& req = req_builder._build_request<online::merriam_webster_dict::Collegiate_Thesaurus>(s);
		https_service.task_enqueue(online::MERRIAM_WEBSTER_HOST, online::PORT_HTTPS, true, move(req), h);
	}
};

template <class offline_handler, class online_handler>
class LEXICON_BUDDY_EXPORT manager
{
	struct interface_value_type
	{
		dict_meta* meta_p{};
		interface_value_type() = default;
		interface_value_type(dict_meta* m) : meta_p{m} {}
	};
	// key: filename_with_ext; value: ptr to the dict meta
	using interface_type = std::map<string, interface_value_type>;
	fs::path dict_dir_;
	fs::path index_dir_;
	fs::path cfg_path_;
	index_agent cfg_;
	dict_map<b_stardict_v32>::map_t b_stardict_v32_map{};
	dict_map<b_stardict_v64>::map_t b_stardict_v64_map{};
	dict_map<b_mdx_data_std_stream>::map_t b_mdx_data_std_stream_map{};
	dict_map<b_mdx_data_mmap>::map_t b_mdx_data_mmap_map{};
	online::control<online_handler> http_engine{};
	// dictionary meta data interface
	interface_type item_interface{};
	void add_dict(const fs::path& dict_dir, const fs::path& index_dir, pt::ptree& item);
	void interface_remove(const string& filename_with_ext) { item_interface.erase(filename_with_ext); }
	void dict_bundle_remove(const string& filename_with_ext) {
		if (b_stardict_v32_map.erase(filename_with_ext)) return;
		if (b_stardict_v64_map.erase(filename_with_ext)) return;
		if (b_mdx_data_mmap_map.erase(filename_with_ext)) return;
		if (b_mdx_data_std_stream_map.erase(filename_with_ext)) return;
	}

	// do not use this after init_from_cfg();
	void cfg_update() { cfg_.cfg_item_discovery(); }
	/// @note init the online service node, not the actual service
	void cfg_init_online_service_node();
	void add_dict(pt::ptree& item) { return add_dict(cfg_.get_dict_dir(), cfg_.get_index_dir(), item); }

public:
	auto get_http_engine() { return &http_engine; }
	auto get_io() { return http_engine.get_io(); }
	// oxford_service oxford_{{}};
	webster_service webster_{{}};
#ifndef NDEBUG
	void debug() { cfg_.debug(); }
#endif

	manager(const char* dict_dir, const char* index_dir, const char* cfg_path, bool load_dict,
			bool init_http_client);

	/// @brief cause manager destructor to save cfg to disk
	void cfg_dirty() { cfg_.dirty(); }
	/// @brief load offline dictionary
	void load_offline_dict();
	/// @brief start http clients for specific hosts
	void start_online_service() {
		// init_oxford_http_client();
		init_webster_http_client();
	}

	void reload_offline_dict() {
		clear_off_res();
		cfg_.cfg_reset(dict_dir_.c_str(), index_dir_.c_str(), cfg_path_.c_str());
		cfg_update();
		load_offline_dict();
	}

	void restart_online_service(bool init_http_client = true) {
		cfg_init_online_service_node();
		http_engine.restart();
		if (init_http_client) {
			// init_oxford_http_client();
			init_webster_http_client();
		}
	}

	void clear_off_res() {
		b_stardict_v32_map.clear();
		b_stardict_v64_map.clear();
		b_mdx_data_std_stream_map.clear();
		b_mdx_data_mmap_map.clear();
	}

	//	void load_oxford_setting() {
	//		auto& c = cfg_.get_cfg();
	//		auto& service = c.get_child("service.Oxford_Dictionaries");
	//		oxford_.load_setting(service);
	//	}

	//	void save_oxford_setting() {
	//		auto& c = cfg_.get_cfg();
	//		auto& service = c.get_child("service.Oxford_Dictionaries");
	//		oxford_.save_setting(service);
	//	}

	void load_webster_setting() {
		auto c = cfg_.get_cfg();
		auto& service = c->get_child("service.MERRIAM-WEBSTER");
		webster_.load_setting(service);
	}

	void save_webster_setting() {
		auto c = cfg_.get_cfg();
		auto& service = c->get_child("service.MERRIAM-WEBSTER");
		webster_.save_setting(service);
	}

	void notify_http_thread() { http_engine.notify_thread(); }

	void query_webster_Collegiate_Dictionary(const query_cluster::key_t& k, const online_handler& h) {
		if (webster_.dict_activated(webster_.Collegiate_Dictionary))
			webster_.query_Collegiate_Dictionary<online_handler>(k, http_engine, h);
	}

	void query_webster_Collegiate_Thesaurus(const query_cluster::key_t& k, const online_handler& h) {
		if (webster_.dict_activated(webster_.Collegiate_Thesaurus))
			webster_.query_Collegiate_Thesaurus<online_handler>(k, http_engine, h);
	}

	/* void query_oxford_entries(const query_cluster::key_type& k, const online_handler& h,
							  const search_ctx& ctx) {
		oxford_.query_entries<online_handler>(ctx, k, http_engine, h);
	}
	void query_oxford_trans(const query_cluster::key_type& k, const online_handler& h,
							const search_ctx& ctx) {
		oxford_.query_translation<online_handler>(ctx, k, http_engine, h);
	}
	void query_oxford_sent(const query_cluster::key_type& k, const online_handler& h, const search_ctx& ctx) {
		oxford_.query_sentence<online_handler>(ctx, k, http_engine, h);
	}
	void query_oxford_thesaurus(const query_cluster::key_type& k, const online_handler& h,
								const search_ctx& ctx) {
		oxford_.query_thesaurus<online_handler>(ctx, k, http_engine, h);
	}*/

	/// @note call manager::cfg_dirty() if you modified dict meta through ptr to item_interface
	/// @warning do not delete/add element through ptr to item_interface cause this do not delete/add actual
	/// resource in the manager. use manager::remove_dict()/manager:import_dict() instead
	const interface_type* get_interface() { return &item_interface; }

	void init_http_client(const char* host, const char* port, bool preheat = true) {
		http_engine.init_http_client(host, port, preheat);
	}

	/* void init_oxford_http_client() {
		http_engine.init_http_client(online::OXFORD_HOST, online::PORT_HTTPS, false, true);
	}*/

	void init_webster_http_client() {
		http_engine.init_http_client(online::MERRIAM_WEBSTER_HOST, online::PORT_HTTPS, false, true);
	}

	void remove_dict(const string& filename_with_ext) {
		interface_remove(filename_with_ext);
		dict_bundle_remove(filename_with_ext);
		// remove node in map, property tree and local disk
		cfg_.remove_item_node(filename_with_ext);
	}

	cfg_ptree_type* import_dict(const fs::path& dict_path) {
		auto* item = cfg_.import(dict_path);
		if (item) add_dict(*item);
		return item;
	}
	/// @note force save cfg_ to disk. normally cfg_ will be save automatically during its destruction
	void cfg_explicit_save() { cfg_.cfg_save(); }
	template <uint8_t predict_row_limit>
	void predict(query_cluster::predict_type& p, const string& input, const search_ctx& ctx);
	template <uint8_t predict_row_limit>
	void predict(query_cluster::predict_type& p, const char* input, const search_ctx& ctx);
	// using marisa::trie::lookup to get word id
	// using word id for query
	void offline_query_str(const string& input, offline_handler& h, const search_ctx& ctx);
	void offline_query_str(const char* input, offline_handler& h, const search_ctx& ctx);
	// get word id from preview
	// using word id for query
	void offline_query(const query_cluster::predict_type& p, query_cluster::key_t& k, offline_handler& h);
	void offline_query(const query_cluster::predict_type::mapped_type& p, offline_handler& h);
};

} // namespace buddy
} // namespace lexicon

#endif // BUDDY_FORWARD_H
//
//  stardictContainer.h
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/6/21.
//  Copyright © 2018 Lohengrin. All rights reserved.
//
#ifndef STARDICTCONTAINER_H
#define STARDICTCONTAINER_H

#include <lexicon_buddy/lexicon_buddy_export.h>
#include <lexicon_buddy/utilities/utilities.hpp>
#include <lexicon_buddy/utilities/unzip_tool.h>
#include <exception>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <string_view>
#include <tuple>
#include <type_traits>
#include <vector>
#include <list>
#include <marisa.h>
namespace lexicon
{
namespace stardict
{
using std::string_view;
using std::string;
using std::tuple;
using std::unique_ptr;
using std::cout;
using std::cerr;
using std::endl;
using std::pair;

/* from https://github.com/huzheng001/stardict-3/blob/master/dict/doc/StarDictFileFormat#L66
{2}. The normal dictionary ".ifo" file's format.
The .ifo file format is described in ".ifo file format" section.

magic data: "StarDict's dict ifo file"
version: must be "2.4.2" or "3.0.0".
Since "3.0.0", StarDict accepts the "idxoffsetbits" option.

Available options:

bookname=      // required
wordcount=     // required
synwordcount=  // required if ".syn" file exists.
idxfilesize=   // required
idxoffsetbits= // New in 3.0.0
author=
email=
website=
description=	// You can use <br> for new line.
date=
sametypesequence= // very important.
dicttype=
*/

struct LEXICON_BUDDY_EXPORT ifo_str
{
	string version;
	string bookname;		   // required
	unsigned int wordcount;	// required
	unsigned int synwordcount; // required if ".syn" file exists.
	uint64_t idxfilesize;	  // required
	string idxoffsetbits;	  // New in 3.0.0
	string author;
	string email;
	string website;
	string description;		 // You can use <br> for new line.
	string date;			 // NOTE: ?use a actual date type
	string sametypesequence; // very important.
	string dicttype;
	ifo_str(const char*);
};

/* https://github.com/huzheng001/stardict-3/blob/master/dict/doc/StarDictFileFormat#L165
	Each entry in the word list contains three fields, one after the other:
		word_str;  // a utf-8 string terminated by '\0'.
		word_data_offset;  // word data's offset in .dict file
		word_data_size;  // word data's total size in .dict file
	If the version is "3.0.0" and "idxoffsetbits=64", word_data_offset will
	be 64-bits unsigned number in network byte order. Otherwise it will be
	32-bits.
*/

// VERSION : < stardict 3.0.0 or >= 3.0.0
// idxoffsetbits : "32"(idx_iter_entry_type=0),"64"(idx_iter_entry_type=1)
enum class res_type
{
	invalid,
	dict_ifo,
	storage_ifo,
	treedict_ifo
};

res_type ifo_content_validation(const char* data);

// TODO: impl
// res_type idx_content_validation(const char* idx_data, const char* ifo_data);

unsigned get_idx_iter_entry_type(string VERSION_str, string idxoffsetbits_str);

inline unsigned get_idx_iter_entry_type(const ifo_str& ifo) {
	return get_idx_iter_entry_type(ifo.version, ifo.idxoffsetbits);
}

// inline constexpr unsigned idx_iter_entry_type_64bit = 1;
static constexpr unsigned bit64 = 1;
static constexpr unsigned bit32 = 0;

template <unsigned idx_iter_entry_type>
class LEXICON_BUDDY_EXPORT idx_iter
{
public:
	using offset_type = std::conditional_t<idx_iter_entry_type == 1, uint64_t, uint32_t>;
	using size_type = uint32_t;
	using difference_type = std::ptrdiff_t;
	using value_type = struct idx_entry
	{
		string_view word_;
		offset_type word_data_offset_;
		uint32_t word_data_size_;
		idx_entry(string_view word, offset_type word_data_offset_, uint32_t word_data_size_)
			: word_{word}, word_data_offset_{word_data_offset_}, word_data_size_{word_data_size_} {}
		idx_entry() = default;
	};
	using pointer = const value_type*;
	using reference = const value_type&;
	using iterator_category = std::input_iterator_tag;

private:
	static constexpr unsigned word_data_offset_bytes{idx_iter_entry_type == 1 ? 8 : 4};
	template <unsigned idx_iter_entry_type_>
	friend bool build_trie_and_coordinate(idx_iter<idx_iter_entry_type_> iter, marisa::Trie& trie_,
										  char* coor_buffer);
	const char* dat_p;
	value_type value{};
	bool value_updated;
	void read_value();

public:
	idx_iter(const char* _dat_p) : dat_p{_dat_p} {}
	idx_iter(const idx_iter&) = default;
	idx_iter& operator=(const idx_iter&) = default;
	reference operator*() {
		if (!value_updated) {
			read_value();
			value_updated = true;
		}
		return value;
	}
	pointer operator->() { return &operator*(); }
	// prefix increment;
	idx_iter& operator++() {
		operator*();
		dat_p += (value.word_.length() + 1 + word_data_offset_bytes + 4);
		// outdate the value!
		value_updated = false;
		return *this;
	}
	bool operator==(const idx_iter& _iter) const { return dat_p == _iter.dat_p; }
	bool operator!=(const idx_iter& _iter) const { return !(dat_p == _iter.dat_p); }
};

template <unsigned idx_iter_entry_type>
idx_iter<idx_iter_entry_type> idx_iter_begin(util::mmap_w idx_map) {
	return {idx_map.data()};
}
template <unsigned idx_iter_entry_type>
idx_iter<idx_iter_entry_type> idx_iter_end(util::mmap_w idx_map) {
	return {idx_map.data() + idx_map.get_filesize()};
}

// support duplicated key
// buffer_size_used will be set by build_trie_and_coordinate_flex() to indicates how many bytes actually
// used
template <unsigned idx_iter_entry_type>
LEXICON_BUDDY_EXPORT bool
build_trie_and_coordinate_flex(idx_iter<idx_iter_entry_type> iter, idx_iter<idx_iter_entry_type> iter_end,
							   marisa::Trie& trie_, char* coor_buffer, uint64_t* buffer_size_used) {
	bool complete{};
	marisa::Keyset key_set;
	// data type of original stardict idx
	using word_data_offset_type = typename idx_iter<idx_iter_entry_type>::offset_type;
	using word_data_size_type = typename idx_iter<idx_iter_entry_type>::size_type;
	// data types for first part of coordinate file
	using coor_offset_type = word_data_offset_type;
	using coor_num_type = uint16_t;

	// using data_type = typename idx_iter<idx_iter_entry_type>::value_type;
	struct coor_type
	{
		word_data_offset_type offset;
		word_data_size_type length;
		coor_type(word_data_offset_type offset_, word_data_size_type length_)
			: offset{offset_}, length{length_} {}
		coor_type() = default;
		coor_type(const coor_type&) = default;
	};
	using id_type = decltype(std::declval<marisa::Key>().id());
	using map_t1 = std::map<string, std::list<coor_type>>;
	using map_t2 = std::map<id_type, std::list<coor_type>>;
	map_t1 m1{}; // to store data from stardict idx
	map_t2 m2{}; // to store sorted data with marisa trie key's id
	for (; iter != iter_end; ++iter) {
		auto& v = *iter;
		key_set.push_back(v.word_.data(), v.word_.size());
		coor_type coordinate{};
		coordinate.offset = v.word_data_offset_;
		coordinate.length = v.word_data_size_;
		auto find = m1.find({v.word_.data(), v.word_.size()});
		if (find != m1.end()) {
			find->second.emplace_back(coordinate);
			COUT_DEBUG("duplicated key: ");
			COUT_DEBUG(v.word_);
			COUT_DEBUG(endl);
		} else {
			auto ret = m1.try_emplace({v.word_.data(), v.word_.size()} /*, std::list<coor_type>{}*/)
						   .first->second.emplace_back(coordinate);
		}
	}
	complete = true;
	// lambda get string and save it to key_set
	/*
	auto dat_get = [&]() {
		auto str_ptr = iter.dat_p;
		coor_type coordinate{};
		key_set.push_back(str_ptr);
		while (*(iter.dat_p)) {
			++(iter.dat_p);
		}
		++(iter.dat_p);
		if constexpr (idx_iter_entry_type) {
			coordinate.offset = util::endianSwap_64bit(iter.dat_p);
		} else {
			coordinate.offset = util::endianSwap_32bit(iter.dat_p);
		}
		iter.dat_p += sizeof(word_data_offset_type);
		coordinate.length = util::endianSwap_32bit(iter.dat_p);
		iter.dat_p += sizeof(word_data_size_type);
		auto find = m1.find(str_ptr);
		if (find != m1.end()) {
			find->second.emplace_back(coordinate);
#ifndef NDEBUG
			cout << "duplicated key: " << str_ptr << endl;
#endif
		} else {
			auto ret = m1.try_emplace(str_ptr).first->second.emplace_back(coordinate);
		}
		if (iter.dat_p >= iter.dat_p_end) end = true;
	};
	while (!end) {
		dat_get();
	}
	*/
	// build trie;
	trie_.build(key_set);
	COUT_DEBUG("trie built, trie num_key: ");
	COUT_DEBUG(trie_.num_keys());
	COUT_DEBUG(", key_set num_key: ");
	COUT_DEBUG(key_set.num_keys());
	COUT_DEBUG(endl);
	marisa::Agent agent0;
	for (auto& i : m1) {
		agent0.set_query(i.first.c_str());
		if (trie_.lookup(agent0)) {
			auto id = agent0.key().id();
			m2.try_emplace(id, i.second);
		} else {
			cerr << "error. can't find key: " << i.first.c_str() << endl;
			return false;
		}
	}
	// save data to coordinate buffer
	auto word_count = m1.size();
	auto word_data_offset_ptr = coor_buffer + (sizeof(coor_offset_type) + sizeof(coor_num_type)) * word_count;
	auto p = coor_buffer;
	for (auto& i : m2) {
		auto& list = i.second;
		coor_offset_type offset = word_data_offset_ptr - coor_buffer;
		coor_num_type num = list.size();
		std::copy((char*)&offset, (char*)&offset + sizeof(coor_offset_type), p);
		p += sizeof(coor_offset_type);
		std::copy((char*)&num, (char*)&num + sizeof(coor_num_type), p);
		p += sizeof(coor_num_type);
		for (auto& j : list) {
			std::copy((char*)&(j.offset), (char*)&(j.offset) + sizeof(j.offset), word_data_offset_ptr);
			word_data_offset_ptr += sizeof(j.offset);
			std::copy((char*)&(j.length), (char*)&(j.length) + sizeof(j.length), word_data_offset_ptr);
			word_data_offset_ptr += sizeof(j.length);
		}
	}
	*buffer_size_used = word_data_offset_ptr - coor_buffer;
	return complete;
}
// the light-weight idx_agent works just like a set of functions which points to a external resource
// (the idx_agent does not managing the life time of the resource)
template <unsigned idx_iter_entry_type>
class LEXICON_BUDDY_EXPORT idx_agent
{
	using iterator = idx_iter<idx_iter_entry_type>;
	util::mmap_w* resource_ptr;
	const char* const b_ptr;
	const char* const e_ptr;

public:
	idx_agent(util::mmap_w* _res_ptr)
		: resource_ptr{_res_ptr}, b_ptr{_res_ptr->data()}, e_ptr{_res_ptr->data() + _res_ptr->file_length} {}
	iterator begin() { return iterator{b_ptr}; }
	iterator end() { return iterator{e_ptr}; }
	std::size_t count() {
		std::size_t c = 0;
		auto end_ = end();
		for (auto begin_ = begin(); begin_ != end_; ++begin_)
			++c;
		return c;
	}
#ifndef NDEBUG
	void convert_2_txt(std::ostream& ostr) {
		auto iter = begin();
		auto iter_end = end();
		unsigned c{};
		if (iter == iter_end) throw std::runtime_error{"An error occurred while display .idx"};
		do {
			auto v = *iter;
			ostr << v.word_str << ' ';
			ostr << std::to_string(v.word_data_offset) << ' ';
			ostr << std::to_string(v.word_data_offset) << '\n';
			++c;
			++iter;
		} while (iter != iter_end);
		ostr << "============\n" << c << " records";
	}
#endif
};

/// @brief @class dict_record copy record data from .dict and build an index (type and begin+end pointers)
/// for record's internal structure
struct dict_record
{
	struct element
	{
		char type_; // type identifier
		const char* begin_;
		std::size_t size_;
		element(char type, const char* begin, std::size_t size) : type_{type}, begin_{begin}, size_{size} {}
		element& operator=(const element&) = default;
	};
	std::vector<element> index_{};
	string sametypesequence{}; // a copy from idx
	string str;				   // decompressed data
							   // allocate space only

	// copy record from decompressed dict data
	dict_record(string _sametypesequence, const char* src, unsigned long long _dat_len,
				bool build_index_now = true)
		: sametypesequence{_sametypesequence}, str{src, _dat_len} {
		if (build_index_now) build_index();
	}
	// copy record from decompressed dict file
	dict_record(string _sametypesequence, std::istream& src, std::streampos pos, unsigned long long _dat_len,
				bool build_index_now = true)
		: sametypesequence{_sametypesequence} {
		src.seekg(pos);
		char buf[4096];
		// pipeline
		while (_dat_len > 4096) {
			src.read(buf, 4096);
			str.append(buf, 4096);
			_dat_len -= 4096;
		}
		src.read(buf, _dat_len);
		str.append(buf, _dat_len);
		if (build_index_now) build_index();
	}
	void build_index();
	dict_record() = default;
	dict_record(const dict_record&) = default;
	dict_record& operator=(const dict_record&) = default;
	dict_record(dict_record&& dict_record) = default;
	dict_record& operator=(dict_record&& dict_record) = default;
};

using dict_record_v = std::vector<dict_record>;
using std_stream = std::ifstream;
using memory = util::mmap_w;
std::ostream& operator<<(std::ostream& out, const dict_record& v);
std::ostream& operator<<(std::ostream& out, const dict_record_v& v);

// template <unsigned idx_iter_entry_type>
// void idx_agent<idx_iter_entry_type>::display_in_str(std::ostream &ostr){};
template <unsigned idx_iter_entry_type, bool mmap = true>
class LEXICON_BUDDY_EXPORT dict_container
{
public:
	template <unsigned idx_iter_entry_type_, bool mmap_>
	friend class WCR_agent;
	using iterator = idx_iter<idx_iter_entry_type>;

private:
	using _idx_agent = idx_agent<idx_iter_entry_type>;
	ifo_str ifo;
	util::mmap_w idx_res; // always mmap .idx
	_idx_agent idx_ag;
	using T = std::conditional_t<mmap, memory, std_stream>;
	T dict;
	unzip::unzip_agent<unzip::comp_type::dictzip> uz;
	static constexpr unsigned DICT_FORMAT_BIT = 0;
	//[0] dict format, default .dz , 1 means decompressed dict
	std::bitset<1> state;
	dict_container(const dict_container&) = delete;
	dict_container& operator=(const dict_container&) = delete;
	dict_container(dict_container&&) = default;
	dict_container& operator=(dict_container&&) = default;
	static bool is_dz(const char* path) {
		while (*path++) {
		}
		if (*(path - 4) == '.' && *(path - 3) == 'd' && *(path - 2) == 'z') {
			return true;
		} else {
			return false;
		}
	}

public:
	using offset_type = typename idx_iter<idx_iter_entry_type>::offset_type;
	using search_type = offset_type;
	using record_type = dict_record;

	dict_container(const char* ifo_path, const char* idx_path, const char* dict_path)
		: ifo{ifo_path}, idx_res{idx_path}, idx_ag{&idx_res}, dict{dict_path} {
		state[0] = !is_dz(dict_path);
	}
	// decompress ".dict.dz" file blocks (fake random file access)
	dict_record dictzip_random_decom(offset_type offset, uint32_t data_size) {
		unique_ptr<char[]> ptr(new char[data_size]);
		uz.unzip_random(dict, data_size, offset, ptr.get());
		return dict_record(ifo.sametypesequence, ptr.get(), data_size);
	}
	iterator idx_begin() { return idx_ag.begin(); }
	iterator idx_end() { return idx_ag.end(); }
};

///@note WCR stand for Word Coordinate Record
template <unsigned idx_iter_entry_type, bool mmap = true>
class LEXICON_BUDDY_EXPORT WCR_agent
{
public:
	using difference_type = std::ptrdiff_t;
	using word_coor_type = typename idx_iter<idx_iter_entry_type>::value_type;
	using value_type = tuple<word_coor_type, dict_record>;
	using reference = const value_type&;
	using pointer = const value_type*;
	using iterator_category = std::input_iterator_tag;

	WCR_agent(dict_container<idx_iter_entry_type, mmap>* dict_c, bool decomp = true)
		: dict_c_{dict_c}, begin{dict_c_->idx_begin()}, end{dict_c_->idx_end()} {
		if (decomp) decompress();
	}
	WCR_agent(WCR_agent&&) = default;
	WCR_agent& operator=(WCR_agent&&) = default;
	WCR_agent(const WCR_agent&) = delete;
	WCR_agent& operator=(const WCR_agent&) = delete;
	bool reach_end() { return begin == end; }
	/// @warning always check reach_end() before invoking this method
	WCR_agent& operator++() {
		++begin;
		v_updated = false;
		return *this;
	}
	reference operator*() {
		if (!v_updated) {
			read_value();
			v_updated = true;
		}
		return v_;
	}
	bool operator==(const WCR_agent& arg) { return begin == arg.begin; }

private:
	void read_value() {
		if (decom_buf_size_)
			read_value_plain();
		else
			read_value_decom();
	}
	void read_value_decom() {
		v_ = {*begin, dict_c_->dictzip_random_decom(begin->word_data_offset_, begin->word_data_size_)};
	}
	void read_value_plain() {
		v_ = {*begin, dict_record(dict_c_->ifo.sametypesequence, decom_buf_.get() + begin->word_data_offset_,
								  begin->word_data_size_)};
	}
	void decompress() {
		if constexpr (mmap) {
			auto [buf, buf_size] = unzip::unzip_agent<unzip::comp_type::dictzip>::unzip_all(
				dict_c_->dict.data(), dict_c_->dict.get_filesize());
			if (buf_size) {
				decom_buf_ = std::move(buf);
				decom_buf_size_ = buf_size;
			}
		} else {
			auto [buf, buf_size] = unzip::unzip_agent<unzip::comp_type::dictzip>::unzip_all(dict_c_->dict);
			if (buf_size) {
				decom_buf_ = std::move(buf);
				decom_buf_size_ = buf_size;
			}
		}
	}
	// using chunk_range_type = pair<std::size_t, std::size_t>;
	// chunk_range_type get_chunk_range() {}
	// void decom_chunk(chunk_range_type chunk_) {}
	dict_container<idx_iter_entry_type, mmap>* dict_c_;
	typename dict_container<idx_iter_entry_type, mmap>::iterator begin;
	typename dict_container<idx_iter_entry_type, mmap>::iterator end;
	unique_ptr<char[]> decom_buf_;
	std::size_t decom_buf_size_{};
	// chunk_range_type chunk_range;
	value_type v_;
	bool v_updated = false;
};
} // namespace stardict
} // namespace lexicon
#endif // STARDICTCONTAINER_H
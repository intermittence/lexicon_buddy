//
//  online_dict.h
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/8/11.
//  Copyright © 2018 Lohengrin. All rights reserved.
//
#ifndef ONLINE_DICT_H
#define ONLINE_DICT_H

#include <lexicon_buddy/lexicon_buddy_export.h>
#include <lexicon_buddy/lexicon/lexicon_types.h>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <exception>
#include <iostream>
#include <sstream>
#include <string>
namespace lexicon
{
namespace online
{
namespace http = boost::beast::http;
constexpr char OXFORD_HOST[] = "od-api.oxforddictionaries.com";
constexpr char OXFORD_HOST_AND_PORT[] = "od-api.oxforddictionaries.com:443";
constexpr char MERRIAM_WEBSTER_HOST[] = "www.dictionaryapi.com";
constexpr char MERRIAM_WEBSTER_PATH_PREFIX[] = "/api/v3/references/";

template <dict_api dict>
const char* lang_enum_to_char(lang lang_enum);

template <>
const char* lang_enum_to_char<dict_api::Oxford>(lang lang_enum);

constexpr const char* oxford_query_type_to_str(oxford_query_type t) {
	switch (t) {
		case lexicon::online::oxford_query_type::Dictionary_entries:
			return "/api/v2/entries/";
		case lexicon::online::oxford_query_type::Thesaurus:
			return "/api/v2/thesaurus/";
		case lexicon::online::oxford_query_type::Translation:
			return "/api/v2/translations/";
		case lexicon::online::oxford_query_type::Sentences:
			return "/api/v2/sentences/";
		default:
			throw std::runtime_error{"not_implemented"};
	}
}

using std::string;

template <dict_api dict>
/// @typedef key_type
/// @typedef query_type
/// @typedef dictionary_type
/// @typedef trans_pair: source and target language
struct provider_traits;

template <>
struct provider_traits<dict_api::Oxford>
{
	struct key_type
	{
		string ID;
		string Key;
	};
	using query_type = oxford_query_type;
	using request_value_type = http::request<http::empty_body>;
};

template <>
struct provider_traits<dict_api::Merriam_Webster>
{
	struct key_type
	{
		string Collegiate_key;
		string Thesaurus_key;
	};
	using query_type = merriam_webster_dict;
	using request_value_type = http::request<http::empty_body>;
};

template <dict_api dict, class p_traits>
class provider
{
public:
	using key_type = typename p_traits::key_type;
	using query_type = typename p_traits::query_type;
	using request_value_type = typename p_traits::request_value_type;
	// most of the literals (base url, etc) will be hard coded into the build_request routine
	provider(const key_type& k) : key{k} {}
	provider() {}
	template <query_type type>
	request_value_type _build_request(const string& query_str, const std::pair<lang, lang>& srcl_dstl);
	template <query_type type>
	request_value_type _build_request(const string& query_str, lang source_lang);
	void set_key(const key_type& arg) { key = arg; }
	void set_key(key_type&& arg) { key = std::move(arg); }
	key_type get_key() { return key; }

private:
	key_type key{};
	void append_key(request_value_type& req);
};

template <>
class provider<dict_api::Merriam_Webster, provider_traits<dict_api::Merriam_Webster>>
{
public:
	using p_traits = provider_traits<dict_api::Merriam_Webster>;
	using key_type = typename p_traits::key_type;
	using query_type = typename p_traits::query_type;
	using request_value_type = typename p_traits::request_value_type;
	provider(const key_type& k) : key{k} {}
	provider() {}
	template <query_type dict>
	request_value_type _build_request(const string& query_str) {
		//	request url for MERRIAM-WEBSTER'S COLLEGIATE® DICTIONARY WITH AUDIO
		//	https://www.dictionaryapi.com/api/v3/references/collegiate/json/voluminous?key=your-DICTIONARY-api-key
		//	request url for MERRIAM-WEBSTER'S COLLEGIATE® THESAURUS
		//	https://www.dictionaryapi.com/api/v3/references/thesaurus/json/umpire?key=your-THESAURUS-api-key
		string Request_str{MERRIAM_WEBSTER_PATH_PREFIX};
		if constexpr (dict == merriam_webster_dict::Collegiate_Dictionary) {
			Request_str += "collegiate";
		} else {
			Request_str += "thesaurus";
		}
		Request_str += "/json/";
		Request_str += query_str + "?key=";
		if constexpr (dict == merriam_webster_dict::Collegiate_Dictionary) {
			Request_str += key.Collegiate_key;
		} else {
			Request_str += key.Thesaurus_key;
		}
		http::request<http::empty_body> req{http::verb::get, Request_str, 11};
		req.set(http::field::host, MERRIAM_WEBSTER_HOST);
		req.set(http::field::accept, "application/json");
		return req;
	}

	void set_key(const key_type& arg) { key = arg; }
	void set_key(key_type&& arg) { key = std::move(arg); }
	key_type get_key() { return key; }

private:
	key_type key{};
	void append_key(request_value_type& req);
};

using provider_oxford = provider<dict_api::Oxford, provider_traits<dict_api::Oxford>>;
using provider_webster = provider<dict_api::Merriam_Webster, provider_traits<dict_api::Merriam_Webster>>;

template <>
inline void provider_oxford::append_key(request_value_type& req) {
	req.set("app_id", key.ID);
	req.set("app_key", key.Key);
}
template <>
template <provider_oxford::query_type type>
inline provider_oxford::request_value_type provider_oxford::_build_request(const string& query_str,
																		   lang source_lang) {
	static_assert(
		type == oxford_query_type::Dictionary_entries || type == oxford_query_type::Thesaurus ||
			type == oxford_query_type::Sentences,
		"only word, synonyms_antonyms and sentences query are implemented for this method overload");
	//	 Request URL:
	//	 https://od-api.oxforddictionaries.com:443/api/v2/entries/en/word
	//	 https://od-api.oxforddictionaries.com:443/api/v2/entries/en/word/synonyms;antonyms
	//	 https://od-api.oxforddictionaries.com:443/api/v2/entries/en/word/sentences
	//	 Request Headers:
	//	 {
	//	  "Accept": "application/json",
	//	  "app_id": "9f9c20af",
	//	  "app_key": "f1d6fd7c5353328395c916f1815d5ffa"
	//	 }
	string Request_str{};
	Request_str += string{oxford_query_type_to_str(type)} + lang_enum_to_char<dict_api::Oxford>(source_lang) +
				   '/' + query_str;
	http::request<http::empty_body> req{http::verb::get, Request_str, 11};
	req.set(http::field::host, OXFORD_HOST_AND_PORT);
	req.set(http::field::accept, "application/json");
	append_key(req);
	return req;
}

template <>
template <>
provider_oxford::request_value_type provider_oxford::_build_request<provider_oxford::query_type::Translation>(
	const string& query_str, const std::pair<lang, lang>& srcl_dstl);

/// @brief tranform Merriam Webster json to html format with additional style
// class Merriam_Webster_json_style0
//{
//    // todo using hash map for lookup
//    // todo json object style
//    // todo json object value style
//    // todo json object string style
//    void to_html(const string& src, string& dst);
//    // todo
//    void get_handler();
//};
} // namespace online
} // namespace lexicon
#endif // ONLINE_DICT_H

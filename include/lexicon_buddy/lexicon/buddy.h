//
//  buddy.h
//  lexicon_buddy
//
//  Created by Lohengrin on 1/18/2019 09:49:34.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef buddy_H
#define buddy_H

#include <lexicon_buddy/lexicon/buddy_forward.h>
#include <type_traits>
#include <lexicon_buddy/utilities/lang_platform.hpp>

namespace lexicon
{
namespace buddy
{

#ifndef lan_case_and_return
#define lan_case_and_return(e)                                                                               \
	case lang::e:                                                                                            \
		return true
#endif

#ifndef lan_switch_case1
#define lan_switch_case1(e)                                                                                  \
	switch (pair.second) {                                                                                   \
		lan_case_and_return(e);                                                                              \
		default:                                                                                             \
			return false;                                                                                    \
	}                                                                                                        \
	break
#endif

#ifndef lan_switch_case2
#define lan_switch_case2(e1, e2)                                                                             \
	switch (pair.second) {                                                                                   \
		lan_case_and_return(e1);                                                                             \
		lan_case_and_return(e2);                                                                             \
		default:                                                                                             \
			return false;                                                                                    \
	}                                                                                                        \
	break
#endif

template <class T>
dict_bundle<mdx::mdx_data<T>>::file_set_::file_set_(const fs::path& dict_dir, const fs::path& index_dir,
													pt::ptree& item) {
	auto temp1 = dict_dir;
	auto temp2 = index_dir;
	auto temp3 = index_dir;
	mdx_path = GET_U8STRING(
		(temp1 / (item.get<string>("<xmlattr>.basename") + item.get<string>("<xmlattr>.res_bundle_type"))));
	trie_path = GET_U8STRING((temp2 / item.get<string>("index.search")));
	coor_path = GET_U8STRING((temp3 / item.get<string>("index.coordinate")));
}

template <class T>
dict_bundle<mdx::mdx_data<T>>::dict_bundle(const fs::path& dict_dir, const fs::path& index_dir,
										   pt::ptree& item)
	: files(dict_dir, index_dir, item), dict(files.mdx_path),
	  search_agent(files.trie_path.c_str(), files.coor_path.c_str()), d_mt(&item) {
	auto basename = item.get<string>("<xmlattr>.basename");
	dict_name = basename + item.get<string>("<xmlattr>.res_bundle_type");
	try {
		auto alias = item.get<string>("<xmlattr>.alias");
		d_mt.set_alias(alias);
	} catch (...) {
		d_mt.set_alias(basename);
	}
}
template <unsigned idx_iter_entry_type>
dict_bundle<stardict::dict_container<idx_iter_entry_type>>::dict_bundle(const fs::path& dict_dir,
																		const fs::path& index_dir,
																		pt::ptree& item)
	: files(dict_dir, index_dir, item),
	  dict(files.ifo_path.c_str(), files.idx_path.c_str(), files.dictdz_path.c_str()),
	  search_agent(files.trie_path.c_str(), files.coor_path.c_str()), d_mt(&item) {
	auto basename = item.get<string>("<xmlattr>.basename");
	dict_name = basename + item.get<string>("<xmlattr>.res_bundle_type");
	try {
		auto alias = item.get<string>("<xmlattr>.alias");
		d_mt.set_alias(alias);
	} catch (...) {
		d_mt.set_alias(basename);
	}
}

template <unsigned idx_iter_entry_type>
dict_bundle<stardict::dict_container<idx_iter_entry_type>>::file_set_::file_set_(const fs::path& dict_dir,
																				 const fs::path& index_dir,
																				 const pt::ptree& item) {
	auto basename = item.get<string>("<xmlattr>.basename");
	auto basepath = dict_dir / basename;
	auto temp1 = basepath;
	auto temp2 = basepath;
	auto temp3 = basepath;
	dictdz_path = GET_U8STRING(temp1.replace_extension(".dict.dz"));
	idx_path = GET_U8STRING(temp2.replace_extension(".idx"));
	ifo_path = GET_U8STRING(temp3.replace_extension(".ifo"));
	trie_path = GET_U8STRING((index_dir / item.get<string>("index.search")));
	coor_path = GET_U8STRING((index_dir / item.get<string>("index.coordinate")));
}

template <class offline_handler, class online_handler>
manager<offline_handler, online_handler>::manager(const char* dict_dir, const char* index_dir,
												  const char* cfg_path, bool load_dict, bool init_http_client)
	: dict_dir_(dict_dir), index_dir_(index_dir), cfg_path_(cfg_path), cfg_(dict_dir, index_dir, cfg_path) {
	cfg_update();
	if (load_dict) load_offline_dict();
	http_engine.init_worker_thread();
	if (init_http_client) {
		// init_oxford_http_client();
		init_webster_http_client();
	}
	cfg_init_online_service_node();
}

template <class offline_handler, class online_handler>
void manager<offline_handler, online_handler>::cfg_init_online_service_node() {
	auto cp = cfg_.get_cfg();
	auto& c = *cp;
	pt::ptree* s_p{};
	// ptree service element existence check
	auto iter = c.find("service");
	if (iter == c.not_found()) {
		// create service element
		s_p = &(c.put_child("service", pt::ptree{}));
		// create Oxford_Dictionaries element from oxford_ default setting
		// s_p->put_child("Oxford_Dictionaries", oxford_.setting_2_ptree());
		s_p->put_child("MERRIAM-WEBSTER", webster_.setting_2_ptree());
	} else {
		s_p = &(iter->second);
		// ptree oxford element existence check
		//        auto oxford_iter = s_p->find("Oxford_Dictionaries");
		//        if (oxford_iter == s_p->not_found()) {
		//            // create Oxford_Dictionaries element from oxford_ default setting
		//            s_p->put_child("Oxford_Dictionaries", oxford_.setting_2_ptree());
		//        } else {
		//            // oxford_ loading setting from Oxford_Dictionaries element
		//            oxford_.load_setting(oxford_iter->second);
		//        }
		// ptree webster element existence check
		auto webster_iter = s_p->find("MERRIAM-WEBSTER");
		if (webster_iter == s_p->not_found()) {
			// create MERRIAM-WEBSTER element from webster_ default setting
			s_p->put_child("MERRIAM-WEBSTER", webster_.setting_2_ptree());
		} else {
			// webster_ loading setting from MERRIAM-WEBSTER element
			webster_.load_setting(webster_iter->second);
		}
	}
}

template <class offline_handler, class online_handler>
void manager<offline_handler, online_handler>::load_offline_dict() {
	auto dict_dir = cfg_.get_dict_dir();
	auto index_dir = cfg_.get_index_dir();
	auto instance = cfg_.get_cfg_instance_node();
	for (auto& i : *instance) {
		if (i.first == "item") add_dict(dict_dir, index_dir, i.second);
	}
}

#ifndef offline_query_str_single
#ifndef NDEBUG
#define offline_query_str_single(map, c_str)                                                                 \
	for (auto& i : map) {                                                                                    \
		cout << "check dictionary: " << i.first << endl;                                                     \
		auto& dict_b = i.second;                                                                             \
		if (dict_b.d_mt.get_meta().enabled && cool_with(ctx, dict_b.d_mt)) {                                 \
			cout << "query dictionary: " << i.first << endl;                                                 \
			marisa::Agent agent;                                                                             \
			agent.set_query(c_str);                                                                          \
			if (dict_b.search_agent.trie.lookup(agent)) {                                                    \
				typename decltype(map)::mapped_type::record_bundle record_{};                                \
				dict_b.query(agent.key().id(), record_);                                                     \
				h.operator()(record_);                                                                       \
			}                                                                                                \
		}                                                                                                    \
	}
#else
#define offline_query_str_single(map, c_str)                                                                 \
	for (auto& i : map) {                                                                                    \
		auto& dict_b = i.second;                                                                             \
		if (dict_b.d_mt.get_meta().enabled && cool_with(ctx, dict_b.d_mt)) {                                 \
			marisa::Agent agent;                                                                             \
			agent.set_query(c_str);                                                                          \
			if (dict_b.search_agent.trie.lookup(agent)) {                                                    \
				typename decltype(map)::mapped_type::record_bundle record_{};                                \
				dict_b.query(agent.key().id(), record_);                                                     \
				h.operator()(record_);                                                                       \
			}                                                                                                \
		}                                                                                                    \
	}
#endif
#endif

template <class offline_handler, class online_handler>
LEXICON_BUDDY_EXPORT void manager<offline_handler, online_handler>::offline_query_str(const char* input,
																					  offline_handler& h,
																					  const search_ctx& ctx) {
	offline_query_str_single(b_stardict_v32_map, input);
	offline_query_str_single(b_stardict_v64_map, input);
	offline_query_str_single(b_mdx_data_std_stream_map, input);
	offline_query_str_single(b_mdx_data_mmap_map, input);
}

template <class offline_handler, class online_handler>
LEXICON_BUDDY_EXPORT void manager<offline_handler, online_handler>::offline_query_str(const string& input,
																					  offline_handler& h,
																					  const search_ctx& ctx) {
	auto key = input.c_str();
	return offline_query_str(key, h, ctx);
}

/// the interface for GUI
//#ifndef offline_query_single
//#define offline_query_single(v) \
//	for (auto& i : iter_->second.v) {                                                                        \
//		std::remove_pointer<decltype(iter_->second.v)::value_type::first_type>::type::record_bundle          \
//			record_{};                                                                                       \
//		i.first->query(i.second, record_);                                                                   \
//		h.operator()(record_);                                                                               \
//	}
//#endif

template <class offline_handler, class online_handler>
LEXICON_BUDDY_EXPORT void
manager<offline_handler, online_handler>::offline_query(const query_cluster::predict_type& p,
														query_cluster::key_t& k, offline_handler& h) {
	auto iter_ = p.find(k);
	if (iter_ != p.end()) offline_query(iter_->second, h);
}

#ifndef offline_query_single_v
#define offline_query_single_v(v)                                                                            \
	for (auto& i : p.v) {                                                                                    \
		std::remove_pointer<decltype(p.v)::value_type::first_type>::type::record_bundle record_{};           \
		i.first->query(i.second, record_);                                                                   \
		h.operator()(record_);                                                                               \
	}
#endif

template <class offline_handler, class online_handler>
LEXICON_BUDDY_EXPORT void
manager<offline_handler, online_handler>::offline_query(const query_cluster::predict_type::mapped_type& p,
														offline_handler& h) {
	offline_query_single_v(stardict_v32_result);
	offline_query_single_v(stardict_v64_result);
	offline_query_single_v(mdx_data_std_stream_result);
	offline_query_single_v(mdx_data_mmap_result);
}

template <class offline_handler, class online_handler>
LEXICON_BUDDY_EXPORT void manager<offline_handler, online_handler>::add_dict(const fs::path& dict_dir,
																			 const fs::path& index_dir,
																			 pt::ptree& item) {
	auto dict_name = item.get<string>("<xmlattr>.basename") + item.get<string>("<xmlattr>.res_bundle_type");
	auto t = item.get<res_type>("<xmlattr>.res_bundle_type");
	// TODO: memory mapping or file stream?
	switch (t) {
		case res_type::mdx: {
			auto ret = b_mdx_data_mmap_map.try_emplace(dict_name, dict_dir, index_dir, item);
			if (ret.second) item_interface.try_emplace(dict_name, &(ret.first->second.d_mt));
		} break;
		case res_type::startdict_dict_dz: {
			auto offset_bit = item.get<string>("<xmlattr>.extra");
			if (offset_bit == "32") {
				auto ret = b_stardict_v32_map.try_emplace(dict_name, dict_dir, index_dir, item);
				if (ret.second) item_interface.try_emplace(dict_name, &(ret.first->second.d_mt));
			} else if (offset_bit == "64") {
				auto ret = b_stardict_v64_map.try_emplace(dict_name, dict_dir, index_dir, item);
				if (ret.second) item_interface.try_emplace(dict_name, &(ret.first->second.d_mt));
			} else {
				throw std::runtime_error{"invalid stardict offset data size\n"};
			}
		} break;
		default:
			break;
	}
}

// TODO:generate predict for search
#ifndef predict_single
#define predict_single(m, r, c_str)                                                                          \
	for (auto& i : m) {                                                                                      \
		auto& dict_b = i.second;                                                                             \
		if (dict_b.d_mt.get_meta().enabled && cool_with(ctx, dict_b.d_mt)) {                                 \
			marisa::Agent agent;                                                                             \
			agent.set_query(c_str);                                                                          \
			uint8_t row_size{};                                                                              \
			while (dict_b.search_agent.trie.predictive_search(agent) && (++row_size < predict_row_limit)) {  \
				auto iter_ = p.find({agent.key().ptr(), agent.key().length()});                              \
				if (iter_ != p.end()) {                                                                      \
					iter_->second.r.emplace_back(&dict_b, agent.key().id());                                 \
				} else {                                                                                     \
					auto iter_new = p.try_emplace(string{agent.key().ptr(), agent.key().length()},           \
												  query_cluster::mapped_t{});                                \
					iter_new.first->second.r.emplace_back(&dict_b, agent.key().id());                        \
				}                                                                                            \
			}                                                                                                \
		}                                                                                                    \
	}
#endif

template <class offline_handler, class online_handler>
template <uint8_t predict_row_limit>
LEXICON_BUDDY_EXPORT void manager<offline_handler, online_handler>::predict(query_cluster::predict_type& p,
																			const char* input,
																			const search_ctx& ctx) {
	predict_single(b_stardict_v32_map, stardict_v32_result, input);
	predict_single(b_stardict_v64_map, stardict_v64_result, input);
	predict_single(b_mdx_data_std_stream_map, mdx_data_std_stream_result, input);
	predict_single(b_mdx_data_mmap_map, mdx_data_mmap_result, input);
}

template <class offline_handler, class online_handler>
template <uint8_t predict_row_limit>
LEXICON_BUDDY_EXPORT void manager<offline_handler, online_handler>::predict(query_cluster::predict_type& p,
																			const string& input,
																			const search_ctx& ctx) {
	auto key = input.c_str();
	return predict<predict_row_limit>(p, key, ctx);
}

} // namespace buddy
} // namespace lexicon
#endif // buddy_H

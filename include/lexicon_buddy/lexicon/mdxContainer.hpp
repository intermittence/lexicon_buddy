﻿//
//  mdxContainer.hpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/03/14.
//  Copyright © 2018 Lohengrin. All rights reserved.
//

#ifndef mdxContainer_hpp
#define mdxContainer_hpp

#include <lexicon_buddy/lexicon_buddy_export.h>
#include <lexicon_buddy/utilities/encoding_support.hpp>
#include <lexicon_buddy/utilities/unzip_tool.h>
#include <lexicon_buddy/utilities/utilities.hpp>
#include <lexicon_buddy/lexicon/boost_ptree_extension.h>
#include <lexicon_buddy/lexicon/myUsing.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <exception>
#include <iostream>
#include <limits>
#include <memory>
#include <set>
#include <string>
#include <list>
#include <tuple>
#include <type_traits>
#include <vector>
#include <charconv>
#include <marisa.h>
#if Unix_Style
#include <iconv.h>
#else
#include <Windows.h>
#endif
/*
 * code in this page create mdx structure info and mdx iterators
 * mdx file format:
 * https://bitbucket.org/xwang/mdict-analysis
 * https://github.com/zhansliu/writemdict/blob/master/fileformat.md
 */
namespace lexicon
{
namespace index
{
class index_agent;
}
namespace mdx
{
using std::cout;
using std::cerr;
using std::endl;
using std::invalid_argument;
using std::move;
using std::pair;
using std::tuple;
using std::unique_ptr;
using std::vector;
using unzip::comp_type;
using util::endianSwap_32bit;
using util::endianSwap_64bit;
using util::char_to_uint32;
using util::char_to_uint64;
using util::mmap_w;
using std::copy;
using std::copy_n;
using std::reverse_copy;

uint32_t getMdxXMLLength(std::ifstream& mdxFile_Opened);
uint32_t getMdxXMLLength(const char* mmap_file);
namespace pt = boost::property_tree;

using std_stream = std::ifstream;
using memory = mmap_w;

template <class T, bool not_compressed = false>
class mdx_data;
using mdx_data_std_stream = mdx_data<std_stream>;
using mdx_data_mmap = mdx_data<memory>;

struct LEXICON_BUDDY_EXPORT h_sec_i
{
	h_sec_i() = default;
	// copy or move is disabled just in case of I putting some resource
	// invariants in mdxInfo
	h_sec_i(const h_sec_i& source) = delete;
	h_sec_i& operator=(const h_sec_i& source) = delete;
	h_sec_i(h_sec_i&& source) = delete;
	h_sec_i& operator=(h_sec_i&& source) = delete;
	explicit h_sec_i(std::ifstream& mdxFile);
	explicit h_sec_i(const mmap_w& mmap);
	// bool valid = false;
	// Header Section
	size_t length;
	// int checksum;
	// in the mdx xml(encoded in UTF-16LE) header
	string xmlEncoding{"UTF-16LE"};
	string GeneratedByEngineVersion;
	string RequiredEngineVersion;
	uint8_t Encrypted;
	string Encoding;
	string Format;
	string CreationDate;
	bool Compact;
	bool Compat; /*a typo of mdx format*/
	bool KeyCaseSensitive;
	string Description;
	string Title;
	string DataSourceFormat;
	string StyleSheet;
	string RegisterBy;
	string RegCode;
	struct style
	{
		string name;
		string begin;
		string end;
	};
	using stylesheet_v = vector<style>;
	stylesheet_v name_style{};
	// string CreationDate;
private:
	void parse_stylesheets(const string& arg);
	void read_property(std::istream& strm);
};

// keyword_sect info
struct LEXICON_BUDDY_EXPORT k_sec_i
{
	// base on the first byte of mdx file/mmap
	uint64_t keyword_sect_offset;
	uint64_t num_blocks;
	uint64_t num_entries;
	uint64_t key_index_decomp_len;
	uint64_t key_index_comp_len;
	uint64_t key_blocks_len;
	uint32_t checksum;
	// static constexpr size_t keyword_sect_to_key_index = 8 * 5 + 4;
	size_t keyword_sect_to_key_index{};
	// key_index data(may be compressed
	// extract from the data(endian swapped)
	uint32_t key_index_comp_type;
	uint64_t key_index_comp_checksum;
	// uint64_t key_index_compressed_data_offset;
	// key_blocks[] offset(when mdx file is decompressed)
	// base on the first byte of mdx file/mmap
	uint64_t key_blocks_offset;
	void read(std::istream& mdxFile, std::streamoff offset, bool version_2_0);
	void read(const mmap_w& mmap, std::ptrdiff_t offset, bool version_2_0);

private:
	void read(const char* p, bool version_2_0);
};

// record_sect info
struct LEXICON_BUDDY_EXPORT r_sec_i
{
	// base on the first byte of mdx file/mmap
	uint64_t sect_offset;
	uint64_t num_blocks;
	uint64_t num_entries;
	uint64_t index_len;
	uint64_t blocks_len;
	// static constexpr size_t record_sect_offset_to_com_decom_sizes_offset = 8 * 4;
	size_t sect_offset_to_com_decom_sizes_offset{};
	// comp_size[] decomp_size[] offset
	uint64_t com_decom_sizes_offset;

	// rec_block[] offset(when mdx file is decompressed)
	uint64_t block_offset;
	// rec_block[0] comp_type
	uint32_t block_0_comp_type;
	void read(std::istream& mdxFile, std::streamoff offset, bool version_2_0);
	void read(const mmap_w& mmap, std::ptrdiff_t offset, bool version_2_0);

private:
	void read(const char* p, bool version_2_0);
	void read_record0_comp_type(const char* p);
};

// forward
class decom_routine;
// forward
namespace debug
{
class mdx_profiler;
}
// forward
namespace key_index
{
struct slide
{
	// TODO: handle SN
	int SN;
	uint64_t num_entries;
	uint16_t first_size;
	string first_word;
	uint16_t last_size;
	string last_word;
	uint64_t comp_size;
	uint64_t decomp_size;
	// start on the first byte in header of the first block.
	// index for key_blocks based on actual file layout
	std::ptrdiff_t origin_offset_block_begin;
	bool comp_type_flag{}; // init to false
	uint32_t comp_type;
};

// index and size info of key blocks
using key_index_v = std::vector<slide>;

// for each key blocks store info of key_index: num_entries, com size, decom
// size of each key block
LEXICON_BUDDY_EXPORT key_index_v read_key_index(const char* dat_ptr,	// point to key_index
												uint64_t num_blocks,	// the unit is the basic size of
												unsigned encoding_unit, // encoding(e.g. utf8=1 utf16=2)
												bool version_2_0);
// calculate the offset based on actual comp_type of each key_block
LEXICON_BUDDY_EXPORT void key_index_comp_t_offset_init(const char* dat_ptr, key_index_v& v);
LEXICON_BUDDY_EXPORT void key_index_comp_t_offset_init(ifstream& strm, std::streampos pos, key_index_v& v);
/*
 * mdx key iteration(for building trie only)
 * since file io would be way too slow, the key iteration only work with mmap
 * the key_index blocks(=comp info header + data) must be consecutive!
 * the iterator begin at the start of first byte of first decomp data(not the header!)
 */
struct LEXICON_BUDDY_EXPORT key_offset
{
	std::size_t offset{};
	const char* ptr0{}; // begin of key str
	const char* ptrx{}; // one byte pass the end of key str, not including null terminator
	const char* ptr1{}; // one byte pass the end of key str, including null terminator
};

class key_block_iter
{
public:
	friend class key_block_container;
	using difference_type = std::ptrdiff_t;
	using value_type = key_offset;
	using pointer = const value_type*;
	using reference = const value_type&;
	using iterator_category = std::input_iterator_tag;
	key_block_iter(key_block_iter&&) = default;
	key_block_iter& operator=(key_block_iter&&) = default;
	key_block_iter(const key_block_iter&) = default;
	key_block_iter& operator=(const key_block_iter&) = default;
	unsigned get_encoding_unit() { return encoding_unit_; }
	// for debug and safety concern, length limit is imposed on the null-terminated key string
	constexpr static unsigned string_len_limit = 5000;
	reference operator*();
	pointer operator->() { return &operator*(); }
	// prefix increment
	// Precondition: instance is dereferenceable.
	key_block_iter& operator++() noexcept;
	bool operator!=(const key_block_iter& _iter) const { return !(p_ == _iter.p_); }
	bool operator==(const key_block_iter& _iter) const { return p_ == _iter.p_; }

private:
	key_block_iter() = default;
	key_block_iter(bool version_2_0, string encoding_canonical, const char* p);
	bool version_2_0_;
	unsigned encoding_unit_; // utf8: 1, utf16: 2 in bytes
	string encoding_canonical_;
	const char* p_;
	value_type value_{}; // memory location of the string, default init indicates 'null'
	bool value_updated_ = false;
};

class key_block_container
{
public:
	//	template <class T, bool not_compressed>
	//	friend class mdx_data;
	friend class mdx_data<std_stream>;
	friend class mdx_data<memory>;
	key_block_container(key_block_container&&) = default;
	key_block_container& operator=(key_block_container&&) = default;
	key_block_container(const key_block_container&) = delete;
	key_block_container& operator=(const key_block_container&) = delete;
	bool ownership() { return !buf_.get(); }
	key_block_iter begin();
	key_block_iter end();

private:
	key_block_container() = default;
	bool version_2_0_;
	unsigned encoding_unit_; // utf8: 1, utf16: 2 in bytes
	string encoding_canonical_;
	unique_ptr<char[]> buf_;
	const char* dat_;
	std::size_t dat_size_;
};

// NOTE: the iter works only for decompressed key blocks in consecutive memory buffer
// TODO: make iter task for one key block?
// iter using 8 bytes to store offset (even mdx version <2.0)
class LEXICON_BUDDY_EXPORT iter
{
	using difference_type = std::ptrdiff_t;
	using value_type = key_offset;
	using pointer = value_type* const; // user should not modify the 'value'
	using reference = pair<const char*, uint64_t>&;
	using iterator_category = std::input_iterator_tag;
	template <bool version_2_0>
	friend bool build_trie_and_coordinate_flex(iter& iter_, const char* encoding, marisa::Trie& trie_,
											   char* coor_buffer, uint64_t* buffer_size_used);
	bool version_2_0;
	// TODO: check
	// key_index_v* index_ptr; // hold the index structure.
	// the block must be stored in consecutive memory

	// size_t			  current_block;
	// const char* const blocks_begin; // start of the data blocks(first byte of first block header)
	const char* dat_p_begin; // ?should be const
	const char* dat_p;		 // pointed to the keyword blocks
	const char* dat_p_end;
	unsigned encoding_unit; // utf8: 1, utf16: 2 in bytes
	value_type value{};		// memory location of the string, default init indicates 'null'
	string encoding_canonical_;

public:
	unsigned get_encoding_unit() { return encoding_unit; }
	// for debug and safety concern, length limit is imposed on the null-terminated key string
	constexpr static unsigned string_len_limit = 5000;
	// _dat_p_end must point to the end of actual content(decompressed)
	iter(const char* const _dat_p_begin, const char* const _dat_p_end, bool begin,
		 const char* encoding_canonical, bool is_version_2_0);
	iter(const iter&) = default;
	iter& operator=(const iter&) = default;
	iter& operator=(iter&&) = default;
	bool isEnd() const noexcept { return value.ptr0 == nullptr; }
	value_type operator*() const {
		// one-beyond-the-last will return a default 'value'
		return value;
	}
	pointer operator->() { return &value; }
	// prefix increment
	iter& operator++() noexcept;
	bool operator==(const iter& _iter) const {
		if (dat_p == _iter.dat_p) return true;
		return false;
	}
	bool operator!=(const iter& _iter) const { return !operator==(_iter); }
};

template <bool version_2_0>
using key_value_map = std::map<std::string, std::list<std::conditional_t<version_2_0, uint64_t, uint32_t>>>;

template <bool version_2_0>
// using icu
// save key and coordinate. duplicated keys are combined into one.
LEXICON_BUDDY_EXPORT bool store_key_and_index_flex(key_block_iter begin, key_block_iter end,
												   const string& encoding, key_value_map<version_2_0>& m1) {
	// using num_type = uint16_t;
	constexpr size_t buffer_size = 500;
	// if (string{encoding} == encoding_su::utf8_info::NAME) {
	bool utf8 = encoding_su::encoding_equal<encoding_su::utf8_info>(encoding);
	if (utf8) {
		for (; begin != end; ++begin) {
			auto iter_value = *begin;
			// save it to string container and key_set
			auto ret_find = m1.find(iter_value.ptr0);
			if (ret_find != m1.end()) {
				ret_find->second.push_back(iter_value.offset);
#ifndef NDEBUG
				cout << "duplication found: " << iter_value.ptr0 << '\n';
#endif
			} else {
				m1.try_emplace(iter_value.ptr0).first->second.emplace_back(iter_value.offset);
			}
		}
	} else {
		encoding_su::icu_convertor_utf8 converter{encoding};
		for (; begin != end; ++begin) {
			char tmp_dst[buffer_size]{};
			auto tmp_dst_p = tmp_dst;
			auto iter_value = *begin;
			// if end, break
			// convert string to utf8
			size_t outbytes = sizeof tmp_dst;
			// exclude null terminator
			size_t inbytes = iter_value.ptrx - iter_value.ptr0;
			auto inbytestmp = inbytes;
			auto outbytestmp = outbytes;
			auto tmp_src_p = iter_value.ptr0;
			auto ret = converter.to_utf8(&tmp_dst_p, outbytestmp, (const char**)&tmp_src_p, inbytestmp);
			if (!ret) return false;
			// null-terminated
			*tmp_dst_p = 0;
			// save it to string container and key_set
			auto ret_find = m1.find(tmp_dst);
			if (ret_find != m1.end()) {
				ret_find->second.push_back(iter_value.offset);
#ifndef NDEBUG
				cout << "duplication found: " << tmp_dst << '\n';
#endif
			} else {
				m1.try_emplace(tmp_dst).first->second.emplace_back(iter_value.offset);
			}
		}
	}
	return true;
}

template <bool version_2_0>
// using icu
// save key and coordinate. duplicated keys are combined into one.
LEXICON_BUDDY_EXPORT bool store_key_and_index_flex(iter& iter_, const char* encoding,
												   key_value_map<version_2_0>& m1) {
	// using offset_type = std::conditional_t<version_2_0, uint64_t, uint32_t>;
	// using num_type = uint16_t;
	constexpr size_t buffer_size = 500;
	// char* end_p{};
	// auto encoding_unit = iter_.get_encoding_unit();
	if (string{encoding} == encoding_su::utf8_info::NAME) {
		for (;;) {
			auto iter_value = *iter_;
			// if end, break
			if (iter_.isEnd()) break;
			// temporary buffer for iconv
			char tmp_src[buffer_size]{};
			std::copy(iter_value.ptr0, iter_value.ptrx, tmp_src);
			// auto tmp_src_p = tmp_src;
			// save it to string container and key_set
			auto ret_find = m1.find(tmp_src);
			if (ret_find != m1.end()) {
				ret_find->second.push_back(iter_value.offset);
#ifndef NDEBUG
				cout << "duplication found: " << tmp_src << '\n';
#endif
			} else {
				m1.try_emplace(tmp_src).first->second.emplace_back(iter_value.offset);
			}
			++iter_;
		}
	} else {
		encoding_su::icu_convertor_utf8 converter{encoding};
		for (;;) {
			char tmp_dst[buffer_size]{};
			auto tmp_dst_p = tmp_dst;
			auto iter_value = *iter_;
			// if end, break
			if (iter_.isEnd()) break;
			// convert string to utf8
			size_t outbytes = buffer_size;
			// exclude null terminator
			size_t inbytes = iter_value.ptrx - iter_value.ptr0;
			// temporary buffer for iconv
			char tmp_src[buffer_size]{};
			std::copy(iter_value.ptr0, iter_value.ptrx, tmp_src);
			auto inbytestmp = inbytes;
			auto outbytestmp = outbytes;
			auto tmp_src_p = tmp_src;
			auto ret = converter.to_utf8(&tmp_dst_p, outbytestmp, (const char**)&tmp_src_p, inbytestmp);
			if (!ret) return false;
			// null-terminated
			*tmp_dst_p = 0;
			// save it to string container and key_set
			auto ret_find = m1.find(tmp_dst);
			if (ret_find != m1.end()) {
				ret_find->second.push_back(iter_value.offset);
#ifndef NDEBUG
				cout << "duplication found: " << tmp_dst << '\n';
#endif
			} else {
				m1.try_emplace(tmp_dst).first->second.emplace_back(iter_value.offset);
			}
			++iter_;
		}
	}
	return true;
}

template <bool version_2_0>
LEXICON_BUDDY_EXPORT bool build_trie_and_coordinate_flex(const key_value_map<version_2_0>& m1,
														 marisa::Trie& trie_, char* coor_buffer,
														 uint64_t* buffer_size_used) {
	using offset_type = std::conditional_t<version_2_0, uint64_t, uint32_t>;
	using num_type = uint16_t;
	// constexpr size_t buffer_size = 500;
	// char* end_p{};
	marisa::Keyset key_set;
	// transform data with m1 and m2
	std::map<size_t, std::list<offset_type>> m2{};
	// store keys in key_set
	for (auto& i : m1) {
		key_set.push_back(i.first.c_str());
	}
	// build trie. key id generated
	trie_.build(key_set);
#ifndef NDEBUG
	cout << "trie built, trie num_key: " << trie_.num_keys() << ", key_set num_key: " << key_set.num_keys()
		 << '\n';
#endif
	// <key, data> -> <id, data>
	marisa::Agent agent0;
	for (auto& i : m1) {
		agent0.set_query(i.first.c_str());
		if (trie_.lookup(agent0)) {
			auto id = agent0.key().id();
			m2.try_emplace(id, std::move(i.second));
		} else {
			cerr << "error. can't find key: " << i.first.c_str() << endl;
			return false;
		}
	}
	// save data to coordinate buffer
	auto word_count = m1.size();
	auto mdx_offset_ptr = coor_buffer + (sizeof(offset_type) + sizeof(num_type)) * word_count;
	auto p = coor_buffer;
	for (auto& i : m2) {
		auto& list = i.second;
		offset_type offset = mdx_offset_ptr - coor_buffer;
		num_type num = list.size();
		std::copy_n((char*)&offset, sizeof(offset_type), p);
		p += sizeof(offset_type);
		std::copy_n((char*)&num, sizeof(num_type), p);
		p += sizeof(num_type);
		for (auto j : list) {
			std::copy_n((char*)&(j), sizeof(offset_type), mdx_offset_ptr);
			mdx_offset_ptr += sizeof(offset_type);
		}
	}
	*buffer_size_used = mdx_offset_ptr - coor_buffer;
	return true;
}

} // namespace key_index
// forward
namespace record
{
/// hold info of single record
struct slide
{
	int SN{-1};
	uint64_t comp_size;
	uint64_t decomp_size;
	bool comp_type_flag{}; // init to false
	uint32_t comp_type;
	std::ptrdiff_t origin_offset_block_begin;
};

using record_index_v = std::vector<slide>;
using record_index_map = std::map<std::ptrdiff_t, // offset of each block after decompression
								  slide>;
LEXICON_BUDDY_EXPORT record_index_v read_record_index(const char* dat_ptr, uint64_t num_blocks,
													  bool version_2_0);
LEXICON_BUDDY_EXPORT void record_comp_t_offset_init(const char* dat_ptr, record_index_v& v);
LEXICON_BUDDY_EXPORT void record_comp_t_offset_init(ifstream& strm, std::streampos pos, record_index_v& v);
} // namespace record

/*
 * mdx_data includes a handle or pointer to the mdx file(or mapped memory)
 * also, some structure info of the mdx file is init during construction
 */
struct LEXICON_BUDDY_EXPORT mdx_type
{
	using data_offset = uint64_t;
	using data_content = string;
	using index_record = record::record_index_map;
	using key_index_v = key_index::key_index_v;
	using record_index_v = record::record_index_v;
};

using dict_record = mdx_type::data_content;
using dict_record_v = vector<dict_record>;

std::ostream& operator<<(std::ostream& out, const dict_record_v& r);

mdx_type::index_record::mapped_type key_block_check(const mdx_type::index_record& index,
													mdx_type::data_offset offset,
													mdx_type::data_offset& offset_r);

mdx_type::data_content data_assembly(unsigned encoding_unit, const string& encoding_canonical, bool to_utf8,
									 const char* ptr);

template <class T, bool not_compressed = false>
class query_policy;

template <class T>
class WCR_agent;

/// query_policy does the query and handle decompression and dynamic mem allocation
// mdx container delegates decompression, allocating extra space and query behavior to query_policy
// not_compressed==false means unknown
template <class T, bool not_compressed>
class LEXICON_BUDDY_EXPORT query_policy
{
public:
	using data_offset = mdx_type::data_offset;
	using data_content = mdx_type::data_content;
	using index_record = mdx_type::index_record;

private:
	unique_ptr<char[]> buf_raw{};
	unique_ptr<char[]> buf_decomp{};
	std::size_t buf_raw_s{};
	std::size_t buf_decomp_s{};
	int SN{-1};

	char* ptr_raw0{};
	char* ptr_raw1{};
	const char* ptr_decomp0{};
	char* ptr_decomp1{};

	void allocate_raw(std::size_t size) {
		if (size > buf_raw_s) {
			buf_raw.reset(new char[size]);
			buf_raw_s = size;
		}
	}
	void allocate_decomp(std::size_t size) {
		if (size > buf_decomp_s) {
			buf_decomp.reset(new char[size]);
			buf_decomp_s = size;
		}
	}
	// step 1 allocation and load
	// T== std_stream && not_compressed==false: allocate space for one or both
	// T== std_stream && not_compressed==true: allocate space for decomp
	// T== memory && not_compressed==false: allocate space for decomp
	// step 2 decomp
	// T== std_stream && not_compressed==false: decomp or not
	// T== std_stream && not_compressed==true: do nothing
	// T== memory && not_compressed==false:  decomp or not
	void load_decomp(T& t, mdx_type::index_record::mapped_type slide, data_offset blocks_offset);

public:
	// query do not know the content of mdx_data, so all the info required must be pass as method argument
	// step 1 check SN. if SN misses, call load_decomp
	// step 2 extract data
	data_content query(T& t, data_offset t_offset /*record block offset*/, const index_record& index,
					   data_offset offset, unsigned encoding_unit, const string& encoding_canonical,
					   bool to_utf8 = true) {
		data_offset offset_r{}; /* offset related to the start of decompressed actual dict content,*/
		auto slide = key_block_check(index, offset, offset_r);
		if (slide.SN == -1) return data_content{};
		if (SN != slide.SN) {
			// load
			// TODO: check offset
			auto blocks_offset = t_offset + slide.origin_offset_block_begin;
			load_decomp(t, slide, blocks_offset);
			SN = slide.SN;
		}
		return data_assembly(encoding_unit, encoding_canonical, to_utf8, ptr_decomp0 + offset_r);
	}
};

/// query_policy specialization with memory storage and plain file format,  allocation is not required
template <>
class LEXICON_BUDDY_EXPORT query_policy<memory, true>
{
public:
	using data_offset = mdx_type::data_offset;
	using data_content = mdx_type::data_content;
	using index_record = mdx_type::index_record;

private:
	const char* ptr_decomp0{};
	// char* ptr_decomp1{};

public:
	using dict_record = data_content;
	using dict_record_v = std::vector<dict_record>;
	data_content query(memory& t, data_offset t_offset, const index_record& index, data_offset offset,
					   // data_offset blocks_offset,
					   unsigned encoding_unit, const string& encoding_canonical, bool to_utf8) {
		data_offset offset_r{};
		auto slide = key_block_check(index, offset, offset_r);
		if (slide.SN == -1) return data_content{};
		ptr_decomp0 = t.data() + t_offset + slide.origin_offset_block_begin + 8 + offset_r;
		return data_assembly(encoding_unit, encoding_canonical, to_utf8, ptr_decomp0 + offset_r);
	}
};

// the mdx container(store mdx structural info and data handle)
template <class T, bool not_compressed>
class LEXICON_BUDDY_EXPORT mdx_data : public query_policy<T, not_compressed>
{
protected:
	static_assert((std::is_same<std_stream, T>::value || std::is_same<memory, T>::value),
				  "type argument for mdx_data must be either disk or memory(mmap)");
	static_assert(!not_compressed, "currently reading explicit decompressed mdx is not supported");
	friend decom_routine;
	friend debug::mdx_profiler;
	friend index::index_agent;
	template <class T0>
	friend class WCR_agent;
	using base_type = query_policy<T, not_compressed>;

public:
	using data_storage = T;
	using data_offset = mdx_type::data_offset;
	using data_content = mdx_type::data_content;
	using index_record = mdx_type::index_record;
	using key_index_v = mdx_type::key_index_v;
	using record_index_v = mdx_type::record_index_v;

protected:
	std::string _path;
	T t;					// mdx file
	std::size_t data_size;  // data in T(bytes)ss
	std::bitset<2> state{}; // init to zeros
	static constexpr size_t valid_bit = 0;
	static constexpr size_t version_bit = 1; // 0: <2.0 1: >=2.0
	// struture info of header, keyword and record sections.
	h_sec_i _h_sec;
	unsigned encoding_unit;
	string encoding_canonical;
	k_sec_i _k_sec;
	r_sec_i _r_sec;
	key_index::key_index_v _key_index_v{};
	record::record_index_v _record_index_v{};
	index_record _record_index_map{};
	static void build_index_record(const record_index_v& v, index_record& m) {
		typename std::remove_cv<decltype(m.begin()->first)>::type block_offset{};
		for (auto& i : v) {
			m.emplace(block_offset, i);
			// TODO: check
			block_offset += i.decomp_size;
		}
	}

	void set_version_bit() {
		if (_h_sec.GeneratedByEngineVersion >= "2.0") state[version_bit] = 1;
	}

public:
	// todo move to mdx namespace and impl operator<<

	using offset_type = data_offset;
	using record_type = string;
	bool is_version_2_0() { return state[version_bit]; }
	mdx_data(const std::string& path);
	explicit mdx_data(mdx_data&&) = delete;
	explicit mdx_data(const mdx_data&) = delete;
	mdx_data& operator=(const mdx_data&) = delete;
	mdx_data& operator=(mdx_data&&) = delete;
	bool is_valid() const noexcept { return state[valid_bit]; }
	// NOTE: deprecate
	comp_type key_com_type() { return static_cast<comp_type>(_k_sec.key_index_comp_type); }
	string get_encoding() const noexcept { return _h_sec.Encoding; }
	string get_encoding_canonical() const noexcept { return encoding_canonical; }
	uint32_t get_key_size() { return _key_index_v.size(); }
	/* for memory mapped file only
	 * trick member template depend on template parameter and
	 * use default argument make it work like a normal member function
	 */
	template <class Q = T>
	typename std::enable_if_t<std::is_same<memory, Q>::value, const char*> key_index_ptr() {
		// check type Q against T in case call member function with explicit template argument
		static_assert(std::is_same<Q, T>::value, "do not specify template argument for key_index_ptr()");
		return t.data() + (_k_sec.keyword_sect_offset + _k_sec.keyword_sect_to_key_index);
	}
	// disk file version(based on first byte of mdx file)
	uint64_t record_block_offset();
	T& native() { return t; }
	// memory mapped file version
	// const char* record_block_ptr();
	unsigned get_encoding_unit() { return encoding_unit; }
	const index_record& get_record_index_map() { return _record_index_map; }
	std::size_t key_block_count() { return _key_index_v.size(); }
	key_index::key_block_container get_key_block(std::size_t n);
};

/// inherit mdx_data and query_policy, does the mdx query
// the mdx container guest
// not_compressed==false means unknown, and this var effects routine of query
/* u can just cast mdx_data to mdx_guest to use _query*/
template <class T, bool not_compressed = false>
class LEXICON_BUDDY_EXPORT mdx_guest : public mdx_data<T, not_compressed>
{
	using data_storage = T;
	using data_offset = mdx_type::data_offset;
	using data_content = mdx_type::data_content;

public:
	using mdx_data<T, not_compressed>::mdx_data;
	using query_t = query_policy<T, not_compressed>;
	using mdx_t = mdx_data<T, not_compressed>;
	// _query do not worry about trailing </> (if there is one)
	// query get parameter from mdx_data and pass it to query_policy to query
	data_content _query(data_offset offset, bool to_utf8 = true) {
		if (mdx_t::_h_sec.Compat || mdx_t::_h_sec.Compact) {
			auto raw = query_t::query(mdx_t::t, mdx_t::_r_sec.block_offset, mdx_t::_record_index_map, offset,
									  mdx_t::encoding_unit, mdx_t::encoding_canonical, to_utf8);
			auto& name_style = mdx_t::_h_sec.name_style;
			// position of '`' (grave_accent)
			vector<string::size_type> grave_accent_marks;
			// list of mark names
			vector<double> mark_names;
			string::size_type i_pos{};
			for (auto& i : raw) {
				if (i == '`') grave_accent_marks.emplace_back(i_pos);
				++i_pos;
			}
			// difference between string size of unfold and raw
			int dif{};
			auto raw_begin_p = raw.data();
			// calculate unfold size and store mark names
			for (string::size_type p = 0; p + 1 < grave_accent_marks.size(); p += 2) {
				unsigned n = 256;
				auto begin = raw_begin_p + grave_accent_marks[p] + 1;
				auto end = raw_begin_p + grave_accent_marks[p + 1];
				std::from_chars(begin, end, n);
				// be careful:  smallest n = 1 since name str falls in closed interval "1"~"255"
				mark_names.emplace_back(n);
				dif += name_style[n - 1].begin.size() + name_style[n - 1].end.size();
				dif -= 2 + end - begin;
			}
			string record_styled{};
			record_styled.resize(raw.size() + dif);
			// populate string
			// check if there's at least one pair of grave accent
			if (grave_accent_marks.size() > 1) {
				string::size_type g_markv_pos = 0;
				string::size_type marknamev_pos = 0;
				auto dst_iter = record_styled.begin();
				auto raw_iter = raw.begin();
				// populate target with raw before first mark
				// copy raw before mark
				auto raw_iter_end = raw_iter + grave_accent_marks[g_markv_pos];
				copy(raw_iter, raw_iter_end, dst_iter);
				dst_iter += raw_iter_end - raw_iter;
				raw_iter = raw.begin() + grave_accent_marks[g_markv_pos + 1] + 1;
				// populate target with raw and marks
				for (; g_markv_pos + 3 < grave_accent_marks.size(); g_markv_pos += 2, ++marknamev_pos) {
					// copy style first line
					auto style_line_iter = name_style[mark_names[marknamev_pos] - 1].begin.begin();
					auto style_line_iter_end = name_style[mark_names[marknamev_pos] - 1].begin.end();
					copy(style_line_iter, style_line_iter_end, dst_iter);
					dst_iter += style_line_iter_end - style_line_iter;
					// copy raw between this mark and next mark
					auto raw_iter_end = raw.begin() + grave_accent_marks[g_markv_pos + 2];
					copy(raw_iter, raw_iter_end, dst_iter);
					dst_iter += raw_iter_end - raw_iter;
					raw_iter = raw.begin() + grave_accent_marks[g_markv_pos + 3] + 1;
					// copy style second line
					style_line_iter = name_style[mark_names[marknamev_pos] - 1].end.begin();
					style_line_iter_end = name_style[mark_names[marknamev_pos] - 1].end.end();
					copy(style_line_iter, style_line_iter_end, dst_iter);
					dst_iter += style_line_iter_end - style_line_iter;
				}
				// populate target with raw and last mark.
				if (g_markv_pos + 1 < grave_accent_marks.size()) {
					// copy style first line
					auto style_line_iter = name_style[mark_names[marknamev_pos] - 1].begin.begin();
					auto style_line_iter_end = name_style[mark_names[marknamev_pos] - 1].begin.end();
					copy(style_line_iter, style_line_iter_end, dst_iter);
					dst_iter += style_line_iter_end - style_line_iter;
					// copy raw between this mark and end
					auto raw_iter_end = raw.end();
					copy(raw_iter, raw_iter_end, dst_iter);
					dst_iter += raw_iter_end - raw_iter;
					// copy style second line
					style_line_iter = name_style[mark_names[marknamev_pos] - 1].end.begin();
					style_line_iter_end = name_style[mark_names[marknamev_pos] - 1].end.end();
					copy(style_line_iter, style_line_iter_end, dst_iter);
				}
				return record_styled;
			} else {
				return raw;
			}
		} else {
			return query_t::query(mdx_t::t, mdx_t::_r_sec.block_offset, mdx_t::_record_index_map, offset,
								  mdx_t::encoding_unit, mdx_t::encoding_canonical, to_utf8);
		}
	}
};

/// @note iteration over the whole mdx. WCR stand for Word Coordinate Record
template <class T>
class WCR_agent
{
public:
	using difference_type = std::ptrdiff_t;
	struct key_offset_c
	{
		std::size_t offset{};
		string word;
		key_offset_c() = default;
		key_offset_c(const key_index::key_block_iter::value_type& v)
			: offset{v.offset}, word{v.ptr0, v.ptrx} {}
		key_offset_c& operator=(const key_index::key_block_iter::value_type& v) {
			offset = v.offset;
			word = {v.ptr0, v.ptrx};
			return *this;
		}
	};
	using word_coor_type = key_offset_c;
	using value_type = tuple<word_coor_type, dict_record>;
	using reference = const value_type&;
	using pointer = const value_type*;
	using iterator_category = std::input_iterator_tag;

	WCR_agent(mdx_guest<T>* dict_c, bool utf8 = true)
		: dict_c_{dict_c}, encoding_canonical_{dict_c_->get_encoding_canonical()},
		  converter{encoding_canonical_}, version_2_0_{dict_c_->is_version_2_0()},
		  block_count_{dict_c_->key_block_count()}, c_{dict_c_->get_key_block(0)}, block_iter_{c_.begin()},
		  block_iter_end_{c_.end()} {
		utf8_encoding(utf8);
		// skip "hypothetical" empty block until a non-empty block or reach the end
		while (block_iter_ == block_iter_end_) {
			if (reach_end()) break;
			next_block();
		}
	}

	WCR_agent(WCR_agent&&) = default;
	WCR_agent& operator=(WCR_agent&&) = default;
	WCR_agent(const WCR_agent&) = delete;
	WCR_agent& operator=(const WCR_agent&) = delete;
	// default on
	void utf8_encoding(bool utf8) {
		bool src_utf8 = encoding_su::encoding_equal<encoding_su::utf8_info>(encoding_canonical_);
		dst_encoding_utf8 = (src_utf8 or utf8) ? true : false;
		require_convert_utf8 = (src_utf8 == dst_encoding_utf8) ? false : true;
	}
	bool reach_end() { return block_id_ == block_count_ - 1 and block_iter_ == block_iter_end_; }
	/// @warning precondition: reach_end() return false
	/// @note implicit precondition: "hypothetical" empty block skipped during construction
	WCR_agent& operator++() {
		++block_iter_;
		v_updated = false;
		while (block_iter_ == block_iter_end_) {
			if (reach_end()) break;
			next_block();
		}
		return *this;
	}
	/// @warning precondition: reach_end() return false
	reference operator*() {
		if (!v_updated) {
			read_value();
			v_updated = true;
		}
		return v_;
	}

	// bool operator==(const WCR_agent& arg) { return block_iter_ == arg.block_iter_; }

private:
	void read_value() {
		if (require_convert_utf8) {
			word_coor_type word_coor = *block_iter_;
			// utf8 conversion
			size_t inbytes = block_iter_->ptrx - block_iter_->ptr0;
			auto inbytestmp = inbytes;
			auto outbytestmp = sizeof key_conver_buf;
			auto tmp_src_p = block_iter_->ptr0;
			auto tmp_dst_p = key_conver_buf;
			converter.to_utf8_throw(&tmp_dst_p, outbytestmp, (const char**)&tmp_src_p, inbytestmp);
			// null-terminated
			*tmp_dst_p = 0;
			word_coor.word = {key_conver_buf, tmp_dst_p};
			v_ = {word_coor, dict_c_->_query(block_iter_->offset, false)};
		} else {
			v_ = {*block_iter_, dict_c_->_query(block_iter_->offset, false)};
		}
	}

	void next_block() {
		++block_id_;
		c_ = dict_c_->get_key_block(block_id_);
		block_iter_ = c_.begin();
		block_iter_end_ = c_.end();
	}
	mdx_guest<T>* dict_c_;
	string encoding_canonical_;
	encoding_su::icu_convertor_utf8 converter;
	bool dst_encoding_utf8;
	bool require_convert_utf8;
	char key_conver_buf[256];
	bool version_2_0_;
	size_t block_id_ = 0;
	size_t block_count_;
	// block container
	key_index::key_block_container c_;
	// block iterator
	key_index::key_block_iter block_iter_;
	key_index::key_block_iter block_iter_end_;
	value_type v_;
	bool v_updated = false;
};

/**
 * U can't easily check whether the mdx index/record is compressed or not, and the only option this
 * project currently provides is decompressing the whole file(so u know it's a plain mdx). Following
 * this idea, the library handle mdx file in two ways: decompressed or unknown. Decompression and
 * mmap the mdx file makes query straight forward, but doing this increases memory footprint. U
 * should track the file when decompression
 */
/// decomp .mdx file
/// @warning @class decom_routine is broken!
// TODO: check decom_routine against mdx version<2.0
class decom_routine
{
private:
	// pos of each slide in the container(vector<slide>)
	constexpr static int _0_H_SECT_P = 0;
	constexpr static int _1_K_SECT_HEAD_P = 1;
	constexpr static int _2_K_SECT_INDEX_P = 2;
	constexpr static int _3_K_SECT_BLOCKS_P = 3;
	constexpr static int _4_R_SECT_HEAD_P = 4;
	constexpr static int _5_R_SECT_SIZES_P = 5;
	constexpr static int _6_R_SECT_BLOCKS_P = 6;

	using decom_dat = pair<uint64_t, unique_ptr<char[]>>; // size and data
	using buf_slide = std::tuple<comp_type, uint64_t, unique_ptr<char[]>>;
	constexpr static int COMP_P = 0;
	constexpr static int SIZE_P = 1;
	constexpr static int BUFF_P = 2;

public:
	// dst_path is unchecked
	static void decomp(const string& src_path, const string& dst_path);
	/* get size of the entire keyword section and put decomp in vector
	 * TODO: and all the compression type will be correctly updated
	 * when decomp the keyword section, this method assume every key_block uses the same compression
	 * algorithm
	 */
	static uint64_t k_sec_de(mdx_data_std_stream& src, vector<buf_slide>& v);
	/* get the whole size of record section and put decomp in vector
	 * TODO: and all the compression type will be correctly updated
	 * when decomp the record section, this method assume every record uses the same compression
	 * algorithm
	 */
	static uint64_t r_sec_de(mdx_data_std_stream& src, vector<buf_slide>& v);
};
};	 // namespace mdx
};	 // namespace lexicon
#endif /* mdxContainer_hpp */
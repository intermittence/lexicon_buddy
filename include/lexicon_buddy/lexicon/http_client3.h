﻿//
//  http_client3.h
//  lexicon_buddy
//
//  Created by Lohengrin on 2019/4/3.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef HTTP_CLIENT3_H
#define HTTP_CLIENT3_H

#ifndef BOOST_COROUTINES_NO_DEPRECATION_WARNING
#define BOOST_COROUTINES_NO_DEPRECATION_WARNING
//#pragma message                                                                                              \
//	"BOOST_COROUTINES_NO_DEPRECATION_WARNING defined to suppress boost.coroutines deprecation warning"
#endif
#include <lexicon_buddy/lexicon/http_common.h>
#include <lexicon_buddy/from_beast_example/socks_client.hpp>
#include <boost/asio/spawn.hpp>
#include <unordered_map>
#include <thread>

namespace lexicon
{
namespace online
{
using std::cout;
using std::cerr;
using std::endl;
using error_code = boost::system::error_code;
using tcp = boost::asio::ip::tcp;
namespace http = boost::beast::http;
namespace asio = boost::asio;
using boost::beast::flat_buffer;
using boost::asio::strand;
using boost::asio::io_context;
using boost::asio::yield_context;
using request_type = http::request<http::empty_body>;
constexpr const char* SCHEME_SOCKS4 = "socks4";
constexpr const char* SCHEME_SOCKS5 = "socks5";

struct socks_url
{
	std::string scheme;
	std::string host;
	std::string port;
	std::string path;
	std::string query;
	std::string fragment;
	std::string username;
	std::string password;
};

inline bool is_scheme_char(const char c) {
	return isalpha(c) || isdigit(c) || c == '+' || c == '-' || c == '.';
}

bool parse_socks_url(const std::string& url, socks_url& result);

inline bool is_socks_url_valid(const socks_url& arg) {
	if (!(arg.host.empty() || arg.port.empty() || arg.scheme.empty()) &&
		(arg.scheme == SCHEME_SOCKS4 || arg.scheme == SCHEME_SOCKS5)) {
		return true;
	} else {
		return false;
	}
}

// state from user (class session) perspective
enum class session_state
{
	closed = 0,
	closing,
	connecting,
	working,
	standby,
	try_next_time,
	shut_down
};

// all possible states of connection
enum class conn_internal_state
{
	resolve = 0,
	socket_connect,
	sock_handshake,
	ssl_handshake,
	connected,
	fatal_error
};
#ifndef state_2_string_case
#define state_2_string_case(t, v)                                                                            \
	case t::v:                                                                                               \
		return #v
#endif
inline const char* state_2_string(session_state s) {
	switch (s) {
		state_2_string_case(session_state, closed);
		state_2_string_case(session_state, closing);
		state_2_string_case(session_state, connecting);
		state_2_string_case(session_state, working);
		state_2_string_case(session_state, standby);
		state_2_string_case(session_state, try_next_time);
		state_2_string_case(session_state, shut_down);
	}
}

inline const char* state_2_string(conn_internal_state s) {
	switch (s) {
		state_2_string_case(conn_internal_state, resolve);
		state_2_string_case(conn_internal_state, socket_connect);
		state_2_string_case(conn_internal_state, sock_handshake);
		state_2_string_case(conn_internal_state, ssl_handshake);
		state_2_string_case(conn_internal_state, connected);
		state_2_string_case(conn_internal_state, fatal_error);
	}
}

// member "stream_" detector
template <class T, class = int>
struct Has_stream_ : std::false_type
{};

template <class T>
// cast prevent overload for the comma operator
struct Has_stream_<T, decltype((void)T::stream_, 0)> : std::true_type
{};

/// @brief an HTTP client. This uses the Curiously Recurring Template Pattern so that the same code works with
/// both SSL streams and regular sockets.
/// @tparam lower_layer lower_layer's internal stream should be able to reuse after disconnect
/// (nullable/movable). boost::asio::ip::tcp::socket used for plain http meets the requirement but
/// boost::asio::ssl::stream for https does not. if lower_layer impl ssl with boost::asio::ssl::stream, a
/// wrapper class is required.
/// @tparam Handler should be either MoveConstructible or CopyConstructible and meet the requirement as
/// std::vector's element type
/// @warning thread safety. Handler for sessions may run concurrently and therefor it's not thread safe.
template <class lower_layer, class Handler>
class session
{
public:
	using handler_type = Handler;
	using clock_ = std::chrono::system_clock;
	constexpr static int STANDBY_TIMEOUT_L = 30;

	struct task
	{
		request_type req_;
		handler_type handler_;
		task(const request_type& req, const handler_type& handler) : req_(req), handler_(handler) {}
	};
	using task_container = std::vector<task>;

private:
	// static constexpr uint8_t task_limit = task_limit_;
	task_container task_v{};
	response_type res_;
	// Access the derived class, this is part of
	// the Curiously Recurring Template Pattern idiom.
	lower_layer& derived() { return static_cast<lower_layer&>(*this); }
	error_code session_ec_;
	string cookie{};
	uint8_t conn_c{};
	static constexpr uint8_t conn_limit = 3;

protected:
	session_state s_state_{session_state::closed};

public:
	/// @brief user interface
	/// @param req will be moved since it's likely same req will be used for only one session one time
	template <uint8_t task_limit_ = 30>
	void accept_task(const Handler& handler, const request_type& req) {
		// static_assert(std::is_same_v<Handler, std::decay_t<Handler_arg>>, "Handler type mismatch");
		if (task_v.size() >= task_limit_)
			throw http_client_error("enqueue failed because task queue reaches limit");
		// avoid spawning new coroutine if possible
		// spawn when
		if (s_state_ == session_state::shut_down) throw http_client_error{"session already shut_down"};
		task_v.emplace_back(req, handler);
		switch (s_state_) {
			case session_state::closed:
				// state must be set since it's currently closed
				s_state_ = session_state::connecting;
				boost::asio::spawn(strand_, std::bind(&session::do_session, derived().shared_from_this(),
													  std::placeholders::_1));
				break;
			case session_state::standby:
				// state must be set since it's currently standby
				s_state_ = session_state::working;
				// cancel standby timer
				timer_s_.cancel();
				break;
			default:
				break;
		}
	}

	/// @param req will be moved since it's likely same req will be used for only one session one time
	template <uint8_t task_limit_ = 30>
	void accept_task_delay(const Handler& handler, const request_type& req, uint32_t delay_milliseconds,
						   yield_context yield) {
		// static_assert(std::is_same_v<Handler, std::decay_t<Handler_arg>>, "Handler type mismatch");
		error_code error{};
		boost::asio::steady_timer timer_delay{derived().context()};
		timer_delay.expires_after(std::chrono::milliseconds(delay_milliseconds));
	wait:
		timer_delay.async_wait(yield[error]);
		// another routine canceled timer
		if (error == boost::asio::error::operation_aborted) return;
		if (error) {
			HTTP_CLIENT_FAIL(error, "accept_task_delay");
			return;
		}
		if (timer_delay.expiry() < std::chrono::steady_clock::now()) {
			accept_task<task_limit_>(handler, req);
		} else {
			goto wait;
		}
	}

	void preheat() {
		COUT_DEBUG(derived().get_host());
		COUT_DEBUG(" preheat()");
		COUT_DEBUG(endl);
		switch (s_state_) {
			case session_state::closed:
				// state must be set since it's currently closed
				s_state_ = session_state::connecting;
				boost::asio::spawn(strand_, std::bind(&session::do_session, derived().shared_from_this(),
													  std::placeholders::_1));
				break;
			default:
				break;
		}
	}

	void require_shut_down() {
		MSG_DEBUG("[http_coro] require_shut_down");
		s_state_ = session_state::shut_down;
		// cancel async operation from top to bottom
		timer_s_.cancel(session_ec_);
		derived().cancel_async();
	}

	auto& strand() { return strand_; }

protected:
	io_context::strand strand_;
	flat_buffer buffer_;
	boost::asio::steady_timer timer_s_;

	void do_session(yield_context yield) {
		COUT_DEBUG(derived().get_host());
		COUT_DEBUG(" do_session() state=");
		COUT_DEBUG(state_2_string(s_state_));
		COUT_DEBUG(endl);
	dispatch:
		switch (s_state_) {
			case session_state::connecting:
				COUT_DEBUG(derived().get_host());
				COUT_DEBUG(" do_session() goto connect");
				COUT_DEBUG(endl);
				goto connect;
			case session_state::working:
				conn_c = 0;
				goto task_0;
			case session_state::shut_down:
				conn_c = 0;
				goto shutdown;
			case session_state::try_next_time:
				conn_c = 0;
				// try_next_time indicate lower layer failed this time, do_session should clear all task and
				for (auto&& i : task_v) {
					i.handler_(session_ec_);
				}
				task_v.clear();
				// reset state
				s_state_ = session_state::closed;
				return;
			default:
				return;
		}
	connect:
		++conn_c;
		if (conn_c > conn_limit) {
			s_state_ = session_state::try_next_time;
			goto dispatch;
		}
		derived().run(yield);
		goto dispatch;
	task_0:
		COUT_DEBUG("send req to server");
		COUT_DEBUG(endl);
		while (!task_v.empty()) {
			//  write:
			COUT_DEBUG(task_v.front().req_);
			COUT_DEBUG(endl);
			http::async_write(derived().stream(), task_v.front().req_, yield[session_ec_]);
			if (session_ec_) {
				if (session_ec_ != boost::asio::error::operation_aborted) {
					HTTP_CLIENT_FAIL(session_ec_, "http write");
				} else if (s_state_ == session_state::shut_down) {
					goto shutdown;
				}
				goto clear;
			}
			// read:
			res_ = {};
			http::async_read(derived().stream(), buffer_, res_, yield[session_ec_]);
			COUT_DEBUG(res_.base());
			COUT_DEBUG(endl);
			if (session_ec_) {
				if (session_ec_ == http::error::end_of_stream) {
					COUT_DEBUG("http read: end of stream");
					COUT_DEBUG(endl);
					goto close;
				} else {
					// print error if it's not cancelled by user
					if (session_ec_ != boost::asio::error::operation_aborted) {
						HTTP_CLIENT_FAIL(session_ec_, "http read");
					} else if (s_state_ == session_state::shut_down) {
						goto shutdown;
					}
					goto clear;
				}
			} else {
				if (res_[http::field::transfer_encoding] == "chunked") {
					task_v.front().handler_(res_);
					task_v.erase(task_v.begin());
					COUT_DEBUG("http read: chunked");
					COUT_DEBUG(endl);
					http::async_read(derived().stream(), buffer_, res_, yield[session_ec_]);
					if (session_ec_) {
						if (session_ec_ == http::error::end_of_stream) {
							COUT_DEBUG("http read: end of stream");
							COUT_DEBUG(endl);
							goto close;
						} else {
							// print error if it's not cancelled by user
							if (session_ec_ != boost::asio::error::operation_aborted) {
								HTTP_CLIENT_FAIL(session_ec_, "http read");
							} else if (s_state_ == session_state::shut_down) {
								goto shutdown;
							}
							goto clear;
						}
					}
				} else {
					task_v.front().handler_(res_);
					task_v.erase(task_v.begin());
				}
			}
		}
		// standby
		s_state_ = session_state::standby;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-label"
	standby:
#pragma clang diagnostic pop
		timer_s_.expires_after(std::chrono::seconds(STANDBY_TIMEOUT_L));
	standby_wait:
		COUT_DEBUG("standby_wait: ");
		COUT_DEBUG(endl);
		timer_s_.async_wait(yield[session_ec_]);
		// another routine canceled timer
		if (session_ec_ == boost::asio::error::operation_aborted) {
			// user cancel timer to submit task
			if (s_state_ == session_state::working) {
				COUT_DEBUG("leave standby, goto task");
				COUT_DEBUG(endl);
				goto task_0;
			}
			// user cancel timer to shutdown stream (quickly)
			else if (s_state_ == session_state::shut_down) {
				goto shutdown;
			} else {
				HTTP_CLIENT_FAIL(error_code{}, "http_coro internal state error");
				return;
			}
		}
		if (session_ec_) {
			HTTP_CLIENT_FAIL(session_ec_, "standby timer");
			return;
		}
		// Verify that the timer really expired since the deadline may have moved.
		if (timer_s_.expiry() < std::chrono::steady_clock::now()) {
			s_state_ = session_state::closed;
			MSG_DEBUG("[http_coro] standby timeout, close");
			// timeout, close the stream
		} else {
			goto standby_wait;
		}
	clear:
		for (auto&& i : task_v) {
			i.handler_(session_ec_);
		}
		task_v.clear();
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-label"
	closing:
#pragma clang diagnostic pop
		COUT_DEBUG("closing: ");
		COUT_DEBUG(endl);
		s_state_ = session_state::closing;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-label"
	close:
#pragma clang diagnostic pop
		derived().close(yield);
		if (s_state_ != session_state::shut_down) {
			if (task_v.empty()) {
				s_state_ = session_state::closed;
			} else {
				s_state_ = session_state::connecting;
				COUT_DEBUG("goto connect");
				COUT_DEBUG(endl);
				goto connect;
			}
		}
		return;
	shutdown:
		// enter shutdown, socket already closed by require_shut_down()
		// todo clear stream
		if constexpr (Has_stream_<lower_layer>::value) derived().clear_after_shutdown();
		// clear task
		for (auto&& i : task_v) {
			i.handler_(session_ec_);
		}
		task_v.clear();
	}

public:
	// Take ownership of the buffer
	explicit session(io_context& ioc, flat_buffer&& buffer)
		: strand_(ioc), buffer_(std::move(buffer)), timer_s_(strand_.context()) {}
};

#ifndef http_close_method
#define http_close_method                                                                                    \
	socket_.shutdown(tcp::socket::shutdown_send, socket_ec_);                                                \
	if (socket_ec_) HTTP_CLIENT_FAIL(socket_ec_, "socket shutdown");                                         \
	if (this->s_state_ != session_state::shut_down) {                                                        \
		this->s_state_ = session_state::closed;                                                              \
	} else {                                                                                                 \
		socket_.close(socket_ec_);                                                                           \
		if (socket_ec_) HTTP_CLIENT_FAIL(socket_ec_, "socket close");                                        \
	}                                                                                                        \
	if (results_.empty()) {                                                                                  \
		in_state = conn_internal_state::resolve;                                                             \
	} else {                                                                                                 \
		in_state = conn_internal_state::socket_connect;                                                      \
	}
#endif

#ifndef https_close_method
#define https_close_method                                                                                   \
	stream_->async_shutdown(yield[socket_ec_]);                                                              \
	socket_.shutdown(tcp::socket::shutdown_send, socket_ec_);                                                \
	if (socket_ec_) HTTP_CLIENT_FAIL(socket_ec_, "socket shutdown");                                         \
	stream_.reset();                                                                                         \
	if (this->s_state_ != session_state::shut_down) {                                                        \
		this->s_state_ = session_state::closed;                                                              \
	} else {                                                                                                 \
		socket_.close(socket_ec_);                                                                           \
		if (socket_ec_) HTTP_CLIENT_FAIL(socket_ec_, "socket close");                                        \
	}                                                                                                        \
	if (results_.empty()) {                                                                                  \
		in_state = conn_internal_state::resolve;                                                             \
	} else {                                                                                                 \
		in_state = conn_internal_state::socket_connect;                                                      \
	}

#endif

/// @brief plain http
template <class Handler>
class plain_session : public session<plain_session<Handler>, Handler>,
					  public std::enable_shared_from_this<plain_session<Handler>>
{
	// io_context::strand strand_;
	tcp::socket socket_;
	string host_;
	string port_;
	tcp::resolver resolver_;
	// host resolved once and save to results_ (reconnection will use saved resolving results)
	tcp::resolver::results_type results_;
	conn_internal_state in_state{conn_internal_state::resolve};
	error_code socket_ec_;
	// handle internal error based on session_state and socket_ec_
	void process_error() {}

public:
	string get_host() { return host_; }
	// Called by the base class
	// io_context::strand& strand() { return strand_; }
	// Called by the base class
	void close(__attribute__((unused)) yield_context& yield) { http_close_method }
	void close() { http_close_method }

	void cancel_async() {
		resolver_.cancel();
		// socket_.cancel(socket_ec_);
		socket_.close(socket_ec_);
	}

	using base_type = session<plain_session<Handler>, Handler>;
	// Create the session
	plain_session(io_context& io_ctx, flat_buffer&& buffer, char const* host, char const* port)
		: session<plain_session<Handler>, Handler>(io_ctx, std::move(buffer)), socket_(io_ctx), host_(host),
		  port_(port), resolver_(io_ctx) {}

	// Called by the base class
	tcp::socket& stream() { return socket_; }
	// Called by the base class
	// state& state() { return in_state; }

	// Start the asynchronous operation
	// Called by the base class
	void run(yield_context& yield) {
		auto process_error = [&](const char* stage_name) -> void {
			// connection clean up
			switch (in_state) {
				case conn_internal_state::socket_connect:
					socket_.close(socket_ec_);
					if (results_.size() > 0) {
						in_state = conn_internal_state::socket_connect;
					} else {
						in_state = conn_internal_state::resolve;
					}
					break;
				case conn_internal_state::resolve:
					break;
				default:
					break;
			}
			// print error and set in_state for some special cases
			if (socket_ec_ == boost::asio::error::operation_aborted &&
				this->s_state_ == session_state::shut_down) {
				// normal
			} else {
				// print error
				HTTP_CLIENT_FAIL(socket_ec_, stage_name);
				// resolving failed
				if (socket_ec_.category() == boost::asio::error::misc_category &&
					socket_ec_.value() == boost::asio::error::not_found) {
					in_state = conn_internal_state::resolve;
				}
				this->s_state_ = session_state::try_next_time;
			}
		};
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-label"
	dispatch:
		switch (in_state) {
			case conn_internal_state::resolve:
				goto resolve;
			case conn_internal_state::socket_connect:
				goto socket_connect;
			case conn_internal_state::fatal_error:
				this->s_state_ = session_state::shut_down;
				return;
			default:
				return;
		}
	resolve:
		in_state = conn_internal_state::resolve;
		results_ = resolver_.async_resolve(host_, port_, yield[socket_ec_]);
		if (socket_ec_) return process_error("resolve");

	socket_connect_enter:
		in_state = conn_internal_state::socket_connect;
	socket_connect:
		boost::asio::async_connect(socket_, results_.begin(), results_.end(), yield[socket_ec_]);
		if (socket_ec_) return process_error("socket_connect");
		in_state = conn_internal_state::connected;
		// tell base class connection is ready
		this->s_state_ = session_state::working;
	}
#pragma clang diagnostic pop
};

///@brief flex plain session with socks proxy setting detection. extra proxy information is required for
/// construction (compare to plain_session)
template <class Handler>
class plain_session_flex : public session<plain_session_flex<Handler>, Handler>,
						   public std::enable_shared_from_this<plain_session_flex<Handler>>
{
	// io_context::strand strand_;
	tcp::socket socket_;
	string host_;
	string port_;
	tcp::resolver resolver_;
	socks_url socks_url_;
	bool proxy_valid{false};
	// host resolved once and save to results_ (reconnection will use saved resolving results)
	tcp::resolver::results_type results_;
	conn_internal_state in_state{conn_internal_state::resolve};
	error_code socket_ec_;
	// handle internal error based on session_state and socket_ec_
	void process_error() {}

public:
	string get_host() { return host_; }
	// Called by the base class
	// io_context::strand& strand() { return strand_; }
	// Called by the base class
	void close(__attribute__((unused)) yield_context& yield) { http_close_method }

	void close() { http_close_method }

	void cancel_async() {
		resolver_.cancel();
		// socket_.cancel(socket_ec_);
		socket_.close(socket_ec_);
	}

	using base_type = session<plain_session_flex<Handler>, Handler>;
	// Create the session
	plain_session_flex(io_context& io_ctx, flat_buffer&& buffer, char const* host, char const* port,
					   const std::string& socks_url_str)
		: session<plain_session_flex<Handler>, Handler>(io_ctx, std::move(buffer)), /* strand_(io_ctx),*/
		  socket_(io_ctx), host_(host), port_(port), resolver_(io_ctx) {
		parse_socks_url(socks_url_str, socks_url_);
		proxy_valid = is_socks_url_valid(socks_url_);
	}
	plain_session_flex(io_context& io_ctx, flat_buffer&& buffer, char const* host, char const* port,
					   const socks_url& socks_url__)
		: session<plain_session_flex<Handler>, Handler>(io_ctx, std::move(buffer)), /* strand_(io_ctx),*/
		  socket_(io_ctx), host_(host), port_(port), resolver_(io_ctx), socks_url_(socks_url__) {
		proxy_valid = is_socks_url_valid(socks_url_);
	}
	plain_session_flex(io_context& io_ctx, flat_buffer&& buffer, char const* host, char const* port)
		: session<plain_session_flex<Handler>, Handler>(io_ctx, std::move(buffer)), /*strand_(io_ctx),*/
		  socket_(io_ctx), host_(host), port_(port), resolver_(io_ctx), socks_url_() {}

	// Called by the base class
	tcp::socket& stream() { return socket_; }
	// Called by the base class
	// state& state() { return in_state; }

	bool proxy_setting_valid() { return proxy_valid; }

	// Start the asynchronous operation
	// Called by the base class
	void run(yield_context& yield) {
		auto socks_enabled = proxy_setting_valid();
		auto process_error = [&](const char* stage_name) -> void {
			// connection clean up
			switch (in_state) {
				case conn_internal_state::sock_handshake:
					socket_.shutdown(tcp::socket::shutdown_send, socket_ec_);
					socket_.close(socket_ec_);
					in_state = conn_internal_state::socket_connect;
					break;
				case conn_internal_state::socket_connect:
					socket_.close(socket_ec_);
					if (results_.size() > 0) {
						in_state = conn_internal_state::socket_connect;
					} else {
						in_state = conn_internal_state::resolve;
					}
					break;
				case conn_internal_state::resolve:
					break;
				default:
					break;
			}
			// print error and set in_state for some special cases
			if (socket_ec_ == boost::asio::error::operation_aborted &&
				this->s_state_ == session_state::shut_down) {
				// normal
			} else {
				// print error
				HTTP_CLIENT_FAIL(socket_ec_, stage_name);
				// resolving failed
				if (socket_ec_.category() == boost::asio::error::misc_category &&
					socket_ec_.value() == boost::asio::error::not_found) {
					in_state = conn_internal_state::resolve;
				}
				this->s_state_ = session_state::try_next_time;
			}
		};
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-label"
	dispatch:
		switch (in_state) {
			case conn_internal_state::resolve:
				goto resolve;
			case conn_internal_state::socket_connect:
				goto socket_connect;
			case conn_internal_state::sock_handshake:
				goto sock_handshake;
			case conn_internal_state::fatal_error:
				this->s_state_ = session_state::shut_down;
				return;
			default:
				return;
		}
	resolve:
		if (socks_enabled) {
			results_ = resolver_.async_resolve(socks_url_.host, socks_url_.port, yield[socket_ec_]);
		} else {
			results_ = resolver_.async_resolve(host_, port_, yield[socket_ec_]);
		}
		if (socket_ec_) return process_error("resolve");
	socket_connect_enter:
		in_state = conn_internal_state::socket_connect;
	socket_connect:
		boost::asio::async_connect(socket_, results_.begin(), results_.end(), yield[socket_ec_]);
		if (socket_ec_) return process_error("socket_connect");
	sock_handshake_enter:
#pragma clang diagnostic pop
		in_state = conn_internal_state::sock_handshake;
	sock_handshake:
		if (socks_enabled) {
			// todo proxy_setting_valid?
			int socks_version = socks_url_.scheme == "socks4" ? 4 : 5;
			socks_version = socks_url_.scheme == "socks5" ? 5 : 0;
			if (socks_version == 0) {
				HTTP_CLIENT_FAIL(error_code{}, "incorrect socks version");
				socket_.shutdown(tcp::socket::shutdown_send, socket_ec_);
				socket_.close(socket_ec_);
				this->s_state_ = session_state::shut_down;
				return;
			}
			socks::async_handshake(socket_, host_, static_cast<unsigned short>(std::atoi(port_.c_str())),
								   socks_version, socks_url_.username, socks_url_.password, true,
								   yield[socket_ec_]);
			if (socket_ec_) return process_error("sock_handshake");
		}
		in_state = conn_internal_state::connected;
		// tell base class connection is ready
		this->s_state_ = session_state::working;
	}
};

/// @brief https
template <class Handler>
class https_session : public session<https_session<Handler>, Handler>,
					  public std::enable_shared_from_this<https_session<Handler>>
{
public:
	using stream_type = ssl::stream<tcp::socket&>;
	using base_type = session<https_session<Handler>, Handler>;

private:
	// tcp::socket socket_;
	ssl::context& ctx_;
	// io_context::strand strand_;
	string host_;
	string port_;
	tcp::resolver resolver_;
	// host resolved once and save to results_ (reconnection will use saved resolving results)
	tcp::resolver::results_type results_;
	tcp::socket socket_;
	boost::optional<stream_type> stream_{};
	ssl_cache_boost_optional cache{stream_};
	conn_internal_state in_state{conn_internal_state::resolve};
	error_code socket_ec_;

public:
	string get_host() { return host_; }
	// Called by the base class
	void close(yield_context& yield) {
		MSG_DEBUG("[http_coro] async_shutdown");
		https_close_method
	}

	void cancel_async() {
		resolver_.cancel();
		// socket_.cancel(socket_ec_);
		socket_.close(socket_ec_);
	}

	void clear_after_shutdown() { stream_.reset(); }

	// Called by the base class
	auto& stream() { return *stream_; }
	// Called by the base class
	// state& state() { return in_state; }

	// Start the asynchronous operation
	// Called by the base class
	void run(yield_context& yield) {
		auto process_error = [&](const char* stage_name) -> void {
			// connection clean up
			switch (in_state) {
				case conn_internal_state::ssl_handshake:
					socket_.shutdown(tcp::socket::shutdown_send, socket_ec_);
					socket_.close(socket_ec_);
					stream_.reset();
					in_state = conn_internal_state::socket_connect;
					break;
				case conn_internal_state::socket_connect:
					socket_.close(socket_ec_);
					stream_.reset();
					if (results_.size() > 0) {
						in_state = conn_internal_state::socket_connect;
					} else {
						in_state = conn_internal_state::resolve;
					}
					break;
				case conn_internal_state::resolve:
					break;
				default:
					break;
			}
			// print error and set in_state for some special cases
			if (socket_ec_ == boost::asio::error::operation_aborted &&
				this->s_state_ == session_state::shut_down) {
				// normal
			} else {
				// print error
				HTTP_CLIENT_FAIL(socket_ec_, stage_name);
				// resolving failed
				if (socket_ec_.category() == boost::asio::error::misc_category &&
					socket_ec_.value() == boost::asio::error::not_found) {
					in_state = conn_internal_state::resolve;
				}
				this->s_state_ = session_state::try_next_time;
			}
		};
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-label"
	dispatch:
		switch (in_state) {
			case conn_internal_state::resolve:
				goto resolve;
			case conn_internal_state::socket_connect:
				goto socket_connect;
			case conn_internal_state::ssl_handshake:
				goto ssl_handshake;
			case conn_internal_state::fatal_error:
			default:
				return;
		}
	resolve:
		results_ = resolver_.async_resolve(host_, port_, yield[socket_ec_]);
		if (socket_ec_) return process_error("resolve");
	socket_connect_enter:
		in_state = conn_internal_state::socket_connect;
	socket_connect:
		stream_.emplace(socket_, ctx_);
		boost::asio::async_connect(socket_, results_.begin(), results_.end(), yield[socket_ec_]);
		if (socket_ec_) return process_error("socket_connect");
	ssl_handshake_enter:
#pragma clang diagnostic pop
		in_state = conn_internal_state::ssl_handshake;
	ssl_handshake:
		cache.reuse();
		// Set SNI Hostname (many hosts need this to handshake successfully)
		if (!SSL_set_tlsext_host_name(stream_->native_handle(), host_.c_str())) {
			socket_ec_.assign(static_cast<int>(::ERR_get_error()), boost::asio::error::get_ssl_category());
			std::cerr << socket_ec_.message() << "\n";
			socket_.shutdown(tcp::socket::shutdown_send, socket_ec_);
			socket_.close(socket_ec_);
			this->s_state_ = session_state::shut_down;
		}
		socket_.set_option(tcp::no_delay(true));
		stream_->async_handshake(ssl::stream_base::client, yield[socket_ec_]);
		if (socket_ec_) return process_error("ssl_handshake");
		if (SSL_session_reused(stream_->native_handle())) {
			COUT_DEBUG("a ssl session was reused.\n");
		} else {
			// cache the new ssl session
			cache.save();
		}
		in_state = conn_internal_state::connected;
		// tell base class connection is ready
		this->s_state_ = session_state::working;
	}

	// Create the session
	https_session(io_context& io_ctx, ssl::context& ctx, flat_buffer&& buffer, char const* host,
				  char const* port)
		: session<https_session<Handler>, Handler>(io_ctx, std::move(buffer)), /*strand_(io_ctx),*/ ctx_(ctx),
		  host_(host), port_(port), resolver_(io_ctx), socket_(io_ctx) {}
};

/// @brief https with socks proxy flexibility
template <class Handler>
class https_session_flex : public session<https_session_flex<Handler>, Handler>,
						   public std::enable_shared_from_this<https_session_flex<Handler>>
{
public:
	// todo wrong type
	using stream_type = ssl::stream<tcp::socket&>;
	using base_type = session<https_session_flex<Handler>, Handler>;

private:
	ssl::context& ctx_;
	// io_context::strand strand_;
	string host_;
	string port_;
	tcp::resolver resolver_;
	bool proxy_valid{false};
	// host resolved once and save to results_ (reconnection will use saved resolving results)
	tcp::resolver::results_type results_;
	tcp::socket socket_;
	socks_url socks_url_;
	boost::optional<stream_type> stream_{};
	ssl_cache_boost_optional cache{stream_};
	conn_internal_state in_state{conn_internal_state::resolve};
	error_code socket_ec_;

public:
	string get_host() { return host_; }
	// Called by the base class
	// io_context::strand& strand() { return strand_; }
	// Called by the base class
	void close(yield_context& yield) {
		MSG_DEBUG("[http_coro] async_shutdown");
		https_close_method
	}

	void cancel_async() {
		resolver_.cancel();
		// socket_.cancel(socket_ec_);
		socket_.close(socket_ec_);
	}

	void clear_after_shutdown() { stream_.reset(); }

	// Called by the base class
	auto& stream() { return *stream_; }
	// Called by the base class
	// state& state() { return in_state; }

	// Start the asynchronous operation
	// Called by the base class
	void run(yield_context& yield) {
		auto socks_enabled = proxy_setting_valid();
		auto process_error = [&](const char* stage_name) -> void {
			// print error and set in_state for some special cases
			if (socket_ec_ == boost::asio::error::operation_aborted &&
				this->s_state_ == session_state::shut_down) {
				// normal
			} else {
				// connection clean up
				switch (in_state) {
					case conn_internal_state::ssl_handshake:
					case conn_internal_state::sock_handshake:
						socket_.shutdown(tcp::socket::shutdown_send, socket_ec_);
						socket_.close(socket_ec_);
						stream_.reset();
						in_state = conn_internal_state::socket_connect;
						break;
					case conn_internal_state::socket_connect:
						socket_.close(socket_ec_);
						stream_.reset();
						if (results_.size() > 0) {
							in_state = conn_internal_state::socket_connect;
						} else {
							in_state = conn_internal_state::resolve;
						}
						break;
					case conn_internal_state::resolve:
						break;
					default:
						break;
				}
				// print error
				HTTP_CLIENT_FAIL(socket_ec_, stage_name);
				// resolving failed
				if (socket_ec_.category() == boost::asio::error::misc_category &&
					socket_ec_.value() == boost::asio::error::not_found) {
					in_state = conn_internal_state::resolve;
				}
				this->s_state_ = session_state::try_next_time;
			}
		};
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-label"
	dispatch:
		switch (in_state) {
			case conn_internal_state::resolve:
				goto resolve;
			case conn_internal_state::socket_connect:
				goto socket_connect;
			case conn_internal_state::sock_handshake:
				goto sock_handshake;
			case conn_internal_state::ssl_handshake:
				goto ssl_handshake;
			case conn_internal_state::fatal_error:
				this->s_state_ = session_state::shut_down;
				return;
			default:
				return;
		}
	resolve:
		COUT_DEBUG("https_session_flex: resolve");
		COUT_DEBUG(endl);
		if (socks_enabled) {
			results_ = resolver_.async_resolve(socks_url_.host, socks_url_.port, yield[socket_ec_]);
		} else {
			results_ = resolver_.async_resolve(host_, port_, yield[socket_ec_]);
		}
		if (socket_ec_) return process_error("resolve");
	socket_connect_enter:
		in_state = conn_internal_state::socket_connect;
	socket_connect:
		// tcp connect to proxy or dst
		stream_.emplace(socket_, ctx_);
		boost::asio::async_connect(socket_, results_.begin(), results_.end(), yield[socket_ec_]);
		if (socket_ec_) return process_error("socket_connect");
	sock_handshake_enter:
		in_state = conn_internal_state::sock_handshake;
	sock_handshake:
		// socks procedure
		if (socks_enabled) {
			// todo proxy_setting_valid?
			int socks_version = socks_url_.scheme == "socks4" ? 4 : 5;
			socks_version = socks_url_.scheme == "socks5" ? 5 : 0;
			if (socks_version == 0) {
				HTTP_CLIENT_FAIL(error_code{}, "incorrect socks version");
				process_error("sock_handshake");
				socket_.shutdown(tcp::socket::shutdown_send, socket_ec_);
				socket_.close(socket_ec_);
				this->s_state_ = session_state::shut_down;
				return;
			}
			string host_s{host_};
			socks::async_handshake(socket_, host_s, static_cast<unsigned short>(std::atoi(port_.c_str())),
								   socks_version, socks_url_.username, socks_url_.password, true,
								   yield[socket_ec_]);
			if (socket_ec_) return process_error("sock_handshake");
		}
	ssl_handshake_enter:
#pragma clang diagnostic pop
		in_state = conn_internal_state::ssl_handshake;
	ssl_handshake:
		cache.reuse();
		// Set SNI Hostname (many hosts need this to handshake successfully)
		if (!SSL_set_tlsext_host_name(stream_->native_handle(), host_.c_str())) {
			socket_ec_.assign(static_cast<int>(::ERR_get_error()), boost::asio::error::get_ssl_category());
			std::cerr << socket_ec_.message() << "\n";
			socket_.shutdown(tcp::socket::shutdown_send, socket_ec_);
			socket_.close(socket_ec_);
			this->s_state_ = session_state::shut_down;
		}
		socket_.set_option(tcp::no_delay(true));
		stream_->async_handshake(ssl::stream_base::client, yield[socket_ec_]);
		if (socket_ec_) return process_error("ssl_handshake");
		if (SSL_session_reused(stream_->native_handle())) {
			COUT_DEBUG("a ssl session was reused.\n");
		} else {
			// cache the new ssl session
			cache.save();
		}
		in_state = conn_internal_state::connected;
		// tell base class connection is ready
		this->s_state_ = session_state::working;
	}

	// Create the session
	https_session_flex(io_context& io_ctx, ssl::context& ctx, flat_buffer&& buffer, char const* host,
					   char const* port, const std::string& socks_url_str)
		: session<https_session_flex<Handler>, Handler>(io_ctx, std::move(buffer)), /*strand_(io_ctx),*/
		  ctx_(ctx), host_(host), port_(port), resolver_(io_ctx), socket_(io_ctx) {
		parse_socks_url(socks_url_str, socks_url_);
		proxy_valid = is_socks_url_valid(socks_url_);
	}

	https_session_flex(io_context& io_ctx, ssl::context& ctx, flat_buffer&& buffer, char const* host,
					   char const* port, const socks_url& socks_url__)
		: session<https_session_flex<Handler>, Handler>(io_ctx, std::move(buffer)), /*strand_(io_ctx),*/
		  ctx_(ctx), host_(host), port_(port), resolver_(io_ctx), socket_(io_ctx), socks_url_(socks_url__) {
		proxy_valid = is_socks_url_valid(socks_url_);
	}

	https_session_flex(io_context& io_ctx, ssl::context& ctx, flat_buffer&& buffer, char const* host,
					   char const* port)
		: session<https_session_flex<Handler>, Handler>(io_ctx, std::move(buffer)), /*strand_(io_ctx),*/
		  ctx_(ctx), host_(host), port_(port), resolver_(io_ctx), socket_(io_ctx), socks_url_() {}

	bool proxy_setting_valid() { return proxy_valid; }
};

template <class Handler>
class control
{
public:
	using plain_t = plain_session_flex<Handler>;
	using ssl_t = https_session_flex<Handler>;
	// key: "host:port"
	using container_type_plain = std::unordered_map<string, std::shared_ptr<plain_t>>;
	// key: "host:port"
	using container_type_ssl = std::unordered_map<string, std::shared_ptr<ssl_t>>;
	static constexpr uint8_t WORKER_WAIT = 0;
	static constexpr uint8_t WORKER_RUN = 1;
	static constexpr uint8_t WORKER_EXIT = 2;

private:
	std::mutex m;
	std::mutex m_w; // mutex for waiting
	std::condition_variable cv;
	std::condition_variable cv_w; // cv for waiting
	uint8_t worker_thread_state{WORKER_WAIT};
	// indicate closing the thread
	bool closing{};
	socks_url proxy_setting;
	bool proxy_valid{};
	bool is_proxy_valid() {}
	std::unique_ptr<std::thread> tp{};
	boost::asio::io_context ioc_{};
	container_type_plain http_clients{};
	container_type_ssl http_ssl_clients{};
	ssl::context ctx{ssl::context::tlsv12_client}; // replace sslv23_client

public:
	control() { proxy_valid = is_socks_url_valid(proxy_setting); }
	control(socks_url proxy) : proxy_setting{proxy} { proxy_valid = is_socks_url_valid(proxy_setting); }
	void set_socks(const socks_url& proxy) { proxy_setting = proxy; }
	const socks_url& get_socks() { return proxy_setting; }
	control(const control&) = delete;
	control& operator=(const control&) = delete;
	control(control&&) = delete;
	control& operator=(control&&) = delete;
	auto get_io() { return &ioc_; }

	void init_worker_thread() {
		auto bind_ = [& ioc = this->ioc_, &m = this->m, &cv = this->cv,
					  &worker_thread_state = this->worker_thread_state]() {
			COUT_DEBUG("[worker] worker thread start");
			COUT_DEBUG(endl);
			ioc.run();
			for (;;) {
				COUT_DEBUG("[worker] worker thread wait for condition variable");
				COUT_DEBUG(endl);
				std::unique_lock<std::mutex> lk(m);
				cv.wait(lk, [&worker_thread_state] {
					return worker_thread_state != WORKER_WAIT; /*take action when it's not WORKER_WAIT*/
				});
				// exclusive access to worker_thread_state must be guaranteed
				if (worker_thread_state != WORKER_EXIT) worker_thread_state = WORKER_WAIT; /*reset*/
				lk.unlock();
				cv.notify_one();
				COUT_DEBUG("[worker] worker thread unlock.");
				COUT_DEBUG(endl);
				if (worker_thread_state == WORKER_EXIT) break;
				ioc.restart();
				ioc.run();
			}
			COUT_DEBUG("[worker] worker thread exit");
			COUT_DEBUG(endl);
		};
		tp.reset(new std::thread(bind_));
	}

	void init_http_client(const char* host, const char* port, bool socks, bool ssl, bool preheat = true) {
		if (ssl) {
			if (socks) {
				auto p = http_ssl_clients.try_emplace(
					string{host} + ':' + port,
					std::make_shared<ssl_t>(ioc_, ctx, flat_buffer{}, host, port, proxy_setting));
				if (preheat && p.second) {
					boost::asio::post(ioc_, std::bind(&ssl_t::preheat, p.first->second));
				}
			} else {
				auto p = http_ssl_clients.try_emplace(
					string{host} + ':' + port, std::make_shared<ssl_t>(ioc_, ctx, flat_buffer{}, host, port));
				if (preheat && p.second) {
					boost::asio::post(ioc_, std::bind(&ssl_t::preheat, p.first->second));
				}
			}
		} else {
			if (socks) {
				auto p = http_clients.try_emplace(
					string{host} + ':' + port,
					std::make_shared<plain_t>(ioc_, flat_buffer{}, host, port, proxy_setting));
				if (preheat && p.second) {
					boost::asio::post(ioc_, std::bind(&plain_t::preheat, p.first->second));
				}
			} else {
				auto p = http_clients.try_emplace(string{host} + ':' + port,
												  std::make_shared<plain_t>(ioc_, flat_buffer{}, host, port));
				if (preheat && p.second) {
					boost::asio::post(ioc_, std::bind(&plain_t::preheat, p.first->second));
				}
			}
		}
	}

	/// @brief accept a task with dst_host http_client (post a new session creation task for dst_host if
	/// dst_host unfound)
	/// @param read_handler must be CopyConstructible since it will be std::bind to session::accept_task. if
	/// read_handler is MoveConstructible, you can move it during binding by pass a prvalue reference to
	/// task_enqueue. because of the limit and requirement of std::bind (it does not support the idea of
	/// one_shot_bind @link http://talesofcpp.fusionfenix.com/post-14/true-story-moving-past-bind), the
	/// underlying session::accept_task (which want to manage the lifetime of handler instance) will still
	/// copy read_handler than move it.
	template <class Handler_arg, uint8_t task_limit_ = 30>
	void task_enqueue(const char* dst_host, const char* dst_port, bool ssl, request_type&& req,
					  Handler_arg&& read_handler) {
		static_assert(std::is_same_v<Handler, std::decay_t<Handler_arg>>, "Handler type mismatch");
		static_assert(std::is_copy_constructible_v<std::decay_t<Handler_arg>>,
					  "read_handler must be CopyConstructible");
		auto key_ = string{dst_host} + ':' + dst_port;
		if (ssl) {
			auto ret = http_ssl_clients.find(key_);
			if (ret == http_ssl_clients.end()) {
				if (proxy_valid) {
					auto result = http_ssl_clients.try_emplace(
						key_,
						std::make_shared<ssl_t>(ioc_, ctx, flat_buffer{}, dst_host, dst_port, proxy_setting));
					if (!result.second)
						throw http_client_error{string{"fail to init http client for host: "} + key_};
					ret = result.first;
				} else {
					auto result = http_ssl_clients.try_emplace(
						key_, std::make_shared<ssl_t>(ioc_, ctx, flat_buffer{}, dst_host, dst_port));
					if (!result.second)
						throw http_client_error{string{"fail to init http client for host: "} + key_};
					ret = result.first;
				}
			}
			boost::asio::post(ret->second->strand(),
							  std::bind(&ssl_t::template accept_task<task_limit_>, ret->second,
										std::forward<Handler_arg>(read_handler), std::move(req)));
		} else {
			auto ret = http_clients.find(key_);
			if (ret == http_clients.end()) {
				if (proxy_valid) {
					auto result = http_clients.try_emplace(
						key_,
						std::make_shared<plain_t>(ioc_, flat_buffer{}, dst_host, dst_port, proxy_setting));
					if (!result.second)
						throw http_client_error{string{"fail to init http client for host: "} + key_};
					ret = result.first;
				} else {
					auto result = http_clients.try_emplace(
						key_, std::make_shared<plain_t>(ioc_, flat_buffer{}, dst_host, dst_port));
					if (!result.second)
						throw http_client_error{string{"fail to init http client for host: "} + key_};
					ret = result.first;
				}
			}
			boost::asio::post(ret->second->strand(),
							  std::bind(&plain_t::template accept_task<task_limit_>, ret->second,
										std::forward<Handler_arg>(read_handler), std::move(req)));
		}
	}

	template <class Handler_arg, uint8_t task_limit_ = 30>
	void task_enqueue(const URL& u, Handler_arg&& read_handler) {
		static_assert(std::is_same_v<Handler, std::decay_t<Handler_arg>>, "Handler type mismatch");
		if (u.port.empty()) throw http_client_error{"URL port number is empty."};
		return task_enqueue<Handler_arg, task_limit_>(
			u.host.c_str(), u.port.c_str(), u.scheme == "https" ? true : false, URL_2_http_request(u),
			std::forward<Handler>(read_handler));
	}
	/// @brief spawn a coroutine to accept task with delay
	template <class Handler_arg, uint8_t task_limit_ = 30>
	void task_enqueue_delay(const char* dst_host, const char* dst_port, bool ssl, request_type req,
							Handler_arg&& read_handler, uint32_t delay_milliseconds) {
		static_assert(std::is_same_v<Handler, std::decay_t<Handler_arg>>, "Handler type mismatch");
		static_assert(std::is_copy_constructible_v<std::decay_t<Handler_arg>>,
					  "read_handler must be CopyConstructible");
		auto key_ = string{dst_host} + ':' + dst_port;
		if (ssl) {
			auto ret = http_ssl_clients.find(key_);
			if (ret == http_ssl_clients.end()) {
				if (proxy_valid) {
					auto result = http_ssl_clients.try_emplace(
						key_, std::make_shared<ssl_t>(ioc_, ctx, flat_buffer{}, dst_host,
													  (ssl ? PORT_HTTPS : PORT_HTTP), proxy_setting));
					if (!result.second)
						throw http_client_error{string{"fail to init http client for host: "} + key_};
					ret = result.first;
				} else {
					auto result = http_ssl_clients.try_emplace(
						key_, std::make_shared<ssl_t>(ioc_, ctx, flat_buffer{}, dst_host,
													  (ssl ? PORT_HTTPS : PORT_HTTP)));
					if (!result.second)
						throw http_client_error{string{"fail to init http client for host: "} + key_};
					ret = result.first;
				}
			}
			boost::asio::spawn(ret->second->strand(),
							   std::bind(&ssl_t::template accept_task_delay<task_limit_>, ret->second,
										 std::forward<Handler>(read_handler), std::move(req),
										 delay_milliseconds, std::placeholders::_1));
		} else {
			auto ret = http_clients.find(key_);
			if (ret == http_clients.end()) {
				if (proxy_valid) {
					auto result = http_clients.try_emplace(
						key_, std::make_shared<plain_t>(ioc_, flat_buffer{}, dst_host,
														(ssl ? PORT_HTTPS : PORT_HTTP), proxy_setting));
					if (!result.second)
						throw http_client_error{string{"fail to init http client for host: "} + key_};
					ret = result.first;
				} else {
					auto result = http_clients.try_emplace(
						key_, std::make_shared<plain_t>(ioc_, flat_buffer{}, dst_host,
														(ssl ? PORT_HTTPS : PORT_HTTP)));
					if (!result.second)
						throw http_client_error{string{"fail to init http client for host: "} + key_};
					ret = result.first;
				}
			}
			boost::asio::spawn(ret->second->strand(),
							   std::bind(&plain_t::template accept_task_delay<task_limit_>, ret->second,
										 std::forward<Handler>(read_handler), std::move(req),
										 delay_milliseconds, std::placeholders::_1));
		}
	}

	template <class Handler_arg, uint8_t task_limit_ = 30>
	void task_enqueue_delay(const URL& u, Handler&& read_handler, uint32_t delay_milliseconds) {
		if (u.port.empty()) throw http_client_error{"URL port number is empty."};
		return task_enqueue_delay<Handler_arg, task_limit_>(
			u.host.c_str(), u.port.c_str(), u.scheme == "https" ? true : false, URL_2_http_request(u),
			std::forward<Handler>(read_handler), delay_milliseconds);
	}
	/// @brief notify worker thread to start the actual io task. user normally register numbers of task and
	/// then call this method to start actual work
	void notify_thread() {
		auto check = ioc_.stopped();
		if (check) /*ioc stopped*/ {
			{
				std::lock_guard<std::mutex> lk(m);
				worker_thread_state = WORKER_RUN;
				COUT_DEBUG("main() signals data ready for processing");
				COUT_DEBUG(endl);
			}
			cv.notify_one();
		}
		// ioc running.
	}

	void close_all() {
		closing = true;
		// shutdown http clients
		COUT_DEBUG("shutdown http clients");
		COUT_DEBUG(endl);
		for (auto i : http_clients) {
			boost::asio::post(ioc_, std::bind(&plain_t::require_shut_down, i.second));
		}
		for (auto i : http_ssl_clients) {
			boost::asio::post(ioc_, std::bind(&ssl_t::require_shut_down, i.second));
		}
		notify_thread();
		// close thread
		{
			std::lock_guard<std::mutex> lk(m);
			worker_thread_state = WORKER_EXIT;
			COUT_DEBUG("main() signals worker to exit");
			COUT_DEBUG(endl);
		}
		cv.notify_one();
		tp->join();
	}

	void clear_all() {
		http_clients.clear();
		http_ssl_clients.clear();
	}

	void restart() {
		close_all();
		clear_all();
		// clear the stat
		closing = false;
		init_worker_thread();
	}

	~control() {
		if (!closing) {
			COUT_DEBUG("~control() close_all() invoked");
			COUT_DEBUG(endl);
			close_all();
		}
	}
};

} // namespace online
} // namespace lexicon

#endif // HTTP_CLIENT3_H

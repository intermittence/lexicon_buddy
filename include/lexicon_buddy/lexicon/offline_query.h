//
//  offline_query.h
//  lexicon_buddy
//
//  Created by Lohengrin on 1/10/2019 09:34:08.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef offline_query_H
#define offline_query_H
#include <lexicon_buddy/utilities/utilities.hpp>
#include <lexicon_buddy/lexicon/stardictcontainer.h>
#include <lexicon_buddy/lexicon/mdxContainer.hpp>
#include <vector>
#include <map>
#include <marisa.h>
namespace lexicon
{

namespace buddy
{
template <class offline_handler, class online_hanlder>
class manager;
}

namespace offline_query
{
using std::string;
template <class dict_container>
class query_traits;

template <unsigned idx_iter_entry_type>
class query_traits<lexicon::stardict::dict_container<idx_iter_entry_type>>
{
public:
	using offset_type = typename lexicon::stardict::dict_container<idx_iter_entry_type>::offset_type;
	// offset and size
	// {{word_data_offset(32/64bit),word_data_size(32bit)}, ..., ...}
	using coordinate_data_type = struct coordinate_data_type_
	{
		offset_type offset;
		uint32_t size;
	};
};

template <class T>
class query_traits<lexicon::mdx::mdx_data<T>>
{
public:
	using offset_type = typename lexicon::mdx::mdx_data<T>::offset_type;
	using coordinate_data_type = offset_type;
};

template <class dict_container_t, template <class> class traits = query_traits>
// trie and coordinate data are mmaped
class search_mmap
{
    template <class offline_handler, class online_hanlder>
	friend class buddy::manager;

public:
	using dict_container_type = dict_container_t;
	using offset_type = typename traits<dict_container_t>::offset_type;
	using coordinate_data_type = typename traits<dict_container_t>::coordinate_data_type;
	using trie_type = marisa::Trie;
	using coordinate_container_type = util::mmap_w;
	using key_type = string /*utf-8*/;
	using result_type = std::map<key_type, size_t>;
	using content_type = string; /*utf-8*/

private:
	coordinate_container_type coor;
	trie_type trie;

public:
	const trie_type* trie_ptr() { return &trie; }
	const coordinate_container_type* coordinate_ptr() { return &coor; }
	search_mmap(const char* trie_, const char* coordinate_) : coor{coordinate_} {
		trie.load(trie_);
#ifndef NDEBUG
		std::cout << "trie loaded, num_keys: " << trie.num_keys() << " num_nodes: " << trie.num_nodes()
				  << std::endl;
#endif
	}
	const char* coor_data() const { return coor.data(); }
	/// @param key utf-8 encoded key
	void prefix_search(const char* key, result_type& r) const {
		marisa::Agent agent;
		agent.set_query(key);
		r.clear();
		// r.dat = coor.data();
		while (trie.common_prefix_search(agent)) {
			key_type key(agent.key().ptr(), agent.key().length());
			r.emplace(key, agent.key().id());
		}
	}
	/// @param key utf-8 encoded key
	void predict(const char* key, result_type& r, uint8_t limit = 8) const {
		decltype(limit) count{};
		marisa::Agent agent;
		agent.set_query(key);
		r.clear();
		// r.dat = coor.data();
		while (trie.predictive_search(agent) && count < limit) {
			key_type key(agent.key().ptr(), agent.key().length());
			r.emplace(key, agent.key().id());
			++count;
		}
	}
};
// deprecated
inline stardict::dict_record id_to_data(const search_mmap<stardict::dict_container<0>>& search,
										stardict::dict_container<0>& container, size_t id) {
	auto coor_ptr = search.coor_data() + id * 8;
	return container.dictzip_random_decom(util::char_to_uint32(coor_ptr), util::char_to_uint32(coor_ptr + 4));
}
// deprecated
inline stardict::dict_record id_to_data(const search_mmap<stardict::dict_container<1>>& search,
										stardict::dict_container<1>& container, size_t id) {
	auto coor_ptr = search.coor_data() + id * 12;
	return container.dictzip_random_decom(util::char_to_uint64(coor_ptr), util::char_to_uint32(coor_ptr + 8));
}

// deprecated
inline mdx::dict_record id_to_data(const search_mmap<mdx::mdx_data_mmap>& search,
								   mdx::mdx_data_mmap& container, size_t id) {
	// TODO: handle data compression
	mdx::query_policy<mdx::memory, false> q;
	auto coor_ptr = search.coor_data() + id * 8;
	return reinterpret_cast<mdx::mdx_guest<mdx::memory, false>&>(container)._query(
		util::char_to_uint32(coor_ptr), true);
}
// deprecated
inline mdx::dict_record id_to_data(const search_mmap<mdx::mdx_data_std_stream>& search,
								   mdx::mdx_data_std_stream& container, size_t id) {
	mdx::query_policy<mdx::std_stream, false> q;
	auto coor_ptr = search.coor_data() + id * 8;
	return reinterpret_cast<mdx::mdx_guest<mdx::std_stream, false>&>(container)._query(
		util::char_to_uint32(coor_ptr), true);
}

template <unsigned idx_iter_entry_type>
stardict::dict_record_v
id_to_data_flex(const search_mmap<stardict::dict_container<idx_iter_entry_type>>& search,
				stardict::dict_container<idx_iter_entry_type>& container, size_t id) {
	using word_data_offset_type = typename stardict::idx_iter<idx_iter_entry_type>::offset_type;
	using offset_type = word_data_offset_type;
	using word_data_size_type = typename stardict::idx_iter<idx_iter_entry_type>::size_type;
	using num_type = uint16_t;
	stardict::dict_record_v results{};
	offset_type offset;
	num_type num;
	auto p0 = search.coor_data() + id * (sizeof(offset_type) + sizeof(num_type));
	std::copy(p0, p0 + sizeof(offset_type), (char*)&offset);
	auto p1 = p0 + sizeof(offset_type);
	std::copy(p1, p1 + sizeof(num_type), (char*)&num);
	auto coor_ptr = search.coor_data() + offset;
	for (; num > 0; --num) {
		word_data_offset_type word_offset;
		word_data_size_type word_size;
		std::copy(coor_ptr, coor_ptr + sizeof(word_data_offset_type), (char*)&word_offset);
		coor_ptr += sizeof(word_data_offset_type);
		std::copy(coor_ptr, coor_ptr + sizeof(word_data_size_type), (char*)&word_size);
		coor_ptr += sizeof(word_data_size_type);
		results.emplace_back(container.dictzip_random_decom(word_offset, word_size));
	}
	return results;
}

template <class storage>
mdx::dict_record_v id_to_data_flex(const search_mmap<mdx::mdx_data<storage>>& search,
								   mdx::mdx_data<storage>& container, size_t id) {
	mdx::dict_record_v results{};
	using num_type = uint16_t;
	if (container.is_version_2_0()) {
		using offset_type = uint64_t;
		auto ptr = search.coor_data() + id * (sizeof(offset_type) + sizeof(num_type));
		offset_type offset = util::iter_to_integer_nbytes<sizeof(offset_type)>(ptr);
		ptr += sizeof(offset_type);
		offset_type num = util::iter_to_integer_nbytes<sizeof(num_type)>(ptr);
		auto mdx_offset_ptr = search.coor_data() + offset;
		for (; num > 0; --num) {
			results.emplace_back(reinterpret_cast<mdx::mdx_guest<storage, false>&>(container)._query(
				util::iter_to_integer_nbytes<sizeof(offset_type)>(mdx_offset_ptr), true));
			mdx_offset_ptr += sizeof(offset_type);
		}
	} else {
		using offset_type = uint32_t;
		auto ptr = search.coor_data() + id * (sizeof(offset_type) + sizeof(num_type));
		offset_type offset = util::iter_to_integer_nbytes<sizeof(offset_type)>(ptr);
		ptr += sizeof(offset_type);
		offset_type num = util::iter_to_integer_nbytes<sizeof(num_type)>(ptr);
		auto mdx_offset_ptr = search.coor_data() + offset;
		for (; num > 0; --num) {
			results.emplace_back(reinterpret_cast<mdx::mdx_guest<storage, false>&>(container)._query(
				util::iter_to_integer_nbytes<sizeof(offset_type)>(mdx_offset_ptr), true));
			mdx_offset_ptr += sizeof(offset_type);
		}
	}
	return results;
}

} // namespace offline_query
} // namespace lexicon

#endif // offline_query_H

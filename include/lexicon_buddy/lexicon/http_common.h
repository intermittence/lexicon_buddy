//
//  http_common.h
//  lexicon_buddy
//
//  Created by Lohengrin on 2019/2/6.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#ifndef HTTP_COMMON_H
#define HTTP_COMMON_H

#include <iomanip>
#include <lexicon_buddy/lexicon_buddy_export.h>
#include <iostream>
#include <string_view>
#include <boost/asio/ssl.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
//#include <boost/asio/ssl/error.hpp>
//#include <boost/asio/ssl/stream.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>

#ifndef HTTP_CLIENT_FAIL
#define HTTP_CLIENT_FAIL(ec, what) std::cerr << what << ": " << ec.message() << "\n"
#endif

#ifndef HTTP_CLIENT_MSG
#define HTTP_CLIENT_MSG(what) std::cerr << what << "\n"
#endif

// todo debug macro

namespace lexicon
{
namespace online
{

constexpr char PORT_HTTP[] = "80";
constexpr char PORT_HTTPS[] = "443";

using std::string;
using std::string_view;
using tcp = boost::asio::ip::tcp; // from <boost/asio/ip/tcp.hpp>
namespace ssl = boost::asio::ssl; // from <boost/asio/ssl.hpp>
namespace asio = boost::asio;
namespace http = boost::beast::http; // from <boost/beast/http.hpp>

inline void fail(boost::system::error_code ec, char const* what) {
	std::cerr << what << ": " << ec.message() << "\n";
}

// ssl_session_cache do not copy ssl session. it only add a reference to ssl session
// and change the reference count
class ssl_session_cache
{
	ssl::stream<tcp::socket>& strm_;
	/// cache hold and manage a reference count
	SSL_SESSION* cache{};

public:
	ssl_session_cache(ssl::stream<tcp::socket>& strm) : strm_{strm} {}
	void save() {
		release();
		// reference count +1
		cache = ::SSL_get1_session(strm_.native_handle());
	}
	void reuse() {
		::SSL_clear(strm_.native_handle());
		::SSL_set_session(strm_.native_handle(), cache);
	}
	void release() {
		if (cache)
			// reference count -1, release when count=0
			::SSL_SESSION_free(cache);
	}
	~ssl_session_cache() { release(); }
};

class ssl_session_cache_optional
{
	boost::optional<ssl::stream<tcp::socket>>& strm_;
	/// cache hold and manage a reference count
	SSL_SESSION* cache{};

public:
	ssl_session_cache_optional(boost::optional<ssl::stream<tcp::socket>>& strm) : strm_{strm} {}
	void save() {
		release();
		// reference count +1
		cache = ::SSL_get1_session(strm_->native_handle());
	}
	void release() {
		if (cache)
			// reference count -1, release when count=0
			::SSL_SESSION_free(cache);
	}
	void reuse() {
		// SSL_clear(strm_->native_handle());
		if (cache) ::SSL_set_session(strm_->native_handle(), cache);
	}
	~ssl_session_cache_optional() { release(); }
};

template <class ssl_stream_optional = boost::optional<ssl::stream<tcp::socket&>>>
class ssl_cache
{
	ssl_stream_optional& strm_;
	/// cache hold and manage a reference count
	SSL_SESSION* cache{};

public:
	ssl_cache(ssl_stream_optional& strm) : strm_{strm} {}
	void save() {
		release();
		// reference count +1
		cache = ::SSL_get1_session(strm_->native_handle());
	}
	void release() {
		if (cache)
			// reference count -1, release when count=0
			::SSL_SESSION_free(cache);
	}
	void reuse() {
		// SSL_clear(strm_->native_handle());
		if (cache) ::SSL_set_session(strm_->native_handle(), cache);
	}
	~ssl_cache() { release(); }
};

using ssl_cache_boost_optional = ssl_cache<boost::optional<ssl::stream<tcp::socket&>>>;

LEXICON_BUDDY_EXPORT inline void MSG_DEBUG_F(const char* msg) {
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	std::time_t now_t = std::chrono::system_clock::to_time_t(now);
	std::cout << '[' << std::put_time(std::localtime(&now_t), "%F %T") << "] " << msg << std::endl;
}
#ifndef MSG_DEBUG
#ifndef NDEBUG
#define MSG_DEBUG(msg) MSG_DEBUG_F((msg))
#else
#define MSG_DEBUG(msg) ((void)0)
#endif
#endif

#ifndef COUT_DEBUG
#ifndef NDEBUG
#define COUT_DEBUG(msg) std::cout << (msg)
#else
#define COUT_DEBUG(msg) ((void)0)
#endif
#endif

// exception types
class http_client_error : public std::runtime_error
{
	// use runtime_error constructor
	using std::runtime_error::runtime_error;
};

class control_error : public std::runtime_error
{
	// use runtime_error constructor
	using std::runtime_error::runtime_error;
};

struct URL
{
	string scheme;
	string userinfo;
	string host;
	string port;
	string path;
	string query;
	string fragment;
};

// https://en.wikipedia.org/wiki/URL
// URI = scheme:[//authority]path[?query][#fragment]
// authority = [userinfo@]host[:port]
/// @warning free function urlstr_2_URL do not set port number with scheme's default when url string does not
/// specify port number
URL urlstr_2_URL(string_view urlstr, bool& valid);
URL urlstr_2_URL(string_view urlstr);

http::request<http::empty_body> URL_2_http_request(const URL& url);

using response_type = http::response<http::string_body>;

} // namespace online
} // namespace lexicon
#endif // HTTP_COMMON_H

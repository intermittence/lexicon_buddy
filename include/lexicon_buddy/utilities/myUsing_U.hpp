//
//  myUsing.hpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/03/12.
//  Copyright © 2018 Lohengrin. All rights reserved.
//

#ifndef myUsing_U_h
#define myUsing_U_h
#include <iostream>
#include <bitset>
using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::wcout;
using std::wstring;
using std::bitset;
using std::ifstream;
using std::ofstream;
#endif /* myUsing_U_h */

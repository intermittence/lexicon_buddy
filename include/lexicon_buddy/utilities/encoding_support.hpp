//
// Created by Lohengrin on 2018/05/4.
//

#ifndef LEXICON_BUDDY_ENCODING_SUPPORT_HPP
#define LEXICON_BUDDY_ENCODING_SUPPORT_HPP

#include <exception>
#include <iostream>
#include <string>
#include <type_traits>
#include <lexicon_buddy/utilities/lang_platform.hpp>
#include <unicode/ucnv.h>
namespace encoding_su
{
using std::string;

template <class encoding_str>
bool encoding_equal(const string& str) {
	if (str == encoding_str::NAME || str == encoding_str::ALIAS0 || str == encoding_str::ALIAS1 ||
		str == encoding_str::ALIAS2 || str == encoding_str::ALIAS3 || str == encoding_str::ALIAS4) {
		return true;
	}
	return false;
}

struct utf8_info
{
	constexpr static char NAME[] = "UTF-8";
	constexpr static char ALIAS0[] = "utf-8";
	constexpr static char ALIAS1[] = "UTF8";
	constexpr static char ALIAS2[] = "utf8";
	constexpr static char ALIAS3[] = "";
	constexpr static char ALIAS4[] = "";
	constexpr static unsigned UNIT = 1;
#ifdef __APPLE__
	constexpr static char locale_[] = "en_US.UTF-8";
#else
	constexpr static char locale_[] = "";
#endif
	friend bool encoding_equal<utf8_info>(const string&);
};

// for mdx file, UTF-16 means UTF-16LE
struct mdx_utf16_info
{
	constexpr static char NAME[] = "UTF-16LE";
	constexpr static char ALIAS0[] = "utf-16";
	constexpr static char ALIAS1[] = "UTF-16";
	constexpr static char ALIAS2[] = "utf16";
	constexpr static char ALIAS3[] = "UTF16";
	constexpr static char ALIAS4[] = "utf-16LE";
	constexpr static unsigned UNIT = 2;
	constexpr static char locale_[] = "";
	friend bool encoding_equal<mdx_utf16_info>(const string&);
};

struct utf16_info
{
	constexpr static char NAME[] = "UTF-16";
	constexpr static char ALIAS0[] = "utf-16";
	constexpr static char ALIAS1[] = "UTF16";
	constexpr static char ALIAS2[] = "utf16";
	constexpr static char ALIAS3[] = "";
	constexpr static char ALIAS4[] = "";
	constexpr static unsigned UNIT = 2;
	constexpr static char locale_[] = "";
	friend bool encoding_equal<mdx_utf16_info>(const string&);
};

struct utf16LE_info
{
	constexpr static char NAME[] = "UTF-16LE";
	constexpr static char ALIAS0[] = "utf-16LE";
	constexpr static char ALIAS1[] = "UTF16LE";
	constexpr static char ALIAS2[] = "utf16LE";
	constexpr static char ALIAS3[] = "";
	constexpr static char ALIAS4[] = "";
	constexpr static unsigned UNIT = 2;
	constexpr static char locale_[] = "";
	friend bool encoding_equal<mdx_utf16_info>(const string&);
};

struct utf16BE_info
{
	constexpr static char NAME[] = "UTF-16BE";
	constexpr static char ALIAS0[] = "utf-16BE";
	constexpr static char ALIAS1[] = "UTF16BE";
	constexpr static char ALIAS2[] = "utf16BE";
	constexpr static char ALIAS3[] = "";
	constexpr static char ALIAS4[] = "";
	constexpr static unsigned UNIT = 2;
	constexpr static char locale_[] = "";
	friend bool encoding_equal<mdx_utf16_info>(const string&);
};

struct gbk_info
{
	constexpr static char NAME[] = "GBK";
	constexpr static char ALIAS0[] = "gbk";
	constexpr static char ALIAS1[] = "";
	constexpr static char ALIAS2[] = "";
	constexpr static char ALIAS3[] = "";
	constexpr static char ALIAS4[] = "";
	constexpr static unsigned UNIT = 1;
	constexpr static char locale_[] = "";
	friend bool encoding_equal<gbk_info>(const string&);
};

/*about BIG5

https://en.wikipedia.org/wiki/Big5
Strictly speaking, the Big5 encoding contains only DBCS characters. However, in practice, the Big5 codes
are always used together with an unspecified, system-dependent single-byte character set (ASCII, or an
8-bit character set such as code page 437), so that you will find a mix of DBCS characters and single-byte
characters in Big5-encoded text.

The numerical value of individual Big5 codes are frequently given as a 4-digit hexadecimal number, which
describes the two bytes that comprise the Big5 code as if the two bytes were a big endian representation of
a 16-bit number.*/
struct big5_info
{
	constexpr static char NAME[] = "BIG5";
	constexpr static char ALIAS0[] = "big5";
	constexpr static char ALIAS1[] = "";
	constexpr static char ALIAS2[] = "";
	constexpr static char ALIAS3[] = "";
	constexpr static char ALIAS4[] = "";
	constexpr static unsigned UNIT = 1;
	constexpr static char locale_[] = "";
	friend bool encoding_equal<big5_info>(const string&);
};

struct iso8859_1_info
{
	constexpr static char NAME[] = "ISO8859-1";
	constexpr static char ALIAS0[] = "iso8859-1";
	constexpr static char ALIAS1[] = "ISO-8859-1";
	constexpr static char ALIAS2[] = "";
	constexpr static char ALIAS3[] = "";
	constexpr static char ALIAS4[] = "";
	constexpr static unsigned UNIT = 1;
	constexpr static char locale_[] = "";
	friend bool encoding_equal<iso8859_1_info>(const string&);
};

struct encoding_info
{
	const char* const NAME;
	const unsigned UNIT;
	const char* const locale_;
};

// input encoding name
// ouput encoding UNIT

/*template <class... Ts>
decltype(typename std::enable_if<sizeof...(Ts)==0>::type()) testvar(){}*/

template <class str_type, class T0>
// 1 Tx template parameter
encoding_info _get_encoding_info(str_type&& str) {
	if (encoding_equal<T0>(str)) return encoding_info{T0::NAME, T0::UNIT, T0::locale_};
	return encoding_info{};
}

template <class str_type, class T0, class T1, class... Ts>
// >=2 Tx template parameters
encoding_info _get_encoding_info(str_type&& str) {
	if (encoding_equal<T0>(str)) return encoding_info{T0::NAME, T0::UNIT, T0::locale_};
	return _get_encoding_info<str_type, T1, Ts...>(std::forward<str_type>(str));
};

template <
	// class str_type,
	class T0, class... Ts>
// the struct is necessary for "using"
struct en_caller
{
	template <class str_type>
	static encoding_info get_encoding_info(str_type&& str) {
		static_assert(
			std::is_same<typename std::remove_cv_t<std::remove_reference_t<str_type>>, std::string>::value,
			"get_encoding_info only accept std::string para");
		return _get_encoding_info<str_type, T0, Ts...>(std::forward<str_type>(str));
	}
	template <class str_type>
	static unsigned get_byte_unit(str_type&& str) {
		static_assert(
			std::is_same<typename std::remove_cv_t<std::remove_reference_t<str_type>>, std::string>::value,
			"get_encoding_info only accept std::string para");
		return get_encoding_info(std::forward<str_type>(str)).UNIT;
	}
};

// special class for mdx encoding name rule (UTF-16 means UTF-16LE)
using encoding_check_mdx = en_caller<utf8_info, mdx_utf16_info, gbk_info, big5_info, iso8859_1_info>;
//  general class for encoding name rule
using encoding_check =
	en_caller<utf8_info, utf16_info, utf16LE_info, utf16BE_info, gbk_info, big5_info, iso8859_1_info>;

#if Unix_Style
inline void iconv_errmsg(size_t ret) {
	if (ret == static_cast<size_t>(-1)) {
		switch (errno) {
			case E2BIG:
				std::cerr << "There is not sufficient room at *outbuf." << std::endl;
				break;
			case EILSEQ:
				std::cerr << "An invalid multibyte sequence has been encountered in "
							 "the input."
						  << std::endl;
				break;
			case EINVAL:
				std::cerr << "An incomplete multibyte sequence has been "
							 "encountered in "
							 "the input."
						  << std::endl;
				break;
			default:
				break;
		}
	}
}

inline void iconv_errthrow(size_t ret) {
	if (ret == static_cast<size_t>(-1)) {
		switch (errno) {
			case E2BIG:
				throw std::runtime_error{"There is not sufficient room at *outbuf."};
			case EILSEQ:
				throw std::runtime_error{"An invalid multibyte sequence has been encountered in "
										 "the input."};
			case EINVAL:
				throw std::runtime_error{"An incomplete multibyte sequence has been "
										 "encountered in "
										 "the input."};
			default:
				break;
		}
	}
}
#endif

class icu_convertor_error : public std::runtime_error
{
	using std::runtime_error::runtime_error;
};

class icu_convertor_utf8
{
	UErrorCode err0{};
	UErrorCode err1{};
	UErrorCode errX{};
	UConverter* to_utf16{};
	UConverter* utf16_to_utf8{};
	//	std::unique_ptr<char[]> pivot_buf{};
	//	uint64_t buf_size{};

	void open_utf16_to_utf8() {
		if (!utf16_to_utf8) {
			ucnv_close(utf16_to_utf8);
			utf16_to_utf8 = ucnv_open(utf8_info::NAME, &err1);
			if (U_FAILURE(err1)) {
				throw std::runtime_error{u_errorName(err1)};
			}
		}
	}

	void err_msg(UErrorCode e) const noexcept { std::cerr << "ICU error: " << u_errorName(e) << std::endl; }

	void err_throw(UErrorCode e) const { throw icu_convertor_error{string{"ICU error: "} + u_errorName(e)}; }

	void be_moved() {
		to_utf16 = nullptr;
		utf16_to_utf8 = nullptr;
	}

public:
	icu_convertor_utf8(const char* encoding) {
		open_utf16_to_utf8();
		open(encoding);
	}

	icu_convertor_utf8(const string& encoding) {
		open_utf16_to_utf8();
		open(encoding.c_str());
	}

	icu_convertor_utf8(icu_convertor_utf8&& arg)
		: err0{arg.err0}, err1{arg.err1}, errX{arg.errX}, to_utf16{arg.to_utf16}, utf16_to_utf8{
																					  arg.utf16_to_utf8} {
		arg.be_moved();
	}

	icu_convertor_utf8& operator=(icu_convertor_utf8&& arg) {
		err0 = arg.err0;
		err1 = arg.err1;
		errX = arg.errX;
		to_utf16 = arg.to_utf16;
		utf16_to_utf8 = arg.utf16_to_utf8;
		arg.be_moved();
		return *this;
	}

	icu_convertor_utf8(const icu_convertor_utf8& arg) = delete;
	icu_convertor_utf8& operator=(const icu_convertor_utf8& arg) = delete;

	icu_convertor_utf8() { open_utf16_to_utf8(); }

	void open(const char* encoding) {
		if (!to_utf16) {
			ucnv_close(to_utf16);
			to_utf16 = ucnv_open(encoding, &err0);
			if (U_FAILURE(err0)) {
				throw std::runtime_error{u_errorName(err0)};
			}
		}
	}

	bool to_utf8(char** target, const char* targetLimit, const char** source,
				 const char* sourceLimit) noexcept {
		ucnv_convertEx(utf16_to_utf8, to_utf16, target, targetLimit, source, sourceLimit, NULL, NULL, NULL,
					   NULL, TRUE, TRUE, &errX);
		if (U_FAILURE(errX)) {
			err_msg(errX);
			return false;
		}
		return true;
	}

	bool to_utf8(char** target, unsigned long targetSize, const char** source,
				 unsigned long sourceSize) noexcept {
		return to_utf8(target, *target + targetSize, source, *source + sourceSize);
	}

	void to_utf8_throw(char** target, const char* targetLimit, const char** source, const char* sourceLimit) {
		ucnv_convertEx(utf16_to_utf8, to_utf16, target, targetLimit, source, sourceLimit, NULL, NULL, NULL,
					   NULL, TRUE, TRUE, &errX);
		if (U_FAILURE(errX)) err_throw(errX);
	}

	void to_utf8_throw(char** target, unsigned long targetSize, const char** source,
					   unsigned long sourceSize) {
		return to_utf8_throw(target, *target + targetSize, source, *source + sourceSize);
	}

	~icu_convertor_utf8() {
		if (!to_utf16) {
			ucnv_close(to_utf16);
		}
		if (!utf16_to_utf8) {
			ucnv_close(utf16_to_utf8);
		}
	}
};

// the max internal buffer size = src_size + src_size>>1
template <bool can_throw = false, class str_type = string>
str_type to_utf8str(const char* src, unsigned long len, const char* encoding_canonical) {
	auto utf8_size = len + (len >> 1);
	std::unique_ptr<char[]> buf{new char[utf8_size]};
	icu_convertor_utf8 converter{encoding_canonical};
	auto buf_p0 = buf.get();
	auto buf_p = buf_p0;
	if constexpr (can_throw)
		converter.to_utf8_throw(&buf_p, utf8_size, &src, len);
	else
		converter.to_utf8(&buf_p, utf8_size, &src, len);
	return str_type{buf_p0, (unsigned long)(buf_p - buf_p0)};
}

template <bool can_throw = false, class str_type = string>
str_type to_utf8str(const char* src, const char* src_end, const char* encoding_canonical) {
	return to_utf8str<can_throw, str_type>(src, src_end - src, encoding_canonical);
}

template <class str_type = string>
str_type to_utf8str(const char* src, unsigned long len, const string& encoding_canonical) {
	auto utf8_size = len + (len >> 1);
	std::unique_ptr<char[]> buf{new char[utf8_size]};
	icu_convertor_utf8 converter{encoding_canonical};
	auto buf_p0 = buf.get();
	auto buf_p = buf_p0;
	converter.to_utf8(&buf_p, utf8_size, &src, len);
	return str_type{buf_p0, (unsigned long)(buf_p - buf_p0)};
}

template <class str_type = string>
str_type to_utf8str(const char* src, const char* src_end, const string& encoding_canonical) {
	return to_utf8str<str_type>(src, src_end - src, encoding_canonical);
}

} // namespace encoding_su

#endif // LEXICON_BUDDY_ENCODING_SUPPORT_HPP

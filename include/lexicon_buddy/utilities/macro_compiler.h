//
//  macro_compiler.h
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/7/29.
//  Copyright © 2018 Lohengrin. All rights reserved.
//
#ifndef MACRO_COMPILER_H
#define MACRO_COMPILER_H

// unused
#ifndef _lexicon_LOCAL
#if defined(__clang__) || defined(__GNUC__) || defined(__GNUG__)
#define _lexicon_LOCAL __attribute__((visibility("hidden")))
#else
#define _lexicon_LOCAL
#endif
#endif

#endif // MACRO_COMPILER_H

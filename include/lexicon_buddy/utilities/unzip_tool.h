//
// Created by Lohengrin on 2018/04/28.
//

#ifndef LEXICON_BUDDY_UNZIP_TOOL_H
#define LEXICON_BUDDY_UNZIP_TOOL_H
#include <lexicon_buddy/lexicon_buddy_export.h>
#include <boost/iostreams/stream.hpp>
#include <climits>
#include <iostream>
#include <lzo/lzo1x.h>
#include <map>
#include <memory>
#include <type_traits>
#include <zlib.h>
#include <lexicon_buddy/lexicon/lexicon_types.h>
#include <lexicon_buddy/utilities/utilities.hpp>
// namespace unzip actually includes not only unzip utilities but also file validation and others
namespace unzip
{
using std::istream;
using std::ostream;
using std::pair;
using std::unique_ptr;

const char* comp_type_to_string(comp_type type);
const char* comp_type_to_string(unsigned int type);

const char* err2msg(int code, comp_type type);
class unzip_err : public std::runtime_error
{
	// use runtime_error constructor
	using std::runtime_error::runtime_error;
};
// gz_header in zlib.h
// typedef struct gz_header_s {
//    int     text;       /* true if compressed data believed to be text */
//    uLong   time;       /* modification time */
//    int     xflags;     /* extra flags (not used when writing a gzip file) */
//    int     os;         /* operating system */
//    Bytef   *extra;     /* pointer to extra field or Z_NULL if none */
//    uInt    extra_len;  /* extra field length (valid if extra != Z_NULL) */
//    uInt    extra_max;  /* space at extra (only when reading header) */
//    Bytef   *name;      /* pointer to zero-terminated file name or Z_NULL */
//    uInt    name_max;   /* space at name (only when reading header) */
//    Bytef   *comment;   /* pointer to zero-terminated comment or Z_NULL */
//    uInt    comm_max;   /* space at comment (only when reading header) */
//    int     hcrc;       /* true if there was or will be a header crc */
//    int     done;       /* true when done reading gzip header (not used
//                           when writing a gzip file) */
//} gz_header;

// TODO: re check endianness for all gz dictzip function
// get gheader
LEXICON_BUDDY_EXPORT void get_gzheader(istream& src, gz_header_s& gh, uint64_t* gh_len,
									   unsigned char*& gh_data);

// the gz_header contains pointers and it is not responsible for manage the memory of gz header, the
// whole header data stays in para: src
// !!!!deprecated
LEXICON_BUDDY_EXPORT void get_gzheader(const unsigned char* src, gz_header_s& gh, uint64_t* gh_len);

// dictzip field
// detail: https://linux.die.net/man/1/dictzip
struct LEXICON_BUDDY_EXPORT dictzip_s
{
	uint16_t XLEN;
	char SI1;
	char SI2;
	uint16_t LEN;
	uint16_t VER;
	uint16_t CHLEN;
	uint16_t CHCNT;
	const char* data_p;
	dictzip_s() = default;
	dictzip_s(gz_header_s& gh);
};

// offset begin of first chunk before comp = 0
// offset begin of first chunk after comp = 0
// <offset end of each chunk before comp, after comp>
using dz_index = std::map<uint64_t, uint64_t>;

LEXICON_BUDDY_EXPORT void dictzip_build_index(dictzip_s& dz, dz_index& index);

constexpr uInt ZLIB_CHUNK = 256 * 1024;
LEXICON_BUDDY_EXPORT void _unzip_error_handle(const comp_type type, int error_code);
// handle zlib error with detail error msg
LEXICON_BUDDY_EXPORT void _unzip_error_handle(const comp_type type, int error_code, z_streamp z_str_p);
LEXICON_BUDDY_EXPORT void _unzip_error_handle(uint64_t decom_expect_size, uint64_t decom_actual_size);
// called only once
LEXICON_BUDDY_EXPORT int lzo_initial();

// zlib z_stream wrapper
class LEXICON_BUDDY_EXPORT z_stream_wrapper
{
public:
	// parameter for constructor:
	// auto detection for gz and zlib header
	static constexpr int GZ_ZLIB_HEADER_AUTO_DETEC = 0;
	// no need to read header
	static constexpr int RAW_INFLATE = 1;
	// header and gz header only
	static constexpr int GZ_ONLY = 2;
	z_stream z_str;
	z_stream_wrapper& operator=(const z_stream_wrapper& source) = delete;
	z_stream_wrapper(const z_stream_wrapper&) = delete;
	z_stream_wrapper(int mode = GZ_ZLIB_HEADER_AUTO_DETEC) : init_mode{mode} {
		z_str.zalloc = nullptr;
		z_str.zfree = nullptr;
		z_str.opaque = nullptr;
		z_str.next_in = nullptr;
		init();
	}
	void reset() {
		auto ret = inflateEnd(&z_str);
		_unzip_error_handle(comp_type::zlib, ret, &z_str);
		init();
	}
	~z_stream_wrapper() noexcept {
		// deallocate and handle error
		try {
			auto ret = inflateEnd(&z_str);
			_unzip_error_handle(comp_type::zlib, ret, &z_str);
		} catch (const std::exception& e) { // caught by reference to base
			std::cerr << "an exception was caught during z_stream_wrapper destruction, with message: "
					  << e.what() << '\n';
		}
	}

private:
	int init_mode;
	void init() {
		/*
		 https://zlib.net/manual.html

		 "windowBits can also be greater than 15 for optional gzip decoding. Add 32 to windowBits
		 to enable zlib and gzip decoding with automatic header detection, or add 16 to decode only
		 the gzip format (the zlib format will return a Z_DATA_ERROR)."

		 "windowBits can also be –8..–15 for raw inflate. In this case, -windowBits determines the
		 window size. inflate() will then process raw deflate data, not looking for a zlib or gzip
		 header, not generating a check value, and not looking for any check values for comparison
		 at the end of the stream."

		 "inflateInit2 does not perform any decompression apart from reading the zlib header if
		 present: actual decompression be done by inflate(). (So next_in and avail_in may be
		 modified, but next_out and avail_out are unused and unchanged.) The current implementation
		 of inflateInit2() does not process any header information—that is deferred until inflate()
		 is called."
		*/
		int ret{};
		switch (init_mode) {
			case GZ_ZLIB_HEADER_AUTO_DETEC:
				ret = inflateInit2(&z_str, 32 + MAX_WBITS);
				break;
			case RAW_INFLATE:
				ret = inflateInit2(&z_str, -15);
				break;
			case GZ_ONLY:
				ret = inflateInit2(&z_str, 16 + MAX_WBITS);
				break;
			default:
				throw std::invalid_argument{"z_stream_wrapper, invalid init mode"};
		}
		_unzip_error_handle(comp_type::zlib, ret, &z_str);
	}
};

template <comp_type t>
struct LEXICON_BUDDY_EXPORT unzip_para;
template <>
struct LEXICON_BUDDY_EXPORT unzip_para<comp_type::zlib>
{
	const comp_type format{comp_type::zlib};
	// buffer not used
	unique_ptr<unsigned char[]> internal_buffer{nullptr};
	uint64_t buffer_size{0};
	explicit unzip_para() = default;
	unzip_para& operator=(const unzip_para& source) = delete;
	unzip_para(const unzip_para&) = delete;
};

template <>
struct LEXICON_BUDDY_EXPORT unzip_para<comp_type::raw>
{
	const comp_type format{comp_type::raw};
	// buffer not used
	unique_ptr<unsigned char[]> internal_buffer{nullptr};
	uint64_t buffer_size{0};
	explicit unzip_para() = default;
	unzip_para& operator=(const unzip_para& source) = delete;
	unzip_para(const unzip_para&) = delete;
};

template <>
struct LEXICON_BUDDY_EXPORT unzip_para<comp_type::dictzip>
{
	const comp_type format{comp_type::dictzip};
	// internal_buffer points a copy of origin gz header
	unique_ptr<unsigned char[]> internal_buffer{nullptr};
	uint64_t buffer_size{0};
	gz_header_s gh{};
	uint64_t gh_len{};
	dictzip_s dz{};
	dz_index index{};
	bool init_ok{};
	explicit unzip_para() = default;
	unzip_para& operator=(const unzip_para& source) = delete;
	unzip_para(const unzip_para&) = delete;
	void init(istream& src) {
		unsigned char* ptr;
		get_gzheader(src, gh, &gh_len, ptr);
		internal_buffer.reset(ptr);
		dz = dictzip_s{gh};
		// init index
		dictzip_build_index(dz, index);
		init_ok = true;
	}
	bool random_access_ready() { return init_ok; }
	/// @return maxium possible plain file size
	uint64_t plain_file_size_max() {
		auto rb_iter = index.rbegin();
		if (rb_iter != index.rend())
			return rb_iter->first;
		else
			return 0;
	}
};

template <>
struct LEXICON_BUDDY_EXPORT unzip_para<comp_type::LZO>
{
	const comp_type format{comp_type::LZO};
	unique_ptr<char[]> internal_buffer{nullptr};
	uint64_t buffer_size{0};
	// initialization in the cpp file
	static int lzo_init_flag;
};

// not thread safe
template <comp_type t>
// unzip_agent<comp_type::zlib> can handle zlib and gzip format
// one instance for each compressed file/data
class LEXICON_BUDDY_EXPORT unzip_agent
{
	static_assert(t != comp_type::gzip, "use unzip_agent<comp_type::zlib> for gzip format!");

private:
	unzip_para<t> para_{};
	// compress libs most likely have their own allocator so there is no need calling this:
	unsigned char* alloc_buff(uint64_t ask) {
		if (ask <= para_.buffer_size) return para_.internal_buffer.get();
		para_.internal_buffer.reset(new unsigned char[ask]);
		para_.buffer_size = ask;
		return para_.internal_buffer.get();
	}

public:
	unzip_agent() = default;
	unzip_agent(const unzip_agent&) = delete;
	unzip_agent(unzip_agent&&) = delete;
	unzip_agent& operator=(const unzip_agent&) = delete;
	unzip_agent& operator=(unzip_agent&&) = delete;
	// _unzip_next unzip the whole compressed data
	// use _unzip_next for LZO, zlib(which also support gzip) and dictzip(compatible with gzip
	// format)
	void unzip_strm(const char* src, uint64_t src_size, char* dst, uint64_t dst_expect_size);
};

// a wrapper function to free us from write zip format dependant code
// do not support dictzip
void unzip_strm_generic(comp_type type, const char* src, uint64_t src_size, char* dst,
						uint64_t dst_expect_size);

// unzip_agent<comp_type::dictzip> instance is checked and associated to one .dz file stream or
// mmap(not both) when client call unzip_random()
template <>
class LEXICON_BUDDY_EXPORT unzip_agent<comp_type::dictzip>
{
	static constexpr comp_type t = comp_type::dictzip;

private:
	struct chunk_decompessor_istream
	{
		istream& dz_begin_;
		static constexpr unsigned BUF_LEN = 256 * 256; // length of local char buffer
		char source[BUF_LEN];
		unzip_para<t> para_{};
		z_stream_wrapper z_strmw_{z_stream_wrapper::RAW_INFLATE};
		chunk_decompessor_istream(istream& dz_begin);
		/// @return (success) decompression size or throw
		uint64_t unzip_chunk(dz_index::iterator chunk_iter, dz_index::iterator chunk_iter_begin, char* dst,
							 uint64_t dst_len_available);
	};
	struct chunk_decompessor_memory
	{
		const char* dz_begin_;
		uint64_t dz_len_;
		unzip_para<t> para_{};
		z_stream_wrapper z_strmw_{z_stream_wrapper::RAW_INFLATE};
		chunk_decompessor_memory(const char* dz_begin, uint64_t dz_len);
		/// @return (success) decompression size or throw
		uint64_t unzip_chunk(dz_index::iterator chunk_iter, dz_index::iterator chunk_iter_begin, char* dst,
							 uint64_t dst_len_available);
	};

	unzip_para<t> para_{};

public:
	unzip_agent() = default;
	unzip_agent(const unzip_agent&) = delete;
	unzip_agent(unzip_agent&&) = delete;
	unzip_agent& operator=(const unzip_agent&) = delete;
	unzip_agent& operator=(unzip_agent&&) = delete;

	// for unzip_agent<comp_type::dictzip> only
	// unzip a CHUNK of dictzip data(fake random access)
	// const char* overload
	void unzip_random(const char* src_begin, // must ptr to start of compressed data
					  uint64_t src_len,
					  uint32_t word_data_size,   // query para: size of uncompressed data
					  uint64_t word_data_offset, // query para: offset in uncompressed data
					  char* dst);				 // sink
	// for unzip_agent<comp_type::dictzip> only
	// unzip a CHUNK of dictzip data(fake random access)
	// mmap overload
	void unzip_random(const util::mmap_w& src_begin, // mmap
					  uint32_t word_data_size,		 // query para: size of uncompressed data
					  uint64_t word_data_offset,	 // query para: offset in uncompressed data
					  char* dst)					 // sink
	{
		unzip_random(src_begin.data(), src_begin.get_filesize(), word_data_size, word_data_offset, dst);
	}

	// istream overload
	void unzip_random(istream& src_begin,		 // input stream at the start of compressed data
					  uint32_t word_data_size,   // query para: size of uncompressed data
					  uint64_t word_data_offset, // query para: offset in uncompressed data
					  char* dst);				 // sink

	static pair<unique_ptr<char[]>, uint64_t> unzip_all(const char* src_begin, uint64_t src_len);
	static pair<unique_ptr<char[]>, uint64_t> unzip_all(istream& src);
	static uint64_t unzip_all(const char* src_begin, uint64_t src_len, ostream& dst_bein);
	static uint64_t unzip_all(istream& src, ostream& dst);
};

} // namespace unzip
#endif // LEXICON_BUDDY_UNZIP_TOOL_H

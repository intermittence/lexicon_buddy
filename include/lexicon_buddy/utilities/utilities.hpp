//
//  utilities.hpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/03/15.
//  Copyright © 2018 Lohengrin. All rights reserved.
//
#ifndef utilities_hpp
#define utilities_hpp

#include <lexicon_buddy/lexicon_buddy_export.h>
#include <lexicon_buddy/lexicon/myUsing.hpp>
#include <lexicon_buddy/lexicon/lexicon_types.h>
#include <lexicon_buddy/utilities/lang_platform.hpp>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <exception>
#include <fcntl.h>
#include <fstream>
//#include <iconv.h>
#include <string>
#if Unix_Style
#include <sys/mman.h>
#include <unistd.h>
#else
#include <windows.h> // include windows.h first
#include <memoryapi.h>
#endif
#include <sys/stat.h>
#include <sys/types.h>
#include <ctime>
#include <algorithm>
#include <cryptopp/ripemd.h>
#if Unix_Style
#include <boost/filesystem.hpp>
#else
#include <filesystem>
#endif

#ifndef COUT_DEBUG
#ifndef NDEBUG
#define COUT_DEBUG(msg) std::cout << (msg)
#else
#define COUT_DEBUG(msg) ((void)0)
#endif
#endif

namespace util
{
using std::copy;
using std::copy_n;
using std::reverse_copy;
#if Unix_Style
namespace fs = boost::filesystem;
using boost::system::error_code;
#else
namespace fs = std::filesystem;
using std::error_code;
#endif
/*
 * include endianswap, memory map,  and other convenient utilities
 * */
/// Do not use union for integral data 'transform'. It's U.B.
/// Rationale @link https://stackoverflow.com/questions/1522994/store-an-int-in-a-char-array
union char_uint16_t {
	char chars[2];
	uint16_t n;
};

union char_uint32_t {
	char chars[4];
	uint32_t n;
};
union char_uint64_t {
	char chars[8];
	uint64_t n;
};
// TODO: avoid using union
/**
 * @brief read an uint16 from buffer(little endianess)
 * @warning By copying bytes to an uint16_t var, proper alignment is guaranteed. If you try to avoid
 * this memory copying and directly cast char * to uint16_t *, the exact result is U.B.. You can avoid
 * the overhead of a function call by replace it with a macro.
 */
inline uint16_t char_to_uint16(const char* integralData) {
	char_uint16_t source;
	copy(integralData, integralData + 2, source.chars);
	return source.n;
	//	uint16_t dst;
	//	*(char*)(&dst) = *integralData;
	//	*((char*)(&dst) + 1) = *(integralData + 1);
	//	return dst;
	// return *((const uint16_t*)integralData);
}

inline uint32_t char_to_uint32(const char* integralData) {
	char_uint32_t source;
	copy(integralData, integralData + 4, source.chars);
	return source.n;
	//	uint32_t dst;
	//	*(char*)(&dst) = *integralData;
	//	*((char*)(&dst) + 1) = *(integralData + 1);
	//	*((char*)(&dst) + 2) = *(integralData + 2);
	//	*((char*)(&dst) + 3) = *(integralData + 3);
	//	return dst;
	// return *((const uint32_t*)integralData);
}

inline uint64_t char_to_uint64(const char* integralData) {
	char_uint64_t source;
	copy(integralData, integralData + 8, source.chars);
	return source.n;
	//	uint64_t dst;
	//	*(char*)(&dst) = *integralData;
	//	*((char*)(&dst) + 1) = *(integralData + 1);
	//	*((char*)(&dst) + 2) = *(integralData + 2);
	//	*((char*)(&dst) + 3) = *(integralData + 3);
	//	*((char*)(&dst) + 4) = *(integralData + 4);
	//	*((char*)(&dst) + 5) = *(integralData + 5);
	//	*((char*)(&dst) + 6) = *(integralData + 6);
	//	*((char*)(&dst) + 7) = *(integralData + 7);
	//	return dst;
	// return *((const uint64_t*)integralData);
}
// cast comp_type to uint32_t and output with big endian
inline void comp_type_update_big_endian(char* dst, unzip::comp_type t) {
	char_uint32_t u;
	u.n = static_cast<uint32_t>(t);
	// output with endian swap
	reverse_copy(u.chars, u.chars + 4, dst);
}

inline uint16_t endianSwap_16bit(const uint16_t integralData) {
	union {
		uint16_t little;
		char raw[2];
	} source, destination;
	source.little = integralData;
	reverse_copy(source.raw, source.raw + 2, destination.raw);
	return destination.little;
}

inline uint16_t endianSwap_16bit(const char* integralData) {
	union {
		uint16_t little;
		char raw[2];
	} destination;
	reverse_copy(integralData, integralData + 2, destination.raw);
	return destination.little;
}

inline uint32_t endianSwap_32bit(const uint32_t integralData) {
	union {
		uint32_t little;
		char raw[4];
	} source, destination;
	source.little = integralData;
	reverse_copy(source.raw, source.raw + 4, destination.raw);
	return destination.little;
}

inline uint32_t endianSwap_32bit(const char* integralData) {
	union {
		uint32_t little;
		char raw[4];
	} destination;
	reverse_copy(integralData, integralData + 4, destination.raw);
	return destination.little;
}

inline uint64_t endianSwap_64bit(const uint64_t integralData) {
	union {
		uint64_t little;
		char raw[8];
	} source, destination;
	source.little = integralData;

	reverse_copy(source.raw, source.raw + 8, destination.raw);
	return destination.little;
}

inline uint64_t endianSwap_64bit(const char* integralData) {
	union {
		uint64_t little;
		char raw[8];
	} destination;
	reverse_copy(integralData, integralData + 8, destination.raw);
	return destination.little;
}

template <unsigned num_bytes>
struct uint_nbytes;

template <>
struct uint_nbytes<1>
{ using type = uint8_t; };
template <>
struct uint_nbytes<2>
{ using type = uint16_t; };
template <>
struct uint_nbytes<4>
{ using type = uint32_t; };
template <>
struct uint_nbytes<8>
{ using type = uint64_t; };

template <unsigned num_bytes>
using uint_nbytes_t = typename uint_nbytes<num_bytes>::type;

template <unsigned num_bytes>
struct integer_type_traits
{
	using value_type = uint_nbytes_t<num_bytes>;
	using difference_type = char;
};

template <unsigned num_bytes, class InputIterator, class out_traits = integer_type_traits<num_bytes>>
auto iter_to_integer_nbytes(InputIterator in_iter) -> typename out_traits::value_type {
	static_assert(sizeof(*in_iter) <= sizeof(typename out_traits::difference_type),
				  "sizeof(*in_iter) larger than sizeof(out_traits::difference_type)\n");
	typename out_traits::value_type out;
	copy_n(in_iter, num_bytes, (typename out_traits::difference_type*)&out);
	return out;
}

template <unsigned num_bytes, class in_traits = integer_type_traits<num_bytes>,
		  class out_traits = integer_type_traits<num_bytes>>
auto endianSwap_nbytes(typename in_traits::value_type in) -> typename out_traits::value_type {
	static_assert(sizeof(in) <= sizeof(out_traits::value_type),
				  "sizeof(in) larger than sizeof(out_traits::value_type)\n");
	typename out_traits::value_type out;
	reverse_copy(&in, &in + num_bytes, (typename out_traits::difference_type*)&out);
	return out;
}

template <unsigned num_bytes, class RandomAccessIterator, class out_traits = integer_type_traits<num_bytes>>
auto endianSwap_nbytes(RandomAccessIterator in_iter) -> typename out_traits::value_type {
	static_assert(sizeof(*in_iter) <= sizeof(out_traits::difference_type),
				  "sizeof(*in_iter) larger than sizeof(out_traits::difference_type)\n");
	typename out_traits::value_type out;
	reverse_copy(in_iter, in_iter + num_bytes, (typename out_traits::difference_type*)&out);
	return out;
}

void displayCharAndInt(const string& path, uint32_t length);

/// check file existence
/// portable c++ stl solution
inline bool exists_stl(const char* name) {
	ifstream f(name);
	return f.good();
}

#if Unix_Style
/// actually checking accessibility
inline bool exists_fopen(const char* name) {
	if (FILE* file = fopen(name, "r")) {
		fclose(file);
		return true;
	} else {
		return false;
	}
}
#endif

#if Unix_Style
/// actually checking accessibility
inline bool exists_posix0(const char* name) { return (access(name, F_OK) != -1); }
#endif
/// quickest solution
inline bool exists_posix1(const char* name) {
	struct stat buffer;
	return (stat(name, &buffer) == 0);
}

inline bool exists_posix1(const fs::path& name) { return exists_posix1(GET_U8_C_STR(name)); }

inline std::time_t file_last_change_date(const char* filepath) {
	struct stat buffer;
	stat(filepath, &buffer);
#if Unix_Style
	return std::max(buffer.st_ctime, buffer.st_mtime);
#else
	return max(buffer.st_ctime, buffer.st_mtime);
#endif
}
// todo cross platform
inline std::time_t file_last_change_date(const fs::path& filepath) {
	return file_last_change_date(GET_U8_C_STR(filepath));
}

inline std::ifstream::pos_type filesize(const char* filepath) {
	std::ifstream in(filepath, std::ifstream::ate | std::ifstream::binary);
	return in.tellg();
}

inline std::ifstream::pos_type filesize(std::ifstream& _ifstream) {
	auto save = _ifstream.tellg();
	_ifstream.seekg(0, std::ios_base::end);
	auto size = _ifstream.tellg();
	_ifstream.seekg(save);
	return size;
}

// posix version of get file size
inline off_t filesize(int fd) {
	struct stat buf
	{};
	fstat(fd, &buf);
	return buf.st_size;
}
// NOTE: refine mmap
/// read only memory mapping wrapper
#if Unix_Style
struct mmap_w
{
private:
	int fd{-1};
	void* mmap_addr{nullptr};
	bool mapped{false}; // false means something is wrong
	void unmap_then_close() {
		if (fd == -1) {
		} else if (!mapped || mmap_addr == nullptr) {
			close(fd);
			fd = -1;
			throw std::runtime_error{"mmap_str unmap_then_close(): file is not mapped"};
		} else {
			if (munmap(mmap_addr, file_length) == -1) {
				mmap_addr = nullptr;
				close(fd);
				fd = -1;
				throw std::runtime_error{"mmap_str unmap_then_close(): fail to munmap"};
			} else {
				mmap_addr = nullptr;
				close(fd);
				fd = -1;
			}
			mapped = false;
		}
	}
	// for move semantic
	void clean() {
		fd = -1;
		mmap_addr = nullptr;
		mapped = false;
	}

public:
	std::string pathname;
	uint64_t file_length;
	int mmap_prot;
	int mmap_flags;
	off_t mmap_offset;
	mmap_w() = delete;
	explicit mmap_w(const mmap_w&) = delete;
	mmap_w& operator=(const mmap_w&) = delete;
	explicit mmap_w(mmap_w&& _mmap_str)
		: fd{std::forward<mmap_w>(_mmap_str).fd}, mmap_addr{std::forward<mmap_w>(_mmap_str).mmap_addr},
		  pathname{std::forward<mmap_w>(_mmap_str).pathname},
		  file_length{std::forward<mmap_w>(_mmap_str).file_length},
		  mmap_prot{std::forward<mmap_w>(_mmap_str).mmap_prot},
		  mmap_flags{std::forward<mmap_w>(_mmap_str).mmap_flags},
		  mmap_offset{std::forward<mmap_w>(_mmap_str).mmap_offset} {
		// fd=_mmap_str.fd;
		_mmap_str.clean();
	}
	mmap_w& operator=(mmap_w&& _mmap_str) {
		if (&_mmap_str != this) {
			pathname = std::forward<mmap_w>(_mmap_str).pathname;
			fd = _mmap_str.fd;
			file_length = _mmap_str.file_length;
			mmap_prot = _mmap_str.mmap_prot;
			mmap_flags = _mmap_str.mmap_flags;
			mmap_addr = _mmap_str.mmap_addr;
			mmap_offset = _mmap_str.mmap_offset;
			_mmap_str.clean();
		}
		return *this;
	}
	explicit mmap_w(const char* path, int input_mmap_prot = PROT_READ, int input_mmap_flags = MAP_PRIVATE,
					off_t input_mmap_offset = 0)
		: pathname{path}, mmap_prot{input_mmap_prot}, mmap_flags{input_mmap_flags}, mmap_offset{
																						input_mmap_offset} {
		if ((fd = open(path, O_RDONLY | O_CLOEXEC)) == -1)
			throw std::runtime_error{"mmap_str: fail to open file"};
		auto temp = filesize(fd);
		if (temp > 0) {
			file_length = temp;
		} else {
			close(fd);
			throw std::runtime_error{"mmap_str constructor: file size should > 0"};
		}
		if ((mmap_addr = mmap(nullptr, file_length, mmap_prot, mmap_flags, fd, mmap_offset)) == (void*)-1) {
			close(fd);
			throw std::runtime_error{"mmap_str constructor: mmap failed"};
		}
		mapped = true;
	}
	const char* data() const {
		if (mapped) {
			return (const char*)mmap_addr;
		} else {
			return nullptr;
		}
	}
	operator const char*() const { return data(); }
	const char* data_end() const {
		if (mapped) {
			return (const char*)mmap_addr + file_length;
		} else {
			return nullptr;
		}
	}
	uint64_t get_filesize() const { return file_length; }
	~mmap_w() {
		if (!mapped || mmap_addr == nullptr) return;
		unmap_then_close();
	}
};
#else
/// print windows last error message string
inline void win_print_last_error() {
	wchar_t msg[1000];
	// va_list args = NULL;
	auto code = GetLastError();
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, code, 0, msg, 1000, NULL);
	std::wcerr << "code " << code << ' ' << msg;
}
/// read only memory mapping wrapper
// TODO: impl file length and other functionalities like unix style version
struct mmap_w
{
private:
	std::string file_path_;
	HANDLE file_;
	HANDLE file_mapping_{};
	LPVOID map_view_{};
	void release_all() {
		if (!UnmapViewOfFile(map_view_)) {
			std::cerr << "error when unmap file view: \n";
			win_print_last_error();
		}
		if (!CloseHandle(file_mapping_)) {
			std::cerr << "error when close file mapping: \n";
			win_print_last_error();
		}
		if (!CloseHandle(file_)) {
			std::cerr << "error when close file: \n";
			win_print_last_error();
		}
	}

public:
	uint64_t file_length;
	explicit mmap_w() = delete;
	explicit mmap_w(const mmap_w& m) = delete;
	mmap_w& operator=(const mmap_w& m) = delete;
	// explicit mmap(mmap&& m) {}
	// mmap_w& operator=(mmap&& m) {}
	// read only
	explicit mmap_w(const char* file)
		: file_path_{file}, file_{CreateFileA(file, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
											  FILE_ATTRIBUTE_NORMAL, NULL)},
		  file_mapping_{CreateFileMappingA(file_, NULL, PAGE_READONLY, 0, 0, NULL)},
		  map_view_{MapViewOfFile(file_mapping_, FILE_MAP_READ, 0, 0, 0)} {}
	explicit mmap_w(mmap_w&& map) = default;
	mmap_w& operator=(mmap_w&& map) = default;
	const char* get_file_path() { return file_path_.c_str(); }
	const std::string& file_path_s() { return file_path_; }
	uint64_t get_filesize() const { return GetFileSize(file_, NULL); }
	const char* data() const { return static_cast<const char*>(map_view_); }
	operator const char*() const { return data(); }
	const char* data_end() const {
		// TODO: impl
		// if (mapped) {
		return static_cast<const char*>(map_view_) + get_filesize();
		//} else {
		//	return nullptr;
		//}
	}
	~mmap_w() { release_all(); }
};
#endif

template <class T>
uint64_t get_filesize(T& t);

// full specialized function is normal function. So inline
template <>
inline uint64_t get_filesize(ifstream& t) {
	return filesize(t);
}

template <>
inline uint64_t get_filesize(mmap_w& t) {
	return t.get_filesize();
}

template <class transform_engine, unsigned long BLOCK_SIZE, unsigned long FINAL_BLOCK_SIZE0>
class ripemd
{
public:
	static void generate(const char* msg, unsigned long msg_size, CryptoPP::word32* digest) {
		// padding buffer
		char tailandpad[BLOCK_SIZE * 2]{};
		decltype(msg_size) src_lastblock_len = msg_size % BLOCK_SIZE;
		decltype(msg_size) block_num = msg_size / BLOCK_SIZE;
		auto tail0 = msg + block_num * BLOCK_SIZE;
		auto tail1 = tail0 + src_lastblock_len;
		copy(tail0, tail1, tailandpad);
		// padding step1
		decltype(src_lastblock_len) tail_block_num_after_pad = src_lastblock_len / FINAL_BLOCK_SIZE0 + 1;
		uint32_t padding1_bit_len =
			BLOCK_SIZE * (tail_block_num_after_pad) - (BLOCK_SIZE - FINAL_BLOCK_SIZE0) - src_lastblock_len;
		*(tailandpad + src_lastblock_len) = 0x80;
		// padding step2
		auto msg_size_in_bit = msg_size * 8;
		copy((const char*)&msg_size_in_bit, (const char*)&msg_size_in_bit + 4,
			 tailandpad + src_lastblock_len + padding1_bit_len);
		copy((const char*)&msg_size_in_bit + 4, (const char*)&msg_size_in_bit + 8,
			 tailandpad + src_lastblock_len + padding1_bit_len + 4);
		// init and process
		transform_engine::InitState((typename transform_engine::HashWordType*)digest);
		for (decltype(block_num) i = 0; i < block_num; ++i) {
			transform_engine::Transform(digest, (CryptoPP::word32*)msg);
			msg += 64;
		}
		auto tail_p = tailandpad;
		for (decltype(tail_block_num_after_pad) i = 0; i < tail_block_num_after_pad; ++i) {
			transform_engine::Transform(digest, (CryptoPP::word32*)tail_p);
			tail_p += 64;
		}
	}
};
// ripemd160:
// Digest length: 20 bytes.
// Block size: 64 bytes.
// Max. final block size: 55 bytes.
// State size: 20 bytes
using ripemd160 = ripemd<CryptoPP::RIPEMD160, 64, 56>;
// ripemd128:
// Digest length: 16 bytes.
// Block size: 64 bytes.
// Max. final block size: 55 bytes.
// State size: 16 bytes.
using ripemd128 = ripemd<CryptoPP::RIPEMD128, 64, 56>;

namespace mdx_decrypt
{
#define SWAPNIBBLE(byte) (((byte) >> 4) | ((byte) << 4))
inline void keyword_index_encrypt(unsigned char* buf, size_t buflen, unsigned char* key, size_t keylen) {
	unsigned char previous = 0x36;
	for (size_t i = 0; i < buflen; i++) {
		buf[i] = SWAPNIBBLE(buf[i] ^ ((unsigned char)i) ^ key[i % keylen] ^ previous);
		previous = buf[i];
	}
}

inline void keyword_index_generate_16byteskey(const char* checksum, const char* key) {
	char msg[] = "\x00\x00\x00\x00\x95\x36\x00\x00";
	copy(checksum, checksum + 4, msg);
	ripemd128::generate(msg, sizeof(msg) - 1, (uint32_t*)key);
};

inline void keyword_index_decrypt(unsigned char* buf, size_t buflen, unsigned char* key, size_t keylen) {
	unsigned char previous = 0x36;
	for (size_t i = 0; i < buflen; i++) {
		auto tmp = buf[i];
		buf[i] = SWAPNIBBLE(buf[i]) ^ ((unsigned char)i) ^ key[i % keylen] ^ previous;
		previous = tmp;
	}
}

} // namespace mdx_decrypt

///@brief call inside main
inline fs::path get_exe_folder(char* argv[]) {
	fs::path p{argv[0]};
	if (p.has_filename()) {
		return p.parent_path();
	} else {
		return p;
	}
}

} // namespace util
#endif /* utilities_hpp */
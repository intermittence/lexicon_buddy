//
//  preview.hpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/06/1.
//  Copyright © 2018 Lohengrin. All rights reserved.
//

// print profile for different dictionary

#ifndef LEXICON_BUDDY_PREVIEW_HPP
#define LEXICON_BUDDY_PREVIEW_HPP

#include <lexicon_buddy/lexicon/mdxContainer.hpp>
#include <ostream>
namespace lexicon
{
namespace mdx
{

namespace key_index
{
namespace debug
{
// TODO: test
// log one key_block
// routine automatically handle data of different compression format(the data header and data
// compression format must match)
void key_block_log(const slide& i,
				   const char* dat_ptr, // point to key_blocks[x]
				   const string& encoding_canonical, std::ostream& log, bool show_content);

} // namespace debug
} // namespace key_index

namespace debug
{
class mdx_profiler
{
public:
	static void profile(const string& mdx_path, std::ostream& log, bool detail);
};
} // namespace debug
} // namespace mdx
} // namespace lexicon

#endif // LEXICON_BUDDY_PREVIEW_HPP

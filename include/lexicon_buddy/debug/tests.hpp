//
//  tests.hpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/06/2.
//  Copyright © 2018 Lohengrin. All rights reserved.
//

#ifndef LEXICON_BUDDY_TESTS_HPP
#define LEXICON_BUDDY_TESTS_HPP

#include <lexicon_buddy/lexicon/mdxContainer.hpp>
#include <lexicon_buddy/utilities/encoding_support.hpp>
#include <lexicon_buddy/utilities/utilities.hpp>
#include <lexicon_buddy/debug/preview.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/locale.hpp>
#include <exception>
#include <iostream>
#include <string>

using boost::locale::generator;
using boost::locale::localization_backend_manager;
using boost::locale::norm_type;
using boost::locale::normalize;
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;
using std::wcout;
using std::wstring;

int test1() {

	try {
		const char* path = "/Users/zhanggelin/Desktop/files/dictStudy/test/ETDict.txt";

		util::mmap_w		  mmap_file{path};
		lexicon::mdx::h_sec_i test{mmap_file};
		// cout<<test.DataSourceFormat;//for debug
		// lexicon::k_sec_i xxx;
	} catch (std::exception& e) {
		cout << e.what() << '\n';
	}
	cout << ' ';
	return 0;
}

//void test_key_function(const std::string& src, const std::string& dst, const std::string& log) {
//	using namespace lexicon::mdx;
//	lexicon::mdx::decom_routine::decomp(src, dst);
//	cout << "decomp completed" << '\n';
//	lexicon::mdx::mdx_data_mmap mdx_obj{dst};
//	cout << "mdx<mmap> initialization complete" << '\n';
//	const char*   dat_ptr = mdx_obj.key_index_ptr();
//	std::ofstream log_str{log};
//	cout << "log file is open" << '\n';
//	auto encoding_unit = encoding_su::encoding_check::get_byte_unit(mdx_obj.get_encoding());
//	cout << "read key_index" << '\n';
//	auto key_index_v_ = key_index::read_key_index(dat_ptr, 0, encoding_unit);
//	cout << "log keys" << '\n';
//	key_index::debug::key_log(key_index_v_, dat_ptr, encoding_unit, log_str);
//}

#endif // LEXICON_BUDDY_TESTS_HPP

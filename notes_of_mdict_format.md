## notes of mdict format

### compression:

>[zhansliu's work:](https://github.com/zhansliu/writemdict/blob/master/fileformat.md)
>
>Various data blocks are compressed using the same scheme. These all look like these:
>
>| `compress(data)`  | Length  |                                                        |
>| ----------------- | ------- | ------------------------------------------------------ |
>| `comp_type`       | 4 bytes | Compression type. See below.                           |
>| `checksum`        | 4 bytes | ADLER32 checksum of the uncompressed data. Big-endian. |
>| `compressed_data` | varying | Compressed version of `data`.                          |

### `keyword_sect`:

#### `key_index`:

(tested with mdx file **version 1.3**)

the `first_size[i]` and `last_size[i]`  is the full length (encoding unit as basic unit) of `first_word[i]` and `last_word[i]`.

(todo test with mdx version >=2.0) `comp_size[i]` ( `decomp_size[i]` does not)  include the **mdx** extra header part (`comp_type` + `checksum`) before compressed data.

#### Keyword Blocks:

(tested with mdx file **version 1.3**)

`offset[0]` (4 bytes): 

> [zhansliu's work:](https://github.com/zhansliu/writemdict/blob/master/fileformat.md)
>
> The offset should be interpreted as follows: Decompress all record blocks, and concatenate them together, and let `records` denote the resulting array of bytes. The record corresponding to `key[i]` then starts at `records[offset[i]]`.

Be careful. The imaginary "resulting array of bytes" **do not include** `comp_type` (4 bytes) and `checksum` (4 bytes).
//
// Created by Lohengrin on 2018/04/28.
//
#include <lexicon_buddy/utilities/unzip_tool.h>
#include <lexicon_buddy/utilities/utilities.hpp>
#include <iostream>

namespace unzip
{
namespace
{
constexpr unsigned loop_limits = 5000;
}

/*  no_comp = 0,
	LZO		= 1,
	zlib	= 2,
	gzip	= 3,
	dictzip = 4*/

const char* comp_type_to_string(comp_type type) {
	const char* p{};
	switch (type) {
		case comp_type::no_comp:
			p = "no_comp";
			break;
		case comp_type::LZO:
			p = "LZO";
			break;
		case comp_type::zlib:
			p = "zlib";
			break;
		case comp_type::gzip:
			p = "gzip";
			break;
		case comp_type::dictzip:
			p = "dictzip";
			break;
		case comp_type::raw:
			p = "raw";
			break;
	}
	return p;
}
const char* comp_type_to_string(unsigned int type) {
	if (type > static_cast<unsigned int>(comp_type::LAST))
		throw std::invalid_argument{"numeric comp_type out of range"};
	return comp_type_to_string(static_cast<comp_type>(type));
}

struct
{
	int value;
	const char* name;
} LZO_errors[] = {{LZO_E_OK, "LZO_E_OK"},
				  {LZO_E_INPUT_NOT_CONSUMED, "LZO_E_INPUT_NOT_CONSUMED"},
				  {LZO_E_INPUT_OVERRUN, "LZO_E_INPUT_OVERRUN"},
				  {LZO_E_OUTPUT_OVERRUN, "LZO_E_OUTPUT_OVERRUN"},
				  {LZO_E_LOOKBEHIND_OVERRUN, "LZO_E_LOOKBEHIND_OVERRUN"},
				  {LZO_E_EOF_NOT_FOUND, "LZO_E_EOF_NOT_FOUND"},
				  {LZO_E_ERROR, "LZO_E_ERROR"}};
struct
{
	int value;
	const char* name;
} zlib_errors[] = {{Z_OK, "Z_OK"},
				   {Z_STREAM_END, "Z_STREAM_END"},
				   {Z_NEED_DICT, "Z_NEED_DICT"},
				   {Z_ERRNO, "Z_ERRNO"},
				   {Z_STREAM_ERROR, "Z_STREAM_ERROR"},
				   {Z_DATA_ERROR, "Z_DATA_ERROR"},
				   {Z_MEM_ERROR, "Z_MEM_ERROR"},
				   {Z_BUF_ERROR, "Z_BUF_ERROR"},
				   {Z_VERSION_ERROR, "Z_VERSION_ERROR"}};

const char* err2msg(int code, comp_type type) {
	switch (type) {
		case comp_type::LZO:
			for (int i = 0; LZO_errors[i].name; ++i)
				if (LZO_errors[i].value == code) return LZO_errors[i].name;
			return "unknown";
		case comp_type::zlib:
			for (int i = 0; zlib_errors[i].name; ++i)
				if (zlib_errors[i].value == code) return zlib_errors[i].name;
			return "unknown";
		default:
			return "unknown code for unknow comp_type";
	}
}

// gz_header in zlib.h
// typedef struct gz_header_s {
//    int     text;       /* true if compressed data believed to be text */
//    uLong   time;       /* modification time */
//    int     xflags;     /* extra flags (not used when writing a gzip file) */
//    int     os;         /* operating system */
//    Bytef   *extra;     /* pointer to extra field or Z_NULL if none */
//    uInt    extra_len;  /* extra field length (valid if extra != Z_NULL) */
//    uInt    extra_max;  /* space at extra (only when reading header) */
//    Bytef   *name;      /* pointer to zero-terminated file name or Z_NULL */
//    uInt    name_max;   /* space at name (only when reading header) */
//    Bytef   *comment;   /* pointer to zero-terminated comment or Z_NULL */
//    uInt    comm_max;   /* space at comment (only when reading header) */
//    int     hcrc;       /* true if there was or will be a header crc */
//    int     done;       /* true when done reading gzip header (not used
//                           when writing a gzip file) */
//} gz_header;

// FLG in gzip header
//	bit 0 FTEXT
//	bit 1 FHCRC
//	bit 2 FEXTRA
//	bit 3 FNAME
//	bit 4 FCOMMENT
//	bit 5 reserved
//	bit 6 reserved
//	bit 7 reserved

void get_gzheader(std::istream& src, gz_header_s& gh, uint64_t* gh_len, unsigned char*& gh_data) {
	auto src_begin = src.tellg();
	// reset gz header
	gh = gz_header_s{};
	unsigned char block0[10];
	src.read(reinterpret_cast<char*>(block0), 10);
	unsigned char ID1 = block0[0];
	unsigned char ID2 = block0[1];
	unsigned char FLG = block0[3];

	if (!(ID1 == 31 && ID2 == 139)) throw std::runtime_error{"gzip format validation failed"};
	gh.text = (FLG & 1) ? 1 : 0;
	gh.time = util::char_to_uint32(reinterpret_cast<const char*>(block0 + 4));
	gh.xflags = block0[8];
	gh.os = block0[9];
	if (FLG & 1 << 2) {
		// gh.extra = const_cast<unsigned char *>(temp);
		char block1[2];
		src.read(block1, 2);
		gh.extra_len = util::char_to_uint16(block1);
		gh.extra_max = gh.extra_len;
		src.seekg(gh.extra_len, std::ios::cur);
	}
	//	bit 3 FNAME
	if (FLG & 1 << 3) {
		// gh.name = const_cast<unsigned char *>(temp);
		// iteration over zero-terminated string
		while (src.get()) {
			if ((++(gh.name_max)) > loop_limits)
				throw std::runtime_error{"exceed loop_limits when go through gz header's name field"};
		}
	}
	decltype(src.tellg()) comment_offset;
	//	bit 4 FCOMMENT
	if (FLG & 1 << 4) {
		comment_offset = src.tellg() - src_begin;
		// gh.comment = const_cast<unsigned char *>(temp);
		// iteration over zero-terminated string
		while (src.get()) {
			if ((++(gh.comm_max)) > loop_limits)
				throw std::runtime_error{"exceed loop_limits when go through gz header's comment field"};
		}
	}

	//	bit 1 FHCRC
	uint16_t CRC16{};
	if (FLG & 1 << 1) {
		gh.hcrc = 1;
		char blockCRC[2];
		src.read(blockCRC, 2);
		CRC16 = util::char_to_uint16(blockCRC);
	}
	auto len = src.tellg() - src_begin;
	//
	unique_ptr<unsigned char[]> data_wrapper{new unsigned char[static_cast<unsigned long>(len)]};
	src.seekg(0);
	auto ptr = data_wrapper.get();
	// TODO: copy the fucking data!
	src.read(reinterpret_cast<char*>(ptr), len);
	if (FLG & 1 << 2) gh.extra = ptr + 12;
	if (FLG & 1 << 3) {
		gh.name = ptr + 12 + gh.extra_len;
#ifndef NDEBUG
		// cout << "gh.name(file name): " << reinterpret_cast<char*>(gh.name) << '\n';
#endif
	}
	if (FLG & 1 << 4) gh.comment = ptr + comment_offset;
	// do CRC check
	if (FLG & 1 << 1) {
		uLong crc = crc32(0L, Z_NULL, 0);
		crc = crc32(crc, gh_data, static_cast<uInt>(len - 2));
		if (static_cast<uint16_t>(crc) != CRC16) throw std::runtime_error{"gzip header crc check failed"};
	}
	gh.done = 1;
	*gh_len = len;
	gh_data = data_wrapper.get();
	data_wrapper.release();
}

// the gz_header_s contains pointers and it is not responsible for manage the memory of gz header,
// the whole header data stays in para: src
void get_gzheader(const unsigned char* src, gz_header_s& gh, uint64_t* gh_len) {
	// reset gz header
	gh = gz_header_s{};
	auto temp = src;
	// identify gzip format
	// TODO: header CRC check
	if (!(*temp == 31 && *(++temp) == 139)) throw std::runtime_error{"gzip format validation failed"};
	temp += 2;
	unsigned char FLG = *temp++;

	gh.text = (FLG & 1) ? 1 : 0;
	gh.time = util::char_to_uint32(reinterpret_cast<const char*>(temp));
	temp += 4;
	gh.xflags = *temp++;
	gh.os = *temp++;
	//	bit 2 FEXTRA
	if (FLG & 1 << 2) {
		gh.extra = const_cast<unsigned char*>(temp);
		gh.extra_len = util::char_to_uint16(reinterpret_cast<const char*>(temp));
		gh.extra_max = gh.extra_len;
		temp += gh.extra_len;
	}
	//	bit 3 FNAME
	if (FLG & 1 << 3) {
		gh.name = const_cast<unsigned char*>(temp);
		// iteration over zero-terminated string
		while (*temp++) {
			if ((++(gh.name_max)) > loop_limits)
				throw std::runtime_error{"exceed loop_limits when go through gz header's name field"};
		}
	}

	//	bit 4 FCOMMENT
	if (FLG & 1 << 4) {
		gh.comment = const_cast<unsigned char*>(temp);
		// iteration over zero-terminated string
		while (*temp++) {
			if ((++(gh.comm_max)) > loop_limits)
				throw std::runtime_error{"exceed loop_limits when go through gz header's comment field"};
		}
	}

	//	bit 1 FHCRC
	if (FLG & 1 << 1) {
		gh.hcrc = 1;
		uint16_t CRC16 = util::char_to_uint16(reinterpret_cast<const char*>(temp));
		// do CRC check
		uLong crc = crc32(0L, Z_NULL, 0);
		crc = crc32(crc, src, static_cast<uInt>(temp - src));
		if (static_cast<uint16_t>(crc) != CRC16) throw std::runtime_error{"gzip header crc check failed"};
	}

	*gh_len = temp + 2 - src;
	gh.done = 1;
}

// struct dictzip_s {
//	uint16_t XLEN;
//	char SI1;
//	char SI2;
//	uint16_t LEN;
//	uint16_t VER;
//	uint16_t CHLEN;
//	uint16_t CHCNT;
//	const char *data_p;
//	dictzip_s(gz_header_s &gh);
//};

dictzip_s::dictzip_s(gz_header_s& gh) {
	if (!gh.done) throw std::runtime_error{"error during dictzip_s construction: gz header error"};
	if (gh.extra == Z_NULL)
		throw std::runtime_error{"error during dictzip_s construction: gz header do not have an extra field"};
	XLEN = static_cast<uint16_t>(gh.extra_len);
	auto ptr = reinterpret_cast<char*>(gh.extra);
	SI1 = *ptr++;
	SI2 = *ptr++;
	if (!(SI1 == 'R' && SI2 == 'A'))
		throw std::runtime_error{
			"error during dictzip_s construction: gz extra field SI1 SI2 identifier miss match"};
	LEN = util::char_to_uint16(ptr);
	ptr += 2;
	VER = util::char_to_uint16(ptr);
	ptr += 2;
	CHLEN = util::char_to_uint16(ptr);
	ptr += 2;
	CHCNT = util::char_to_uint16(ptr);
	ptr += 2;
	data_p = ptr;
}

void dictzip_build_index(dictzip_s& dz, dz_index& index) {
	if (!index.empty()) index.clear();
	auto CNT = dz.CHCNT;
	auto CHLEN = dz.CHLEN;
	auto ptr = static_cast<const char*>(dz.data_p);
	uint64_t offset_plain{};	  // end of each chunk
	uint64_t offset_after_comp{}; // end of each chunk after compression
	while (CNT) {
		offset_plain += CHLEN;
		offset_after_comp += util::char_to_uint16(ptr);
		index.emplace(offset_plain, offset_after_comp);
		--CNT;
		ptr += 2;
	}
}

// initial non const static memeber:
int unzip_para<comp_type::LZO>::lzo_init_flag = lzo_initial();

// pass comp_type::zlib to function for handling gzip error since zlib is used to decomp gzip stream
void _unzip_error_handle(const comp_type type, int error_code) {
	switch (type) {
		case comp_type::LZO:
			if (error_code < 0) {
				throw unzip_err{string{"unzip/init unzip fail when using LZO, error code: "} +
								err2msg(error_code, comp_type::LZO)};
			}
			break;
		case comp_type::zlib:
		case comp_type::raw:
			if (error_code < 0) {
				throw unzip_err{string{"unzip/init unzip fail when using zlib, error code: "} +
								err2msg(error_code, comp_type::zlib)};
			}
			break;
		default:
			break;
	}
}

// zlib error handle with detail
void _unzip_error_handle(const comp_type type, int error_code, z_streamp z_str_p) {
	if (type != comp_type::zlib && type != comp_type::raw)
		throw std::invalid_argument{
			"invalid comp_type, if you pass z_streamp, type should be comp_type::zlib"};
	if (z_str_p->msg == nullptr) {
		// if somehow zlib do not set the msg, delegate the error handle
		// to two para overload _unzip_error_handle
		_unzip_error_handle(type, error_code);
	} else {
		if (error_code < 0) {
			throw unzip_err{string{"unzip/init unzip fail when using zlib, error code: "} +
							err2msg(error_code, comp_type::zlib) + ", error msg: " + z_str_p->msg};
		}
	}
}

void _unzip_error_handle(uint64_t decom_expect_size, uint64_t decom_actual_size) {
	if (decom_expect_size != decom_actual_size)
		throw std::range_error{
			"mismatch of decom_expect_size and decom_actual_size, and the dst data is corrupted"};
}

// called only once
int lzo_initial() {
	auto i = lzo_init();
	_unzip_error_handle(comp_type::LZO, i);
	return i;
}
template <>
void unzip_agent<comp_type::LZO>::unzip_strm(const char* src, uint64_t src_size, char* dst,
											 uint64_t dst_expect_size) {
	if (src_size == 0) throw std::invalid_argument{"_unzip_next: src_size==0"};
	lzo_uint dst_output_size_temp = dst_expect_size;
	// decomp
	auto i = lzo1x_decompress(reinterpret_cast<const unsigned char*>(src), src_size,
							  reinterpret_cast<unsigned char*>(dst), &dst_output_size_temp, nullptr);
	// handle error
	_unzip_error_handle(para_.format, i);
	_unzip_error_handle(dst_expect_size, dst_output_size_temp);
}
template <>
void unzip_agent<comp_type::zlib>::unzip_strm(const char* src, uint64_t src_size, char* dst,
											  uint64_t dst_expect_size) {
	if (src_size == 0) throw std::invalid_argument{"_unzip_next: src_size==0"};
	z_stream_wrapper z_str_w{};
	auto src_size_left = src_size;
	auto src_left_p = src;
	auto dst_left_p = dst;
	uint64_t dst_wrote{0};
	unsigned have;

	// TODO: test
	while (src_size_left > 0) {
		z_str_w.z_str.next_in = const_cast<Bytef*>(reinterpret_cast<const Bytef*>(src_left_p));
		// each time input a ZLIB_CHUNK size of compressed data(and use multiple inflate() to
		// decomp)
		auto _in_size = src_size_left > ZLIB_CHUNK ? ZLIB_CHUNK : static_cast<uInt>(src_size_left);
		z_str_w.z_str.avail_in = _in_size;
		do {
			z_str_w.z_str.avail_out = ZLIB_CHUNK;
			z_str_w.z_str.next_out = reinterpret_cast<Bytef*>(dst_left_p);
			auto ret = inflate(&z_str_w.z_str, Z_NO_FLUSH);
			_unzip_error_handle(para_.format, ret);
			// size of decomp data produced by single call of inflate()
			have = ZLIB_CHUNK - z_str_w.z_str.avail_out;
			dst_left_p += have;
			dst_wrote += have;
		} while (z_str_w.z_str.avail_out == 0); // out buffer is full
		// move pointer to start of unread block
		src_left_p += _in_size;
		// update unread src size
		src_size_left -= _in_size;
	}
}

template <>
void unzip_agent<comp_type::raw>::unzip_strm(const char* src, uint64_t src_size, char* dst,
											 uint64_t dst_expect_size) {
	if (src_size == 0) throw std::invalid_argument{"_unzip_next: src_size==0"};
	z_stream_wrapper z_str_w{z_stream_wrapper::RAW_INFLATE};
	auto src_size_left = src_size;
	auto src_left_p = src;
	auto dst_left_p = dst;
	uint64_t dst_wrote{0};
	unsigned have;

	// TODO: test
	while (src_size_left > 0) {
		z_str_w.z_str.next_in = const_cast<Bytef*>(reinterpret_cast<const Bytef*>(src_left_p));
		// each time input a ZLIB_CHUNK size of compressed data(and use multiple inflate() to
		// decomp)
		auto _in_size = src_size_left > ZLIB_CHUNK ? ZLIB_CHUNK : static_cast<uInt>(src_size_left);
		z_str_w.z_str.avail_in = _in_size;
		do {
			z_str_w.z_str.avail_out = ZLIB_CHUNK;
			z_str_w.z_str.next_out = reinterpret_cast<Bytef*>(dst_left_p);
			auto ret = inflate(&z_str_w.z_str, Z_NO_FLUSH);
			_unzip_error_handle(para_.format, ret);
			// size of decomp data produced by single call of inflate()
			have = ZLIB_CHUNK - z_str_w.z_str.avail_out;
			dst_left_p += have;
			dst_wrote += have;
		} while (z_str_w.z_str.avail_out == 0); // out buffer is full
		// move pointer to start of unread block
		src_left_p += _in_size;
		// update unread src size
		src_size_left -= _in_size;
	}
}

// a wrapper function to free us from write zip format dependant code
// do not support dictzip
void unzip_strm_generic(comp_type type, const char* src, uint64_t src_size, char* dst,
						uint64_t dst_expect_size) {
	switch (type) {
		case comp_type::LZO: {
			unzip_agent<comp_type::LZO> _agent_LZO;
			_agent_LZO.unzip_strm(src, src_size, dst, dst_expect_size);
			break;
		}
		case comp_type::zlib:
		case comp_type::gzip: {
			unzip_agent<comp_type::zlib> _agent_zlib;
			_agent_zlib.unzip_strm(src, src_size, dst, dst_expect_size);
			break;
		}
		case comp_type::raw: {
			unzip_agent<comp_type::raw> _agent_zlib_raw;
			_agent_zlib_raw.unzip_strm(src, src_size, dst, dst_expect_size);
			break;
		}
		case comp_type::no_comp:
			throw unzip_err{"error, type == comp_type::no_comp"};
		default:
			throw unzip_err{"error, unsupported compression format"};
	}
}

// for unzip_agent<comp_type dictzip> only
// unzip a CHUNK of dictzip data(fake random access)
// mmap overload
void unzip_agent<comp_type::dictzip>::unzip_random(
	const char* src_begin, // ptr to start of compressed data
	uint64_t src_len,
	uint32_t word_data_size,   // query para: size of uncompressed data
	uint64_t word_data_offset, // query para: offset in uncompressed data
	char* dst)				   // sink
{
	// length of local char buffer
	constexpr unsigned BUF_LEN = 256 * 256;
	// check whether agent is ready for "random acess". if not, get ready
	if (!para_.random_access_ready()) {
		boost::iostreams::stream<boost::iostreams::array_source> strz(src_begin, src_len);
		para_.init(strz);
	}
	// gz header length
	auto GHLEN = para_.gh_len;
	// search the dz extra field based on query para
	auto CHLEN = para_.dz.CHLEN;
	// index key: offset indicates end of each chunk before comp
	// value:after comp

	auto chunk_start0 = para_.index.lower_bound(word_data_offset);
	auto chunk_start0_was_end = (chunk_start0 == para_.index.end());
	if (chunk_start0_was_end) throw std::invalid_argument{"dictzip query(word_data_offset) out of range"};

	auto chunk_start0_was_begin = (chunk_start0 == para_.index.begin());
	if (!chunk_start0_was_begin) --chunk_start0;
	auto chunk_start1 = para_.index.upper_bound(word_data_offset);
	auto chunk_end = para_.index.upper_bound(word_data_offset + word_data_size);
	if (chunk_start1 == para_.index.end() || chunk_end == para_.index.end())
		throw std::invalid_argument{"dictzip query out of range"};
	// offset of chunk's(after compression) end
	uint64_t chunk_off0 = !chunk_start0_was_begin ? chunk_start0->second : 0;
	uint64_t chunk_off1 = chunk_start1->second;
	// offset of chunk's(plain data) end
	uint64_t chunk_off_plain0 = !chunk_start0_was_begin ? chunk_start0->first : 0;
	char sink[BUF_LEN];
	// chunk_begin_p indicate the start of any compressed chunk (size = CHLEN)
	auto chunk_begin_p = reinterpret_cast<unsigned char*>(const_cast<char*>(src_begin + chunk_off0 + GHLEN));
	// size of first chunk after compression in the range of query
	auto chunk_comp_size = chunk_off1 - chunk_off0;
	auto irrelevant = word_data_offset - chunk_off_plain0;
	z_stream_wrapper z_strmw{z_stream_wrapper::RAW_INFLATE};
	z_strmw.z_str.next_in = chunk_begin_p;
	z_strmw.z_str.avail_in = chunk_comp_size;
	z_strmw.z_str.next_out = reinterpret_cast<unsigned char*>(sink);
	z_strmw.z_str.avail_out = irrelevant;
	// dump the irrelevant
	if (irrelevant) {
		auto ret = inflate(&z_strmw.z_str, Z_NO_FLUSH);
		_unzip_error_handle(comp_type::zlib, ret, &z_strmw.z_str);
	}
	// redirect target
	z_strmw.z_str.next_out = reinterpret_cast<unsigned char*>(dst);
	z_strmw.z_str.avail_out = word_data_size;
	// extract query result
	auto ret = inflate(&z_strmw.z_str, Z_NO_FLUSH);
	_unzip_error_handle(comp_type::zlib, ret, &z_strmw.z_str);
	// pipeline: extract(decomp) the query result routine needs to decomp the next stream (block) since
	// avail_out!=0 so z_stream should be recycled (deallocate old slide window and allocate new space) for
	// decomp data from next stream there is no need checking if avail_in==0 since current zlib inflate(tested
	// on zlib 1.2.11) impl guarantees that
	while (z_strmw.z_str.avail_out != 0
		   //&& z_strmw.z_str.avail_in == 0
	) {
		z_strmw.reset();
		chunk_start0 = chunk_start1;
		chunk_start1 = para_.index.upper_bound(chunk_start1->first);
		auto length = chunk_start1->second - chunk_start0->second;
		z_strmw.z_str.avail_in = length;
		ret = inflate(&z_strmw.z_str, Z_NO_FLUSH);
		_unzip_error_handle(comp_type::zlib, ret, &z_strmw.z_str);
	}
}

// istream overload
void unzip_agent<comp_type::dictzip>::unzip_random(
	istream& src_begin,		   // input stream at the start of compressed data
	uint32_t word_data_size,   // query para: size of uncompressed data
	uint64_t word_data_offset, // query para: offset in uncompressed data
	char* dst)				   // sink
{
	src_begin.seekg(0);
	// check whether agent is ready for "random acess". if not, get ready
	if (!para_.random_access_ready()) {
		para_.init(src_begin);
		src_begin.seekg(0);
	}
	// gz header length
	auto GHLEN = para_.gh_len;				// temporary
	constexpr unsigned BUF_LEN = 256 * 256; // length of local char buffer

	char source[BUF_LEN];
	char sink[BUF_LEN];
	// search the dz extra field based on query para
	// "_para.index"
	// key:   offset indicates end of each chunk before comp
	// value: after comp
	auto chunk_start0 = para_.index.lower_bound(word_data_offset);
	auto chunk_start0_was_end = (chunk_start0 == para_.index.end());
	if (chunk_start0_was_end) throw std::invalid_argument{"dictzip query(word_data_offset) out of range"};
	auto chunk_start0_was_begin = (chunk_start0 == para_.index.begin());
	if (!chunk_start0_was_begin) --chunk_start0;
	auto chunk_start1 = para_.index.upper_bound(word_data_offset);
	auto chunk_end = para_.index.upper_bound(word_data_offset + word_data_size);
	if (chunk_start1 == para_.index.end() || chunk_end == para_.index.end())
		throw std::invalid_argument{"dictzip query out of range"};
	// offset of chunk's(after compression) end
	uint64_t chunk_off0 = !chunk_start0_was_begin ? chunk_start0->second : 0;
	uint64_t chunk_off1 = chunk_start1->second;
	// offset of chunk's(plain data) end
	uint64_t chunk_off_plain0 = !chunk_start0_was_begin ? chunk_start0->first : 0;

	// chunk_begin_p indicate the start of any compressed chunk (size = CHLEN)
	auto chunk_begin_p = static_cast<std::streamoff>(chunk_off0);
	// size of first chunk after compression in the range of query
	auto chunk_comp_size = chunk_off1 - chunk_off0;
	src_begin.seekg(static_cast<std::streamoff>(chunk_begin_p + GHLEN));
	src_begin.read(source, chunk_comp_size);
	// irrelevant len(plain data) of data in CHUNK before word_data_offset
	auto irrelevant = word_data_offset - chunk_off_plain0;
	z_stream_wrapper z_strmw{z_stream_wrapper::RAW_INFLATE};
	auto src_p = reinterpret_cast<Bytef*>(source);
	auto sin_p = reinterpret_cast<Bytef*>(sink);
	z_strmw.z_str.next_in = src_p;
	z_strmw.z_str.avail_in = static_cast<uInt>(chunk_comp_size);
	// discard the irrelavant
	z_strmw.z_str.next_out = sin_p;
	z_strmw.z_str.avail_out = irrelevant;
	if (irrelevant) {
		auto ret = inflate(&z_strmw.z_str, Z_NO_FLUSH);
		_unzip_error_handle(comp_type::zlib, ret, &z_strmw.z_str);
	}
	// redirect the decomp destination to dst
	z_strmw.z_str.next_out = reinterpret_cast<Bytef*>(dst);
	z_strmw.z_str.avail_out = word_data_size;
	// extract(decomp) the first part of query result
	auto ret = inflate(&z_strmw.z_str, Z_NO_FLUSH);
	_unzip_error_handle(comp_type::zlib, ret, &z_strmw.z_str);
	// pipeline: extract(decomp) the query result routine needs to decomp the next stream (block) since
	// avail_out!=0 so z_stream should be recycled (deallocate old slide window and allocate new space) for
	// decomp data from next stream there is no need checking if avail_in==0 since current(tested on
	// zlib 1.2.11) impl guarantees that

	while (z_strmw.z_str.avail_out != 0
		   //&& z_strmw.z_str.avail_in == 0
	) {
		z_strmw.reset();
		chunk_begin_p = chunk_start1->second;
		chunk_start0 = chunk_start1;
		chunk_start1 = para_.index.upper_bound(chunk_start1->first);
		auto length = chunk_start1->second - chunk_start0->second;
		src_begin.seekg(static_cast<std::streamoff>(chunk_begin_p + GHLEN));
		// copy one chunks in to memory
		src_begin.read(source, length);
		z_strmw.z_str.next_in = src_p;
		z_strmw.z_str.avail_in = length;
		ret = inflate(&z_strmw.z_str, Z_NO_FLUSH);
		_unzip_error_handle(comp_type::zlib, ret, &z_strmw.z_str);
	}
}

unzip_agent<comp_type::dictzip>::chunk_decompessor_memory::chunk_decompessor_memory(const char* dz_begin,
																					uint64_t dz_len)
	: dz_begin_{dz_begin}, dz_len_{dz_len} {
	boost::iostreams::stream<boost::iostreams::array_source> strz(dz_begin, dz_len);
	para_.init(strz);
}

unzip_agent<comp_type::dictzip>::chunk_decompessor_istream::chunk_decompessor_istream(istream& dz_begin)
	: dz_begin_{dz_begin} {
	para_.init(dz_begin);
}

uint64_t unzip_agent<comp_type::dictzip>::chunk_decompessor_memory::unzip_chunk(
	dz_index::iterator chunk_iter, dz_index::iterator chunk_iter_begin, char* dst,
	uint64_t dst_len_available) {
	z_strmw_.reset();
	auto tmp_iter = chunk_iter;
	const char* chunk_begin_p = dz_begin_ + para_.gh_len;
	uint64_t offset = (tmp_iter != chunk_iter_begin) ? (--tmp_iter)->second : 0;
	chunk_begin_p += offset;
	z_strmw_.z_str.next_in = (Bytef*)(chunk_begin_p);
	z_strmw_.z_str.next_out = (Bytef*)(dst);
	z_strmw_.z_str.avail_in = chunk_iter->second - offset;
	z_strmw_.z_str.avail_out = dst_len_available;
	auto ret = inflate(&z_strmw_.z_str, Z_NO_FLUSH);
	_unzip_error_handle(comp_type::zlib, ret, &z_strmw_.z_str);
	return (dst_len_available - z_strmw_.z_str.avail_out);
}

uint64_t unzip_agent<comp_type::dictzip>::chunk_decompessor_istream::unzip_chunk(
	dz_index::iterator chunk_iter, dz_index::iterator chunk_iter_begin, char* dst,
	uint64_t dst_len_available) {
	z_strmw_.reset();
	auto tmp_iter = chunk_iter;
	uint64_t offset = (tmp_iter != chunk_iter_begin) ? (--tmp_iter)->second : 0;
	z_strmw_.z_str.next_in = (Bytef*)(source);
	z_strmw_.z_str.next_out = (Bytef*)(dst);
	z_strmw_.z_str.avail_in = chunk_iter->second - offset;
	z_strmw_.z_str.avail_out = dst_len_available;
	dz_begin_.seekg(para_.gh_len + offset);
	dz_begin_.read(source, z_strmw_.z_str.avail_in);
	auto ret = inflate(&z_strmw_.z_str, Z_NO_FLUSH);
	_unzip_error_handle(comp_type::zlib, ret, &z_strmw_.z_str);
	return (dst_len_available - z_strmw_.z_str.avail_out);
}

#ifndef unzip_allchunks_to_buf
#define unzip_allchunks_to_buf(d)                                                                            \
	auto plain_file_s = (d).para_.plain_file_size_max();                                                     \
	unique_ptr<char[]> out{new char[plain_file_s]};                                                          \
	auto available = plain_file_s;                                                                           \
	auto p = out.get();                                                                                      \
	auto iter_begin = (d).para_.index.begin();                                                               \
	auto iter_end = (d).para_.index.end();                                                                   \
	auto iter = (d).para_.index.begin();                                                                     \
	for (; iter != iter_end; ++iter) {                                                                       \
		auto decomp_size = (d).unzip_chunk(iter, iter_begin, p, available);                                  \
		available -= decomp_size;                                                                            \
		p += decomp_size;                                                                                    \
	}                                                                                                        \
	return {std::move(out), plain_file_s};
#endif

#ifndef unzip_allchunks_to_strm
#define unzip_allchunks_to_strm(d)                                                                           \
	constexpr unsigned BUF_LEN = 256 * 256;                                                                  \
	char source[BUF_LEN];                                                                                    \
	auto iter_begin = (d).para_.index.begin();                                                               \
	auto iter_end = (d).para_.index.end();                                                                   \
	auto iter = (d).para_.index.begin();                                                                     \
	auto pos0 = dst.tellp();                                                                                 \
	for (; iter != iter_end; ++iter) {                                                                       \
		auto decomp_size = (d).unzip_chunk(iter, iter_begin, source, BUF_LEN);                               \
		dst.write(source, decomp_size);                                                                      \
	}                                                                                                        \
	return dst.tellp() - pos0
#endif

pair<unique_ptr<char[]>, uint64_t> unzip_agent<comp_type::dictzip>::unzip_all(const char* src_begin,
																			  uint64_t src_len) {
	chunk_decompessor_memory decompressor{src_begin, src_len};
	unzip_allchunks_to_buf(decompressor);
}

pair<unique_ptr<char[]>, uint64_t> unzip_agent<comp_type::dictzip>::unzip_all(istream& src) {
	chunk_decompessor_istream decompressor{src};
	unzip_allchunks_to_buf(decompressor);
}

uint64_t unzip_agent<comp_type::dictzip>::unzip_all(const char* src_begin, uint64_t src_len, ostream& dst) {
	chunk_decompessor_memory decompressor{src_begin, src_len};
	unzip_allchunks_to_strm(decompressor);
}

uint64_t unzip_agent<comp_type::dictzip>::unzip_all(istream& src, ostream& dst) {
	chunk_decompessor_istream decompressor{src};
	unzip_allchunks_to_strm(decompressor);
}

} // namespace unzip

//
// Created by Lohengrin on 2018/05/4.
//

#include <lexicon_buddy/utilities/encoding_support.hpp>

namespace encoding_su
{
// this won't be necessary for C++17
#if __cplusplus < 201703L
constexpr char utf8_info::NAME[];
constexpr char utf8_info::ALIAS0[];
constexpr char utf8_info::ALIAS1[];
constexpr char utf8_info::ALIAS2[];
constexpr char utf8_info::locale_[];
constexpr char utf16_info::NAME[];
constexpr char utf16_info::ALIAS0[];
constexpr char utf16_info::ALIAS1[];
constexpr char utf16_info::ALIAS2[];
constexpr char utf16_info::locale_[];
constexpr char gbk_info::NAME[];
constexpr char gbk_info::ALIAS0[];
constexpr char gbk_info::ALIAS1[];
constexpr char gbk_info::ALIAS2[];
constexpr char gbk_info::locale_[];
constexpr char big5_info::NAME[];
constexpr char big5_info::ALIAS0[];
constexpr char big5_info::ALIAS1[];
constexpr char big5_info::ALIAS2[];
constexpr char big5_info::locale_[];
#endif

// print all supported locale in system:
// $ locale -a

} // namespace encoding_su

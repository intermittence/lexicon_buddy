//
//  preview.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/06/1.
//  Copyright © 2018 Lohengrin. All rights reserved.
//

#include <lexicon_buddy/debug/preview.hpp>
#include <lexicon_buddy/utilities/encoding_support.hpp>
#include <lexicon_buddy/utilities/macro_compiler.h>
#include <lexicon_buddy/utilities/unzip_tool.h>
#include <algorithm>
#include <boost/locale.hpp>
namespace lexicon
{
namespace mdx
{

namespace key_index
{
namespace debug
{

void key_index_log(key_index_v& v, std::ostream& log, const string& encoding_canonical) {
	using namespace boost::locale::conv;
	unsigned int c{};
	for (auto& i : v) {
		log << "num_entries[" << c << "]: " << i.num_entries << '\n'
			<< "first_size[" << c << "]: " << i.first_size
			<< '\n'
			// convert to UTF-8 if not
			<< "first_word[" << c << "]: " << to_utf<char>(i.first_word, encoding_canonical) << '\n'
			<< "last_size[" << c << "]: " << i.last_size
			<< '\n'
			// convert to UTF-8 if not
			<< "last_word[" << c << "]: " << to_utf<char>(i.last_word, encoding_canonical) << '\n'
			<< "comp_size[" << c << "]: " << i.comp_size << '\n'
			<< "decomp_size[" << c << "]: " << i.decomp_size << '\n';
		++c;
	}
}

// log the decompressed data
// TODO: when version< 2.0
void key_block_log_plain(unsigned long long num_entries,
						 const char* dat_ptr, // point to key_block[x]
						 const string& encoding_canonical, std::ostream& log) {
	using namespace boost::locale::conv;
	auto encoding_unit = encoding_su::encoding_check_mdx::get_byte_unit(encoding_canonical);

	if (encoding_unit == 1) {
		for (decltype(num_entries) j = 0; j < num_entries; ++j) {
			auto offset = util::endianSwap_64bit(dat_ptr);
			dat_ptr += 8;
			auto ptr_end = dat_ptr;
			while (*ptr_end) {
				++ptr_end;
			}
			// ptr_end += 1;
			// write data
			log << offset << ' ';
			// log.write(dat_ptr, ptr_end - dat_ptr);
			// to UTF-8 and write
			log << to_utf<char>(dat_ptr, ptr_end, encoding_canonical);
			log.put('\n');
			dat_ptr = ptr_end;
		}
	} else {
		for (decltype(num_entries) j = 0; j < num_entries; ++j) {
			auto offset = util::endianSwap_64bit(dat_ptr);
			dat_ptr += 8;
			auto ptr_end = dat_ptr;
			while (std::any_of(ptr_end, ptr_end + encoding_unit, [](auto c) { return c; })) {
				ptr_end += encoding_unit;
			}
			// ptr_end += encoding_unit;
			// write data
			log << offset << ' ';
			// log.write(dat_ptr, ptr_end - dat_ptr);
			// to UTF-8 and write
			log << to_utf<char>(dat_ptr, ptr_end, encoding_canonical);
			log.put('\n');
			dat_ptr = ptr_end;
		}
	}
}

// TODO: test. go through key_index and key blocks num_entries and comp decomp size
// log ONE key_block
// routine automatically handle data of different compression format(based on comp_type indicated by
// data header)
// TODO: when version< 2.0
void key_block_log(const slide& i,
				   const char* dat_ptr, // point to key_blocks[x] byte 0
				   const string& encoding_canonical, std::ostream& log, bool show_content) {
	// auto encoding_unit = encoding_su::encoding_check::get_byte_unit(encoding_canonical);
	// get key_index data
	auto num_entries = i.num_entries;
	auto comp_t = static_cast<comp_type>(util::endianSwap_32bit(dat_ptr));
	auto comp_t_str = unzip::comp_type_to_string(comp_t);
	auto checksum = util::endianSwap_32bit(dat_ptr + 4);
	log << "comp_type: " << comp_t_str << '\n' << "checksum: " << checksum << '\n';
	if (show_content) {
		// TODO: decomp
		if (comp_t != comp_type::no_comp) {
			// NOTE: decomp_size? careful!
			unique_ptr<char[]> decomp_buff{new char[i.decomp_size - 8]};
			// NOTE: dat_ptr? careful!
			unzip::unzip_strm_generic(comp_t, dat_ptr + 8, i.comp_size, decomp_buff.get(), i.decomp_size);
			key_block_log_plain(num_entries, decomp_buff.get(), encoding_canonical, log);
		} else {
			// NOTE: dat_ptr? careful!
			key_block_log_plain(num_entries, dat_ptr + 8, encoding_canonical, log);
		}
	}
}
} // namespace debug
} // namespace key_index

namespace record
{
namespace debug
{
// TODO: bookmark
// TODO: when version< 2.0
void record_block_log(const slide& i, std::ostream& log, const string& encoding_canonical,
					  const char* dat_ptr, bool show_content) {

	log << "comp_size: " << i.comp_size << '\n' << "decomp_size: " << i.decomp_size << '\n';
	auto comp_t = static_cast<comp_type>(i.comp_type);
	if (i.comp_type_flag)
		// unzip::comp_type_to_string will throw if static_cast goes wrong(i.comp_type out of range)
		log << "comp_type: " << unzip::comp_type_to_string(comp_t) << '\n';
	log << "offset: " << i.origin_offset_block_begin << '\n';
	if (show_content && i.origin_offset_block_begin && dat_ptr) {
		auto decomp_data_size = i.decomp_size - 8;
		log << "record block content: \n";
		using namespace boost::locale::conv;
		if (comp_t != comp_type::no_comp) {
			unique_ptr<char[]> decomp_buff{new char[decomp_data_size]};
			unzip::unzip_strm_generic(comp_t, dat_ptr + 8, i.comp_size, decomp_buff.get(), i.decomp_size);
			log << to_utf<char>(decomp_buff.get(), decomp_buff.get() + decomp_data_size, encoding_canonical)
				<< '\n';
		} else {
			log << to_utf<char>(dat_ptr + 8, dat_ptr + 8 + decomp_data_size, encoding_canonical) << '\n';
		}
	}
}
} // namespace debug
} // namespace record
namespace debug
{
// TODO: when version< 2.0
// when detail==true, each key and record will be written in log
void mdx_profiler::profile(const string& mdx_path, std::ostream& log, bool detail) {
	mdx_data_std_stream mdx_dat{mdx_path};
	log << '\n' << mdx_path << '\n';
	// header section:
	log << "===========Header Section=============" << '\n'
		<< "GeneratedByEngineVersion: " << mdx_dat._h_sec.GeneratedByEngineVersion << '\n'
		<< "RequiredEngineVersion: " << mdx_dat._h_sec.RequiredEngineVersion << '\n'
		<< "Encrypted: " << mdx_dat._h_sec.Encrypted << '\n'
		<< "Encoding: " << mdx_dat._h_sec.Encoding << '\n'
		<< "Format: " << mdx_dat._h_sec.Format << '\n'
		<< "CreationDate: " << mdx_dat._h_sec.CreationDate << '\n'
		<< "Compact: " << mdx_dat._h_sec.Compact << '\n'
		<< "Compat: " << mdx_dat._h_sec.Compat << '\n'
		<< "KeyCaseSensitive: " << mdx_dat._h_sec.KeyCaseSensitive << '\n'
		<< "Description: " << mdx_dat._h_sec.Description << '\n'
		<< "Title: " << mdx_dat._h_sec.Title << '\n'
		<< "DataSourceFormat: " << mdx_dat._h_sec.DataSourceFormat << '\n'
		<< "StyleSheet: " << mdx_dat._h_sec.StyleSheet << '\n'
		<< "RegisterBy: " << mdx_dat._h_sec.RegisterBy << '\n'
		<< "RegCode: " << mdx_dat._h_sec.RegCode << '\n';
	log.flush();
	if (mdx_dat._h_sec.Encrypted) {
		log << '\n' << "LOGGING RETURNED BEFORE COMPLETION SINCE THE DATA IS ENCRYPTED.";
		log.flush();
		return;
	}

	// TODO: handle encoding based on header section
	// keyword section:
	log << "===========Keyword Section=============" << '\n';
	// <keyword section "header">
	log << "num_blocks: " << mdx_dat._k_sec.num_blocks << '\n'
		<< "num_entries: " << mdx_dat._k_sec.num_entries << '\n'
		<< "key_index_decomp_len: " << mdx_dat._k_sec.key_index_decomp_len << '\n'
		<< "key_index_comp_len	: " << mdx_dat._k_sec.key_index_comp_len << '\n'
		<< "key_blocks_len: " << mdx_dat._k_sec.key_blocks_len << '\n'
		<< "checksum: " << mdx_dat._k_sec.checksum << '\n';
	// <key index log>
	/*key index's comp_type and checksum(data header)*/
	log << "-----key_index-----" << '\n'
		<< "comp_type: " << unzip::comp_type_to_string(mdx_dat._k_sec.key_index_comp_type) << '\n'
		<< "checksum: " << mdx_dat._k_sec.key_index_comp_checksum << '\n';
	// content of key index

	log << "***key_index_data***" << '\n';
	// load and possibly decomp the key_index data
	unique_ptr<char[]> decomp_buf{new char[mdx_dat._k_sec.key_index_decomp_len]};
	if (!mdx_dat._k_sec.key_index_comp_type) {
		auto comp_t = static_cast<comp_type>(mdx_dat._k_sec.key_index_comp_type);
		unique_ptr<char[]> comp_buf{new char[mdx_dat._k_sec.key_index_comp_len]};
		mdx_dat.t.seekg(static_cast<std::streamoff>(mdx_dat._k_sec.keyword_sect_offset +
													mdx_dat._k_sec.keyword_sect_to_key_index));
		mdx_dat.t.read(comp_buf.get(), static_cast<std::streamsize>(mdx_dat._k_sec.key_index_comp_len));
		unzip::unzip_strm_generic(comp_t, comp_buf.get(), mdx_dat._k_sec.key_index_comp_len, decomp_buf.get(),
								  mdx_dat._k_sec.key_index_decomp_len);
	} else {
		mdx_dat.t.seekg(static_cast<std::streamoff>(mdx_dat._k_sec.keyword_sect_offset +
													mdx_dat._k_sec.keyword_sect_to_key_index));
		mdx_dat.t.read(decomp_buf.get(), static_cast<std::streamsize>(mdx_dat._k_sec.key_index_decomp_len));
	}
	auto encoding_unit = encoding_su::encoding_check_mdx::get_byte_unit(mdx_dat._h_sec.Encoding);
	auto key_index_list = key_index::read_key_index(decomp_buf.get(), mdx_dat._k_sec.num_blocks,
													encoding_unit, mdx_dat.is_version_2_0());
	// log key_index
	key_index::debug::key_index_log(key_index_list, log, mdx_dat._h_sec.Encoding);
	log << "***end of key_index_data***" << '\n';
	log.flush();
	// <key blocks log> log each block's comp_type and checksum
	log << "***key_block_data***" << '\n';
	auto& k_sec = mdx_dat._k_sec;
	auto offset0 = k_sec.keyword_sect_offset + k_sec.keyword_sect_to_key_index + k_sec.key_index_comp_type
					   ? k_sec.key_index_comp_len
					   : k_sec.key_index_decomp_len;
	unsigned int index_count{};
	for (auto& i : key_index_list) {
		log << "key_block[" << index_count << ']' << '\n';
		// read data header
		char header_buff[8];
		mdx_dat.t.seekg(static_cast<std::streamoff>(offset0));
		mdx_dat.t.read(header_buff, 8);
		auto com_t = static_cast<comp_type>(util::endianSwap_32bit(header_buff));
		auto check_sum = util::endianSwap_32bit(header_buff + 4);
		log << "comp_type: " << comp_type_to_string(com_t) << '\n' << "checksum: " << check_sum << '\n';
		auto data_buff_size = (com_t == comp_type::no_comp) ? i.decomp_size : i.comp_size;
		unique_ptr<char[]> data_buff{new char[data_buff_size]};
		mdx_dat.t.read(data_buff.get(), static_cast<std::streamsize>(data_buff_size));
		// log key_block
		key_index::debug::key_block_log(i, data_buff.get(), mdx_dat._h_sec.Encoding, log, detail);
		++index_count;
	}
	log.flush();
	// record section:
	log << "===========Record section=============" << '\n' << "***Record section header***" << '\n';
	auto& r_sec = mdx_dat._r_sec;
	log << "num_blocks: " << r_sec.num_blocks << '\n'
		<< "num_entries: " << r_sec.num_entries << '\n'
		<< "index_len: " << r_sec.index_len << '\n'
		<< "blocks_len: " << r_sec.blocks_len << '\n';
	// <record id and record data compression type>
	auto index_buff_s = 16 * r_sec.num_blocks;
	unique_ptr<char[]> index_buff{new char[index_buff_s]};
	auto offset_seek = static_cast<std::streamoff>(mdx_dat._r_sec.block_offset +
												   mdx_dat._r_sec.sect_offset_to_com_decom_sizes_offset);
	mdx_dat.t.seekg(offset_seek);
	mdx_dat.t.read(index_buff.get(), static_cast<std::streamoff>(index_buff_s));
	auto record_index_v =
		record::read_record_index(index_buff.get(), encoding_unit, mdx_dat.is_version_2_0());
	record::record_comp_t_offset_init(mdx_dat.t, static_cast<std::streamoff>(mdx_dat._r_sec.block_offset),
									  record_index_v);
	// print record index

	log << "record_index:";
	unsigned int count{};
	auto offset_tmp = static_cast<std::streamoff>(mdx_dat._r_sec.block_offset);
	unique_ptr<char[]> buf_tmp{};
	unsigned long long buf_tmp_s{};
	for (auto& i : record_index_v) {
		log << "record[" << count << "]: \n";
		mdx_dat.t.seekg(offset_tmp + i.origin_offset_block_begin);
		auto buf_i_s = i.comp_type ? i.comp_size : i.decomp_size;
		if (buf_i_s > buf_tmp_s) {
			buf_tmp.reset(new char[buf_i_s]);
			buf_tmp_s = buf_i_s;
		}
		// read a record
		mdx_dat.t.read(buf_tmp.get(), static_cast<std::streamsize>(buf_i_s));
		// decomp if necessary and log record
		record::debug::record_block_log(i, log, mdx_dat._h_sec.Encoding, buf_tmp.get(), detail);
		++count;
	}
	log.flush();
	log << "===========End of Log=============" << '\n';
	log.flush();
}
} // namespace debug
} // namespace mdx
} // namespace lexicon

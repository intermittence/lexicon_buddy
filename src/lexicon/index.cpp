//
//  index.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 12/28/2018 03:36:37.
//  Copyright © 2018 Lohengrin. All rights reserved.
//
#include <iostream>
#include <lexicon_buddy/lexicon/index.h>
#include <lexicon_buddy/lexicon/mdxContainer.hpp>
#include <lexicon_buddy/lexicon/stardictcontainer.h>
#include <lexicon_buddy/utilities/utilities.hpp>

namespace lexicon
{
namespace index
{

bool res_file_ext_filter(const string& extension) {
	auto t = str_to_res_type(extension);
	return res_file_ext_filter(t);
}

bool res_file_ext_filter(const char* extension) {
	auto t = str_to_res_type(extension);
	return res_file_ext_filter(t);
}

bool res_file_ext_filter(res_type t) {
	if (t == decltype(t)::mdx || t == decltype(t)::startdict_dict_dz) return true;
	return false;
}

string index_agent::get_node_res_filename(const ptree_type& item_node) {
	return item_node.get<string>("<xmlattr>.basename") + item_node.get<string>("<xmlattr>.res_bundle_type");
}

void index_agent::res_map_rebuild() {
	res_map = decltype(res_map){};
	auto& instance = cfg_.get_child("instance");
	auto dict_path = get_dict_dir();
	for (auto& i : instance) {
		if (i.first == "item") res_map_add_item(i.second, dict_path);
	}
}

void index_agent::res_map_add_item(const ptree_type& item_container, const fs::path& dict_path) {
	auto filename = item_container.get<string>("<xmlattr>.basename") +
					item_container.get<string>("<xmlattr>.res_bundle_type");
	auto tmp = dict_path / filename;
	if (res_map.find(tmp) != res_map.end()) throw xml_error{"res_map_add_item: dict duplication"};
	res_map.try_emplace(std::move(tmp), item_container.get<string>("<xmlattr>.alias", ""));
}

bool index_agent::res_already_exist(const fs::path& file_path) {
	return res_map.find(file_path) != res_map.end();
}

LEXICON_BUDDY_EXPORT index_agent::~index_agent() { cfg_save_if_dirty(); }

index_agent::node_genre index_agent::get_node_genre(const string& name) {
	if (name == "item") {
		return node_genre::item;
	} else if (name == "instance") {
		return node_genre::instance;
	} else if (name == "index") {
		return node_genre::index;
	} else if (name == "magic") {
		return node_genre::magic;
	} else {
		return node_genre::unknown;
	}
}

index_agent::node_state index_agent::get_node_state(const value_type& node) {
	auto genre = get_node_genre(node.first);
	switch (genre) {
		case node_genre::item: {
			auto basename = node.second.get<string>("<xmlattr>.basename", "");
			if (basename == "") {
				return node_state::item_empty;
			} else if (node.second.get<string>("<xmlattr>.res_bundle_type", "") == "" ||
					   node.second.get<string>("<xmlattr>.res_media_type", "") == "" ||
					   node.second.get<string>("<xmlattr>.update", "") == "") {
				return node_state::error;
			} else {
				auto dict_folder = get_dict_dir();
				auto dict_type = str_to_res_type(node.second.get<string>("<xmlattr>.res_bundle_type", ""));
				switch (dict_type) {
					case res_type::mdx: {
						auto dict_path = get_dict_dir() / basename;
						dict_path.replace_extension(".mdx");
						if (util::exists_posix1(dict_path)) {
							auto last_change = util::file_last_change_date(dict_path);
							if (last_change > node.second.get<std::time_t>("<xmlattr>.update", 0))
								return node_state::res_outdate;
						} else {
							return node_state::node_file_error;
						}
					} break;
					case res_type::startdict_dict_dz: {
						auto dict_path = get_dict_dir() / basename;
						auto ifo_path = dict_path;
						auto idx_path = dict_path;
						dict_path.replace_extension(".dict.dz");
						ifo_path.replace_extension(".ifo");
						idx_path.replace_extension(".idx");
						if (util::exists_posix1(dict_path) && util::exists_posix1(ifo_path) &&
							util::exists_posix1(idx_path)) {
							auto last_change = util::file_last_change_date(dict_path);
							auto last_change_ifo = util::file_last_change_date(ifo_path);
							auto last_change_idx = util::file_last_change_date(idx_path);
#if Unix_Style
							auto last_update_max =
								std::max(std::max(last_change, last_change_ifo), last_change_idx);
#else
							auto last_update_max = max(max(last_change, last_change_ifo), last_change_idx);
#endif
							if (last_update_max > node.second.get<std::time_t>("<xmlattr>.update", 0))
								return node_state::res_outdate;
						} else {
							return node_state::node_file_error;
						}
					}
						return node_state::ok;
					default:
						return node_state::error;
				}
				// index duplication check
				uint8_t index_count{};
				for (auto& i : node.second) {
					if (i.first == "index") ++index_count;
				}
				if (index_count > 1) {
					return node_state::error;
				} else if (index_count == 0) {
					return node_state::no_index;
				}
				return node_state::ok;
			}
		}
		case node_genre::index: {
			auto s_filename = node.second.get<string>("search", "");
			auto c_filename = node.second.get<string>("coordinate", "");
			if (s_filename == "" || c_filename == "") {
				return node_state::index_empty;
			} else if (node.second.get<string>("<xmlattr>.update", "") == "") {
				return node_state::error;
			} else {
				// todo index file check
				auto trie_path = get_index_dir() / s_filename;
				auto coor_path = get_index_dir() / c_filename;
				if (util::exists_posix1(trie_path) && util::exists_posix1(coor_path)) {
					return node_state::ok;
				} else {
					return node_state::node_file_error;
				}
			}
		}
		case node_genre::instance:
			if (node.second.get<string>("<xmlattr>.res_dir", "") == "" ||
				node.second.get<string>("<xmlattr>.index_dir", "") == "" ||
				node.second.get<string>("<xmlattr>.update", "") == "") {
				return node_state::error;
			} else {
				return node_state::ok;
			}
		case node_genre::magic:
			if (node.second.get<string>("", "") == magic_word) {
				return node_state::ok;
			} else {
				return node_state::error;
			}
		default:
			return node_state::error;
	}
}

// instance's update time will always be updated even no actual changes happened
LEXICON_BUDDY_EXPORT void index_agent::node_update(value_type& node, node_genre genre, node_state state) {
	switch (genre) {
		case node_genre::item:
			switch (state) {
				case node_state::ok: {
					uint8_t index_count{};
					value_type* node_index{};
					for (auto& i : node.second) {
						auto type = get_node_genre(i.first);
						if (type == node_genre::index) {
							node_index = &i;
							++index_count;
						}
					}
					// index node missing or duplicated
					if (index_count == 0 || index_count > 1) {
						node.second.put("<xmlattr>.flag", "delete");
					} else {
						auto index_state = get_node_state(*node_index);
						if (index_state == node_state::node_file_error) {
							node.second.put("<xmlattr>.flag", "delete");
						} else {
							node_update(*node_index, node_genre::index, index_state);
						}
					}
				} break;
				case node_state::res_outdate:
					try {
						// delete index node
						node.second.erase("index");
						// update res update
						auto res_last_update = get_dict_dir() / get_node_res_filename(node.second);
						node.second.put("<xmlattr>.update", util::file_last_change_date(res_last_update));
						// rebuild index
						auto res_str = node.second.get<string>("<xmlattr>.res_bundle_type", "");
						auto res_t = str_to_res_type(res_str.c_str());
						auto res_path = (get_dict_dir() / node.second.get<string>("<xmlattr>.basename", ""))
											.replace_extension(res_str);
						auto index_node = cfg_build_and_add_index(res_t, res_path);
						// add index node
						node.second.add_child("index", index_node);
					} catch (...) {
						// if fail
						node.second.put("<xmlattr>.flag", "delete");
					}
					break;
				case node_state::node_file_error:
				default:
					node.second.put("<xmlattr>.flag", "delete");
					break;
			}
			break;
		case node_genre::index:
			switch (state) {
				case node_state::ok:
				default:
					break;
			}
			break;
		case node_genre::instance:
			switch (state) {
				case node_state::ok:
					for (auto& i : node.second) {
						auto genre = get_node_genre(i.first);
						if (genre == node_genre::item) {
							auto state = get_node_state(i);
							node_update(i, genre, state);
						}
					}
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
}
void index_agent::delete_error_node() {
	auto& instance = cfg_.get_child("instance");
	bool cleared{};
	while (!cleared) {
		bool node_deleted{};
		for (auto i = instance.begin(); i != instance.end(); ++i) {
			if (i->first == "item") {
				if (i->second.get<string>("<xmlattr>.flag", "") == "delete") {
					instance.erase(i);
					node_deleted = true;
					break;
				}
			}
		}
		if (!node_deleted) {
			cleared = true;
		}
	}
}

LEXICON_BUDDY_EXPORT index_agent::ptree_type* index_agent::import(const fs::path& src_dict_file) {
	auto copy_dict = [&](res_type t, const fs::path& src, fs::path& target) {
		auto dict_dir = get_dict_dir();
		auto dst = dict_dir / src.filename();
		target = dst;
		switch (t) {
			case res_type::mdx:
				if (src.parent_path() == dst.parent_path()) return true;
#ifndef NDEBUG
				if (!src.has_filename()) throw std::runtime_error{"src is not a file path\n"};
#endif
				fs::copy(src, dst);
				return true;
			case res_type::startdict_dict_dz: {
				auto src_ifo = src;
				auto src_idx = src;
				src_ifo.replace_extension("").replace_extension("").replace_extension(".ifo");
				src_idx.replace_extension("").replace_extension("").replace_extension(".idx");
				auto dst_ifo = dict_dir / src_ifo.filename();
				auto dst_idx = dict_dir / src_idx.filename();
#ifndef NDEBUG
				if (!src.has_filename() || !src_ifo.has_filename() || !src_idx.has_filename())
					throw dict_file_err{"src (or ifo or idx) is not a file path\n"};
#endif
				if (util::exists_posix1(src_ifo) && util::exists_posix1(src_idx)) {
					if (src.parent_path() == dst) {
						return true;
					} else {
						fs::copy(src, dst);
						fs::copy(src_ifo, dst_ifo);
						fs::copy(src_idx, dst_idx);
						return true;
					}
				} else {
					throw dict_corruption{"cfg_add_item error: stardict, missing files"};
				}
			}
			default:
				throw dict_corruption{"cfg_add_item error: wrong res_type"};
		}
	};

	// identify dict type and parent folder
	fs::path dict_file_path_tmp{src_dict_file};
	auto dp_parent = dict_file_path_tmp.parent_path();
	if (dp_parent == get_dict_dir() or dp_parent == get_index_dir())
		throw dict_file_err{"can not import dict from app's internal folder!"};
	auto type = category_filename(src_dict_file);
	auto bundle_type = res_type_to_bundle_type(type);

	build_result_type result{};
	switch (bundle_type) {
		case res_bundle_type::mdx: {
			// file existence
			if (!util::exists_posix1(dict_file_path_tmp))
				throw dict_file_err{GET_U8STRING(dict_file_path_tmp) + " file missing"};
			// name duplication?
			if (res_already_exist(get_dict_dir() / dict_file_path_tmp.filename()))
				throw dict_duplication{GET_U8STRING(dict_file_path_tmp.filename()) + " duplication"};
			// validate
			if (!validate(dict_file_path_tmp, type)) throw dict_corruption{"corrupted mdx"};
			// build index
			result = build_generic(dict_file_path_tmp);
		} break;
		case res_bundle_type::startdict_dict_dz_bundle: {
			dict_file_path_tmp.replace_extension("").replace_extension("");
			auto& dictdz_path = dict_file_path_tmp;
			auto ifo_path = dict_file_path_tmp;
			auto idx_path = dict_file_path_tmp;
			dictdz_path.replace_extension(".dict.dz");
			ifo_path.replace_extension(".ifo");
			idx_path.replace_extension(".idx");
			// file existence
			if (!util::exists_posix1(dictdz_path) || !util::exists_posix1(ifo_path) ||
				!util::exists_posix1(idx_path))
				throw dict_file_err{"stardict file missing"};
			// name duplication?
			if (res_already_exist(get_dict_dir() / dictdz_path.filename()))
				throw dict_duplication{GET_U8STRING(dictdz_path.filename()) + " duplication"};
			// validate
			if (!validate(dictdz_path, res_type::startdict_dict_dz) ||
				!validate(ifo_path, res_type::startdict_ifo) || !validate(idx_path, res_type::startdict_idx))
				throw dict_corruption{"corrupted stardict"};
			// build index
			result = build_generic(idx_path);
		} break;
		default:
			break;
	}

	if (!result.success) throw dict_index_err{"building index failed"};
	// copy
	fs::path target{};
	if (!copy_dict(type, src_dict_file, target)) {
		fs::remove(fs::path{result.coor_path});
		fs::remove(fs::path{result.trie_path});
		throw dict_file_err{"copy_dict failed"};
	}
	auto item_node = cfg_gen_item_node(type, target, result);
	auto ptr = &cfg_.add_child("instance.item", item_node);
	// cfg update
	auto time = system_clock::to_time_t(system_clock::now());
	cfg_.put("instance.<xmlattr>.update", time);
	return ptr;
}

LEXICON_BUDDY_EXPORT index_agent::index_agent(const char* dict_dir, const char* index_dir,
											  const char* cfg_path)
	: cfgpath_(cfg_path), dirty_(true) {
	if (util::exists_posix1(cfg_path)) {
		pt::read_xml(cfg_path, cfg_, pt::xml_parser::trim_whitespace);
		if (cfg_instance_validation_fix(true, true)) {
			res_map_rebuild();
			return;
		}
	}
	cfg_reset(dict_dir, index_dir, cfg_path);
}

LEXICON_BUDDY_EXPORT void index_agent::cfg_load() {
	pt::read_xml(GET_U8_C_STR(cfgpath_), cfg_, pt::xml_parser::trim_whitespace);
}

LEXICON_BUDDY_EXPORT void index_agent::cfg_save() {
	pt::write_xml(GET_U8_C_STR(cfgpath_), cfg_, std::locale(), pt::xml_writer_make_settings<string>('\t', 1));
	dirty_ = false;
}

LEXICON_BUDDY_EXPORT res_type index_agent::category_filename(const fs::path& dict_filename) {
	auto ext = dict_filename.extension().string();
	if (ext == ".mdx") return res_type::mdx;
	if (ext == ".mdd") return res_type::mdd;
	if (ext == ".idx") return res_type::startdict_idx;
	if (ext == ".ifo") return res_type::startdict_ifo;
	if (ext == ".dz") {
		auto filename = dict_filename.filename().string();
		if (filename.find(".dict.dz") != string::npos) return res_type::startdict_dict_dz;
	}
	return res_type::invalid;
}

LEXICON_BUDDY_EXPORT bool index_agent::validate(const char* dict_file_path, res_type dict_type) {
	try {
		switch (dict_type) {
			case res_type::mdx: {
				mdx::mdx_data_mmap mdx_res{dict_file_path};
				return mdx_res.is_valid();
			}
			case res_type::mdd:
				// TODO: not implemented yet
				return false;
			case res_type::startdict_ifo: {
				fs::path ifo{dict_file_path};
				error_code ec;
				if (!fs::is_regular_file(ifo, ec)) return false;
				util::mmap_w ifo_mmap{GET_U8_C_STR(ifo)};
				// ifo validation
				if (stardict::ifo_content_validation(ifo_mmap.data()) != stardict::res_type::dict_ifo)
					return false;
				return true;
			}
			case res_type::startdict_dict_dz:
				return true;
			case res_type::startdict_idx:
				return true;
			default:
				return false;
		}
	} catch (const std::exception& e) {
		std::cerr << e.what() << '\n';
	}
	return false;
}

index_agent::build_result_type index_agent::build_generic(const char* dict_file_path,
														  const char* target_trie_without_suffix,
														  const char* target_coordinate_without_suffix) {
	auto type = category_filename(dict_file_path);
	switch (type) {
		case res_type::mdx:
			return build<res_type::mdx>(dict_file_path, target_trie_without_suffix,
										target_coordinate_without_suffix);
		case res_type::startdict_idx:
			return build<res_type::startdict_idx>(dict_file_path, target_trie_without_suffix,
												  target_coordinate_without_suffix);
		default:
			return build_result_type{};
	}
}

// index_agent::build_result_type index_agent::build_generic_to_dst(const fs::path& dict_file_path,
//																 const fs::path& dst_index_dir) {
//	// two replace operation take care of ".dict.dz"
//	auto dict_filename_withou_suffix =
//		fs::path{dict_file_path}.replace_extension("").replace_extension("").filename();
//	auto trie_path_without_suffix = dst_index_dir / dict_filename_withou_suffix;
//	auto coor_path_without_suffix = dst_index_dir / dict_filename_withou_suffix;
//	return build_generic(GET_U8_C_STR(dict_file_path), GET_U8_C_STR(trie_path_without_suffix),
//						 GET_U8_C_STR(coor_path_without_suffix));
//}

LEXICON_BUDDY_EXPORT index_agent::build_result_type index_agent::build_generic(const char* dict_file_path) {
	// two replace operation take care of ".dict.dz"
	auto dict_filename_withou_suffix =
		fs::path{dict_file_path}.replace_extension("").replace_extension("").filename();
	auto trie_path_without_suffix = get_index_dir() / dict_filename_withou_suffix;
	auto coor_path_without_suffix = get_index_dir() / dict_filename_withou_suffix;
	return build_generic(dict_file_path, GET_U8_C_STR(trie_path_without_suffix),
						 GET_U8_C_STR(coor_path_without_suffix));
}

LEXICON_BUDDY_EXPORT index_agent::build_result_type
index_agent::build_generic(const fs::path& dict_file_path) {
	return build_generic(GET_U8_C_STR(dict_file_path));
}

LEXICON_BUDDY_EXPORT bool index_agent::cfg_instance_validation_fix(bool through_check, bool delete_error) {
	// <magic>xxxxxxxxx</magic>
	auto find_magic = (cfg_.get<string>("magic", "") == magic_word);
	if (!find_magic || !through_check) {
		return find_magic;
	} else {
		// count instance num
		uint32_t instance_num{};
		value_type* ptr{};
		for (auto& i : cfg_) {
			if (get_node_genre(i.first) == node_genre::instance) {
				ptr = &i;
				++instance_num;
			}
		}
		if (instance_num == 0 || instance_num > 1) return false;
		// check and update instance
		auto state = get_node_state(*ptr);
		if (state != node_state::ok) return false;
		node_update(*ptr, node_genre::instance, state);
		if (delete_error) delete_error_node();
		return true;
	}
};

// assume resource is valid
LEXICON_BUDDY_EXPORT void index_agent::cfg_item_set_media_type(pt::ptree& item) {
	auto resource_type = str_to_res_type(item.get<string>("<xmlattr>.res_bundle_type"));
	if (resource_type == res_type::invalid) return;
	switch (resource_type) {
		case lexicon::res_type::mdx:
			// text
			item.put("<xmlattr>.res_media_type", "text");
			break;
		case lexicon::res_type::startdict_dict_dz:
			// check based on stardict ifo's "sametypesequence"
			cfg_item_stardict_set_media_type(item);
			break;
		default:
			throw std::runtime_error{"cfg_item_set_media_type fail, unsupported types"};
	}
}

LEXICON_BUDDY_EXPORT void index_agent::cfg_item_stardict_set_media_type(pt::ptree& item) {
	auto path = get_dict_dir();
	auto base_name = item.get<string>("<xmlattr>.basename");
	path /= base_name;
	path.replace_extension(".ifo");
	stardict::ifo_str ifo{GET_U8_C_STR(path)};
	auto& sametypesequence = ifo.sametypesequence;
	cfg_item_stardict_set_media_type(item, sametypesequence);
}

LEXICON_BUDDY_EXPORT void index_agent::cfg_item_stardict_set_media_type(pt::ptree& item,
																		string sametypesequence) {
	auto contain_text = [&]() -> bool {
		return (sametypesequence.find('m') != string::npos || sametypesequence.find('l') != string::npos ||
				sametypesequence.find('g') != string::npos || sametypesequence.find('t') != string::npos ||
				sametypesequence.find('x') != string::npos || sametypesequence.find('y') != string::npos ||
				sametypesequence.find('k') != string::npos || sametypesequence.find('w') != string::npos ||
				sametypesequence.find('h') != string::npos || sametypesequence.find('n') != string::npos);
	};
	auto contain_sound = [&]() -> bool { return sametypesequence.find('W') != string::npos; };
	auto contain_pic = [&]() -> bool { return sametypesequence.find('P') != string::npos; };
	auto contain_res = [&]() -> bool { return sametypesequence.find('r') != string::npos; };

	string str{};
	bool t = contain_text();
	bool s = contain_sound();
	bool p = contain_pic();
	bool r = contain_res();
	if (r) {
		str = "unknown";
	} else {
		if (t) {
			if (!p && !s) {
				str = "text";
			} else {
				str = "unknown";
			}
		} else {
			if (p) {
				if (s) {
					str = "unknown";
				} else {
					str = "pic";
				}
			} else {
				if (s) {
					str = "sound";
				} else {
					auto res_name = item.get<string>("<xmlattr>.basename");
					throw std::runtime_error{res_name + ": invalid sametypesequence."};
				}
			}
		}
	}
	if (!str.empty()) item.put("<xmlattr>.res_media_type", str);
}

LEXICON_BUDDY_EXPORT index_agent::ptree_type index_agent::cfg_build_and_add_index(res_type t,
																				  const fs::path& file_path) {
	// prepare coordinate and trie path
	auto coor_path = get_index_dir() / (file_path.filename().replace_extension("").replace_extension(""));
	auto trie_path = coor_path;
	// build
	auto build_result =
		build_generic(GET_U8_C_STR(file_path), GET_U8_C_STR(coor_path), GET_U8_C_STR(trie_path));
	// fill index_node
	return cfg_build_result_to_index_node(build_result);
}

LEXICON_BUDDY_EXPORT index_agent::ptree_type
index_agent::cfg_build_result_to_index_node(const build_result_type& build_result) {
	if (!build_result.success) throw std::runtime_error{"index build failed\n"};
	pt::ptree index_node{};
	pt::ptree p_t{GET_U8STRING(fs::path{build_result.trie_path}.filename())};
	pt::ptree p_c{GET_U8STRING(fs::path{build_result.coor_path}.filename())};
	auto tt = util::file_last_change_date(build_result.trie_path.c_str());
	auto tc = util::file_last_change_date(build_result.coor_path.c_str());
	p_t.put("<xmlattr>.update", tt);
	p_c.put("<xmlattr>.update", tc);
	index_node.add_child("search", p_t);
	index_node.add_child("coordinate", p_c);
	// auto now_t = system_clock::to_time_t(system_clock::now());
#if Unix_Style
	index_node.put("<xmlattr>.update", std::max(tt, tc));
#else
	index_node.put("<xmlattr>.update", max(tt, tc));
#endif
	return index_node;
}

const char* extension_2_extra_flag(const fs::path& ext) {
	auto str = GET_U8STRING(ext);
	if (str.find("32")) return "32";
	if (str.find("64")) return "64";
	return "";
}

LEXICON_BUDDY_EXPORT index_agent::ptree_type
index_agent::cfg_gen_item_node(res_type t, const fs::path& file_path, const build_result_type& build_result) {
	if (build_result.success) {
		ptree_type item{};
		auto basename = GET_U8STRING(file_path.filename().replace_extension("").replace_extension(""));
		item.put("<xmlattr>.basename", basename);
		item.put("<xmlattr>.res_bundle_type", res_type_to_str(t));
		// item node: save main dict file last change date
		item.put("<xmlattr>.update", system_clock::to_time_t(system_clock::now()));
		auto c_path = fs::path{build_result.coor_path};
		// item node: offset size
		item.put("<xmlattr>.extra", extension_2_extra_flag(c_path.extension()));
		// generate index_node
		pt::ptree index_node = cfg_build_result_to_index_node(build_result);
		if (index_node.empty()) return ptree_type{};
		// save in cfg
		item.add_child("index", index_node);
		// add media type info
		cfg_item_set_media_type(item);
		res_map_add_item(item, get_dict_dir());
		// cfg_.add_child("instance.item", item);
		return item;
	} else {
		throw std::runtime_error{"index build failed\n"};
	}
}

LEXICON_BUDDY_EXPORT bool index_agent::cfg_item_discovery() {
#ifndef NDEBUG
	cout << "index_agent::cfg_item_discovery()" << endl;
#endif
	dirty_ = true;
	// set new_item when there's a new file (even it's not valid)
	bool new_item{};
	// update updatetime if routine had scaned the dictionary folder (even new dict unfound)
	auto last_update_cfg = cfg_.get<std::time_t>("instance.<xmlattr>.update");
	auto task = [&](res_type t, const fs::path& file_path) {
		auto valid = validate(file_path);
		if (valid) {
			// build index
			switch (t) {
				case res_type::mdx: {
					auto build_result = build_generic(file_path);
					if (build_result.success) {
						auto item = cfg_gen_item_node(t, file_path, build_result);
						cfg_.add_child("instance.item", item);
					}
				} break;
				case res_type::startdict_dict_dz: {
					auto idx_path = file_path;
					idx_path.replace_extension("").replace_extension(".idx");
					auto build_result = build_generic(idx_path);
					if (build_result.success) {
						auto item = cfg_gen_item_node(t, file_path, build_result);
						cfg_.add_child("instance.item", item);
					}
				} break;
				default:
					break;
			}
		}
	};
	for (auto& e : fs::directory_iterator(get_dict_dir())) {
#if Unix_Style
		if (e.status().type() == fs::file_type::regular_file) {
#else
		if (e.is_regular_file()) {
#endif
			auto p = e.path();
#ifndef NDEBUG
			cout << "check file: \n" << p << '\n';
#endif
			auto type = category_filename(p);
			if (res_file_ext_filter(type)) {
				auto p_last_update = util::file_last_change_date(p);
				// a new file came out
				if (p_last_update > last_update_cfg && !res_already_exist(p)) {
					new_item = true;
#ifndef NDEBUG
					cout << "process file: \n" << p << '\n';
#endif
					task(type, p);
				}
			}
		}
	}
	// always update update time even no discovery
	auto time = system_clock::to_time_t(system_clock::now());
	cfg_.put("instance.<xmlattr>.update", time);
	return new_item;
}

LEXICON_BUDDY_EXPORT void index_agent::cfg_reset(const char* dict_dir, const char* index_dir,
												 const char* cfg_path) {
	dirty_ = true;
	cfgpath_ = cfg_path;
	cfg_ = pt::ptree{};
	res_map = decltype(res_map){};
	// add magic word
	cfg_.put("magic", magic_word);
	pt::ptree instance;
	instance.put("<xmlattr>.res_dir", dict_dir);
	instance.put("<xmlattr>.index_dir", index_dir);
	instance.put("<xmlattr>.update", 0);
	cfg_.add_child("instance", instance);
}

LEXICON_BUDDY_EXPORT void index_agent::cfg_reset() {
	auto p_res = get_dict_dir();
	auto p_index = get_index_dir();
	cfg_reset(GET_U8_C_STR(p_res), GET_U8_C_STR(p_index), GET_U8_C_STR(cfgpath_));
}

template <>
LEXICON_BUDDY_EXPORT index_agent::build_result_type
index_agent::build<res_type::mdx>(const char* dict_file_path /*must be valid*/,
								  const char* target_trie_without_suffix,
								  const char* target_coordinate_without_suffix) noexcept {

	mdx::mdx_data_mmap mdx_container{dict_file_path};
	fs::path trie_path{target_trie_without_suffix};
	fs::path coor_path{target_coordinate_without_suffix};
	if (mdx_container.is_version_2_0()) {
		trie_path.replace_extension(mdx_trie64_suffix);
		coor_path.replace_extension(mdx_coor64_suffix);
		return build_mdx_helper<true>(mdx_container, trie_path, coor_path);
	} else {
		trie_path.replace_extension(mdx_trie32_suffix);
		coor_path.replace_extension(mdx_coor32_suffix);
		return build_mdx_helper<false>(mdx_container, trie_path, coor_path);
	}
}

template <>
LEXICON_BUDDY_EXPORT index_agent::build_result_type
index_agent::build<res_type::startdict_idx>(const char* dict_file_path,
											const char* target_trie_without_suffix,
											const char* target_coordinate_without_suffix) noexcept {
	auto trie_path{fs::path{target_trie_without_suffix}};
	auto coor_path{fs::path{target_coordinate_without_suffix}};
	fs::path ifo_p{dict_file_path};
	ifo_p.replace_extension("").replace_extension(".ifo");
	stardict::ifo_str ifo_s{GET_U8_C_STR(ifo_p)};
	bool bit64 = stardict::get_idx_iter_entry_type(ifo_s.version, ifo_s.idxoffsetbits);
	if (bit64) {
		coor_path.replace_extension(stardict_coor64_suffix);
		trie_path.replace_extension(stardict_trie64_suffix);
		return build_stardict_helper<stardict::bit64>(dict_file_path, trie_path,
																		  coor_path);
	} else {
		coor_path.replace_extension(stardict_coor32_suffix);
		trie_path.replace_extension(stardict_trie32_suffix);
		return build_stardict_helper<stardict::bit32>(dict_file_path, trie_path,
																		  coor_path);
	}
}

std::vector<fs::path> index_agent::item_node_files(const ptree_type& pt) {
	auto basename = pt.get("<xmlattr>.basename", "");
	if (basename.empty()) return {};
	auto res_type_str = pt.get("<xmlattr>.res_bundle_type", "");
	auto res_t = str_to_res_type(res_type_str);
	auto res_bundle_t = res_type_to_bundle_type(res_t);
	auto index_folder = get_index_dir();
	auto res_folder = get_dict_dir();
	std::vector<fs::path> ls{};
	switch (res_bundle_t) {
		case decltype(res_bundle_t)::startdict_dict_dz_bundle: {
			// dict
			ls.emplace_back((res_folder / basename).replace_extension(".dict.dz"));
			ls.emplace_back((res_folder / basename).replace_extension(".ifo"));
			ls.emplace_back((res_folder / basename).replace_extension(".idx"));
			// index
			auto trie = pt.get<string>("index.search");
			auto coor = pt.get<string>("index.coordinate");
			ls.emplace_back((index_folder / trie));
			ls.emplace_back((index_folder / coor));
			return ls;
		}
		case decltype(res_bundle_t)::mdd:
			return {};
		case decltype(res_bundle_t)::mdx: {
			// dict
			ls.emplace_back((res_folder / basename).replace_extension(".mdx"));
			// index
			auto trie = pt.get<string>("index.search");
			auto coor = pt.get<string>("index.coordinate");
			ls.emplace_back((index_folder / trie));
			ls.emplace_back((index_folder / coor));
			return ls;
		}
		case decltype(res_bundle_t)::invalid:
			return {};
	}
}

LEXICON_BUDDY_EXPORT void index_agent::remove_item_node(const string& filename_with_ext) {
	auto file_basename = filename_with_ext.substr(0, filename_with_ext.find_first_of('.'));
	auto file_ext = filename_with_ext.substr(filename_with_ext.find_first_of('.'));
	auto res_t = str_to_res_type(file_ext);
	auto bundle_ext = res_bundle_type_to_str(res_type_to_bundle_type(res_t));
	auto instance = get_cfg_instance_node();
	auto iter_b = instance->begin();
	auto iter_e = instance->end();
	auto predicate = [&file_basename, &bundle_ext](decltype(*iter_b)& arg) -> bool {
		if (arg.first != "item") return false;
		return arg.second.get("<xmlattr>.basename", "") == file_basename and
			   arg.second.get("<xmlattr>.res_bundle_type", "") == bundle_ext;
	};
	auto ret = std::find_if(iter_b, iter_e, predicate);
	if (ret == iter_e) return;
	auto file_list = item_node_files(ret->second);
	// remove map before removing node
	res_map.erase(get_dict_dir() / filename_with_ext);
	// remove node
	instance->erase(ret);
	dirty_ = true;
	// delete files
	for (auto&& i : file_list) {
		if (i.has_filename()) fs::remove(i);
	}
}

} // namespace index
} // namespace lexicon
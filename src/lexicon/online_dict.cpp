//
//  online_dict.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/8/11.
//  Copyright © 2018 Lohengrin. All rights reserved.
//
#include <lexicon_buddy/lexicon/online_dict.h>
#include <iostream>
#include <string>
namespace lexicon
{
namespace online
{
#ifndef lang_case_return
#define lang_case_return(e)                                                                                  \
	case lang::e:                                                                                            \
		return #e
#define lang_case_return_2(literal, e)                                                                       \
	case lang::e:                                                                                            \
		return literal
#endif

template <>
LEXICON_BUDDY_EXPORT const char* lang_enum_to_char<dict_api::Oxford>(lang lang_enum) {
	switch (lang_enum) {
		lang_case_return(en);
		lang_case_return(es);
		lang_case_return(nso);
		lang_case_return(zu);
		lang_case_return(ms);
		lang_case_return(id);
		lang_case_return(hi);
		lang_case_return(tn);
		lang_case_return(ur);
		lang_case_return(de);
		lang_case_return(pt);
		lang_case_return(el);
		lang_case_return(qu);
		lang_case_return(te);
		lang_case_return(tk);
		lang_case_return(tg);
		lang_case_return(tpi);
		lang_case_return(tt);
		lang_case_return(xh);
		lang_case_return(lv);
		lang_case_return(sw);
		lang_case_return(ta);
		lang_case_return(gu);
		lang_case_return(ro);
		lang_case_return_2("en-gb", en_gb);
		lang_case_return_2("en-us", en_us);
		default:
			throw std::invalid_argument{"unknow language enum"};
	}
}

template <>
template <>
LEXICON_BUDDY_EXPORT provider_oxford::request_value_type
provider_oxford::_build_request<provider_oxford::query_type::Translation>(
	const string& query_str, const std::pair<lang, lang>& srcl_dstl) {
	//	Request URL
	//	https://od-api.oxforddictionaries.com:443/api/v2/entries/en/word/translations=de
	//	Request Headers
	//	{
	//	  "Accept": "application/json",
	//	  "app_id": "9f9c20af",
	//	  "app_key": "f1d6fd7c5353328395c916f1815d5ffa"
	//	}
	string Request_str;
	Request_str += /*prefix*/ string{oxford_query_type_to_str(query_type::Translation)} +
				   /*source lang*/ lang_enum_to_char<dict_api::Oxford>(srcl_dstl.first) + '/' +
				   /*target lang*/ lang_enum_to_char<dict_api::Oxford>(srcl_dstl.second) + '/' +
				   /*word*/ query_str;
	http::request<http::empty_body> req{http::verb::get, Request_str, 11};
	req.set(http::field::host, OXFORD_HOST_AND_PORT);
	req.set(http::field::accept, "application/json");
	append_key(req);
	return req;
}
} // namespace online
} // namespace lexicon
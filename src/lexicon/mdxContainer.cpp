//
//  mdxContainer.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/03/14.
//  Copyright © 2018 Lohengrin. All rights reserved.
//

#include <lexicon_buddy/lexicon/mdxContainer.hpp>
#include <lexicon_buddy/utilities/lang_platform.hpp>
#include <boost/locale.hpp>
#include <boost/iostreams/stream.hpp>

// handle max macro in windows.h colliding with max in std
#ifndef DUMMY
#define DUMMY
#endif

namespace lexicon
{
namespace mdx
{

std::ostream& operator<<(std::ostream& out, const dict_record_v& r) {
	for (auto& i : r) {
		out << i << '\n';
	}
	return out;
}

uint32_t getMdxXMLLength(std::ifstream& mdxFile_Opened) {
	mdxFile_Opened.seekg(0);
	char mdxXMLLength_Char[4];
	mdxFile_Opened.read(mdxXMLLength_Char, 4);
	return endianSwap_32bit(mdxXMLLength_Char);
}

uint32_t getMdxXMLLength(const char* mmap_file) {
	char mdxXMLLength_Char[4];
	copy(mmap_file, mmap_file + 4, mdxXMLLength_Char);
	return endianSwap_32bit(mdxXMLLength_Char);
}

LEXICON_BUDDY_EXPORT h_sec_i::h_sec_i(std::ifstream& mdxFile) {
	size_t xmlLengthRaw = getMdxXMLLength(mdxFile);
	length = xmlLengthRaw;
	size_t xmlLengthSource = xmlLengthRaw, xmlLengthDestination = xmlLengthRaw;
	size_t* inbytesleft = &xmlLengthSource;
	size_t* outbytesleft = &xmlLengthDestination;
	auto* xmlStrUTF16LE_UTF8 = new char[xmlLengthSource + xmlLengthDestination];
	unique_ptr<char[]> uptr_xmlStrUTF16LE_UTF8(xmlStrUTF16LE_UTF8);
	auto* xmlStrUTF8 = xmlStrUTF16LE_UTF8 + xmlLengthSource;
	// char* xmlStrUTF16LE_UTF8_Start = xmlStrUTF16LE_UTF8;
	auto* xmlStrUTF8Start = xmlStrUTF8;
	mdxFile.read(xmlStrUTF16LE_UTF8, static_cast<std::streamsize>(xmlLengthSource));
	// TODO: use win32 api on windows
#if Unix_Style
	iconv_t cd;
	if ((cd = iconv_open("UTF-8", "UTF-16LE")) == reinterpret_cast<void*>(-1)) {
		throw std::invalid_argument{"iconv_oepn: The conversion from fromcode to tocode is not "
									"supported by the implementation."};
	}

	auto ret = iconv(cd, &xmlStrUTF16LE_UTF8, inbytesleft, &xmlStrUTF8, outbytesleft);
	if (ret == static_cast<size_t>(-1)) {
		iconv_close(cd);
		encoding_su::iconv_errthrow(ret);
	}
	iconv_close(cd);
#else
	/*auto convert_ret =*/WideCharToMultiByte(CP_UTF8, 0, reinterpret_cast<wchar_t*>(xmlStrUTF16LE_UTF8),
											  xmlLengthSource, xmlStrUTF8, xmlLengthDestination, NULL, NULL);
	util::win_print_last_error();
#endif
	// std::istringstream xmlbuffer_UTF8{std::string{xmlStrUTF8Start, xmlLengthRaw - *outbytesleft}};
	// buffer to stream without copy
	boost::iostreams::stream<boost::iostreams::array_source> xmlbuffer_UTF8_strm{
		xmlStrUTF8Start, xmlLengthRaw - *outbytesleft};
	// Parse the XML into the property tree.
	read_property(xmlbuffer_UTF8_strm);
}

LEXICON_BUDDY_EXPORT h_sec_i::h_sec_i(const mmap_w& mmap) {
	auto mmap_file = mmap.data();
	// getMdxXMLLength does the job of check whether ifstream mdxFile is
	// open
	auto xmlLenRaw = static_cast<size_t>(getMdxXMLLength(mmap_file));
	length = static_cast<size_t>(xmlLenRaw);
	auto xmlLenSrc = xmlLenRaw;
	auto xmlLenDst = xmlLenRaw;
	auto* inbytesleft = &xmlLenSrc;
	auto* outbytesleft = &xmlLenDst;
	auto* xmlStrUTF16LE_UTF8 = new char[xmlLenSrc + xmlLenDst];
	unique_ptr<char[]> uptr_xmlStrUTF16LE_UTF8(xmlStrUTF16LE_UTF8);
	char* xmlStrUTF8 = xmlStrUTF16LE_UTF8 + xmlLenSrc;
	char* xmlStrUTF8Start = xmlStrUTF8;
	copy(mmap_file + 4, mmap_file + 4 + xmlLenRaw, xmlStrUTF16LE_UTF8);

#if Unix_Style
	iconv_t cd;
	if ((cd = iconv_open("UTF-8", "UTF-16LE")) == reinterpret_cast<iconv_t>(-1)) {
		throw std::invalid_argument{"iconv_open failed, invalid encoding."};
	}
	auto ret = iconv(cd, &xmlStrUTF16LE_UTF8, inbytesleft, &xmlStrUTF8, outbytesleft);
	if (ret == static_cast<size_t>(-1)) {
		iconv_close(cd);
		encoding_su::iconv_errthrow(ret);
	}
	iconv_close(cd);
#else
	/*auto convert_ret =*/WideCharToMultiByte(CP_UTF8, 0, reinterpret_cast<wchar_t*>(xmlStrUTF16LE_UTF8),
											  xmlLenSrc, xmlStrUTF8, xmlLenDst, NULL, NULL);
	util::win_print_last_error();
#endif
	// std::istringstream xmlbuffer_UTF8{std::string{xmlStrUTF8Start, xmlLenRaw - *outbytesleft}};
	// buffer to stream without copy
	boost::iostreams::stream<boost::iostreams::array_source> xmlbuffer_UTF8_strm{xmlStrUTF8Start,
																				 xmlLenRaw - *outbytesleft};
	// Parse the XML into the property tree.
	read_property(xmlbuffer_UTF8_strm);
}

LEXICON_BUDDY_EXPORT void h_sec_i::parse_stylesheets(const string& arg) {
	string::size_type newline_pos[256];
	std::size_t arg_p{};
	std::size_t newline_pos_end{};
	for (auto& i : arg) {
		if (i == '\n') {
			newline_pos[newline_pos_end] = arg_p;
			++newline_pos_end;
		}
		++arg_p;
	}
	std::size_t p_start = 0;
	for (std::size_t p0 = 0; p0 + 2 < newline_pos_end; p0 += 3) {
		style s;
		s.name = string{arg, p_start, newline_pos[p0] - p_start};
		s.begin = string{arg, newline_pos[p0] + 1, newline_pos[p0 + 1] - newline_pos[p0] - 1};
		s.end = string{arg, newline_pos[p0 + 1] + 1, newline_pos[p0 + 2] - newline_pos[p0 + 1] - 1};
		p_start = newline_pos[p0 + 2] + 1;
		name_style.emplace_back(s);
	}
}

LEXICON_BUDDY_EXPORT void h_sec_i::read_property(std::istream& strm) {
	pt::ptree mdx_info_ptree;
	pt::read_xml(strm, mdx_info_ptree);
	GeneratedByEngineVersion =
		mdx_info_ptree.get<string>(u8"Dictionary.<xmlattr>.GeneratedByEngineVersion", "");
	RequiredEngineVersion = mdx_info_ptree.get<std::string>(u8"Dictionary.<xmlattr>.RequiredEngineVersion");
	try {
		Encrypted = std::stoi(mdx_info_ptree.get<string>(u8"Dictionary.<xmlattr>.Encrypted", ""));
	} catch (const std::exception& e) {
		Encrypted = 0;
	}
	if (Encrypted & 1) {
		// TODO: use a custom exception
		throw std::runtime_error{"fail parsing mdx: first part of keyword section is encrypted.\n"};
	}
	Encoding = mdx_info_ptree.get<string>(u8"Dictionary.<xmlattr>.Encoding");
	Format = mdx_info_ptree.get<string>(u8"Dictionary.<xmlattr>.Format");
	CreationDate = mdx_info_ptree.get<string>(u8"Dictionary.<xmlattr>.CreationDate", "");
	Compact = mdx_info_ptree.get<bool>(u8"Dictionary.<xmlattr>.Compact", false);
	Compat = mdx_info_ptree.get<bool>(u8"Dictionary.<xmlattr>.Compat", false);
	KeyCaseSensitive = mdx_info_ptree.get<bool>(u8"Dictionary.<xmlattr>.KeyCaseSensitive", false);
	Description = mdx_info_ptree.get<string>(u8"Dictionary.<xmlattr>.Description", "");
	Title = mdx_info_ptree.get<string>(u8"Dictionary.<xmlattr>.Title", "");
	StyleSheet = mdx_info_ptree.get<string>(u8"Dictionary.<xmlattr>.StyleSheet", "");
	RegisterBy = mdx_info_ptree.get<string>(u8"Dictionary.<xmlattr>.RegisterBy", "");
	RegCode = mdx_info_ptree.get<string>(u8"Dictionary.<xmlattr>.RegCode", "");
	// parse stylesheet
	if (Compat || Compact) {
		parse_stylesheets(StyleSheet);
	}
}

LEXICON_BUDDY_EXPORT void k_sec_i::read(const char* p, bool version_2_0) {
	if (version_2_0) {
		keyword_sect_to_key_index = 8 * 5 + 4;
		num_blocks = endianSwap_64bit(p);
		num_entries = endianSwap_64bit(p + 8);
		key_index_decomp_len = endianSwap_64bit(p + 8 * 2);
		key_index_comp_len = endianSwap_64bit(p + 8 * 3);
		key_blocks_len = endianSwap_64bit(p + 8 * 4);
		checksum = endianSwap_32bit(p + 8 * 5);
		key_index_comp_type = char_to_uint32(p + keyword_sect_to_key_index);
		key_blocks_offset = keyword_sect_offset + keyword_sect_to_key_index +
							(key_index_comp_type ? key_index_comp_len : (key_index_decomp_len + 8));
	} else {
		keyword_sect_to_key_index = 4 * 4;
		num_blocks = endianSwap_32bit(p);
		num_entries = endianSwap_32bit(p + 4);
		// aligned to version 2.0
		key_index_decomp_len = endianSwap_32bit(p + 4 * 2);
		key_index_comp_len = 0;
		key_blocks_len = endianSwap_32bit(p + 4 * 3);
		checksum = 0;
		key_index_comp_type = 0;
		key_blocks_offset = keyword_sect_offset + keyword_sect_to_key_index + key_index_decomp_len;
	}
}

LEXICON_BUDDY_EXPORT void k_sec_i::read(std::istream& mdxFile, std::streamoff offset, bool version_2_0) {
	// if (!mdxFile.is_open()) throw std::invalid_argument{"r_sec_i: file stream is not open."};
	mdxFile.seekg(offset);
	keyword_sect_offset = offset;
	char temp_ptr[8 * 5 + 4 + 4];
	if (version_2_0) {
		keyword_sect_to_key_index = 8 * 5 + 4;
		mdxFile.get(temp_ptr, keyword_sect_to_key_index + 4);
	} else {
		keyword_sect_to_key_index = 4 * 4;
		mdxFile.get(temp_ptr, keyword_sect_to_key_index);
	}
	read(temp_ptr, version_2_0);
}

LEXICON_BUDDY_EXPORT void k_sec_i::read(const mmap_w& mmap, std::ptrdiff_t offset, bool version_2_0) {

	if (mmap.data() == nullptr) throw std::invalid_argument{"r_sec_i: nullptr."};
	keyword_sect_offset = offset;
	auto temp_ptr = mmap.data() + offset;
	read(temp_ptr, version_2_0);
}

LEXICON_BUDDY_EXPORT void r_sec_i::read(const char* p, bool version_2_0) {
	if (version_2_0) {
		num_blocks = endianSwap_64bit(p);
		num_entries = endianSwap_64bit(p + 8);
		index_len = endianSwap_64bit(p + 8 * 2);
		blocks_len = endianSwap_64bit(p + 8 * 3);
		com_decom_sizes_offset = sect_offset + 8 * 4;
		block_offset = com_decom_sizes_offset + num_blocks * (8 + 8);
	} else {
		num_blocks = endianSwap_32bit(p);
		num_entries = endianSwap_32bit(p + 4);
		index_len = endianSwap_32bit(p + 4 * 2);
		blocks_len = endianSwap_32bit(p + 4 * 3);
		com_decom_sizes_offset = sect_offset + 4 * 4;
		block_offset = com_decom_sizes_offset + num_blocks * (4 + 4);
	}
}

LEXICON_BUDDY_EXPORT void r_sec_i::read_record0_comp_type(const char* p) {
	block_0_comp_type = util::char_to_uint32(p);
}

LEXICON_BUDDY_EXPORT void r_sec_i::read(std::istream& mdxFile, std::streamoff offset, bool version_2_0) {
	// if (!mdxFile.is_open()) throw std::invalid_argument{"r_sec_i: file stream is not open."};
	sect_offset = offset;
	mdxFile.seekg(static_cast<std::streamoff>(offset));
	char temp[8 * 4];
	if (version_2_0) {
		sect_offset_to_com_decom_sizes_offset = 8 * 4;
	} else {
		sect_offset_to_com_decom_sizes_offset = 4 * 4;
	}
	mdxFile.get(temp, sect_offset_to_com_decom_sizes_offset);
	read(temp, version_2_0);

	mdxFile.seekg(static_cast<std::streamoff>(block_offset));
	char temp2[4];
	mdxFile.get(temp2, 4);

	read_record0_comp_type(temp2);
}

LEXICON_BUDDY_EXPORT void r_sec_i::read(const mmap_w& mmap, std::ptrdiff_t offset, bool version_2_0) {
	auto mmap_file = mmap.data();
	if (mmap_file == nullptr) throw std::invalid_argument{"r_sec_i: nullptr."};
	sect_offset = offset;
	auto temp_ptr = mmap_file + offset;
	if (version_2_0) {
		sect_offset_to_com_decom_sizes_offset = 8 * 4;
	} else {
		sect_offset_to_com_decom_sizes_offset = 4 * 4;
	}
	read(temp_ptr, version_2_0);

	read_record0_comp_type(temp_ptr + block_offset);
}

LEXICON_BUDDY_EXPORT mdx_type::index_record::mapped_type key_block_check(const mdx_type::index_record& index,
																		 mdx_type::data_offset offset,
																		 mdx_type::data_offset& offset_r) {
	auto i = index.upper_bound(offset);
	// if (i == index.end()) return typename mdx_c_traits0::index_record::mapped_type{};
	--i;
	// check if offset out of range
	auto offset_r_tmp = offset - i->first;
	if ((offset_r_tmp) > i->second.decomp_size) return typename mdx_type::index_record::mapped_type{};
	offset_r = offset_r_tmp;
	return i->second;
}

LEXICON_BUDDY_EXPORT mdx_type::data_content
data_assembly(unsigned encoding_unit, const string& encoding_canonical, bool to_utf8, const char* ptr) {
	using namespace boost::locale::conv;
	auto ptr_end = ptr;
	// null terminate
	if (encoding_unit == 1) {
		while (*ptr_end) {
			++ptr_end;
		}

	} else {
		while (std::any_of(ptr_end, ptr_end + encoding_unit, [](auto c) { return c; })) {
			ptr_end += encoding_unit;
		}
	}
	// construct data
	if (ptr_end == ptr) {
		return typename mdx_type::data_content{};
	} else {
		if (encoding_canonical == "UTF-8" || !to_utf8) {
			return typename mdx_type::data_content{ptr, static_cast<string::size_type>(ptr_end - ptr)};
		} else {
			return encoding_su::to_utf8str(ptr, ptr_end, encoding_canonical.c_str());
			// return to_utf<char>(ptr, ptr_end, encoding_canonical);
		}
	}
}

// query_policy::load_decomp specialization
// "not_compressed"==true means plain file, and false means unknown(runtime check must be
// implemented)

// ******STEP 1 ALLOCATION AND LOAD******
// T== std_stream && not_compressed==false: allocate space for one or both(based on compression type
// runtime check)

// T== std_stream && not_compressed==true: allocate space for decomp
// T== memory && not_compressed==false: allocate space if data compressed (based on compression type
// runtime check)

// ******STEP 2 DECOMP******
// T== std_stream && not_compressed==false: decomp or not(based on compression type
// runtime check)

// T== std_stream && not_compressed==true: do nothing

// T== memory && not_compressed==false:  decomp or not(based on compression type
// runtime check)
template <>
LEXICON_BUDDY_EXPORT void query_policy<std_stream, false>::load_decomp(
	std_stream& t, typename mdx_type::index_record::mapped_type slide, data_offset blocks_offset) {
	t.seekg(blocks_offset + slide.origin_offset_block_begin);
	char buf[4];
	t.read(buf, 4);
	auto comp_t = static_cast<comp_type>(util::char_to_uint32(buf));

	if (comp_t == comp_type::no_comp) {
		allocate_decomp(slide.decomp_size + 8);
		t.seekg(blocks_offset);
		t.read(buf_decomp.get(), slide.decomp_size);

	} else {
		allocate_raw(slide.comp_size);
		allocate_decomp(slide.decomp_size + 8);
		t.seekg(blocks_offset);
		t.read(buf_raw.get(), slide.comp_size);
		unzip::unzip_strm_generic(comp_t, buf_raw.get() + 8, slide.comp_size - 8, buf_decomp.get() + 8,
								  slide.decomp_size);
	}
	ptr_decomp0 = buf_decomp.get() + 8;
}

template <>
LEXICON_BUDDY_EXPORT void
query_policy<std_stream, true>::load_decomp(std_stream& t, mdx_type::index_record::mapped_type slide,
											data_offset blocks_offset) {
	t.seekg(blocks_offset);
	allocate_decomp(slide.decomp_size + 8);
	t.read(buf_decomp.get(), slide.decomp_size + 8);
	ptr_decomp0 = buf_decomp.get() + 8;
}

template <>
LEXICON_BUDDY_EXPORT void query_policy<memory, false>::load_decomp(memory& t,
																   mdx_type::index_record::mapped_type slide,
																   data_offset blocks_offset) {
	auto comp_t = static_cast<comp_type>(util::char_to_uint32(t.data() + blocks_offset));
	auto ptr_block = t.data() + blocks_offset;
	if (comp_t == comp_type::no_comp) {
		ptr_decomp0 = ptr_block + 8;
	} else {
		allocate_decomp(slide.decomp_size + 8);
		unzip::unzip_strm_generic(comp_t, ptr_block + 8, slide.comp_size - 8, buf_decomp.get() + 8,
								  slide.decomp_size);
		ptr_decomp0 = buf_decomp.get() + 8;
	}
}

template <>
LEXICON_BUDDY_EXPORT mdx_data<memory>::mdx_data(const std::string& path)
	: _path{path}, t{path.c_str()}, data_size{util::get_filesize(t)}, _h_sec{t},
	  encoding_unit{encoding_su::encoding_check_mdx::get_byte_unit(_h_sec.Encoding)}, _k_sec{}, _r_sec{} {
	set_version_bit();
	auto version_2_0 = is_version_2_0();
	// handle keyword section
	auto offset_temp = static_cast<std::ptrdiff_t>(_h_sec.length + 8);
	_k_sec.read(t, offset_temp, version_2_0);
	// handle key_index
	// auto encoding_unit = encoding_su::encoding_check::get_byte_unit(_h_sec.Encoding);
	encoding_canonical = string{encoding_su::encoding_check_mdx::get_encoding_info(get_encoding()).NAME};
	// point to start of key_index
	auto key_index_ptr = t.data() + _k_sec.keyword_sect_offset + _k_sec.keyword_sect_to_key_index;
	unique_ptr<char[]> index_decrypt_buf{};
	if (version_2_0) {
		key_index_ptr += 8;
		if (_h_sec.Encrypted & 2) {
			index_decrypt_buf.reset(new char[_k_sec.key_index_comp_len - 8]);
			char key[16]{};
			copy(key_index_ptr, key_index_ptr + _k_sec.key_index_comp_len - 8, index_decrypt_buf.get());
			util::mdx_decrypt::keyword_index_generate_16byteskey(key_index_ptr - 4, key);
			key_index_ptr = index_decrypt_buf.get();
			util::mdx_decrypt::keyword_index_decrypt((unsigned char*)key_index_ptr,
													 _k_sec.key_index_comp_len - 8, (unsigned char*)key,
													 sizeof(key));
		}
		{
			unique_ptr<char[]> index_decomp_buf{};
			// decompress key_index when mdx version >=2.0
			if (_k_sec.key_index_comp_type) {
				index_decomp_buf.reset(new char[_k_sec.key_index_decomp_len]);
				unzip::unzip_strm_generic(static_cast<comp_type>(_k_sec.key_index_comp_type), key_index_ptr,
										  _k_sec.key_index_comp_len - 8, index_decomp_buf.get(),
										  _k_sec.key_index_decomp_len);
				key_index_ptr = index_decomp_buf.get();
			}
			// parse key_index data
			_key_index_v =
				key_index::read_key_index(key_index_ptr, _k_sec.num_blocks, encoding_unit, version_2_0);
		}
	} else {
		// parse key_index data
		_key_index_v =
			key_index::read_key_index(key_index_ptr, _k_sec.num_blocks, encoding_unit, version_2_0);
	}

	auto key_index_len = _k_sec.key_index_comp_type ? _k_sec.key_index_comp_len : _k_sec.key_index_decomp_len;
	// offset to end of key_index
	offset_temp += (_k_sec.keyword_sect_to_key_index + key_index_len);

	// calculate offset of each key_block
	key_index::key_index_comp_t_offset_init(t.data() + offset_temp, _key_index_v);
	// calculate record section offset:
	auto& ref = _key_index_v[_key_index_v.size() - 1];
	offset_temp += ref.origin_offset_block_begin + (ref.comp_type ? ref.comp_size : ref.decomp_size);
	_r_sec.read(t, offset_temp, version_2_0);
	// build record index
	auto record_index_ptr = t + offset_temp + _r_sec.sect_offset_to_com_decom_sizes_offset;
	_record_index_v = record::read_record_index(record_index_ptr, _r_sec.num_blocks, version_2_0);
	auto record_ptr = record_index_ptr + _r_sec.num_blocks * (version_2_0 ? 16 : 8);
	record::record_comp_t_offset_init(record_ptr, _record_index_v);
	// build map
	build_index_record(_record_index_v, _record_index_map);
	state[valid_bit] = 1;
}

template <>
LEXICON_BUDDY_EXPORT mdx_data<std_stream>::mdx_data(const std::string& path)
	: _path{path}, t{path}, data_size{util::get_filesize(t)}, _h_sec{t},
	  encoding_unit{encoding_su::encoding_check_mdx::get_byte_unit(_h_sec.Encoding)}, _k_sec{}, _r_sec{} {
	set_version_bit();
	auto version_2_0 = is_version_2_0();
	// handle keyword section
	auto offset_temp = static_cast<std::streamoff>(_h_sec.length + 8);
	_k_sec.read(t, offset_temp, version_2_0);
	// handle key_index
	auto key_index_len = _k_sec.key_index_comp_type ? _k_sec.key_index_comp_len : _k_sec.key_index_decomp_len;
	// auto encoding_unit = encoding_su::encoding_check::get_byte_unit(_h_sec.Encoding);
	encoding_canonical = string{encoding_su::encoding_check_mdx::get_encoding_info(get_encoding()).NAME};
	unique_ptr<char[]> buf(new char[key_index_len]);
	// set offset to start of key_index
	offset_temp += _k_sec.keyword_sect_to_key_index;
	t.seekg(offset_temp);
	// read key_index data
	char* key_index_ptr = buf.get();
	t.read(key_index_ptr, static_cast<std::streamsize>(key_index_len));
	// parse key_index data
	unique_ptr<char[]> index_decrypt_buf{};
	if (version_2_0) {
		key_index_ptr += 8;
		if (_h_sec.Encrypted & 2) {
			index_decrypt_buf.reset(new char[_k_sec.key_index_comp_len - 8]);
			char key[16]{};
			copy(key_index_ptr, key_index_ptr + _k_sec.key_index_comp_len - 8, index_decrypt_buf.get());
			util::mdx_decrypt::keyword_index_generate_16byteskey(key_index_ptr - 4, key);
			key_index_ptr = index_decrypt_buf.get();
			util::mdx_decrypt::keyword_index_decrypt((unsigned char*)key_index_ptr,
													 _k_sec.key_index_comp_len - 8, (unsigned char*)key,
													 sizeof(key));
		}
		{
			unique_ptr<char[]> index_decomp_buf{};
			// decompress key_index when version >=2.0
			if (_k_sec.key_index_comp_type) {
				index_decomp_buf.reset(new char[_k_sec.key_index_decomp_len]);
				unzip::unzip_strm_generic(static_cast<comp_type>(_k_sec.key_index_comp_type), key_index_ptr,
										  _k_sec.key_index_comp_len - 8, index_decomp_buf.get(),
										  _k_sec.key_index_decomp_len);
				key_index_ptr = index_decomp_buf.get();
			}
			_key_index_v =
				key_index::read_key_index(key_index_ptr, _k_sec.num_blocks, encoding_unit, version_2_0);
		}
	} else {
		_key_index_v =
			key_index::read_key_index(key_index_ptr, _k_sec.num_blocks, encoding_unit, version_2_0);
	}
	// set offset to end of key_index
	offset_temp += key_index_len;
	// calculate offset of each key_block
	key_index::key_index_comp_t_offset_init(t, offset_temp, _key_index_v);
	// calculate record section offset:
	auto& ref = _key_index_v[_key_index_v.size() - 1];
	offset_temp += ref.origin_offset_block_begin + (ref.comp_type ? ref.comp_size : ref.decomp_size);
	_r_sec.read(t, offset_temp, version_2_0);
	// build record index
	auto record_index_buf_s = static_cast<std::streamsize>(_r_sec.num_blocks * (version_2_0 ? 16 : 8));
	auto record_index_p =
		offset_temp + static_cast<decltype(offset_temp)>(_r_sec.sect_offset_to_com_decom_sizes_offset);
	unique_ptr<char[]> record_index_buf{new char[record_index_buf_s]};
	t.seekg(record_index_p);
	t.read(record_index_buf.get(), record_index_buf_s);
	_record_index_v = record::read_record_index(record_index_buf.get(), _r_sec.num_blocks, version_2_0);
	auto record_p = record_index_p + static_cast<std::streamoff>(_r_sec.num_blocks * (version_2_0 ? 16 : 8));
	record::record_comp_t_offset_init(t, record_p, _record_index_v);
	// build map
	build_index_record(_record_index_v, _record_index_map);
	state[valid_bit] = 1;
}

template <>
LEXICON_BUDDY_EXPORT key_index::key_block_container mdx_data<memory>::get_key_block(std::size_t n) {
	key_index::key_block_container c;
	auto& i = _key_index_v[n];
	// for each block, check and decompress block
	auto key_block_raw_i = t.data() + _k_sec.key_blocks_offset + i.origin_offset_block_begin + 8;
	c.dat_size_ = i.decomp_size;
	if (i.comp_type) {
		// decompression
		c.buf_.reset(new char[i.decomp_size]);
		c.dat_ = c.buf_.get();
		// comp_size[i] ( decomp_size[0] does not) include the mdx extra header part (comp_type +
		// checksum) before compressed data
		unzip::unzip_strm_generic(static_cast<unzip::comp_type>(i.comp_type), key_block_raw_i,
								  i.comp_size - 8, c.buf_.get(), i.decomp_size);
	} else {
		c.dat_ = key_block_raw_i;
	}
	c.encoding_canonical_ = encoding_canonical;
	c.version_2_0_ = is_version_2_0();
	return c;
}

template <>
LEXICON_BUDDY_EXPORT key_index::key_block_container mdx_data<std_stream>::get_key_block(std::size_t n) {
	key_index::key_block_container c;
	auto& i = _key_index_v[n];
	t.seekg(_k_sec.key_blocks_offset + i.origin_offset_block_begin + 8);
	// for each block, check and decompress block
	unique_ptr<char[]> file_buf{new char[i.comp_size - 8]};
	t.read(file_buf.get(), i.comp_size - 8);
	c.dat_size_ = i.decomp_size;
	if (i.comp_type) {
		// decompression
		c.buf_.reset(new char[i.decomp_size]);
		c.dat_ = c.buf_.get();
		// comp_size[i] ( decomp_size[0] does not) include the mdx extra header part (comp_type +
		// checksum) before compressed data
		unzip::unzip_strm_generic(static_cast<unzip::comp_type>(i.comp_type), file_buf.get(), i.comp_size - 8,
								  c.buf_.get(), i.decomp_size);
	} else {
		c.dat_ = file_buf.get();
	}
	c.encoding_canonical_ = encoding_canonical;
	c.version_2_0_ = is_version_2_0();
	return c;
}

template <>
LEXICON_BUDDY_EXPORT uint64_t mdx_data_std_stream::record_block_offset() {
	return _r_sec.sect_offset + _r_sec.block_offset;
}

namespace key_index
{

LEXICON_BUDDY_EXPORT key_index_v
read_key_index(const char* dat_ptr, // point to key_index
			   uint64_t num_blocks,
			   unsigned encoding_unit, // the unit is the basic size of encoding(e.g. utf8=1 utf16=2)
			   bool version_2_0) {
	key_index_v v;
	int sn{};
	if (version_2_0) {
		for (decltype(num_blocks) i = 0; i < num_blocks; ++i) {
			auto&& num_entries = util::endianSwap_64bit(dat_ptr);
			// length of first_word data in bytes
			auto first_size = util::endianSwap_16bit(dat_ptr + 8);
			auto&& first_word = string{dat_ptr + 8 + 2, first_size * encoding_unit};
			dat_ptr += 8 + 2 + first_size * encoding_unit + encoding_unit;
			// length of last_word data in bytes
			auto last_size = util::endianSwap_16bit(dat_ptr);
			auto&& last_word = string{dat_ptr + 2, last_size * encoding_unit};
			dat_ptr += 2 + last_size * encoding_unit + encoding_unit;
			auto&& comp_size = util::endianSwap_64bit(dat_ptr);
			auto&& decomp_size = util::endianSwap_64bit(dat_ptr + 8);
			dat_ptr += 16;

			// offset_block_end += decomp_size;
			v.emplace_back(slide{sn, num_entries, first_size,
								 std::forward<decltype(first_word)>(first_word),		  /*forward string*/
								 last_size, std::forward<decltype(last_word)>(last_word), /*forwad string*/
								 comp_size, decomp_size,
								 0}); // "0": the offset inform can be calculated later
			// with the actual key_blocks' data header(comp_type)
			++sn;
		}
	} else {
		for (decltype(num_blocks) i = 0; i < num_blocks; ++i) {
			auto&& num_entries = util::endianSwap_32bit(dat_ptr);
			// length of first_word data in bytes
			uint16_t first_size = *(unsigned char*)(dat_ptr + 4);
			auto&& first_word = string{dat_ptr + 4 + 1, first_size * encoding_unit};
			dat_ptr += 4 + 1 + first_size * encoding_unit;
			// length of last_word data in bytes
			uint16_t last_size = *(unsigned char*)(dat_ptr);
			auto&& last_word = string{dat_ptr + 1, last_size * encoding_unit};
			dat_ptr += 1 + last_size * encoding_unit;
			// comp_size decomp_size should add 8 to include header in version <2.0
			auto&& comp_size = util::endianSwap_32bit(dat_ptr);
			auto&& decomp_size = util::endianSwap_32bit(dat_ptr + 4);
			dat_ptr += 8;
			// offset_block_end += decomp_size;
			v.emplace_back(slide{sn, num_entries, first_size,
								 std::forward<decltype(first_word)>(first_word),		  /*forward string*/
								 last_size, std::forward<decltype(last_word)>(last_word), /*forwad string*/
								 comp_size, decomp_size,
								 0}); // "0": the offset inform can be calculated later
			// with the actual key_blocks' data header(comp_type)
			++sn;
		}
	}
	return v;
}

// calculate the offset based on actual comp_type of each key_block
LEXICON_BUDDY_EXPORT void key_index_comp_t_offset_init(const char* dat_ptr, key_index_v& v) {
	auto ptr = dat_ptr;
	for (auto& i : v) {
		i.origin_offset_block_begin = ptr - dat_ptr;
		i.comp_type = util::char_to_uint32(ptr);
		i.comp_type_flag = true;
		ptr += (i.comp_type ? i.comp_size : (i.decomp_size + 8));
	}
}

LEXICON_BUDDY_EXPORT void key_index_comp_t_offset_init(ifstream& strm, std::streampos pos, key_index_v& v) {
	char buff[8];
	auto pos_ = pos;
	for (auto& i : v) {
		strm.seekg(pos_);
		strm.read(buff, 8);
		i.origin_offset_block_begin = (pos_ - pos);
		i.comp_type = util::char_to_uint32(buff);
		i.comp_type_flag = true;
		pos_ += (i.comp_type ? i.comp_size : (i.decomp_size + 8));
	}
}

LEXICON_BUDDY_EXPORT key_block_iter::key_block_iter(bool version_2_0, string encoding_canonical,
													const char* p)
	: version_2_0_{version_2_0},
	  encoding_canonical_{
		  encoding_su::encoding_check_mdx::get_encoding_info(string{encoding_canonical}).NAME},
	  p_{p} {
	encoding_unit_ = encoding_su::encoding_check_mdx::get_byte_unit(encoding_canonical_);
}

LEXICON_BUDDY_EXPORT key_block_iter::reference key_block_iter::operator*() {
	if (!value_updated_) {
		value_.offset = version_2_0_ ? util::endianSwap_64bit(p_) : util::endianSwap_32bit(p_);
		auto temp_p0 = p_ + (version_2_0_ ? 8 : 4);
		auto temp_p = temp_p0;
		// check nullity in encoding_unit number of bytes
		auto null_check = [&temp_p](decltype(encoding_unit_) _encoding_unit) {
			bool flag{};
			while (_encoding_unit--) {
				flag |= *temp_p;
				++temp_p;
			}
			return flag;
		};
		while (null_check(encoding_unit_)) {
		}
		value_.ptr0 = temp_p0;
		value_.ptr1 = temp_p;
		value_.ptrx = temp_p - encoding_unit_;
		value_updated_ = true;
	}
	return value_;
}

LEXICON_BUDDY_EXPORT key_block_iter& key_block_iter::operator++() noexcept {
	operator*();
	p_ = value_.ptr1;
	value_updated_ = false;
	return *this;
}

LEXICON_BUDDY_EXPORT key_block_iter key_block_container::begin() {
	return {version_2_0_, encoding_canonical_, dat_};
}

LEXICON_BUDDY_EXPORT key_block_iter key_block_container::end() {
	return {version_2_0_, encoding_canonical_, dat_ + dat_size_};
}

LEXICON_BUDDY_EXPORT iter::iter(const char* const _dat_p_begin, const char* const _dat_p_end, bool begin,
								const char* encoding_canonical, bool is_version_2_0)
	: version_2_0{is_version_2_0},
	  dat_p_begin{_dat_p_begin}, dat_p{begin ? _dat_p_begin : _dat_p_end}, dat_p_end{_dat_p_end},
	  encoding_canonical_{
		  encoding_su::encoding_check_mdx::get_encoding_info(string{encoding_canonical}).NAME} {
	encoding_unit = encoding_su::encoding_check_mdx::get_byte_unit(encoding_canonical_);
	if (dat_p != dat_p_end) {
		value.offset = version_2_0 ? util::endianSwap_64bit(dat_p) : util::endianSwap_32bit(dat_p);
		auto temp_p = dat_p + (version_2_0 ? 8 : 4);
		auto temp_p0 = temp_p;
		auto null_check = [&temp_p](decltype(encoding_unit) _encoding_unit) {
			bool flag{};
			while (_encoding_unit--) {
				flag |= *temp_p;
				++temp_p;
			}
			return flag;
		};
		while (null_check(encoding_unit)) {
		}
		value.ptr0 = temp_p0;
		value.ptr1 = temp_p;
		value.ptrx = temp_p - encoding_unit;
	}
}

LEXICON_BUDDY_EXPORT iter& iter::operator++() noexcept {
	// boundary check before increment--never pass the one-beyond-the-last
	if (value.ptr1) {
		dat_p = value.ptr1;
		if (dat_p >= dat_p_end) {
			// check if iter points to one-beyond-the-last element
			value = value_type{};
		} else {
			// update value
			value.offset =
				version_2_0 ? util::endianSwap_64bit(value.ptr1) : util::endianSwap_32bit(value.ptr1);
			auto temp_p = value.ptr1 + (version_2_0 ? 8 : 4);
			auto temp_p0 = temp_p;
			// check nullity in encoding_unit number of bytes
			auto null_check = [&temp_p](decltype(encoding_unit) _encoding_unit) {
				bool flag{};
				while (_encoding_unit--) {
					flag |= *temp_p;
					++temp_p;
				}
				return flag;
			};
			while (null_check(encoding_unit)) {
			}
			value.ptr0 = temp_p0;
			value.ptr1 = temp_p;
			value.ptrx = temp_p - encoding_unit;
		}
	}
	return *this;
} // namespace key_index

} // namespace key_index

namespace record
{
LEXICON_BUDDY_EXPORT record_index_v read_record_index(const char* dat_ptr, uint64_t num_blocks,
													  bool version_2_0) {
	record_index_v v;
	int sn{};
	if (version_2_0) {
		for (decltype(num_blocks) i = 0; i < num_blocks; ++i) {
			v.emplace_back(slide{sn, endianSwap_64bit(dat_ptr), endianSwap_64bit(dat_ptr + 8), 0, 0, 0});
			dat_ptr += 16;
			++sn;
		}
	} else {
		for (decltype(num_blocks) i = 0; i < num_blocks; ++i) {
			v.emplace_back(slide{sn, endianSwap_32bit(dat_ptr), endianSwap_32bit(dat_ptr + 4), 0, 0, 0});
			dat_ptr += 8;
			++sn;
		}
	}
	return v;
}
LEXICON_BUDDY_EXPORT void record_comp_t_offset_init(const char* dat_ptr, record_index_v& v) {
	auto ptr = dat_ptr;
	for (auto& i : v) {
		i.origin_offset_block_begin = ptr - dat_ptr;
		i.comp_type = util::char_to_uint32(ptr);
		i.comp_type_flag = true;
		// TODO: check i.decomp_size + 8?
		ptr += (i.comp_type ? i.comp_size : (i.decomp_size + 8));
	}
}
LEXICON_BUDDY_EXPORT void record_comp_t_offset_init(ifstream& strm, std::streampos pos, record_index_v& v) {
	char buff[8];
	auto pos_ = pos;
	for (auto& i : v) {
		strm.seekg(pos_);
		strm.read(buff, 8);
		i.origin_offset_block_begin = (pos_ - pos);
		i.comp_type = util::char_to_uint32(buff);
		i.comp_type_flag = true;
		// TODO: check i.decomp_size + 8?
		pos_ += (i.comp_type ? i.comp_size : (i.decomp_size + 8));
	}
}

} // namespace record

// TODO: version?
void decom_routine::decomp(const string& src_path, const string& dst_path) {
	mdx_data_std_stream src{src_path};
	vector<buf_slide> v{};
	// auto k_size = k_sec_de(src, v);
	// auto r_size = r_sec_de(src, v);
	// check compression type, no_comp direct copy, else assemble file with
	// data slides in v
	bool cflag{false};
	for (auto& s : v) {
		if (std::get<0>(s) != comp_type::no_comp) {
			cflag = true;
			break;
		}
	}
	std::ofstream dst(dst_path, std::ios::binary);
	if (!cflag) {
		// direct copy
		dst << src.t.rdbuf();
	} else {
		std::for_each(v.begin(), v.end(), [&dst](const buf_slide& s) {
			dst.write(std::get<2>(s).get(), static_cast<std::streamsize>(std::get<1>(s)));
		});
	}
}

uint64_t decom_routine::k_sec_de(mdx_data_std_stream& src, vector<buf_slide>& v) {
	auto version_2_0 = src.is_version_2_0();
	unsigned encoding_unit = encoding_su::encoding_check_mdx::get_byte_unit(src._h_sec.Encoding);
	// store header as a slide
	unique_ptr<char[]> header{new char[src._k_sec.keyword_sect_to_key_index]};
	src.t.seekg(static_cast<std::streamoff>(src._k_sec.keyword_sect_offset));
	src.t.get(header.get(), src._k_sec.keyword_sect_to_key_index);
	v[_1_K_SECT_HEAD_P] =
		std::make_tuple(comp_type::no_comp, src._k_sec.keyword_sect_to_key_index, move(header));
	// check key_index comp type
	comp_type key_index_comp{static_cast<comp_type>(src._k_sec.key_index_comp_type)};
	// allocato space for key_index origin
	auto key_index_dat_raw_len =
		src._k_sec.key_index_comp_type ? src._k_sec.key_index_comp_len : src._k_sec.key_index_decomp_len;
	unique_ptr<char[]> key_index_raw_dat{new char[key_index_dat_raw_len]};
	// load key_index_raw_dat from file
	if (key_index_dat_raw_len <= std::numeric_limits<std::streamsize>::max DUMMY()) {
		src.t.get(key_index_raw_dat.get(), static_cast<std::streamsize>(key_index_dat_raw_len));
	} else {
		throw std::out_of_range{"buffer size too large for ifstream.get()"};
	}
	auto& key_index_dat = key_index_raw_dat;
	auto& key_index_dat_len = key_index_dat_raw_len;
	if (key_index_comp != comp_type::no_comp) {
		// allocate
		auto key_index_dat_decomp_len = src._k_sec.key_index_decomp_len;
		// allocate space for key_index decomp
		unique_ptr<char[]> key_index_decomp_dat{new char[key_index_dat_decomp_len]};
		auto comp_len = key_index_dat_raw_len - 8;
		auto decomp_len = key_index_dat_decomp_len - 8;
		// decomp

		unzip::unzip_strm_generic(key_index_comp, key_index_raw_dat.get() + 8, comp_len,
								  key_index_decomp_dat.get() + 8, decomp_len);

		// copy compressed data header and update the comp_type
		copy(key_index_raw_dat.get(), key_index_raw_dat.get() + 8, key_index_decomp_dat.get());
		util::comp_type_update_big_endian(key_index_decomp_dat.get(), comp_type::no_comp);
		// set len to decomp_len and swap key_index_decomp_dat to raw
		key_index_dat_len = key_index_dat_decomp_len;
		key_index_raw_dat.swap(key_index_decomp_dat);
	}
	// key_index data key_index_dat is ready for extracting info

	// vector of key block <comp decomp> size
	using pairll = pair<uint64_t, uint64_t>;
	vector<pairll> key_block_sizes;
	auto* ki_pos = key_index_dat.get(); // pointer for key_index_dat
	for (decltype(src._k_sec.num_blocks) i = 0; i < src._k_sec.num_blocks; ++i) {
		ki_pos += 8;
		ki_pos += util::char_to_uint16(ki_pos) * encoding_unit + encoding_unit + 2;
		ki_pos += util::char_to_uint16(ki_pos) * encoding_unit + encoding_unit + 2;
		key_block_sizes.emplace_back(
			std::make_pair(util::char_to_uint64(ki_pos), util::char_to_uint64(ki_pos + 8)));
		ki_pos += 16;
	}

	decltype(key_block_sizes[0].first) comp_size_sum{0};
	decltype(key_block_sizes[0].second) decomp_size_sum{0};
	for (auto i : key_block_sizes) {
		comp_size_sum += i.first;
		decomp_size_sum += i.second;
	}

	char key_block_heander0[4];
	src.t.get(key_block_heander0, 4);
	src.t.seekg(-4, std::ios::cur);
	comp_type key_block0_comp{static_cast<comp_type>(util::char_to_uint32(key_block_heander0))};
	auto raw_size_sum = static_cast<uint32_t>(key_block0_comp) ? comp_size_sum : decomp_size_sum;
	unique_ptr<char[]> key_block_raw_buffer{new char[raw_size_sum]};
	if (raw_size_sum <= std::numeric_limits<std::streamsize>::max DUMMY()) {
		src.t.get(key_block_raw_buffer.get(), static_cast<std::streamsize>(raw_size_sum));
	} else {
		throw std::out_of_range{"buffer size too large for ifstream.get()"};
	}
	if (key_block0_comp != comp_type::no_comp) {
		// allocate space and decom key_blocks[0]~[num_blocks-1]
		unique_ptr<char[]> key_block_decomp_buffer{new char[decomp_size_sum]};
		auto* decomp_buffer_p = key_block_decomp_buffer.get();
		auto* raw_buffer_p = key_block_raw_buffer.get();
		if (decomp_size_sum <= std::numeric_limits<std::streamsize>::max DUMMY()) {
			src.t.get(key_block_decomp_buffer.get(), static_cast<std::streamsize>(decomp_size_sum));
		} else {
			throw std::out_of_range{"buffer size too large for ifstream.get()"};
		}

		// decomp each key block:
		for (auto i : key_block_sizes) {
			// check compression type and handle header(comp_type 4 bytes
			// checksum 4 bytes)
			comp_type key_block_i_comp_type{static_cast<comp_type>(util::char_to_uint32(raw_buffer_p))};
			// skip the header and move to the compressed data
			raw_buffer_p += 8;
			decomp_buffer_p += 8;
			// calculation the size of compress data without the mdx
			// compressed data header
			auto&& in_size = i.first - 8;
			auto&& out_size = i.second - 8;
			// check and unzip
			unzip::unzip_strm_generic(key_block_i_comp_type, raw_buffer_p, in_size, decomp_buffer_p,
									  out_size);
			// update the comp_type
			util::comp_type_update_big_endian(decomp_buffer_p - 8, comp_type::no_comp);
			raw_buffer_p += in_size;
			decomp_buffer_p += out_size;
		}
		key_block_raw_buffer.swap(key_block_decomp_buffer);
	}

	// store k_block as a slide
	v[_3_K_SECT_BLOCKS_P] = std::make_tuple(key_block0_comp, decomp_size_sum, move(key_block_raw_buffer));
	// store key_index as a slide
	v[_2_K_SECT_INDEX_P] = std::make_tuple(key_index_comp, key_index_dat_len, move(key_index_dat));
	// return the whole size needed to store keyword_sect(decomp is
	// considered)
	return (src._k_sec.keyword_sect_to_key_index + key_index_dat_len + decomp_size_sum);
}

uint64_t decom_routine::r_sec_de(mdx_data_std_stream& src, vector<buf_slide>& v) {
	auto version_2_0 = src.is_version_2_0();
	auto r_header_s = src._r_sec.sect_offset_to_com_decom_sizes_offset;
	unique_ptr<char[]> header{new char[r_header_s]};
	src.t.seekg(static_cast<std::streamoff>(src._r_sec.sect_offset));
	src.t.get(header.get(), static_cast<std::streamsize>(r_header_s));
	auto num_blocks = util::endianSwap_64bit(header.get());
	//	auto index_len = util::endianSwap_64bit(header.get() + 16);
	//	auto blocks_len = util::endianSwap_64bit(header.get() + 24);
	// stor header as a slide
	v[_4_R_SECT_HEAD_P] = std::make_tuple(comp_type::no_comp, r_header_s, move(header));
	vector<pair<uint64_t, uint64_t>> com_decom_sizes;
	auto buf_temp = num_blocks * 2 * (version_2_0 ? 8 : 4);
	unique_ptr<char[]> com_decom_sizes_buf{new char[buf_temp]};
	if (buf_temp <= std::numeric_limits<std::streamsize>::max DUMMY()) {
		src.t.get(com_decom_sizes_buf.get(), static_cast<std::streamsize>(buf_temp));
	} else {
		throw std::out_of_range{"buffer size too large for ifstream.get()"};
	}
	uint64_t raw_size_sum{0};
	uint64_t decomp_size_sum{0};
	if (version_2_0) {
		for (decltype(num_blocks) i = 0; i < num_blocks; ++i) {
			auto com_s = util::endianSwap_64bit(com_decom_sizes_buf.get() + i * 16);
			auto decom_s = util::endianSwap_64bit(com_decom_sizes_buf.get() + i * 16 + 8);
			raw_size_sum += com_s;
			decomp_size_sum += decom_s;
			com_decom_sizes.emplace_back(std::make_pair(com_s, decom_s));
		}
	} else {
		for (decltype(num_blocks) i = 0; i < num_blocks; ++i) {
			auto com_s = util::endianSwap_32bit(com_decom_sizes_buf.get() + i * 8);
			auto decom_s = util::endianSwap_32bit(com_decom_sizes_buf.get() + i * 8 + 4);
			raw_size_sum += com_s;
			decomp_size_sum += decom_s;
			com_decom_sizes.emplace_back(std::make_pair(com_s, decom_s));
		}
	}

	unique_ptr<char[]> raw_buf{new char[raw_size_sum]};
	if (raw_size_sum <= std::numeric_limits<std::streamsize>::max DUMMY()) {
		src.t.get(raw_buf.get(), static_cast<std::streamsize>(raw_size_sum));
	} else {
		throw std::out_of_range{"buffer size too large for ifstream.get()"};
	}
	/*
	 * check compression type on record0 and
	 * decom record block if necessary
	 */
	auto record0_comp_t = static_cast<comp_type>(util::char_to_uint32(raw_buf.get()));
	if (record0_comp_t != comp_type::no_comp) {
		unique_ptr<char[]> decomp_buf{new char[decomp_size_sum]};
		auto _ptr0 = raw_buf.get();
		auto _ptr1 = decomp_buf.get();
		for (auto i : com_decom_sizes) {
			comp_type record_i_comp_type{static_cast<comp_type>(util::char_to_uint32(_ptr0))};
			// skip the header and move to the compressed data
			_ptr0 += 8;
			_ptr1 += 8;
			// calculation the size of compress data without the mdx
			// compressed data header
			auto&& in_size = i.first - 8;
			auto&& out_size = i.second - 8;
			unzip::unzip_strm_generic(record_i_comp_type, _ptr0, in_size, _ptr1, out_size);
			// update comp_type
			util::comp_type_update_big_endian(_ptr1 - 8, comp_type::no_comp);
			_ptr0 += in_size;
			_ptr1 += out_size;
		}
		raw_buf.swap(decomp_buf);
	}
	// store decomp buf as a slide
	v[_6_R_SECT_BLOCKS_P] = std::make_tuple(record0_comp_t, decomp_size_sum, move(raw_buf));
	// store sizes as a slide
	v[_5_R_SECT_SIZES_P] = std::make_tuple(comp_type::no_comp, buf_temp, move(com_decom_sizes_buf));
	// return total size needed for record section(compression is
	// considered)
	return src._r_sec.sect_offset_to_com_decom_sizes_offset + buf_temp + decomp_size_sum;
}

} // namespace mdx
} // namespace lexicon

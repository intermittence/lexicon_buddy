//
//  http_common.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2019/2/6.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include <lexicon_buddy/lexicon/http_common.h>
namespace lexicon
{
namespace online
{

// https://en.wikipedia.org/wiki/URL
// URI = scheme:[//authority]path[?query][#fragment]
// authority = [userinfo@]host[:port]
LEXICON_BUDDY_EXPORT URL urlstr_2_URL(string_view urlstr, bool& valid) {
	URL u;
	auto dat_begin = urlstr.data();
	auto dat_end = dat_begin + urlstr.size();
	auto len = urlstr.size();
	// "scheme:"
	auto p_colon = urlstr.find(':');
	if (p_colon == string::npos) {
		valid = false;
		return u;
	}
	valid = true;
	u.scheme = {dat_begin, p_colon};
	auto p_authority_begin = string_view::npos;
	auto p_authority_end = string_view::npos;
	auto p_path_begin = string_view::npos;
	if (len < p_colon + (sizeof("//") - 1) + 1)
	// authority do not exist:
	{
		if (len > p_colon + 1) {
			if (urlstr[p_colon + 1] == '/') {
				p_path_begin = p_colon + 2;
			}
		}
	} else {
		if (urlstr[p_colon + 1] == '/') {
			if (urlstr[p_colon + 2] == '/')
			// authority exist:
			{
				p_authority_begin = p_colon + 3;
				// find path
				p_path_begin = urlstr.find('/', p_authority_begin);
				p_authority_end = (p_path_begin != string_view::npos) ? p_path_begin : len;
			} else
			// authority do not exist:
			{
				p_path_begin = p_colon + 2;
			}
		} else
		// authority do not exist:
		{
			p_path_begin = p_colon + 2;
		}
	}

	// [authority] = [[userinfo@]host[:port]]
	if (p_authority_begin != string_view::npos) {
		// [userinfo@]
		auto p_at = urlstr.find('@', p_authority_begin);
		auto p_host_begin = p_authority_begin;
		if (p_at != string_view::npos) {
			u.userinfo = {dat_begin + p_authority_begin, dat_begin + p_at};
			p_host_begin = p_at + 1;
		}
		// [:port]
		auto p_host_end = p_authority_end;
		auto p_colon2 = urlstr.find(':', p_host_begin);
		if (p_colon2 != string_view::npos) {
			u.port = {dat_begin + p_colon2, dat_begin + p_authority_end};
			p_host_end = p_colon2;
		}
		// host
		u.host = {dat_begin + p_host_begin, dat_begin + p_host_end};
	}
	// path[?query][#fragment]
	if (p_path_begin != string_view::npos) {
		auto p_query_mark = urlstr.find('?');
		auto tmp_end = len;
		auto p_fragment_mark = urlstr.find('#');
		if (p_fragment_mark != string_view::npos) {
			u.fragment = {dat_begin + p_fragment_mark + 1, dat_end};
			tmp_end = p_fragment_mark;
		}
		if (p_query_mark != string_view::npos) {
			u.query = {dat_begin + p_query_mark + 1, dat_begin + tmp_end};
			tmp_end = p_query_mark;
		}
		u.path = {dat_begin + p_authority_end, dat_begin + tmp_end};
	}
	return u;
}

// https://en.wikipedia.org/wiki/URL
// URI = scheme:[//authority]path[?query][#fragment]
// authority = [userinfo@]host[:port]
LEXICON_BUDDY_EXPORT URL urlstr_2_URL(string_view urlstr) {
	URL u;
	auto dat_begin = urlstr.data();
	auto dat_end = dat_begin + urlstr.size();
	auto len = urlstr.size();
	// "scheme:"
	auto p_colon = urlstr.find(':');
	if (p_colon == string::npos) {
		return u;
	}
	u.scheme = {dat_begin, p_colon};
	auto p_authority_begin = string_view::npos;
	auto p_authority_end = string_view::npos;
	auto p_path_begin = string_view::npos;
	if (len < p_colon + (sizeof("//") - 1) + 1)
	// authority do not exist:
	{
		if (len > p_colon + 1) {
			if (urlstr[p_colon + 1] == '/') {
				p_path_begin = p_colon + 2;
			}
		}
	} else {
		if (urlstr[p_colon + 1] == '/') {
			if (urlstr[p_colon + 2] == '/')
			// authority exist:
			{
				p_authority_begin = p_colon + 3;
				// find path
				p_path_begin = urlstr.find('/', p_authority_begin);
				p_authority_end = (p_path_begin != string_view::npos) ? p_path_begin : len;
			} else
			// authority do not exist:
			{
				p_path_begin = p_colon + 2;
			}
		} else
		// authority do not exist:
		{
			p_path_begin = p_colon + 2;
		}
	}

	// [authority] = [[userinfo@]host[:port]]
	if (p_authority_begin != string_view::npos) {
		// [userinfo@]
		auto p_at = urlstr.find('@', p_authority_begin);
		auto p_host_begin = p_authority_begin;
		if (p_at != string_view::npos) {
			u.userinfo = {dat_begin + p_authority_begin, dat_begin + p_at};
			p_host_begin = p_at + 1;
		}
		// [:port]
		auto p_host_end = p_authority_end;
		auto p_colon2 = urlstr.find(':', p_host_begin);
		if (p_colon2 != string_view::npos) {
			u.port = {dat_begin + p_colon2, dat_begin + p_authority_end};
			p_host_end = p_colon2;
		}
		// host
		u.host = {dat_begin + p_host_begin, dat_begin + p_host_end};
	}
	// path[?query][#fragment]
	if (p_path_begin != string_view::npos) {
		auto p_query_mark = urlstr.find('?');
		auto tmp_end = len;
		auto p_fragment_mark = urlstr.find('#');
		if (p_fragment_mark != string_view::npos) {
			u.fragment = {dat_begin + p_fragment_mark + 1, dat_end};
			tmp_end = p_fragment_mark;
		}
		if (p_query_mark != string_view::npos) {
			u.query = {dat_begin + p_query_mark + 1, dat_begin + tmp_end};
			tmp_end = p_query_mark;
		}
		u.path = {dat_begin + p_authority_end, dat_begin + tmp_end};
	}
	return u;
}

LEXICON_BUDDY_EXPORT http::request<http::empty_body> URL_2_http_request(const URL& url) {
	http::request<http::empty_body> req{http::verb::get, url.path.empty() ? "/" : url.path, 11};
	if (url.port.empty()) {
		req.set(http::field::host, url.host);
	} else {
		req.set(http::field::host, url.host + ":" + url.port);
	}
	req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
	// req.set(http::field::accept, "*/*");
	return req;
}

} // namespace online
} // namespace lexicon

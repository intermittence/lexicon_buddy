//
//  buddy.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2019/1/26.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include <lexicon_buddy/lexicon/buddy.h>
namespace lexicon
{
namespace buddy
{

template <>
bool oxford_service::cool_with_src_lang<oxford_service::query_type::Dictionary_entries>(lang l) {
	switch (l) {
		case lang::de:
			return true;
		case lang::en_gb:
			return true;
		case lang::en_us:
			return true;
		case lang::es:
			return true;
		case lang::hi:
			return true;
		case lang::pt:
			return true;
		case lang::tn:
			return true;
		case lang::zu:
			return true;
		case lang::nso:
			return true;
		default:
			return false;
	}
}

template <>
bool oxford_service::cool_with<oxford_service::query_type::Translation>(const search_ctx& ctx) {
	auto& pair = ctx.lang_pair;
	switch (pair.first) {
		case lang::en:
			switch (pair.second) {
				lan_case_and_return(es);
				lan_case_and_return(nso);
				lan_case_and_return(ro);
				lan_case_and_return(ms);
				lan_case_and_return(zu);
				lan_case_and_return(id);
				lan_case_and_return(tn);
				lan_case_and_return(de);
				lan_case_and_return(pt);
				lan_case_and_return(el);
				lan_case_and_return(xh);
				lan_case_and_return(tg);
				lan_case_and_return(tt);
				lan_case_and_return(tpi);
				lan_case_and_return(tk);
				default:
					return false;
			}
			break;
		case lang::es:
			lan_switch_case2(en, qu);
		case lang::nso:
			lan_switch_case1(en);
		case lang::zu:
			lan_switch_case1(en);
		case lang::ms:
			lan_switch_case1(en);
		case lang::id:
			lan_switch_case1(en);
		case lang::tn:
			lan_switch_case1(en);
		case lang::ur:
			lan_switch_case1(en);
		case lang::de:
			lan_switch_case1(en);
		case lang::pt:
			lan_switch_case1(en);
		case lang::el:
			lan_switch_case1(en);
		case lang::qu:
			lan_switch_case1(en);
		case lang::te:
			lan_switch_case1(en);
		case lang::tk:
			lan_switch_case1(en);
		case lang::tpi:
			lan_switch_case1(en);
		case lang::tt:
			lan_switch_case1(en);
		case lang::xh:
			lan_switch_case1(en);
		default:
			return false;
	}
}

LEXICON_BUDDY_EXPORT std::ostream& operator<<(std::ostream& out, const mdx_record_bundle& r) {
	out << "====================== MDX DICT META =======================\n";
	out << "dict name: " << r.d_mt.get_meta().alias_ << '\n';
	out << "dict media type: " << media_type_to_str(r.d_mt.get_meta().m_type_) << '\n';
	out << "====================== MDX DICT META =======================\n";
	out << "----------------------- DICT RECORD ------------------------\n";
	mdx::operator<<(out, r.record);
	out << "----------------------- DICT RECORD ------------------------\n";
	return out;
}

LEXICON_BUDDY_EXPORT std::ostream& operator<<(std::ostream& out, const stardict_record_bundle& r) {
	out << "==================== STARDICT DICT META ====================\n";
	out << "dict name: " << r.d_mt.get_meta().alias_ << '\n';
	out << "dict media type: " << media_type_to_str(r.d_mt.get_meta().m_type_) << '\n';
	out << "==================== STARDICT DICT META ====================\n";
	out << "------------------------ DICT RECORD -----------------------\n";
	stardict::operator<<(out, r.record);
	out << "------------------------ DICT RECORD -----------------------\n";
	return out;
}

LEXICON_BUDDY_EXPORT oxford_service::api_key_t oxford_service::oxford_setting::str_2_key(const string& arg) {
	api_key_t k{};
	auto p = arg.find(':');
	if (p != string::npos) {
		k.ID = string{arg, 0, p};
		k.Key = string{arg, p + 1, arg.size() - p - 1};
	}
	return k;
}

LEXICON_BUDDY_EXPORT string oxford_service::oxford_setting::key_2_str(const oxford_service::api_key_t& arg) {
	string tmp = arg.ID + ':';
	tmp += arg.Key;
	return tmp;
}

LEXICON_BUDDY_EXPORT oxford_service::oxford_setting
oxford_service::oxford_setting::create(const pt::ptree& service) {
	oxford_setting tmp;
	tmp.key = str_2_key(service.get<string>("key", key_default));
	tmp.state = state_t{service.get<string>("<xmlattr>.state", state_default)};
	return tmp;
}

LEXICON_BUDDY_EXPORT string oxford_service::oxford_setting::state_2_str(state_t arg) {
	string state_str{};
	for (size_t i = 3; i <= 3; --i) {
		state_str += arg[i] ? '1' : '0';
	}
	return state_str;
}

LEXICON_BUDDY_EXPORT void oxford_service::oxford_setting::save(pt::ptree& service) {
	service.erase("key");
	service.put("key", key_2_str(key));
	string state_str{};
	for (size_t i = 0; i < 4; ++i) {
		state_str += state[i] ? '1' : '0';
	}
	service.put("<xmlattr>.state", state_str);
}

LEXICON_BUDDY_EXPORT webster_service::api_key_t
webster_service::webster_setting::str_2_key(const string& arg) {
	api_key_t k{};
	auto p = arg.find(':');
	if (p != string::npos) {
		k.Collegiate_key = string{arg, 0, p};
		k.Thesaurus_key = string{arg, p + 1, arg.size() - p - 1};
	}
	return k;
}

LEXICON_BUDDY_EXPORT string
webster_service::webster_setting::key_2_str(const webster_service::api_key_t& arg) {
	string tmp = arg.Collegiate_key + ':';
	tmp += arg.Thesaurus_key;
	return tmp;
}

LEXICON_BUDDY_EXPORT webster_service::webster_setting
webster_service::webster_setting::create(const pt::ptree& service) {
	webster_setting tmp;
	tmp.key = str_2_key(service.get<string>("key", key_default));
	tmp.state = state_t{service.get<string>("<xmlattr>.state", state_default)};
	return tmp;
}

LEXICON_BUDDY_EXPORT string webster_service::webster_setting::state_2_str(state_t arg) {
	string state_str{};
	for (size_t i = 1; i <= 1; --i) {
		state_str += arg[i] ? '1' : '0';
	}
	return state_str;
}

LEXICON_BUDDY_EXPORT void webster_service::webster_setting::save(pt::ptree& service) {
	service.erase("key");
	service.put("key", key_2_str(key));
	string state_str = state_2_str(state);
	service.put("<xmlattr>.state", state_str);
}

LEXICON_BUDDY_EXPORT bool cool_with(const search_ctx& ctx, dict_meta meta) {
	auto& meta_view = meta.get_meta();
	if ((ctx.lang_pair.first == lang::all || meta_view.lang_pair0_.first == lang::all ||
		 ctx.lang_pair.first == meta_view.lang_pair0_.first) &&
		(meta_view.lang_pair0_.second == lang::all || ctx.lang_pair.second == meta_view.lang_pair0_.second)) {
		return true;
	} else if (meta_view.secondary_lan_) {
		if ((ctx.lang_pair.first == lang::all || meta_view.lang_pair1_.first == lang::all ||
			 ctx.lang_pair.first == meta_view.lang_pair1_.first) &&
			(meta_view.lang_pair1_.second == lang::all ||
			 ctx.lang_pair.second == meta_view.lang_pair1_.second))
			return true;
	}
	return false;
}

#ifndef print_prediction_single
#define print_prediction_single(r, s)                                                                        \
	if (!(i.second.r.empty())) {                                                                             \
		out << "   " << s << '\n';                                                                           \
		for (auto& i0 : i.second.r) {                                                                        \
			out << "      " << i0.first->d_mt.get_meta().alias_ << ' ' << "id: " << i0.second << '\n';       \
		}                                                                                                    \
	}

#endif

LEXICON_BUDDY_EXPORT std::ostream& operator<<(std::ostream& out, const query_cluster::predict_type& p) {
	for (auto& i : p) {
		out << "key: " << i.first << '\n';
		print_prediction_single(stardict_v32_result, "stardict_v32_result: ");
		print_prediction_single(stardict_v64_result, "stardict_v64_result: ");
		print_prediction_single(mdx_data_std_stream_result, "mdx_data_std_stream_result: ");
		print_prediction_single(mdx_data_mmap_result, "mdx_data_mmap_result: ");
	}
	out << endl;
	return out;
}

LEXICON_BUDDY_EXPORT void dict_meta::meta::operator<<(const cfg_ptree_type& item) {
	meta m;
	enabled = item.get<bool>("<xmlattr>.enabled", true);
	alias_ = item.get<string>("<xmlattr>.alias", "");
	m_type_ = str_to_media_type(item.get<string>("<xmlattr>.res_media_type"));
	lang_pair0_ = str_to_lang_pair_type(item.get<string>("<xmlattr>.lang_pair0", "all:all"));
	auto lang_pair1_str = item.get<string>("<xmlattr>.lang_pair1", "");
	if (lang_pair1_str.empty()) {
		m.secondary_lan_ = false;
	} else {
		m.secondary_lan_ = true;
		m.lang_pair1_ = str_to_lang_pair_type(lang_pair1_str);
	}
}

LEXICON_BUDDY_EXPORT void dict_meta::meta::operator>>(cfg_ptree_type& item) {
	item.put("<xmlattr>.enabled", enabled);
	item.put("<xmlattr>.alias", alias_);
	item.put("<xmlattr>.res_media_type", media_type_to_str(m_type_));
	item.put("<xmlattr>.lang_pair0", lang_pair_type_to_str(lang_pair0_));
	if (secondary_lan_) {
		item.put("<xmlattr>.lang_pair1", lang_pair_type_to_str(lang_pair1_));
	} else {
		item.put("<xmlattr>.lang_pair1", "");
	}
}

// LEXICON_BUDDY_EXPORT dict_meta::meta dict_meta::cfg_2_meta(const cfg_ptree_type* item) {
//	meta m;
//	m.enabled = item->get<bool>("<xmlattr>.enabled", true);
//	m.alias_ = item->get<string>("<xmlattr>.alias", "");
//	m.m_type_ = str_to_media_type(item->get<string>("<xmlattr>.res_media_type"));
//	m.lang_pair0_ = str_to_lang_pair_type(item->get<string>("<xmlattr>.lang_pair0", "all:all"));
//	auto lang_pair1_str = item->get<string>("<xmlattr>.lang_pair1", "");
//	if (lang_pair1_str.empty()) {
//		m.secondary_lan_ = false;
//	} else {
//		m.secondary_lan_ = true;
//		m.lang_pair1_ = str_to_lang_pair_type(lang_pair1_str);
//	}
//	return m;
//}

// LEXICON_BUDDY_EXPORT void dict_meta::save_meta_in_cfg(cfg_ptree_type* item, const meta& m) {
//	item->put("<xmlattr>.enabled", m.enabled);
//	item->put("<xmlattr>.alias", m.alias_);
//	item->put("<xmlattr>.res_media_type", media_type_to_str(m.m_type_));
//	item->put("<xmlattr>.lang_pair0", lang_pair_type_to_str(m.lang_pair0_));
//	if (m.secondary_lan_) {
//		item->put("<xmlattr>.lang_pair1", lang_pair_type_to_str(m.lang_pair1_));
//	} else {
//		item->put("<xmlattr>.lang_pair1", "");
//	}
//}

} // namespace buddy
} // namespace lexicon

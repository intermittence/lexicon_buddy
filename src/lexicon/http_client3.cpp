//
//  http_client3.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2019/4/3.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include <lexicon_buddy/lexicon/http_client3.h>

namespace lexicon
{
namespace online
{
bool parse_socks_url(const std::string& url, socks_url& result) {
	// Read scheme
	auto it = std::find(url.begin(), url.end(), ':');
	if (it == url.end()) return false;
	for (auto first = url.begin(); first != it; first++) {
		auto& c = *first;
		if (!is_scheme_char(c)) return false;
		result.scheme.push_back(static_cast<char>(std::tolower(c)));
	}
	// Skip ':'
	it++;
	// Eat "//"
	for (int i = 0; i < 2; i++) {
		if (*it++ != '/') return false;
	}
	// Check if the user (and password) are specified.
	auto tmp_it = std::find(it, url.end(), '@');
	if (tmp_it != url.end()) {
		int userpwd_flag = 0;
		for (auto first = it; first != tmp_it; first++) {
			auto& c = *first;
			if (c == ':') {
				userpwd_flag = 1;
				continue;
			}
			if (userpwd_flag == 0)
				result.username.push_back(c);
			else if (userpwd_flag == 1)
				result.password.push_back(c);
			else
				return false;
		}
		std::advance(it, tmp_it - it);
		// Eat '@'
		it++;
	}
	bool is_ipv6_address = *it == '[';
	if (is_ipv6_address) {
		tmp_it = std::find(it, url.end(), ']');
		if (tmp_it == url.end()) return false;
		// Eat '['
		it++;
		for (auto first = it; first != tmp_it; first++)
			result.host.push_back(*first);
		// Skip host.
		std::advance(it, tmp_it - it);
		// Eat ']'
		it++;
	} else {
		tmp_it = std::find(it, url.end(), ':');
		if (tmp_it != url.end()) {
			for (auto first = it; first != tmp_it; first++)
				result.host.push_back(*first);
		} else {
			tmp_it = std::find(it, url.end(), '/');
			for (auto first = it; first != tmp_it; first++)
				result.host.push_back(*first);
			if (tmp_it == url.end()) return true;
		}
		// Skip host.
		std::advance(it, tmp_it - it);
	}
	// Port number is specified.
	if (*it == ':') {
		// Eat ':'
		it++;
		tmp_it = std::find(it, url.end(), '/');
		for (auto first = it; first != tmp_it; first++) {
			auto& c = *first;
			if (!isdigit(c)) return false;
			result.port.push_back(*first);
		}
		if (tmp_it == url.end()) return true;
		// Skip port.
		std::advance(it, tmp_it - it);
	}
	// Parse path.
	tmp_it = std::find(it, url.end(), '?');
	if (tmp_it != url.end()) {
		// Read path.
		for (auto first = it; first != tmp_it; first++)
			result.path.push_back(*first);
		// Skip path.
		std::advance(it, tmp_it - it);
		// Skip '?'
		it++;
	} else {
		tmp_it = std::find(it, url.end(), '#');
		if (tmp_it != url.end()) {
			// Read path.
			for (auto first = it; first != tmp_it; first++)
				result.path.push_back(*first);
			// Skip path.
			std::advance(it, tmp_it - it);
			// Skip '#'
			it++;
			// Read fragment.
			for (auto first = it; first != url.end(); first++)
				result.fragment.push_back(*first);
			return true;
		} else {
			// Read path.
			for (auto first = it; first != url.end(); first++)
				result.path.push_back(*first);
			return true;
		}
	}
	// Read query.
	tmp_it = std::find(it, url.end(), '#');
	if (tmp_it != url.end()) {
		// Read query.
		for (auto first = it; first != tmp_it; first++)
			result.query.push_back(*first);
		// Skip query.
		std::advance(it, tmp_it - it);
		// Skip '#'
		it++;
		// Read fragment.
		for (auto first = it; first != url.end(); first++)
			result.fragment.push_back(*first);
		return true;
	}
	// Read query.
	for (auto first = it; first != url.end(); first++)
		result.query.push_back(*first);
	return true;
}
} // namespace online
} // namespace lexicon

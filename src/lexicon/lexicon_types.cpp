//
//  lexicon_types.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2019/3/28.
//  Copyright © 2019 Lohengrin. All rights reserved.
//
#include <lexicon_buddy/lexicon_buddy_export.h>
#include <lexicon_buddy/lexicon/lexicon_types.h>
#include <algorithm>
namespace lexicon
{
using std::equal;

//std::pair<string, string> dict_filename_separation(const string& arg) {
	
//	auto pos = arg.find_last_of('.');
//	return {arg.substr(0, pos), arg.substr(pos)};
//}

res_bundle_type res_type_to_bundle_type(res_type arg) {
	switch (arg) {
		case res_type::mdx:
			return res_bundle_type::mdx;
		case res_type::startdict_idx:
		case res_type::startdict_ifo:
		case res_type::startdict_dict_dz:
			return res_bundle_type::startdict_dict_dz_bundle;
		default:
			return res_bundle_type::invalid;
	}
}
res_type str_to_res_type(const char* str) {
	if (std::strcmp(str, ".mdx") == 0) {
		return res_type::mdx;
	} else if (std::strcmp(str, ".mdd") == 0) {
		return res_type::mdd;
	} else if (std::strcmp(str, ".dict.dz") == 0) {
		return res_type::startdict_dict_dz;
	} else if (std::strcmp(str, ".idx") == 0) {
		return res_type::startdict_idx;
	} else if (std::strcmp(str, ".ifo") == 0) {
		return res_type::startdict_ifo;
	} else {
		return res_type::invalid;
	}
}
res_type str_to_res_type(const std::string& str) {
	if (str == ".mdx") {
		return res_type::mdx;
	} else if (str == ".mdd") {
		return res_type::mdd;
	} else if (str == ".dict.dz") {
		return res_type::startdict_dict_dz;
	} else if (str == ".idx") {
		return res_type::startdict_idx;
	} else if (str == ".ifo") {
		return res_type::startdict_ifo;
	} else {
		return res_type::invalid;
	}
}

const char* res_type_to_str(res_type t) {
	switch (t) {
		case lexicon::res_type::invalid:
			return "invalid";
		case lexicon::res_type::mdx:
			return ".mdx";
		case lexicon::res_type::mdd:
			return ".mdd";
		case lexicon::res_type::startdict_dict_dz:
			return ".dict.dz";
		case lexicon::res_type::startdict_idx:
			return ".idx";
		case lexicon::res_type::startdict_ifo:
			return ".ifo";
	}
}

const char* res_bundle_type_to_str(res_bundle_type arg) {
	switch (arg) {
		case lexicon::res_bundle_type::mdd:
			return ".mdd";
		case lexicon::res_bundle_type::mdx:
			return ".mdx";
		case lexicon::res_bundle_type::startdict_dict_dz_bundle:
			return ".dict.dz";
		case lexicon::res_bundle_type::invalid:
			return "invalid";
	}
}

res_media_type str_to_media_type(const char* str) {

	if (std::strcmp(str, "unknown") == 0) {
		return res_media_type::unknown;
	} else if (std::strcmp(str, "text") == 0) {
		return res_media_type::text;
	} else if (std::strcmp(str, "pic") == 0) {
		return res_media_type::pic;
	} else if (std::strcmp(str, "sound") == 0) {
		return res_media_type::sound;
	} else {
		return res_media_type::invalid;
	}
}

res_media_type str_to_media_type(const string& str) {

	if (str == "unknown") {
		return res_media_type::unknown;
	} else if (str == "text") {
		return res_media_type::text;
	} else if (str == "pic") {
		return res_media_type::pic;
	} else if (str == "sound") {
		return res_media_type::sound;
	} else {
		return res_media_type::invalid;
	}
}

const char* media_type_to_str(res_media_type t) {
	switch (t) {
		case res_media_type::invalid:
			return "invalid";
		case res_media_type::unknown:
			return "unknown";
		case res_media_type::text:
			return "text";
		case res_media_type::pic:
			return "pic";
		case res_media_type::sound:
			return "sound";
	}
}
namespace buddy
{

#ifndef lang_case_return
#define lang_case_return(e)                                                                                  \
	case lang::e:                                                                                            \
		return #e
#define lang_case_return_2(literal, e)                                                                       \
	case lang::e:                                                                                            \
		return literal
#endif
const char* lang_type_to_str(lang l) {
	switch (l) {
		lang_case_return(all);
		lang_case_return(en);
		lang_case_return(es);
		lang_case_return(nso);
		lang_case_return(zu);
		lang_case_return(ms);
		lang_case_return(id);
		lang_case_return(hi);
		lang_case_return(tn);
		lang_case_return(ur);
		lang_case_return(de);
		lang_case_return(pt);
		lang_case_return(el);
		lang_case_return(qu);
		lang_case_return(te);
		lang_case_return(tk);
		lang_case_return(tg);
		lang_case_return(tpi);
		lang_case_return(tt);
		lang_case_return(xh);
		lang_case_return(lv);
		lang_case_return(sw);
		lang_case_return(ta);
		lang_case_return(gu);
		lang_case_return(ro);
		lang_case_return(empty);
		lang_case_return_2("en-gb", en_gb);
		lang_case_return_2("en-us", en_us);
		lang_case_return_2("zh-CN", zh_CN);
		lang_case_return_2("zh-TW", zh_TW);
	}
}

#ifndef literal_2_lang_enum
#define literal_2_lang_enum(e)                                                                               \
	if (str == #e) return lang::e
#define literal_2_lang_enum_2(literal, e)                                                                    \
	if (str == literal) return lang::e
#endif

lang str_to_lang_type(const string& str) {
	literal_2_lang_enum(all);
	literal_2_lang_enum(en);
	literal_2_lang_enum(es);
	literal_2_lang_enum(hi);
	literal_2_lang_enum(nso);
	literal_2_lang_enum(tn);
	literal_2_lang_enum(zu);
	literal_2_lang_enum(de);
	literal_2_lang_enum(pt);
	literal_2_lang_enum(ms);
	literal_2_lang_enum(id);
	literal_2_lang_enum(ur);
	literal_2_lang_enum(el);
	literal_2_lang_enum(qu);
	literal_2_lang_enum(te);
	literal_2_lang_enum(tk);
	literal_2_lang_enum(tg);
	literal_2_lang_enum(tpi);
	literal_2_lang_enum(tt);
	literal_2_lang_enum(xh);
	literal_2_lang_enum(lv);
	literal_2_lang_enum(sw);
	literal_2_lang_enum(ta);
	literal_2_lang_enum(gu);
	literal_2_lang_enum(ro);
	literal_2_lang_enum(empty);
	literal_2_lang_enum_2("en-gb", en_gb);
	literal_2_lang_enum_2("en-us", en_us);
	literal_2_lang_enum_2("zh-CN", zh_CN);
	literal_2_lang_enum_2("zh-TW", zh_TW);
	throw std::invalid_argument{"no corresponding lang_type for input string\n"};
}

#ifndef literal_2_lang_enum_
#define literal_2_lang_enum_(e)                                                                              \
	if (equal(first, last, #e)) return lang::e
#define literal_2_lang_enum_2_(literal, e)                                                                   \
	if (equal(first, last, literal)) return lang::e
#endif

lang str_to_lang_type(const char* first, const char* last) {
	switch (last - first) {
		case 2:
			literal_2_lang_enum_(en);
			literal_2_lang_enum_(es);
			literal_2_lang_enum_(zu);
			literal_2_lang_enum_(ms);
			literal_2_lang_enum_(id);
			literal_2_lang_enum_(tn);
			literal_2_lang_enum_(ur);
			literal_2_lang_enum_(de);
			literal_2_lang_enum_(pt);
			literal_2_lang_enum_(el);
			literal_2_lang_enum_(qu);
			literal_2_lang_enum_(te);
			literal_2_lang_enum_(tk);
			literal_2_lang_enum_(tt);
			literal_2_lang_enum_(tg);
			literal_2_lang_enum_(xh);
			literal_2_lang_enum_(lv);
			literal_2_lang_enum_(sw);
			literal_2_lang_enum_(ta);
			literal_2_lang_enum_(gu);
			literal_2_lang_enum_(ro);
			break;
		case 3:
			literal_2_lang_enum_(nso);
			literal_2_lang_enum_(tpi);
			literal_2_lang_enum_(all);
			break;
		case 5:
			literal_2_lang_enum_2_("en-gb", en_gb);
			literal_2_lang_enum_2_("en-us", en_gb);
			literal_2_lang_enum_2_("zh-CN", zh_CN);
			literal_2_lang_enum_2_("zh-TW", zh_TW);
			literal_2_lang_enum_(empty);
			break;
	}
	throw std::invalid_argument{"no corresponding lang_type for input string\n"};
}

string lang_pair_type_to_str(lang_type l) {
	return string{lang_type_to_str(l.first)} + ":" + lang_type_to_str(l.second);
}

LEXICON_BUDDY_EXPORT lang_type str_to_lang_pair_type(const string& str) {
	auto ret = str.find_first_of(':');
	if (ret == string::npos)
		throw std::invalid_argument{"no corresponding lang_pair_type for input string\n"};
	auto p = str.c_str();
	auto lan_src = str_to_lang_type(p, p + ret);
	auto lan_dst = str_to_lang_type(p + ret + 1, p + str.size());
	return {lan_src, lan_dst};
}
} // namespace buddy
} // namespace lexicon
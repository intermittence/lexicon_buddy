//
//  stardictContainer.cpp
//  lexicon_buddy
//
//  Created by Lohengrin on 2018/6/21.
//  Copyright © 2018 Lohengrin. All rights reserved.
//
#include <lexicon_buddy/lexicon/stardictcontainer.h>
#include <lexicon_buddy/utilities/encoding_support.hpp>
#include <lexicon_buddy/utilities/unzip_tool.h>
#include <algorithm>
#include <boost/iostreams/stream.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace pt = boost::property_tree;
namespace lexicon
{
namespace stardict
{

LEXICON_BUDDY_EXPORT std::ostream& operator<<(std::ostream& out, const dict_record& v) {
	for (auto& i : v.index_) {
		switch (i.type_) {
			case 'W':
			case 'P':
			case 'X':
			case 'r':
				break;
			default:
				out.write(i.begin_, i.size_);
				out << '\n';
				break;
		}
	}
	return out;
}

LEXICON_BUDDY_EXPORT std::ostream& operator<<(std::ostream& out, const dict_record_v& v) {
	for (auto& i : v) {
		//		out << "type: " << i.sametypesequence << '\n';
		//		out << i.str << '\n';
		out << i << '\n';
	}
	return out;
}

LEXICON_BUDDY_EXPORT res_type ifo_content_validation(const char* data) {
	char dict[] = "StarDict's dict ifo file";
	char storage[] = "StarDict's storage ifo file";
	char treedict[] = "StarDict's treedict ifo file";
	// do not compare the null terminator
	if (std::equal(data, data + sizeof(dict) - 1, dict)) {
		return res_type::dict_ifo;
	} else if (std::equal(data, data + sizeof(storage) - 1, storage)) {
		return res_type::storage_ifo;
	} else if (std::equal(data, data + sizeof(treedict) - 1, treedict)) {
		return res_type::treedict_ifo;
	}
	return res_type::invalid;
}
// res_type idx_content_validation(const char* idx_data, const char* ifo_data) { }
// make sure path denotes a actual valid .ifo file
LEXICON_BUDDY_EXPORT ifo_str::ifo_str(const char* path) {
	// mmap the ifo file
	util::mmap_w mmap{path};
	string msg = string{"invalid ifo file(stardict): "} + path;
	if (mmap.get_filesize() <= 1) throw std::runtime_error{msg};
	auto ptr = mmap.data();
	auto last_byte_ptr = ptr + mmap.get_filesize() - 1;
	// skip first line
	while (*(ptr++) != '\n') {
		if (ptr == last_byte_ptr) throw std::runtime_error{msg};
	}
	auto first_line_len = ptr - mmap.data();
#ifndef NDEBUG
	boost::iostreams::stream<boost::iostreams::array_source> str_str0(
		const_cast<char*>(ptr), static_cast<std::streamsize>(mmap.get_filesize()) - first_line_len);
	cout << "===========boost::iostreams::array_source content: ===========\n";
	for (;;) {
		char x = static_cast<char>(str_str0.get());
		if (str_str0.eof()) break;
		cout << x;
	}
	cout << "===========boost::iostreams::array_source content: ===========" << endl;
#endif

	boost::iostreams::stream<boost::iostreams::array_source> str_str(
		const_cast<char*>(ptr), static_cast<std::streamsize>(mmap.get_filesize()) - first_line_len);
	pt::ptree tree;
	pt::ini_parser::read_ini(str_str, tree);
	/*
	bookname=      // required
	wordcount=     // required
	synwordcount=  // required if ".syn" file exists.
	idxfilesize=   // required
	idxoffsetbits= // New in 3.0.0
	author=
	email=
	website=
	description=	// You can use <br> for new line.
	date=
	sametypesequence= // very important.
	dicttype=
	*/
	version = tree.get<string>(u8"version", "");
	bookname = tree.get<string>(u8"bookname", "");
	wordcount = tree.get<unsigned int>(u8"wordcount", 0);
	synwordcount = tree.get<unsigned int>(u8"synwordcount", 0);
	idxfilesize = tree.get<unsigned long long>(u8"idxfilesize", 0);
	idxoffsetbits = tree.get<string>(u8"idxoffsetbits", "");
	author = tree.get<string>(u8"author", "");
	email = tree.get<string>(u8"email", "");
	website = tree.get<string>(u8"website", "");
	description = tree.get<string>(u8"description", "");
	date = tree.get<string>(u8"date", "");
	sametypesequence = tree.get<string>(u8"sametypesequence", "");
	dicttype = tree.get<string>(u8"dicttype", "");
}

LEXICON_BUDDY_EXPORT unsigned get_idx_iter_entry_type(string VERSION_str, string idxoffsetbits_str) {
	if (VERSION_str >= "3.0.0" && idxoffsetbits_str == "64") {
		// 64bit offset
		return bit64;
	} else {
		// 32bit offset
		return bit32;
	}
}

template <>
LEXICON_BUDDY_EXPORT void idx_iter<bit64>::read_value() {
	string_view s{dat_p};
	auto p1 = dat_p + s.size() + 1;
	value = {s, util::endianSwap_64bit(p1), util::endianSwap_32bit(p1 + word_data_offset_bytes)};
}
template <>
LEXICON_BUDDY_EXPORT void idx_iter<bit32>::read_value() {
	string_view s{dat_p};
	auto p1 = dat_p + s.size() + 1;
	value = {s, util::endianSwap_32bit(p1), util::endianSwap_32bit(p1 + word_data_offset_bytes)};
}

#ifndef null_terminated_index
#define null_terminated_index(type__)                                                                        \
	{                                                                                                        \
		auto b = p;                                                                                          \
		for (;;) {                                                                                           \
			++p;                                                                                             \
			if (p == p_end) {                                                                                \
				index_.emplace_back(type__, b, p - b);                                                       \
				return;                                                                                      \
			}                                                                                                \
			if (!*p) {                                                                                       \
				index_.emplace_back(type__, b, p - b);                                                       \
				break;                                                                                       \
			}                                                                                                \
		}                                                                                                    \
	}
#endif

LEXICON_BUDDY_EXPORT void dict_record::build_index() {
	const char* p = str.data();
	auto p_end = p + str.size();
	if (sametypesequence.empty()) {
		for (;;) {
			switch (*p++) {
				case 'm':
					null_terminated_index('m');
					break;
				case 'l':
					null_terminated_index('l');
					break;
				case 'g':
					null_terminated_index('g');
					break;
				case 't':
					null_terminated_index('t');
					break;
				case 'x':
					null_terminated_index('l');
					break;
				case 'y':
					null_terminated_index('y');
					break;
				case 'k':
					null_terminated_index('k');
					break;
				case 'w':
					null_terminated_index('w');
					break;
				case 'h':
					null_terminated_index('h');
					break;
				case 'n':
					null_terminated_index('n');
					break;
				case 'r':
					null_terminated_index('r');
					break;
				case 'W': {
					auto w_s = util::endianSwap_32bit(p);
					p = p + 4;
					auto b = p;
					p = p + w_s;
					index_.emplace_back('W', b, p - b);
					if (p >= p_end) return;
				} break;
				case 'P': {
					auto w_s = util::endianSwap_32bit(p);
					p = p + 4;
					auto b = p;
					p = p + w_s;
					index_.emplace_back('P', b, p - b);
					if (p >= p_end) return;
				} break;
				case 'X':
					// unexpected
				default:
					// unexpected
					return;
			}
		}
	} else {
		for (auto i : sametypesequence) {
			switch (i) {
				case 'm':
					null_terminated_index('m');
					break;
				case 'l':
					null_terminated_index('l');
					break;
				case 'g':
					null_terminated_index('g');
					break;
				case 't':
					null_terminated_index('t');
					break;
				case 'x':
					null_terminated_index('l');
					break;
				case 'y':
					null_terminated_index('y');
					break;
				case 'k':
					null_terminated_index('k');
					break;
				case 'w':
					null_terminated_index('w');
					break;
				case 'h':
					null_terminated_index('h');
					break;
				case 'n':
					null_terminated_index('n');
					break;
				case 'r':
					null_terminated_index('r');
					break;
				case 'W': {
					auto w_s = util::endianSwap_32bit(p);
					p = p + 4;
					auto b = p;
					p = p + w_s;
					index_.emplace_back('W', b, p - b);
					if (p >= p_end) return;
				} break;
				case 'P': {
					auto w_s = util::endianSwap_32bit(p);
					p = p + 4;
					auto b = p;
					p = p + w_s;
					index_.emplace_back('P', b, p - b);
					if (p >= p_end) return;
				} break;
				case 'X':
					// unexpected
				default:
					// unexpected
					return;
			}
		}
	}
}
} // namespace stardict
} // namespace lexicon
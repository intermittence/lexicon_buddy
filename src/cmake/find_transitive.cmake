#========================================
# code to find transitive dependencies
#========================================
# find lzo marisa and cryptopp
find_library_target("lzo" "lzo2" "lzo1x.h")
find_library_target("marisa" "marisa" "trie.h")
find_library_target("cryptopp" "cryptopp" "ripemd.h")
# Hints of boost search locations
# Either specify BOOST_ROOT or BOOST_INCLUDEDIR and BOOST_LIBRARYDIR.
if(WIN32)
	# using std::filesystem instead of boost::filesystem
	find_package(Boost 1.68 REQUIRED iostreams locale system regex coroutine)
else()
	find_package(Boost 1.68 REQUIRED iostreams locale system regex filesystem coroutine)
endif()
# find openssl 
# todo test
find_package(OpenSSL 1.0.2 REQUIRED SSL)
# use iconv for unix-like system only
if(NOT MSVC)
	find_package(Iconv REQUIRED)
endif()
# find icu
find_package(ICU 60.0 COMPONENTS uc i18n REQUIRED)
find_package(ZLIB 1.0 REQUIRED)
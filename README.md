## Project Name

lexicon_buddy

## Description

lexicon_buddy is a library written in C++17 which supports **Index**, **Cache**, **Query** for mdict (only `.mdx`) and stardict dictionaries (only `.dict.dz`). The Merriam-Webster [Merriam-Webster's Collegiate® Dictionary with Audio](https://www.dictionaryapi.com/products/api-collegiate-dictionary) and [Merriam-Webster's Collegiate® Thesaurus](https://www.dictionaryapi.com/products/api-collegiate-thesaurus) JSON APIs are also supported (for non-commercial use only and API keys are required).

### How to Use

**dict metadata, dict format and a few details**. Read [Dictionary File Format](#dictionary-file-format) before reading following text. The mdict `.mdx` file has two major formats. The stardict `.dict.dz` varies between older version and version 2.0+. different format required different container class and index method.
The `.mdx` may encrypt its structural data. `.mdx` container will automatically decrypt if no extra key is required for decryption.

```c++
// read mdx dict metadata
#include <lexicon_buddy/lexicon/mdxContainer.hpp>
std::ifstream mdx_strm{"test0.mdx"};
lexicon::mdx::h_sec_i h;
h.read_property(mdx_strm);
// read stardict dict metadata
#include <lexicon_buddy/lexicon/stardictcontainer.h>
lexicon::stardict::ifo_str ifo{"test1.ifo"};
```

**dict record, iteration, indexing and query**. **mdict `.mdx` record** is just a null-terminated string. **stardict `.dict.dz` record** contains the record data with an extra index which separates record data into serveral parts with different types (read [huzheng001 stardict official](https://github.com/huzheng001/stardict-3/blob/master/dict/doc/StarDictFileFormat) for the detail). This project use [marisa-trie](https://github.com/s-yata/marisa-trie) to index dictionary keys. As a result, prefix search and (key) look up are supported. Query of trie returns ID(s) which can be use to get one or more (one word->multi-records) coordinates (offset or offset+size of dictionary) corresponding to the query key. With coordinates and dict metadata, library can get the actual record (it's decompressed and the text converted to utf-8).

```c++
// * stardict record *
// example of handle stardict record
std::ostream& operator<<(std::ostream& out, const dict_record& v) {
	for (auto& i : v.index_) {
		switch (i.type_) {
			case 'W':
			case 'P':
			case 'X':
			case 'r':
				break;
			default:
				out.write(i.begin_, i.size_);
				out << '\n';
				break;
		}
	}
	return out;
}

// * iteration *
namespace mdx = lexicon::mdx;
namespace stardict = lexicon::stardict;
// fs::path dict: mdict .mdx or stardict .dict.dz file
// fs::path ifo: stardict .ifo
// fs::path idx: stardict .idx
// create mdx agent
mdx::mdx_guest<mdx::memory> dict_container{dict.string()};
mdx::WCR_agent<mdx::memory> agent(&dict_container);
// or create stardict agent(64bit stardict)
stardict::dict_container<stardict::bit64, false> dict_container{ifo.c_str(),idx.c_str(),dict.c_str()};
stardict::WCR_agent<stardict::bit64, false> agent{&dict_container};
// or create stardict agent(32bit stardict)
stardict::dict_container<stardict::bit32, false> dict_container{ifo.c_str(),idx.c_str(),dict.c_str()};
stardict::WCR_agent<stardict::bit32, false> agent{&dict_container};
// iterate throught whole dictionary
for (; !agent.reach_end(); ++agent) {
	auto& [word_and_coor, record] = *agent;
	// do something with word_and_coor
	// do something with record
}

// * build index *
#include <lexicon_buddy/lexicon/index.h>
const char* mdx_dict = "test0.mdx";
const char* mdx_target_trie_without_suffix = "test0";
const char* mdx_target_coordinate_without_suffix = "test0";
const char* stardict_dict = "test1.dict.dz";
const char* stardict_target_trie_without_suffix = "test1";
const char* stardict_target_coordinate_without_suffix = "test1";
// result0 and result1 contain success flag, the actual trie and coor path(with suffix)
// trie and coor's suffix can not be set in advance before the build process because their format are effect by dict metadata which is unknown before building index.
auto result0 = lexicon::index::index_agent::build_generic(mdx_dict, mdx_target_trie_without_suffix, mdx_target_coordinate_without_suffix);
auto result1 = lexicon::index::index_agent::build_generic(stardict_dict, stardict_target_trie_without_suffix, stardict_target_coordinate_without_suffix);

// * query *
// (dict file(s) and trie + coor file are required)
// example see
// void test_query_mdx(const char* trie, const char* coordinate, const char* word, const char* dict_path)
// and
// void test_query_stardict(const char* trie, const char* coordinate, const char* word, const char* ifo_path,
// const char* idx_path, const char* dict_path)
// from
// lexicon_buddy/test/test.cpp
```

**handler**. Query operation (both offline and online) required handler to consume the result. User should define offline handler and online handler which meet following requirement.

- CopyConstructible

- offline handler

	- has a member method: `void operator()(const lexicon::mdx::dict_record_v& record)`;

	- has a member method: `void operator()(const lexicon::stardict::dict_record_v& record)`;

- online handler

	- has a member method: `void operator()(const boost::beast::http::response<http::string_body>& res)`;

	- has a member method: `void operator()(boost::system::error_code ec)`;

**Merriam-Webster APIs and async http(s) client**. Http client is written with Boost.Beast. 

```c++
// * use http client *
// example see
// void test_control_flex(bool use_proxy, const char* https_url, const char* https_file, const char* http_url, const char* http_file)
// from
// lexicon_buddy/test/test.cpp

// * build Merriam-Webster http request *
using lexicon::online;
provider_traits<dict_api::Merriam_Webster> key;
key.Collegiate_key="xxxx";
key.Thesaurus_key="xxxx";
provider<dict_api::Merriam_Webster, provider_traits<dict_api::Merriam_Webster>> p{key};
std::string word{"apple"};
auto http_request = p._build_request<merriam_webster_dict::Collegiate_Dictionary>(word);
// or merriam_webster_dict::Collegiate_Thesaurus for Thesaurus request
```

**dict manager**. dict manager assembles all functionalities (except record iteration) provided by this library.

```c++
// example see
// void test_buddy(int argc, char* argv[])
// from
// lexicon_buddy/test/test.cpp
```

### TODO

http client and dict manager handler should be moved than copied if possible

async file io on windows (since real random access async file io is only supported by windows)

std::map is currently used as search result container since the "map" operation is handy. But it is not efficient with less than 50 elements (poor memory locality) and it will be replaced with something like `boost::flat_map` (which basically is a vector wrapper which supports map operation). [Benchmark](https://stackoverflow.com/questions/21166675/boostflat-map-and-its-performance-compared-to-map-and-unordered-map).

### Index Format

two kinds of files are required for dict indexing: co (coordinate) and trie.

co contains word offsets in dictionaries

trie encodes IDs for extracting word offsets from co. 

> stardict [link](https://github.com/huzheng001/stardict-3/blob/master/dict/doc/StarDictFileFormat):
> Two or more entries may have the same "word_str" with different 
> word_data_offset and word_data_size. This may be useful for some 
> dictionaries. But this feature is only well supported by 
> StarDict-2.4.8 and newer.

mdx dictionary has a list of **{word_string => index(offset)}** mappings. It may contain mappings from same `word` to different `index`. It's also true for stardict dictionary. The data structure to hold index is required to handle this for mdx and stardict version>=2.4.8 (to make things simple, index for any version of stardict meets the requirement even when it's not required at all). It's also true that some dictionary may contain mappings from different `word` to one `index` (which means two words/variations have the same definition). That's (probably) not a problem for indexing, prefix search and query needed by a dictionary reader, but developers/dictionary creators may need this extra info when they are iterating through whole dictionary. Fortunately, when iterating dictionary with lexicon_buddy, you get not only the decoded word definition, but also the original index (which can be treated as ID) of word definition.

**mdx**

`.mco64` format:
```
offset[0] (8 bytes) num[0] (2 bytes)
......
offset[i]  num[i]
offset[i+1]  num[i+1]
......
mdx_offset[0][0] (8 bytes)
......
mdx_offset[i][0]
mdx_offset[i][1]
......
mdx_offset[i][j-1]
mdx_offset[i+1][0]
......
```
The `i` in `offset[i]` is the id of key which generated by marisa trie lib and **it maybe not strictly alphabetically ordered**.  The `offset[i]` itself is the offset of `mdx_offset[i][0]`. In `mdx_offset[i][j-1]`, `j` = `num[i]`. In other word, `num[i]` means the number of **value** for the same **key**. `mdx_offset` itself is originally stored in `.mdx` file used to looking for corresponding record (null-terminated).

`.mtrie64`: saved and used by marisa trie.

`.mco32` and `.mtrie32`: same as "64" version of file format except in `.mco32`  `offset` and `mdx_offset` occupy 4 bytes.

**stardict**

> [link](https://github.com/huzheng001/stardict-3/blob/master/dict/doc/StarDictFileFormat) : 
> "If the version is "3.0.0" and "idxoffsetbits=64", word_data_offset will be 64-bits unsigned number in network byte order. Otherwise it will be 32-bits"

`.sco64` format:
```
offset[0] (8 bytes) num[0] (2 bytes)
.....
offset[i]  num[i] 
offset[i+1]  num[i+1] 
......
word_data_offset[0][0] (8 bytes) word_data_size[0][0] (4 bytes)
......
word_data_offset[i][0]  word_data_size[i][0] 
word_data_offset[i][1]  word_data_size[i][1] 
......
word_data_offset[i][j-1]  word_data_size[i][j-1] 
word_data_offset[i+1][0]  word_data_size[i+1][0] 
......
```
The `i` in `offset[i]` is the id of key which generated by marisa trie lib and **it maybe not strictly alphabetically ordered**. The `offset[i]` itself is the offset of `word_data_offset[i][0]`. In `word_data_offset[i][j-1]`, `j` = `num[i]`. In other word, `num[i]` means the number of **value** for one **key**. `word_data_offset` and `word_data_size` are originally stored in stardict `.idx` file used to looking for corresponding record.

`.strie64`: saved and used by marisa trie.

`.sco32` and `.strie32`: same as "64" version of file format except in `.sco32`  `offset` and `word_data_offset` occupy 4 bytes.

### Dependency

- **OpenSSL** ([OpenSSL License](https://www.openssl.org/source/license.html))
- **Crypto++®** ([Boost Software License 1.0](https://www.cryptopp.com/License.txt))
- **Boost 1.69** ([Boost Software License](https://www.boost.org/users/license.html)) : Asio, Coroutine, Beast, PropertyTree, System, IOStreams, Filesystem, Locale (removed), Regex
- **ICU**  ([open source license](http://www.unicode.org/copyright.html#License)) : for code page conversion
- **zlib** ([zlib License](http://www.zlib.net/zlib_license.html))
- **LZO** ([GPL v2+](http://www.oberhumer.com/opensource/gpl.html))
- [**marisa-trie**](https://github.com/s-yata/marisa-trie) ([License](https://github.com/s-yata/marisa-trie#source-code-license)): indexing

### Build and Install Instruction

Read the [instruction](Build/Build_and_Install.md).

### Reference

#### Dictionary File Format

- mdict (based on following reverse engineering): [xwang's mdict-analysis](https://bitbucket.org/xwang/mdict-analysis), [zhansliu's work](https://github.com/zhansliu/writemdict/blob/master/fileformat.md)
- [my notes of mdict](notes_of_mdict_format.md)
- startdict: [dhyannataraj's work](http://dhyannataraj.github.io/blog/2010/10/04/Notes-about-stardict-dictionry-format/), [huzheng001 stardict official](https://github.com/huzheng001/stardict-3/blob/master/dict/doc/StarDictFileFormat)

### Author and Contact

cqyzzhgl@gmail.com